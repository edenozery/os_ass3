
kernel:     file format elf32-i386


Disassembly of section .text:

80100000 <multiboot_header>:
80100000:	02 b0 ad 1b 00 00    	add    0x1bad(%eax),%dh
80100006:	00 00                	add    %al,(%eax)
80100008:	fe 4f 52             	decb   0x52(%edi)
8010000b:	e4                   	.byte 0xe4

8010000c <entry>:

# Entering xv6 on boot processor, with paging off.
.globl entry
entry:
  # Turn on page size extension for 4Mbyte pages
  movl    %cr4, %eax
8010000c:	0f 20 e0             	mov    %cr4,%eax
  orl     $(CR4_PSE), %eax
8010000f:	83 c8 10             	or     $0x10,%eax
  movl    %eax, %cr4
80100012:	0f 22 e0             	mov    %eax,%cr4
  # Set page directory
  movl    $(V2P_WO(entrypgdir)), %eax
80100015:	b8 00 a0 10 00       	mov    $0x10a000,%eax
  movl    %eax, %cr3
8010001a:	0f 22 d8             	mov    %eax,%cr3
  # Turn on paging.
  movl    %cr0, %eax
8010001d:	0f 20 c0             	mov    %cr0,%eax
  orl     $(CR0_PG|CR0_WP), %eax
80100020:	0d 00 00 01 80       	or     $0x80010000,%eax
  movl    %eax, %cr0
80100025:	0f 22 c0             	mov    %eax,%cr0

  # Set up the stack pointer.
  movl $(stack + KSTACKSIZE), %esp
80100028:	bc c0 c5 10 80       	mov    $0x8010c5c0,%esp

  # Jump to main(), and switch to executing at
  # high addresses. The indirect call is needed because
  # the assembler produces a PC-relative instruction
  # for a direct jump.
  mov $main, %eax
8010002d:	b8 a0 32 10 80       	mov    $0x801032a0,%eax
  jmp *%eax
80100032:	ff e0                	jmp    *%eax
80100034:	66 90                	xchg   %ax,%ax
80100036:	66 90                	xchg   %ax,%ax
80100038:	66 90                	xchg   %ax,%ax
8010003a:	66 90                	xchg   %ax,%ax
8010003c:	66 90                	xchg   %ax,%ax
8010003e:	66 90                	xchg   %ax,%ax

80100040 <binit>:
  struct buf head;
} bcache;

void
binit(void)
{
80100040:	55                   	push   %ebp
80100041:	89 e5                	mov    %esp,%ebp
80100043:	53                   	push   %ebx

//PAGEBREAK!
  // Create linked list of buffers
  bcache.head.prev = &bcache.head;
  bcache.head.next = &bcache.head;
  for(b = bcache.buf; b < bcache.buf+NBUF; b++){
80100044:	bb f4 c5 10 80       	mov    $0x8010c5f4,%ebx
{
80100049:	83 ec 0c             	sub    $0xc,%esp
  initlock(&bcache.lock, "bcache");
8010004c:	68 40 77 10 80       	push   $0x80107740
80100051:	68 c0 c5 10 80       	push   $0x8010c5c0
80100056:	e8 35 46 00 00       	call   80104690 <initlock>
  bcache.head.prev = &bcache.head;
8010005b:	c7 05 0c 0d 11 80 bc 	movl   $0x80110cbc,0x80110d0c
80100062:	0c 11 80 
  bcache.head.next = &bcache.head;
80100065:	c7 05 10 0d 11 80 bc 	movl   $0x80110cbc,0x80110d10
8010006c:	0c 11 80 
8010006f:	83 c4 10             	add    $0x10,%esp
80100072:	ba bc 0c 11 80       	mov    $0x80110cbc,%edx
80100077:	eb 09                	jmp    80100082 <binit+0x42>
80100079:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80100080:	89 c3                	mov    %eax,%ebx
    b->next = bcache.head.next;
    b->prev = &bcache.head;
    initsleeplock(&b->lock, "buffer");
80100082:	8d 43 0c             	lea    0xc(%ebx),%eax
80100085:	83 ec 08             	sub    $0x8,%esp
    b->next = bcache.head.next;
80100088:	89 53 54             	mov    %edx,0x54(%ebx)
    b->prev = &bcache.head;
8010008b:	c7 43 50 bc 0c 11 80 	movl   $0x80110cbc,0x50(%ebx)
    initsleeplock(&b->lock, "buffer");
80100092:	68 47 77 10 80       	push   $0x80107747
80100097:	50                   	push   %eax
80100098:	e8 c3 44 00 00       	call   80104560 <initsleeplock>
    bcache.head.next->prev = b;
8010009d:	a1 10 0d 11 80       	mov    0x80110d10,%eax
  for(b = bcache.buf; b < bcache.buf+NBUF; b++){
801000a2:	83 c4 10             	add    $0x10,%esp
801000a5:	89 da                	mov    %ebx,%edx
    bcache.head.next->prev = b;
801000a7:	89 58 50             	mov    %ebx,0x50(%eax)
  for(b = bcache.buf; b < bcache.buf+NBUF; b++){
801000aa:	8d 83 5c 02 00 00    	lea    0x25c(%ebx),%eax
    bcache.head.next = b;
801000b0:	89 1d 10 0d 11 80    	mov    %ebx,0x80110d10
  for(b = bcache.buf; b < bcache.buf+NBUF; b++){
801000b6:	3d bc 0c 11 80       	cmp    $0x80110cbc,%eax
801000bb:	72 c3                	jb     80100080 <binit+0x40>
  }
}
801000bd:	8b 5d fc             	mov    -0x4(%ebp),%ebx
801000c0:	c9                   	leave  
801000c1:	c3                   	ret    
801000c2:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801000c9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

801000d0 <bread>:
}

// Return a locked buf with the contents of the indicated block.
struct buf*
bread(uint dev, uint blockno)
{
801000d0:	55                   	push   %ebp
801000d1:	89 e5                	mov    %esp,%ebp
801000d3:	57                   	push   %edi
801000d4:	56                   	push   %esi
801000d5:	53                   	push   %ebx
801000d6:	83 ec 18             	sub    $0x18,%esp
801000d9:	8b 75 08             	mov    0x8(%ebp),%esi
801000dc:	8b 7d 0c             	mov    0xc(%ebp),%edi
  acquire(&bcache.lock);
801000df:	68 c0 c5 10 80       	push   $0x8010c5c0
801000e4:	e8 e7 46 00 00       	call   801047d0 <acquire>
  for(b = bcache.head.next; b != &bcache.head; b = b->next){
801000e9:	8b 1d 10 0d 11 80    	mov    0x80110d10,%ebx
801000ef:	83 c4 10             	add    $0x10,%esp
801000f2:	81 fb bc 0c 11 80    	cmp    $0x80110cbc,%ebx
801000f8:	75 11                	jne    8010010b <bread+0x3b>
801000fa:	eb 24                	jmp    80100120 <bread+0x50>
801000fc:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80100100:	8b 5b 54             	mov    0x54(%ebx),%ebx
80100103:	81 fb bc 0c 11 80    	cmp    $0x80110cbc,%ebx
80100109:	74 15                	je     80100120 <bread+0x50>
    if(b->dev == dev && b->blockno == blockno){
8010010b:	3b 73 04             	cmp    0x4(%ebx),%esi
8010010e:	75 f0                	jne    80100100 <bread+0x30>
80100110:	3b 7b 08             	cmp    0x8(%ebx),%edi
80100113:	75 eb                	jne    80100100 <bread+0x30>
      b->refcnt++;
80100115:	83 43 4c 01          	addl   $0x1,0x4c(%ebx)
80100119:	eb 3f                	jmp    8010015a <bread+0x8a>
8010011b:	90                   	nop
8010011c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  for(b = bcache.head.prev; b != &bcache.head; b = b->prev){
80100120:	8b 1d 0c 0d 11 80    	mov    0x80110d0c,%ebx
80100126:	81 fb bc 0c 11 80    	cmp    $0x80110cbc,%ebx
8010012c:	75 0d                	jne    8010013b <bread+0x6b>
8010012e:	eb 60                	jmp    80100190 <bread+0xc0>
80100130:	8b 5b 50             	mov    0x50(%ebx),%ebx
80100133:	81 fb bc 0c 11 80    	cmp    $0x80110cbc,%ebx
80100139:	74 55                	je     80100190 <bread+0xc0>
    if(b->refcnt == 0 && (b->flags & B_DIRTY) == 0) {
8010013b:	8b 43 4c             	mov    0x4c(%ebx),%eax
8010013e:	85 c0                	test   %eax,%eax
80100140:	75 ee                	jne    80100130 <bread+0x60>
80100142:	f6 03 04             	testb  $0x4,(%ebx)
80100145:	75 e9                	jne    80100130 <bread+0x60>
      b->dev = dev;
80100147:	89 73 04             	mov    %esi,0x4(%ebx)
      b->blockno = blockno;
8010014a:	89 7b 08             	mov    %edi,0x8(%ebx)
      b->flags = 0;
8010014d:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
      b->refcnt = 1;
80100153:	c7 43 4c 01 00 00 00 	movl   $0x1,0x4c(%ebx)
      release(&bcache.lock);
8010015a:	83 ec 0c             	sub    $0xc,%esp
8010015d:	68 c0 c5 10 80       	push   $0x8010c5c0
80100162:	e8 29 47 00 00       	call   80104890 <release>
      acquiresleep(&b->lock);
80100167:	8d 43 0c             	lea    0xc(%ebx),%eax
8010016a:	89 04 24             	mov    %eax,(%esp)
8010016d:	e8 2e 44 00 00       	call   801045a0 <acquiresleep>
80100172:	83 c4 10             	add    $0x10,%esp
  struct buf *b;

  b = bget(dev, blockno);
  if((b->flags & B_VALID) == 0) {
80100175:	f6 03 02             	testb  $0x2,(%ebx)
80100178:	75 0c                	jne    80100186 <bread+0xb6>
    iderw(b);
8010017a:	83 ec 0c             	sub    $0xc,%esp
8010017d:	53                   	push   %ebx
8010017e:	e8 2d 23 00 00       	call   801024b0 <iderw>
80100183:	83 c4 10             	add    $0x10,%esp
  }
  return b;
}
80100186:	8d 65 f4             	lea    -0xc(%ebp),%esp
80100189:	89 d8                	mov    %ebx,%eax
8010018b:	5b                   	pop    %ebx
8010018c:	5e                   	pop    %esi
8010018d:	5f                   	pop    %edi
8010018e:	5d                   	pop    %ebp
8010018f:	c3                   	ret    
  panic("bget: no buffers");
80100190:	83 ec 0c             	sub    $0xc,%esp
80100193:	68 4e 77 10 80       	push   $0x8010774e
80100198:	e8 f3 01 00 00       	call   80100390 <panic>
8010019d:	8d 76 00             	lea    0x0(%esi),%esi

801001a0 <bwrite>:

// Write b's contents to disk.  Must be locked.
void
bwrite(struct buf *b)
{
801001a0:	55                   	push   %ebp
801001a1:	89 e5                	mov    %esp,%ebp
801001a3:	53                   	push   %ebx
801001a4:	83 ec 10             	sub    $0x10,%esp
801001a7:	8b 5d 08             	mov    0x8(%ebp),%ebx
  if(!holdingsleep(&b->lock))
801001aa:	8d 43 0c             	lea    0xc(%ebx),%eax
801001ad:	50                   	push   %eax
801001ae:	e8 8d 44 00 00       	call   80104640 <holdingsleep>
801001b3:	83 c4 10             	add    $0x10,%esp
801001b6:	85 c0                	test   %eax,%eax
801001b8:	74 0f                	je     801001c9 <bwrite+0x29>
    panic("bwrite");
  b->flags |= B_DIRTY;
801001ba:	83 0b 04             	orl    $0x4,(%ebx)
  iderw(b);
801001bd:	89 5d 08             	mov    %ebx,0x8(%ebp)
}
801001c0:	8b 5d fc             	mov    -0x4(%ebp),%ebx
801001c3:	c9                   	leave  
  iderw(b);
801001c4:	e9 e7 22 00 00       	jmp    801024b0 <iderw>
    panic("bwrite");
801001c9:	83 ec 0c             	sub    $0xc,%esp
801001cc:	68 5f 77 10 80       	push   $0x8010775f
801001d1:	e8 ba 01 00 00       	call   80100390 <panic>
801001d6:	8d 76 00             	lea    0x0(%esi),%esi
801001d9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

801001e0 <brelse>:

// Release a locked buffer.
// Move to the head of the MRU list.
void
brelse(struct buf *b)
{
801001e0:	55                   	push   %ebp
801001e1:	89 e5                	mov    %esp,%ebp
801001e3:	56                   	push   %esi
801001e4:	53                   	push   %ebx
801001e5:	8b 5d 08             	mov    0x8(%ebp),%ebx
  if(!holdingsleep(&b->lock))
801001e8:	83 ec 0c             	sub    $0xc,%esp
801001eb:	8d 73 0c             	lea    0xc(%ebx),%esi
801001ee:	56                   	push   %esi
801001ef:	e8 4c 44 00 00       	call   80104640 <holdingsleep>
801001f4:	83 c4 10             	add    $0x10,%esp
801001f7:	85 c0                	test   %eax,%eax
801001f9:	74 66                	je     80100261 <brelse+0x81>
    panic("brelse");

  releasesleep(&b->lock);
801001fb:	83 ec 0c             	sub    $0xc,%esp
801001fe:	56                   	push   %esi
801001ff:	e8 fc 43 00 00       	call   80104600 <releasesleep>

  acquire(&bcache.lock);
80100204:	c7 04 24 c0 c5 10 80 	movl   $0x8010c5c0,(%esp)
8010020b:	e8 c0 45 00 00       	call   801047d0 <acquire>
  b->refcnt--;
80100210:	8b 43 4c             	mov    0x4c(%ebx),%eax
  if (b->refcnt == 0) {
80100213:	83 c4 10             	add    $0x10,%esp
  b->refcnt--;
80100216:	83 e8 01             	sub    $0x1,%eax
  if (b->refcnt == 0) {
80100219:	85 c0                	test   %eax,%eax
  b->refcnt--;
8010021b:	89 43 4c             	mov    %eax,0x4c(%ebx)
  if (b->refcnt == 0) {
8010021e:	75 2f                	jne    8010024f <brelse+0x6f>
    // no one is waiting for it.
    b->next->prev = b->prev;
80100220:	8b 43 54             	mov    0x54(%ebx),%eax
80100223:	8b 53 50             	mov    0x50(%ebx),%edx
80100226:	89 50 50             	mov    %edx,0x50(%eax)
    b->prev->next = b->next;
80100229:	8b 43 50             	mov    0x50(%ebx),%eax
8010022c:	8b 53 54             	mov    0x54(%ebx),%edx
8010022f:	89 50 54             	mov    %edx,0x54(%eax)
    b->next = bcache.head.next;
80100232:	a1 10 0d 11 80       	mov    0x80110d10,%eax
    b->prev = &bcache.head;
80100237:	c7 43 50 bc 0c 11 80 	movl   $0x80110cbc,0x50(%ebx)
    b->next = bcache.head.next;
8010023e:	89 43 54             	mov    %eax,0x54(%ebx)
    bcache.head.next->prev = b;
80100241:	a1 10 0d 11 80       	mov    0x80110d10,%eax
80100246:	89 58 50             	mov    %ebx,0x50(%eax)
    bcache.head.next = b;
80100249:	89 1d 10 0d 11 80    	mov    %ebx,0x80110d10
  }
  
  release(&bcache.lock);
8010024f:	c7 45 08 c0 c5 10 80 	movl   $0x8010c5c0,0x8(%ebp)
}
80100256:	8d 65 f8             	lea    -0x8(%ebp),%esp
80100259:	5b                   	pop    %ebx
8010025a:	5e                   	pop    %esi
8010025b:	5d                   	pop    %ebp
  release(&bcache.lock);
8010025c:	e9 2f 46 00 00       	jmp    80104890 <release>
    panic("brelse");
80100261:	83 ec 0c             	sub    $0xc,%esp
80100264:	68 66 77 10 80       	push   $0x80107766
80100269:	e8 22 01 00 00       	call   80100390 <panic>
8010026e:	66 90                	xchg   %ax,%ax

80100270 <consoleread>:
  }
}

int
consoleread(struct inode *ip, char *dst, int n)
{
80100270:	55                   	push   %ebp
80100271:	89 e5                	mov    %esp,%ebp
80100273:	57                   	push   %edi
80100274:	56                   	push   %esi
80100275:	53                   	push   %ebx
80100276:	83 ec 28             	sub    $0x28,%esp
80100279:	8b 7d 08             	mov    0x8(%ebp),%edi
8010027c:	8b 75 0c             	mov    0xc(%ebp),%esi
  uint target;
  int c;

  iunlock(ip);
8010027f:	57                   	push   %edi
80100280:	e8 db 14 00 00       	call   80101760 <iunlock>
  target = n;
  acquire(&cons.lock);
80100285:	c7 04 24 20 b5 10 80 	movl   $0x8010b520,(%esp)
8010028c:	e8 3f 45 00 00       	call   801047d0 <acquire>
  while(n > 0){
80100291:	8b 5d 10             	mov    0x10(%ebp),%ebx
80100294:	83 c4 10             	add    $0x10,%esp
80100297:	31 c0                	xor    %eax,%eax
80100299:	85 db                	test   %ebx,%ebx
8010029b:	0f 8e a1 00 00 00    	jle    80100342 <consoleread+0xd2>
    while(input.r == input.w){
801002a1:	8b 15 a0 0f 11 80    	mov    0x80110fa0,%edx
801002a7:	39 15 a4 0f 11 80    	cmp    %edx,0x80110fa4
801002ad:	74 2c                	je     801002db <consoleread+0x6b>
801002af:	eb 5f                	jmp    80100310 <consoleread+0xa0>
801002b1:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
      if(myproc()->killed){
        release(&cons.lock);
        ilock(ip);
        return -1;
      }
      sleep(&input.r, &cons.lock);
801002b8:	83 ec 08             	sub    $0x8,%esp
801002bb:	68 20 b5 10 80       	push   $0x8010b520
801002c0:	68 a0 0f 11 80       	push   $0x80110fa0
801002c5:	e8 26 3f 00 00       	call   801041f0 <sleep>
    while(input.r == input.w){
801002ca:	8b 15 a0 0f 11 80    	mov    0x80110fa0,%edx
801002d0:	83 c4 10             	add    $0x10,%esp
801002d3:	3b 15 a4 0f 11 80    	cmp    0x80110fa4,%edx
801002d9:	75 35                	jne    80100310 <consoleread+0xa0>
      if(myproc()->killed){
801002db:	e8 50 39 00 00       	call   80103c30 <myproc>
801002e0:	8b 40 24             	mov    0x24(%eax),%eax
801002e3:	85 c0                	test   %eax,%eax
801002e5:	74 d1                	je     801002b8 <consoleread+0x48>
        release(&cons.lock);
801002e7:	83 ec 0c             	sub    $0xc,%esp
801002ea:	68 20 b5 10 80       	push   $0x8010b520
801002ef:	e8 9c 45 00 00       	call   80104890 <release>
        ilock(ip);
801002f4:	89 3c 24             	mov    %edi,(%esp)
801002f7:	e8 84 13 00 00       	call   80101680 <ilock>
        return -1;
801002fc:	83 c4 10             	add    $0x10,%esp
  }
  release(&cons.lock);
  ilock(ip);

  return target - n;
}
801002ff:	8d 65 f4             	lea    -0xc(%ebp),%esp
        return -1;
80100302:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80100307:	5b                   	pop    %ebx
80100308:	5e                   	pop    %esi
80100309:	5f                   	pop    %edi
8010030a:	5d                   	pop    %ebp
8010030b:	c3                   	ret    
8010030c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    c = input.buf[input.r++ % INPUT_BUF];
80100310:	8d 42 01             	lea    0x1(%edx),%eax
80100313:	a3 a0 0f 11 80       	mov    %eax,0x80110fa0
80100318:	89 d0                	mov    %edx,%eax
8010031a:	83 e0 7f             	and    $0x7f,%eax
8010031d:	0f be 80 20 0f 11 80 	movsbl -0x7feef0e0(%eax),%eax
    if(c == C('D')){  // EOF
80100324:	83 f8 04             	cmp    $0x4,%eax
80100327:	74 3f                	je     80100368 <consoleread+0xf8>
    *dst++ = c;
80100329:	83 c6 01             	add    $0x1,%esi
    --n;
8010032c:	83 eb 01             	sub    $0x1,%ebx
    if(c == '\n')
8010032f:	83 f8 0a             	cmp    $0xa,%eax
    *dst++ = c;
80100332:	88 46 ff             	mov    %al,-0x1(%esi)
    if(c == '\n')
80100335:	74 43                	je     8010037a <consoleread+0x10a>
  while(n > 0){
80100337:	85 db                	test   %ebx,%ebx
80100339:	0f 85 62 ff ff ff    	jne    801002a1 <consoleread+0x31>
8010033f:	8b 45 10             	mov    0x10(%ebp),%eax
  release(&cons.lock);
80100342:	83 ec 0c             	sub    $0xc,%esp
80100345:	89 45 e4             	mov    %eax,-0x1c(%ebp)
80100348:	68 20 b5 10 80       	push   $0x8010b520
8010034d:	e8 3e 45 00 00       	call   80104890 <release>
  ilock(ip);
80100352:	89 3c 24             	mov    %edi,(%esp)
80100355:	e8 26 13 00 00       	call   80101680 <ilock>
  return target - n;
8010035a:	8b 45 e4             	mov    -0x1c(%ebp),%eax
8010035d:	83 c4 10             	add    $0x10,%esp
}
80100360:	8d 65 f4             	lea    -0xc(%ebp),%esp
80100363:	5b                   	pop    %ebx
80100364:	5e                   	pop    %esi
80100365:	5f                   	pop    %edi
80100366:	5d                   	pop    %ebp
80100367:	c3                   	ret    
80100368:	8b 45 10             	mov    0x10(%ebp),%eax
8010036b:	29 d8                	sub    %ebx,%eax
      if(n < target){
8010036d:	3b 5d 10             	cmp    0x10(%ebp),%ebx
80100370:	73 d0                	jae    80100342 <consoleread+0xd2>
        input.r--;
80100372:	89 15 a0 0f 11 80    	mov    %edx,0x80110fa0
80100378:	eb c8                	jmp    80100342 <consoleread+0xd2>
8010037a:	8b 45 10             	mov    0x10(%ebp),%eax
8010037d:	29 d8                	sub    %ebx,%eax
8010037f:	eb c1                	jmp    80100342 <consoleread+0xd2>
80100381:	eb 0d                	jmp    80100390 <panic>
80100383:	90                   	nop
80100384:	90                   	nop
80100385:	90                   	nop
80100386:	90                   	nop
80100387:	90                   	nop
80100388:	90                   	nop
80100389:	90                   	nop
8010038a:	90                   	nop
8010038b:	90                   	nop
8010038c:	90                   	nop
8010038d:	90                   	nop
8010038e:	90                   	nop
8010038f:	90                   	nop

80100390 <panic>:
{
80100390:	55                   	push   %ebp
80100391:	89 e5                	mov    %esp,%ebp
80100393:	56                   	push   %esi
80100394:	53                   	push   %ebx
80100395:	83 ec 30             	sub    $0x30,%esp
}

static inline void
cli(void)
{
  asm volatile("cli");
80100398:	fa                   	cli    
  cons.locking = 0;
80100399:	c7 05 54 b5 10 80 00 	movl   $0x0,0x8010b554
801003a0:	00 00 00 
  getcallerpcs(&s, pcs);
801003a3:	8d 5d d0             	lea    -0x30(%ebp),%ebx
801003a6:	8d 75 f8             	lea    -0x8(%ebp),%esi
  cprintf("lapicid %d: panic: ", lapicid());
801003a9:	e8 82 27 00 00       	call   80102b30 <lapicid>
801003ae:	83 ec 08             	sub    $0x8,%esp
801003b1:	50                   	push   %eax
801003b2:	68 6d 77 10 80       	push   $0x8010776d
801003b7:	e8 a4 02 00 00       	call   80100660 <cprintf>
  cprintf(s);
801003bc:	58                   	pop    %eax
801003bd:	ff 75 08             	pushl  0x8(%ebp)
801003c0:	e8 9b 02 00 00       	call   80100660 <cprintf>
  cprintf("\n");
801003c5:	c7 04 24 d1 81 10 80 	movl   $0x801081d1,(%esp)
801003cc:	e8 8f 02 00 00       	call   80100660 <cprintf>
  getcallerpcs(&s, pcs);
801003d1:	5a                   	pop    %edx
801003d2:	8d 45 08             	lea    0x8(%ebp),%eax
801003d5:	59                   	pop    %ecx
801003d6:	53                   	push   %ebx
801003d7:	50                   	push   %eax
801003d8:	e8 d3 42 00 00       	call   801046b0 <getcallerpcs>
801003dd:	83 c4 10             	add    $0x10,%esp
    cprintf(" %p", pcs[i]);
801003e0:	83 ec 08             	sub    $0x8,%esp
801003e3:	ff 33                	pushl  (%ebx)
801003e5:	83 c3 04             	add    $0x4,%ebx
801003e8:	68 81 77 10 80       	push   $0x80107781
801003ed:	e8 6e 02 00 00       	call   80100660 <cprintf>
  for(i=0; i<10; i++)
801003f2:	83 c4 10             	add    $0x10,%esp
801003f5:	39 f3                	cmp    %esi,%ebx
801003f7:	75 e7                	jne    801003e0 <panic+0x50>
  panicked = 1; // freeze other CPU
801003f9:	c7 05 58 b5 10 80 01 	movl   $0x1,0x8010b558
80100400:	00 00 00 
80100403:	eb fe                	jmp    80100403 <panic+0x73>
80100405:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80100409:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80100410 <consputc>:
  if(panicked){
80100410:	8b 0d 58 b5 10 80    	mov    0x8010b558,%ecx
80100416:	85 c9                	test   %ecx,%ecx
80100418:	74 06                	je     80100420 <consputc+0x10>
8010041a:	fa                   	cli    
8010041b:	eb fe                	jmp    8010041b <consputc+0xb>
8010041d:	8d 76 00             	lea    0x0(%esi),%esi
{
80100420:	55                   	push   %ebp
80100421:	89 e5                	mov    %esp,%ebp
80100423:	57                   	push   %edi
80100424:	56                   	push   %esi
80100425:	53                   	push   %ebx
80100426:	89 c6                	mov    %eax,%esi
80100428:	83 ec 0c             	sub    $0xc,%esp
  if(c == BACKSPACE){
8010042b:	3d 00 01 00 00       	cmp    $0x100,%eax
80100430:	0f 84 b1 00 00 00    	je     801004e7 <consputc+0xd7>
    uartputc(c);
80100436:	83 ec 0c             	sub    $0xc,%esp
80100439:	50                   	push   %eax
8010043a:	e8 61 5b 00 00       	call   80105fa0 <uartputc>
8010043f:	83 c4 10             	add    $0x10,%esp
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80100442:	bb d4 03 00 00       	mov    $0x3d4,%ebx
80100447:	b8 0e 00 00 00       	mov    $0xe,%eax
8010044c:	89 da                	mov    %ebx,%edx
8010044e:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
8010044f:	b9 d5 03 00 00       	mov    $0x3d5,%ecx
80100454:	89 ca                	mov    %ecx,%edx
80100456:	ec                   	in     (%dx),%al
  pos = inb(CRTPORT+1) << 8;
80100457:	0f b6 c0             	movzbl %al,%eax
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
8010045a:	89 da                	mov    %ebx,%edx
8010045c:	c1 e0 08             	shl    $0x8,%eax
8010045f:	89 c7                	mov    %eax,%edi
80100461:	b8 0f 00 00 00       	mov    $0xf,%eax
80100466:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80100467:	89 ca                	mov    %ecx,%edx
80100469:	ec                   	in     (%dx),%al
8010046a:	0f b6 d8             	movzbl %al,%ebx
  pos |= inb(CRTPORT+1);
8010046d:	09 fb                	or     %edi,%ebx
  if(c == '\n')
8010046f:	83 fe 0a             	cmp    $0xa,%esi
80100472:	0f 84 f3 00 00 00    	je     8010056b <consputc+0x15b>
  else if(c == BACKSPACE){
80100478:	81 fe 00 01 00 00    	cmp    $0x100,%esi
8010047e:	0f 84 d7 00 00 00    	je     8010055b <consputc+0x14b>
    crt[pos++] = (c&0xff) | 0x0700;  // black on white
80100484:	89 f0                	mov    %esi,%eax
80100486:	0f b6 c0             	movzbl %al,%eax
80100489:	80 cc 07             	or     $0x7,%ah
8010048c:	66 89 84 1b 00 80 0b 	mov    %ax,-0x7ff48000(%ebx,%ebx,1)
80100493:	80 
80100494:	83 c3 01             	add    $0x1,%ebx
  if(pos < 0 || pos > 25*80)
80100497:	81 fb d0 07 00 00    	cmp    $0x7d0,%ebx
8010049d:	0f 8f ab 00 00 00    	jg     8010054e <consputc+0x13e>
  if((pos/80) >= 24){  // Scroll up.
801004a3:	81 fb 7f 07 00 00    	cmp    $0x77f,%ebx
801004a9:	7f 66                	jg     80100511 <consputc+0x101>
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
801004ab:	be d4 03 00 00       	mov    $0x3d4,%esi
801004b0:	b8 0e 00 00 00       	mov    $0xe,%eax
801004b5:	89 f2                	mov    %esi,%edx
801004b7:	ee                   	out    %al,(%dx)
801004b8:	b9 d5 03 00 00       	mov    $0x3d5,%ecx
  outb(CRTPORT+1, pos>>8);
801004bd:	89 d8                	mov    %ebx,%eax
801004bf:	c1 f8 08             	sar    $0x8,%eax
801004c2:	89 ca                	mov    %ecx,%edx
801004c4:	ee                   	out    %al,(%dx)
801004c5:	b8 0f 00 00 00       	mov    $0xf,%eax
801004ca:	89 f2                	mov    %esi,%edx
801004cc:	ee                   	out    %al,(%dx)
801004cd:	89 d8                	mov    %ebx,%eax
801004cf:	89 ca                	mov    %ecx,%edx
801004d1:	ee                   	out    %al,(%dx)
  crt[pos] = ' ' | 0x0700;
801004d2:	b8 20 07 00 00       	mov    $0x720,%eax
801004d7:	66 89 84 1b 00 80 0b 	mov    %ax,-0x7ff48000(%ebx,%ebx,1)
801004de:	80 
}
801004df:	8d 65 f4             	lea    -0xc(%ebp),%esp
801004e2:	5b                   	pop    %ebx
801004e3:	5e                   	pop    %esi
801004e4:	5f                   	pop    %edi
801004e5:	5d                   	pop    %ebp
801004e6:	c3                   	ret    
    uartputc('\b'); uartputc(' '); uartputc('\b');
801004e7:	83 ec 0c             	sub    $0xc,%esp
801004ea:	6a 08                	push   $0x8
801004ec:	e8 af 5a 00 00       	call   80105fa0 <uartputc>
801004f1:	c7 04 24 20 00 00 00 	movl   $0x20,(%esp)
801004f8:	e8 a3 5a 00 00       	call   80105fa0 <uartputc>
801004fd:	c7 04 24 08 00 00 00 	movl   $0x8,(%esp)
80100504:	e8 97 5a 00 00       	call   80105fa0 <uartputc>
80100509:	83 c4 10             	add    $0x10,%esp
8010050c:	e9 31 ff ff ff       	jmp    80100442 <consputc+0x32>
    memmove(crt, crt+80, sizeof(crt[0])*23*80);
80100511:	52                   	push   %edx
80100512:	68 60 0e 00 00       	push   $0xe60
    pos -= 80;
80100517:	83 eb 50             	sub    $0x50,%ebx
    memmove(crt, crt+80, sizeof(crt[0])*23*80);
8010051a:	68 a0 80 0b 80       	push   $0x800b80a0
8010051f:	68 00 80 0b 80       	push   $0x800b8000
80100524:	e8 67 44 00 00       	call   80104990 <memmove>
    memset(crt+pos, 0, sizeof(crt[0])*(24*80 - pos));
80100529:	b8 80 07 00 00       	mov    $0x780,%eax
8010052e:	83 c4 0c             	add    $0xc,%esp
80100531:	29 d8                	sub    %ebx,%eax
80100533:	01 c0                	add    %eax,%eax
80100535:	50                   	push   %eax
80100536:	8d 04 1b             	lea    (%ebx,%ebx,1),%eax
80100539:	6a 00                	push   $0x0
8010053b:	2d 00 80 f4 7f       	sub    $0x7ff48000,%eax
80100540:	50                   	push   %eax
80100541:	e8 9a 43 00 00       	call   801048e0 <memset>
80100546:	83 c4 10             	add    $0x10,%esp
80100549:	e9 5d ff ff ff       	jmp    801004ab <consputc+0x9b>
    panic("pos under/overflow");
8010054e:	83 ec 0c             	sub    $0xc,%esp
80100551:	68 85 77 10 80       	push   $0x80107785
80100556:	e8 35 fe ff ff       	call   80100390 <panic>
    if(pos > 0) --pos;
8010055b:	85 db                	test   %ebx,%ebx
8010055d:	0f 84 48 ff ff ff    	je     801004ab <consputc+0x9b>
80100563:	83 eb 01             	sub    $0x1,%ebx
80100566:	e9 2c ff ff ff       	jmp    80100497 <consputc+0x87>
    pos += 80 - pos%80;
8010056b:	89 d8                	mov    %ebx,%eax
8010056d:	b9 50 00 00 00       	mov    $0x50,%ecx
80100572:	99                   	cltd   
80100573:	f7 f9                	idiv   %ecx
80100575:	29 d1                	sub    %edx,%ecx
80100577:	01 cb                	add    %ecx,%ebx
80100579:	e9 19 ff ff ff       	jmp    80100497 <consputc+0x87>
8010057e:	66 90                	xchg   %ax,%ax

80100580 <printint>:
{
80100580:	55                   	push   %ebp
80100581:	89 e5                	mov    %esp,%ebp
80100583:	57                   	push   %edi
80100584:	56                   	push   %esi
80100585:	53                   	push   %ebx
80100586:	89 d3                	mov    %edx,%ebx
80100588:	83 ec 2c             	sub    $0x2c,%esp
  if(sign && (sign = xx < 0))
8010058b:	85 c9                	test   %ecx,%ecx
{
8010058d:	89 4d d4             	mov    %ecx,-0x2c(%ebp)
  if(sign && (sign = xx < 0))
80100590:	74 04                	je     80100596 <printint+0x16>
80100592:	85 c0                	test   %eax,%eax
80100594:	78 5a                	js     801005f0 <printint+0x70>
    x = xx;
80100596:	c7 45 d4 00 00 00 00 	movl   $0x0,-0x2c(%ebp)
  i = 0;
8010059d:	31 c9                	xor    %ecx,%ecx
8010059f:	8d 75 d7             	lea    -0x29(%ebp),%esi
801005a2:	eb 06                	jmp    801005aa <printint+0x2a>
801005a4:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    buf[i++] = digits[x % base];
801005a8:	89 f9                	mov    %edi,%ecx
801005aa:	31 d2                	xor    %edx,%edx
801005ac:	8d 79 01             	lea    0x1(%ecx),%edi
801005af:	f7 f3                	div    %ebx
801005b1:	0f b6 92 b0 77 10 80 	movzbl -0x7fef8850(%edx),%edx
  }while((x /= base) != 0);
801005b8:	85 c0                	test   %eax,%eax
    buf[i++] = digits[x % base];
801005ba:	88 14 3e             	mov    %dl,(%esi,%edi,1)
  }while((x /= base) != 0);
801005bd:	75 e9                	jne    801005a8 <printint+0x28>
  if(sign)
801005bf:	8b 45 d4             	mov    -0x2c(%ebp),%eax
801005c2:	85 c0                	test   %eax,%eax
801005c4:	74 08                	je     801005ce <printint+0x4e>
    buf[i++] = '-';
801005c6:	c6 44 3d d8 2d       	movb   $0x2d,-0x28(%ebp,%edi,1)
801005cb:	8d 79 02             	lea    0x2(%ecx),%edi
801005ce:	8d 5c 3d d7          	lea    -0x29(%ebp,%edi,1),%ebx
801005d2:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    consputc(buf[i]);
801005d8:	0f be 03             	movsbl (%ebx),%eax
801005db:	83 eb 01             	sub    $0x1,%ebx
801005de:	e8 2d fe ff ff       	call   80100410 <consputc>
  while(--i >= 0)
801005e3:	39 f3                	cmp    %esi,%ebx
801005e5:	75 f1                	jne    801005d8 <printint+0x58>
}
801005e7:	83 c4 2c             	add    $0x2c,%esp
801005ea:	5b                   	pop    %ebx
801005eb:	5e                   	pop    %esi
801005ec:	5f                   	pop    %edi
801005ed:	5d                   	pop    %ebp
801005ee:	c3                   	ret    
801005ef:	90                   	nop
    x = -xx;
801005f0:	f7 d8                	neg    %eax
801005f2:	eb a9                	jmp    8010059d <printint+0x1d>
801005f4:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
801005fa:	8d bf 00 00 00 00    	lea    0x0(%edi),%edi

80100600 <consolewrite>:

int
consolewrite(struct inode *ip, char *buf, int n)
{
80100600:	55                   	push   %ebp
80100601:	89 e5                	mov    %esp,%ebp
80100603:	57                   	push   %edi
80100604:	56                   	push   %esi
80100605:	53                   	push   %ebx
80100606:	83 ec 18             	sub    $0x18,%esp
80100609:	8b 75 10             	mov    0x10(%ebp),%esi
  int i;

  iunlock(ip);
8010060c:	ff 75 08             	pushl  0x8(%ebp)
8010060f:	e8 4c 11 00 00       	call   80101760 <iunlock>
  acquire(&cons.lock);
80100614:	c7 04 24 20 b5 10 80 	movl   $0x8010b520,(%esp)
8010061b:	e8 b0 41 00 00       	call   801047d0 <acquire>
  for(i = 0; i < n; i++)
80100620:	83 c4 10             	add    $0x10,%esp
80100623:	85 f6                	test   %esi,%esi
80100625:	7e 18                	jle    8010063f <consolewrite+0x3f>
80100627:	8b 7d 0c             	mov    0xc(%ebp),%edi
8010062a:	8d 1c 37             	lea    (%edi,%esi,1),%ebx
8010062d:	8d 76 00             	lea    0x0(%esi),%esi
    consputc(buf[i] & 0xff);
80100630:	0f b6 07             	movzbl (%edi),%eax
80100633:	83 c7 01             	add    $0x1,%edi
80100636:	e8 d5 fd ff ff       	call   80100410 <consputc>
  for(i = 0; i < n; i++)
8010063b:	39 fb                	cmp    %edi,%ebx
8010063d:	75 f1                	jne    80100630 <consolewrite+0x30>
  release(&cons.lock);
8010063f:	83 ec 0c             	sub    $0xc,%esp
80100642:	68 20 b5 10 80       	push   $0x8010b520
80100647:	e8 44 42 00 00       	call   80104890 <release>
  ilock(ip);
8010064c:	58                   	pop    %eax
8010064d:	ff 75 08             	pushl  0x8(%ebp)
80100650:	e8 2b 10 00 00       	call   80101680 <ilock>

  return n;
}
80100655:	8d 65 f4             	lea    -0xc(%ebp),%esp
80100658:	89 f0                	mov    %esi,%eax
8010065a:	5b                   	pop    %ebx
8010065b:	5e                   	pop    %esi
8010065c:	5f                   	pop    %edi
8010065d:	5d                   	pop    %ebp
8010065e:	c3                   	ret    
8010065f:	90                   	nop

80100660 <cprintf>:
{
80100660:	55                   	push   %ebp
80100661:	89 e5                	mov    %esp,%ebp
80100663:	57                   	push   %edi
80100664:	56                   	push   %esi
80100665:	53                   	push   %ebx
80100666:	83 ec 1c             	sub    $0x1c,%esp
  locking = cons.locking;
80100669:	a1 54 b5 10 80       	mov    0x8010b554,%eax
  if(locking)
8010066e:	85 c0                	test   %eax,%eax
  locking = cons.locking;
80100670:	89 45 dc             	mov    %eax,-0x24(%ebp)
  if(locking)
80100673:	0f 85 6f 01 00 00    	jne    801007e8 <cprintf+0x188>
  if (fmt == 0)
80100679:	8b 45 08             	mov    0x8(%ebp),%eax
8010067c:	85 c0                	test   %eax,%eax
8010067e:	89 c7                	mov    %eax,%edi
80100680:	0f 84 77 01 00 00    	je     801007fd <cprintf+0x19d>
  for(i = 0; (c = fmt[i] & 0xff) != 0; i++){
80100686:	0f b6 00             	movzbl (%eax),%eax
  argp = (uint*)(void*)(&fmt + 1);
80100689:	8d 4d 0c             	lea    0xc(%ebp),%ecx
  for(i = 0; (c = fmt[i] & 0xff) != 0; i++){
8010068c:	31 db                	xor    %ebx,%ebx
  argp = (uint*)(void*)(&fmt + 1);
8010068e:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
  for(i = 0; (c = fmt[i] & 0xff) != 0; i++){
80100691:	85 c0                	test   %eax,%eax
80100693:	75 56                	jne    801006eb <cprintf+0x8b>
80100695:	eb 79                	jmp    80100710 <cprintf+0xb0>
80100697:	89 f6                	mov    %esi,%esi
80100699:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi
    c = fmt[++i] & 0xff;
801006a0:	0f b6 16             	movzbl (%esi),%edx
    if(c == 0)
801006a3:	85 d2                	test   %edx,%edx
801006a5:	74 69                	je     80100710 <cprintf+0xb0>
801006a7:	83 c3 02             	add    $0x2,%ebx
    switch(c){
801006aa:	83 fa 70             	cmp    $0x70,%edx
801006ad:	8d 34 1f             	lea    (%edi,%ebx,1),%esi
801006b0:	0f 84 84 00 00 00    	je     8010073a <cprintf+0xda>
801006b6:	7f 78                	jg     80100730 <cprintf+0xd0>
801006b8:	83 fa 25             	cmp    $0x25,%edx
801006bb:	0f 84 ff 00 00 00    	je     801007c0 <cprintf+0x160>
801006c1:	83 fa 64             	cmp    $0x64,%edx
801006c4:	0f 85 8e 00 00 00    	jne    80100758 <cprintf+0xf8>
      printint(*argp++, 10, 1);
801006ca:	8b 45 e4             	mov    -0x1c(%ebp),%eax
801006cd:	ba 0a 00 00 00       	mov    $0xa,%edx
801006d2:	8d 48 04             	lea    0x4(%eax),%ecx
801006d5:	8b 00                	mov    (%eax),%eax
801006d7:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
801006da:	b9 01 00 00 00       	mov    $0x1,%ecx
801006df:	e8 9c fe ff ff       	call   80100580 <printint>
  for(i = 0; (c = fmt[i] & 0xff) != 0; i++){
801006e4:	0f b6 06             	movzbl (%esi),%eax
801006e7:	85 c0                	test   %eax,%eax
801006e9:	74 25                	je     80100710 <cprintf+0xb0>
801006eb:	8d 53 01             	lea    0x1(%ebx),%edx
    if(c != '%'){
801006ee:	83 f8 25             	cmp    $0x25,%eax
801006f1:	8d 34 17             	lea    (%edi,%edx,1),%esi
801006f4:	74 aa                	je     801006a0 <cprintf+0x40>
801006f6:	89 55 e0             	mov    %edx,-0x20(%ebp)
      consputc(c);
801006f9:	e8 12 fd ff ff       	call   80100410 <consputc>
  for(i = 0; (c = fmt[i] & 0xff) != 0; i++){
801006fe:	0f b6 06             	movzbl (%esi),%eax
      continue;
80100701:	8b 55 e0             	mov    -0x20(%ebp),%edx
80100704:	89 d3                	mov    %edx,%ebx
  for(i = 0; (c = fmt[i] & 0xff) != 0; i++){
80100706:	85 c0                	test   %eax,%eax
80100708:	75 e1                	jne    801006eb <cprintf+0x8b>
8010070a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
  if(locking)
80100710:	8b 45 dc             	mov    -0x24(%ebp),%eax
80100713:	85 c0                	test   %eax,%eax
80100715:	74 10                	je     80100727 <cprintf+0xc7>
    release(&cons.lock);
80100717:	83 ec 0c             	sub    $0xc,%esp
8010071a:	68 20 b5 10 80       	push   $0x8010b520
8010071f:	e8 6c 41 00 00       	call   80104890 <release>
80100724:	83 c4 10             	add    $0x10,%esp
}
80100727:	8d 65 f4             	lea    -0xc(%ebp),%esp
8010072a:	5b                   	pop    %ebx
8010072b:	5e                   	pop    %esi
8010072c:	5f                   	pop    %edi
8010072d:	5d                   	pop    %ebp
8010072e:	c3                   	ret    
8010072f:	90                   	nop
    switch(c){
80100730:	83 fa 73             	cmp    $0x73,%edx
80100733:	74 43                	je     80100778 <cprintf+0x118>
80100735:	83 fa 78             	cmp    $0x78,%edx
80100738:	75 1e                	jne    80100758 <cprintf+0xf8>
      printint(*argp++, 16, 0);
8010073a:	8b 45 e4             	mov    -0x1c(%ebp),%eax
8010073d:	ba 10 00 00 00       	mov    $0x10,%edx
80100742:	8d 48 04             	lea    0x4(%eax),%ecx
80100745:	8b 00                	mov    (%eax),%eax
80100747:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
8010074a:	31 c9                	xor    %ecx,%ecx
8010074c:	e8 2f fe ff ff       	call   80100580 <printint>
      break;
80100751:	eb 91                	jmp    801006e4 <cprintf+0x84>
80100753:	90                   	nop
80100754:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
      consputc('%');
80100758:	b8 25 00 00 00       	mov    $0x25,%eax
8010075d:	89 55 e0             	mov    %edx,-0x20(%ebp)
80100760:	e8 ab fc ff ff       	call   80100410 <consputc>
      consputc(c);
80100765:	8b 55 e0             	mov    -0x20(%ebp),%edx
80100768:	89 d0                	mov    %edx,%eax
8010076a:	e8 a1 fc ff ff       	call   80100410 <consputc>
      break;
8010076f:	e9 70 ff ff ff       	jmp    801006e4 <cprintf+0x84>
80100774:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
      if((s = (char*)*argp++) == 0)
80100778:	8b 45 e4             	mov    -0x1c(%ebp),%eax
8010077b:	8b 10                	mov    (%eax),%edx
8010077d:	8d 48 04             	lea    0x4(%eax),%ecx
80100780:	89 4d e0             	mov    %ecx,-0x20(%ebp)
80100783:	85 d2                	test   %edx,%edx
80100785:	74 49                	je     801007d0 <cprintf+0x170>
      for(; *s; s++)
80100787:	0f be 02             	movsbl (%edx),%eax
      if((s = (char*)*argp++) == 0)
8010078a:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
      for(; *s; s++)
8010078d:	84 c0                	test   %al,%al
8010078f:	0f 84 4f ff ff ff    	je     801006e4 <cprintf+0x84>
80100795:	89 5d e4             	mov    %ebx,-0x1c(%ebp)
80100798:	89 d3                	mov    %edx,%ebx
8010079a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
801007a0:	83 c3 01             	add    $0x1,%ebx
        consputc(*s);
801007a3:	e8 68 fc ff ff       	call   80100410 <consputc>
      for(; *s; s++)
801007a8:	0f be 03             	movsbl (%ebx),%eax
801007ab:	84 c0                	test   %al,%al
801007ad:	75 f1                	jne    801007a0 <cprintf+0x140>
      if((s = (char*)*argp++) == 0)
801007af:	8b 45 e0             	mov    -0x20(%ebp),%eax
801007b2:	8b 5d e4             	mov    -0x1c(%ebp),%ebx
801007b5:	89 45 e4             	mov    %eax,-0x1c(%ebp)
801007b8:	e9 27 ff ff ff       	jmp    801006e4 <cprintf+0x84>
801007bd:	8d 76 00             	lea    0x0(%esi),%esi
      consputc('%');
801007c0:	b8 25 00 00 00       	mov    $0x25,%eax
801007c5:	e8 46 fc ff ff       	call   80100410 <consputc>
      break;
801007ca:	e9 15 ff ff ff       	jmp    801006e4 <cprintf+0x84>
801007cf:	90                   	nop
        s = "(null)";
801007d0:	ba 98 77 10 80       	mov    $0x80107798,%edx
      for(; *s; s++)
801007d5:	89 5d e4             	mov    %ebx,-0x1c(%ebp)
801007d8:	b8 28 00 00 00       	mov    $0x28,%eax
801007dd:	89 d3                	mov    %edx,%ebx
801007df:	eb bf                	jmp    801007a0 <cprintf+0x140>
801007e1:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    acquire(&cons.lock);
801007e8:	83 ec 0c             	sub    $0xc,%esp
801007eb:	68 20 b5 10 80       	push   $0x8010b520
801007f0:	e8 db 3f 00 00       	call   801047d0 <acquire>
801007f5:	83 c4 10             	add    $0x10,%esp
801007f8:	e9 7c fe ff ff       	jmp    80100679 <cprintf+0x19>
    panic("null fmt");
801007fd:	83 ec 0c             	sub    $0xc,%esp
80100800:	68 9f 77 10 80       	push   $0x8010779f
80100805:	e8 86 fb ff ff       	call   80100390 <panic>
8010080a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi

80100810 <consoleintr>:
{
80100810:	55                   	push   %ebp
80100811:	89 e5                	mov    %esp,%ebp
80100813:	57                   	push   %edi
80100814:	56                   	push   %esi
80100815:	53                   	push   %ebx
  int c, doprocdump = 0;
80100816:	31 f6                	xor    %esi,%esi
{
80100818:	83 ec 18             	sub    $0x18,%esp
8010081b:	8b 5d 08             	mov    0x8(%ebp),%ebx
  acquire(&cons.lock);
8010081e:	68 20 b5 10 80       	push   $0x8010b520
80100823:	e8 a8 3f 00 00       	call   801047d0 <acquire>
  while((c = getc()) >= 0){
80100828:	83 c4 10             	add    $0x10,%esp
8010082b:	90                   	nop
8010082c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80100830:	ff d3                	call   *%ebx
80100832:	85 c0                	test   %eax,%eax
80100834:	89 c7                	mov    %eax,%edi
80100836:	78 48                	js     80100880 <consoleintr+0x70>
    switch(c){
80100838:	83 ff 10             	cmp    $0x10,%edi
8010083b:	0f 84 e7 00 00 00    	je     80100928 <consoleintr+0x118>
80100841:	7e 5d                	jle    801008a0 <consoleintr+0x90>
80100843:	83 ff 15             	cmp    $0x15,%edi
80100846:	0f 84 ec 00 00 00    	je     80100938 <consoleintr+0x128>
8010084c:	83 ff 7f             	cmp    $0x7f,%edi
8010084f:	75 54                	jne    801008a5 <consoleintr+0x95>
      if(input.e != input.w){
80100851:	a1 a8 0f 11 80       	mov    0x80110fa8,%eax
80100856:	3b 05 a4 0f 11 80    	cmp    0x80110fa4,%eax
8010085c:	74 d2                	je     80100830 <consoleintr+0x20>
        input.e--;
8010085e:	83 e8 01             	sub    $0x1,%eax
80100861:	a3 a8 0f 11 80       	mov    %eax,0x80110fa8
        consputc(BACKSPACE);
80100866:	b8 00 01 00 00       	mov    $0x100,%eax
8010086b:	e8 a0 fb ff ff       	call   80100410 <consputc>
  while((c = getc()) >= 0){
80100870:	ff d3                	call   *%ebx
80100872:	85 c0                	test   %eax,%eax
80100874:	89 c7                	mov    %eax,%edi
80100876:	79 c0                	jns    80100838 <consoleintr+0x28>
80100878:	90                   	nop
80100879:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
  release(&cons.lock);
80100880:	83 ec 0c             	sub    $0xc,%esp
80100883:	68 20 b5 10 80       	push   $0x8010b520
80100888:	e8 03 40 00 00       	call   80104890 <release>
  if(doprocdump) {
8010088d:	83 c4 10             	add    $0x10,%esp
80100890:	85 f6                	test   %esi,%esi
80100892:	0f 85 f8 00 00 00    	jne    80100990 <consoleintr+0x180>
}
80100898:	8d 65 f4             	lea    -0xc(%ebp),%esp
8010089b:	5b                   	pop    %ebx
8010089c:	5e                   	pop    %esi
8010089d:	5f                   	pop    %edi
8010089e:	5d                   	pop    %ebp
8010089f:	c3                   	ret    
    switch(c){
801008a0:	83 ff 08             	cmp    $0x8,%edi
801008a3:	74 ac                	je     80100851 <consoleintr+0x41>
      if(c != 0 && input.e-input.r < INPUT_BUF){
801008a5:	85 ff                	test   %edi,%edi
801008a7:	74 87                	je     80100830 <consoleintr+0x20>
801008a9:	a1 a8 0f 11 80       	mov    0x80110fa8,%eax
801008ae:	89 c2                	mov    %eax,%edx
801008b0:	2b 15 a0 0f 11 80    	sub    0x80110fa0,%edx
801008b6:	83 fa 7f             	cmp    $0x7f,%edx
801008b9:	0f 87 71 ff ff ff    	ja     80100830 <consoleintr+0x20>
801008bf:	8d 50 01             	lea    0x1(%eax),%edx
801008c2:	83 e0 7f             	and    $0x7f,%eax
        c = (c == '\r') ? '\n' : c;
801008c5:	83 ff 0d             	cmp    $0xd,%edi
        input.buf[input.e++ % INPUT_BUF] = c;
801008c8:	89 15 a8 0f 11 80    	mov    %edx,0x80110fa8
        c = (c == '\r') ? '\n' : c;
801008ce:	0f 84 cc 00 00 00    	je     801009a0 <consoleintr+0x190>
        input.buf[input.e++ % INPUT_BUF] = c;
801008d4:	89 f9                	mov    %edi,%ecx
801008d6:	88 88 20 0f 11 80    	mov    %cl,-0x7feef0e0(%eax)
        consputc(c);
801008dc:	89 f8                	mov    %edi,%eax
801008de:	e8 2d fb ff ff       	call   80100410 <consputc>
        if(c == '\n' || c == C('D') || input.e == input.r+INPUT_BUF){
801008e3:	83 ff 0a             	cmp    $0xa,%edi
801008e6:	0f 84 c5 00 00 00    	je     801009b1 <consoleintr+0x1a1>
801008ec:	83 ff 04             	cmp    $0x4,%edi
801008ef:	0f 84 bc 00 00 00    	je     801009b1 <consoleintr+0x1a1>
801008f5:	a1 a0 0f 11 80       	mov    0x80110fa0,%eax
801008fa:	83 e8 80             	sub    $0xffffff80,%eax
801008fd:	39 05 a8 0f 11 80    	cmp    %eax,0x80110fa8
80100903:	0f 85 27 ff ff ff    	jne    80100830 <consoleintr+0x20>
          wakeup(&input.r);
80100909:	83 ec 0c             	sub    $0xc,%esp
          input.w = input.e;
8010090c:	a3 a4 0f 11 80       	mov    %eax,0x80110fa4
          wakeup(&input.r);
80100911:	68 a0 0f 11 80       	push   $0x80110fa0
80100916:	e8 95 3a 00 00       	call   801043b0 <wakeup>
8010091b:	83 c4 10             	add    $0x10,%esp
8010091e:	e9 0d ff ff ff       	jmp    80100830 <consoleintr+0x20>
80100923:	90                   	nop
80100924:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
      doprocdump = 1;
80100928:	be 01 00 00 00       	mov    $0x1,%esi
8010092d:	e9 fe fe ff ff       	jmp    80100830 <consoleintr+0x20>
80100932:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
      while(input.e != input.w &&
80100938:	a1 a8 0f 11 80       	mov    0x80110fa8,%eax
8010093d:	39 05 a4 0f 11 80    	cmp    %eax,0x80110fa4
80100943:	75 2b                	jne    80100970 <consoleintr+0x160>
80100945:	e9 e6 fe ff ff       	jmp    80100830 <consoleintr+0x20>
8010094a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
        input.e--;
80100950:	a3 a8 0f 11 80       	mov    %eax,0x80110fa8
        consputc(BACKSPACE);
80100955:	b8 00 01 00 00       	mov    $0x100,%eax
8010095a:	e8 b1 fa ff ff       	call   80100410 <consputc>
      while(input.e != input.w &&
8010095f:	a1 a8 0f 11 80       	mov    0x80110fa8,%eax
80100964:	3b 05 a4 0f 11 80    	cmp    0x80110fa4,%eax
8010096a:	0f 84 c0 fe ff ff    	je     80100830 <consoleintr+0x20>
            input.buf[(input.e-1) % INPUT_BUF] != '\n'){
80100970:	83 e8 01             	sub    $0x1,%eax
80100973:	89 c2                	mov    %eax,%edx
80100975:	83 e2 7f             	and    $0x7f,%edx
      while(input.e != input.w &&
80100978:	80 ba 20 0f 11 80 0a 	cmpb   $0xa,-0x7feef0e0(%edx)
8010097f:	75 cf                	jne    80100950 <consoleintr+0x140>
80100981:	e9 aa fe ff ff       	jmp    80100830 <consoleintr+0x20>
80100986:	8d 76 00             	lea    0x0(%esi),%esi
80100989:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi
}
80100990:	8d 65 f4             	lea    -0xc(%ebp),%esp
80100993:	5b                   	pop    %ebx
80100994:	5e                   	pop    %esi
80100995:	5f                   	pop    %edi
80100996:	5d                   	pop    %ebp
    procdump();  // now call procdump() wo. cons.lock held
80100997:	e9 f4 3a 00 00       	jmp    80104490 <procdump>
8010099c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
        input.buf[input.e++ % INPUT_BUF] = c;
801009a0:	c6 80 20 0f 11 80 0a 	movb   $0xa,-0x7feef0e0(%eax)
        consputc(c);
801009a7:	b8 0a 00 00 00       	mov    $0xa,%eax
801009ac:	e8 5f fa ff ff       	call   80100410 <consputc>
801009b1:	a1 a8 0f 11 80       	mov    0x80110fa8,%eax
801009b6:	e9 4e ff ff ff       	jmp    80100909 <consoleintr+0xf9>
801009bb:	90                   	nop
801009bc:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

801009c0 <consoleinit>:

void
consoleinit(void)
{
801009c0:	55                   	push   %ebp
801009c1:	89 e5                	mov    %esp,%ebp
801009c3:	83 ec 10             	sub    $0x10,%esp
  initlock(&cons.lock, "console");
801009c6:	68 a8 77 10 80       	push   $0x801077a8
801009cb:	68 20 b5 10 80       	push   $0x8010b520
801009d0:	e8 bb 3c 00 00       	call   80104690 <initlock>

  devsw[CONSOLE].write = consolewrite;
  devsw[CONSOLE].read = consoleread;
  cons.locking = 1;

  ioapicenable(IRQ_KBD, 0);
801009d5:	58                   	pop    %eax
801009d6:	5a                   	pop    %edx
801009d7:	6a 00                	push   $0x0
801009d9:	6a 01                	push   $0x1
  devsw[CONSOLE].write = consolewrite;
801009db:	c7 05 6c 19 11 80 00 	movl   $0x80100600,0x8011196c
801009e2:	06 10 80 
  devsw[CONSOLE].read = consoleread;
801009e5:	c7 05 68 19 11 80 70 	movl   $0x80100270,0x80111968
801009ec:	02 10 80 
  cons.locking = 1;
801009ef:	c7 05 54 b5 10 80 01 	movl   $0x1,0x8010b554
801009f6:	00 00 00 
  ioapicenable(IRQ_KBD, 0);
801009f9:	e8 62 1c 00 00       	call   80102660 <ioapicenable>
}
801009fe:	83 c4 10             	add    $0x10,%esp
80100a01:	c9                   	leave  
80100a02:	c3                   	ret    
80100a03:	66 90                	xchg   %ax,%ax
80100a05:	66 90                	xchg   %ax,%ax
80100a07:	66 90                	xchg   %ax,%ax
80100a09:	66 90                	xchg   %ax,%ax
80100a0b:	66 90                	xchg   %ax,%ax
80100a0d:	66 90                	xchg   %ax,%ax
80100a0f:	90                   	nop

80100a10 <exec>:
#include "x86.h"
#include "elf.h"

int
exec(char *path, char **argv)
{
80100a10:	55                   	push   %ebp
80100a11:	89 e5                	mov    %esp,%ebp
80100a13:	57                   	push   %edi
80100a14:	56                   	push   %esi
80100a15:	53                   	push   %ebx
80100a16:	81 ec 0c 01 00 00    	sub    $0x10c,%esp
  uint argc, sz, sp, ustack[3+MAXARG+1];
  struct elfhdr elf;
  struct inode *ip;
  struct proghdr ph;
  pde_t *pgdir, *oldpgdir;
  struct proc *curproc = myproc();
80100a1c:	e8 0f 32 00 00       	call   80103c30 <myproc>
80100a21:	89 85 f4 fe ff ff    	mov    %eax,-0x10c(%ebp)

  begin_op();
80100a27:	e8 74 25 00 00       	call   80102fa0 <begin_op>

  if((ip = namei(path)) == 0){
80100a2c:	83 ec 0c             	sub    $0xc,%esp
80100a2f:	ff 75 08             	pushl  0x8(%ebp)
80100a32:	e8 a9 14 00 00       	call   80101ee0 <namei>
80100a37:	83 c4 10             	add    $0x10,%esp
80100a3a:	85 c0                	test   %eax,%eax
80100a3c:	0f 84 91 01 00 00    	je     80100bd3 <exec+0x1c3>
    end_op();
    cprintf("exec: fail\n");
    return -1;
  }
  ilock(ip);
80100a42:	83 ec 0c             	sub    $0xc,%esp
80100a45:	89 c3                	mov    %eax,%ebx
80100a47:	50                   	push   %eax
80100a48:	e8 33 0c 00 00       	call   80101680 <ilock>
  pgdir = 0;

  // Check ELF header
  if(readi(ip, (char*)&elf, 0, sizeof(elf)) != sizeof(elf))
80100a4d:	8d 85 24 ff ff ff    	lea    -0xdc(%ebp),%eax
80100a53:	6a 34                	push   $0x34
80100a55:	6a 00                	push   $0x0
80100a57:	50                   	push   %eax
80100a58:	53                   	push   %ebx
80100a59:	e8 02 0f 00 00       	call   80101960 <readi>
80100a5e:	83 c4 20             	add    $0x20,%esp
80100a61:	83 f8 34             	cmp    $0x34,%eax
80100a64:	74 22                	je     80100a88 <exec+0x78>

 bad:
  if(pgdir)
    freevm(pgdir);
  if(ip){
    iunlockput(ip);
80100a66:	83 ec 0c             	sub    $0xc,%esp
80100a69:	53                   	push   %ebx
80100a6a:	e8 a1 0e 00 00       	call   80101910 <iunlockput>
    end_op();
80100a6f:	e8 9c 25 00 00       	call   80103010 <end_op>
80100a74:	83 c4 10             	add    $0x10,%esp
  }
  return -1;
80100a77:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80100a7c:	8d 65 f4             	lea    -0xc(%ebp),%esp
80100a7f:	5b                   	pop    %ebx
80100a80:	5e                   	pop    %esi
80100a81:	5f                   	pop    %edi
80100a82:	5d                   	pop    %ebp
80100a83:	c3                   	ret    
80100a84:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  if(elf.magic != ELF_MAGIC)
80100a88:	81 bd 24 ff ff ff 7f 	cmpl   $0x464c457f,-0xdc(%ebp)
80100a8f:	45 4c 46 
80100a92:	75 d2                	jne    80100a66 <exec+0x56>
  if((pgdir = setupkvm()) == 0)
80100a94:	e8 57 65 00 00       	call   80106ff0 <setupkvm>
80100a99:	85 c0                	test   %eax,%eax
80100a9b:	89 85 f0 fe ff ff    	mov    %eax,-0x110(%ebp)
80100aa1:	74 c3                	je     80100a66 <exec+0x56>
  sz = 0;
80100aa3:	31 ff                	xor    %edi,%edi
  for(i=0, off=elf.phoff; i<elf.phnum; i++, off+=sizeof(ph)){
80100aa5:	66 83 bd 50 ff ff ff 	cmpw   $0x0,-0xb0(%ebp)
80100aac:	00 
80100aad:	8b 85 40 ff ff ff    	mov    -0xc0(%ebp),%eax
80100ab3:	89 85 ec fe ff ff    	mov    %eax,-0x114(%ebp)
80100ab9:	0f 84 8c 02 00 00    	je     80100d4b <exec+0x33b>
80100abf:	31 f6                	xor    %esi,%esi
80100ac1:	eb 7f                	jmp    80100b42 <exec+0x132>
80100ac3:	90                   	nop
80100ac4:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    if(ph.type != ELF_PROG_LOAD)
80100ac8:	83 bd 04 ff ff ff 01 	cmpl   $0x1,-0xfc(%ebp)
80100acf:	75 63                	jne    80100b34 <exec+0x124>
    if(ph.memsz < ph.filesz)
80100ad1:	8b 85 18 ff ff ff    	mov    -0xe8(%ebp),%eax
80100ad7:	3b 85 14 ff ff ff    	cmp    -0xec(%ebp),%eax
80100add:	0f 82 86 00 00 00    	jb     80100b69 <exec+0x159>
80100ae3:	03 85 0c ff ff ff    	add    -0xf4(%ebp),%eax
80100ae9:	72 7e                	jb     80100b69 <exec+0x159>
    if((sz = allocuvm(pgdir, sz, ph.vaddr + ph.memsz)) == 0)
80100aeb:	83 ec 04             	sub    $0x4,%esp
80100aee:	50                   	push   %eax
80100aef:	57                   	push   %edi
80100af0:	ff b5 f0 fe ff ff    	pushl  -0x110(%ebp)
80100af6:	e8 15 6a 00 00       	call   80107510 <allocuvm>
80100afb:	83 c4 10             	add    $0x10,%esp
80100afe:	85 c0                	test   %eax,%eax
80100b00:	89 c7                	mov    %eax,%edi
80100b02:	74 65                	je     80100b69 <exec+0x159>
    if(ph.vaddr % PGSIZE != 0)
80100b04:	8b 85 0c ff ff ff    	mov    -0xf4(%ebp),%eax
80100b0a:	a9 ff 0f 00 00       	test   $0xfff,%eax
80100b0f:	75 58                	jne    80100b69 <exec+0x159>
    if(loaduvm(pgdir, (char*)ph.vaddr, ip, ph.off, ph.filesz) < 0)
80100b11:	83 ec 0c             	sub    $0xc,%esp
80100b14:	ff b5 14 ff ff ff    	pushl  -0xec(%ebp)
80100b1a:	ff b5 08 ff ff ff    	pushl  -0xf8(%ebp)
80100b20:	53                   	push   %ebx
80100b21:	50                   	push   %eax
80100b22:	ff b5 f0 fe ff ff    	pushl  -0x110(%ebp)
80100b28:	e8 83 62 00 00       	call   80106db0 <loaduvm>
80100b2d:	83 c4 20             	add    $0x20,%esp
80100b30:	85 c0                	test   %eax,%eax
80100b32:	78 35                	js     80100b69 <exec+0x159>
  for(i=0, off=elf.phoff; i<elf.phnum; i++, off+=sizeof(ph)){
80100b34:	0f b7 85 50 ff ff ff 	movzwl -0xb0(%ebp),%eax
80100b3b:	83 c6 01             	add    $0x1,%esi
80100b3e:	39 f0                	cmp    %esi,%eax
80100b40:	7e 3d                	jle    80100b7f <exec+0x16f>
    if(readi(ip, (char*)&ph, off, sizeof(ph)) != sizeof(ph))
80100b42:	89 f0                	mov    %esi,%eax
80100b44:	6a 20                	push   $0x20
80100b46:	c1 e0 05             	shl    $0x5,%eax
80100b49:	03 85 ec fe ff ff    	add    -0x114(%ebp),%eax
80100b4f:	50                   	push   %eax
80100b50:	8d 85 04 ff ff ff    	lea    -0xfc(%ebp),%eax
80100b56:	50                   	push   %eax
80100b57:	53                   	push   %ebx
80100b58:	e8 03 0e 00 00       	call   80101960 <readi>
80100b5d:	83 c4 10             	add    $0x10,%esp
80100b60:	83 f8 20             	cmp    $0x20,%eax
80100b63:	0f 84 5f ff ff ff    	je     80100ac8 <exec+0xb8>
    freevm(pgdir);
80100b69:	83 ec 0c             	sub    $0xc,%esp
80100b6c:	ff b5 f0 fe ff ff    	pushl  -0x110(%ebp)
80100b72:	e8 f9 63 00 00       	call   80106f70 <freevm>
80100b77:	83 c4 10             	add    $0x10,%esp
80100b7a:	e9 e7 fe ff ff       	jmp    80100a66 <exec+0x56>
80100b7f:	81 c7 ff 0f 00 00    	add    $0xfff,%edi
80100b85:	81 e7 00 f0 ff ff    	and    $0xfffff000,%edi
80100b8b:	8d b7 00 20 00 00    	lea    0x2000(%edi),%esi
  iunlockput(ip);
80100b91:	83 ec 0c             	sub    $0xc,%esp
80100b94:	53                   	push   %ebx
80100b95:	e8 76 0d 00 00       	call   80101910 <iunlockput>
  end_op();
80100b9a:	e8 71 24 00 00       	call   80103010 <end_op>
  if((sz = allocuvm(pgdir, sz, sz + 2*PGSIZE)) == 0)
80100b9f:	83 c4 0c             	add    $0xc,%esp
80100ba2:	56                   	push   %esi
80100ba3:	57                   	push   %edi
80100ba4:	ff b5 f0 fe ff ff    	pushl  -0x110(%ebp)
80100baa:	e8 61 69 00 00       	call   80107510 <allocuvm>
80100baf:	83 c4 10             	add    $0x10,%esp
80100bb2:	85 c0                	test   %eax,%eax
80100bb4:	89 c6                	mov    %eax,%esi
80100bb6:	75 3a                	jne    80100bf2 <exec+0x1e2>
    freevm(pgdir);
80100bb8:	83 ec 0c             	sub    $0xc,%esp
80100bbb:	ff b5 f0 fe ff ff    	pushl  -0x110(%ebp)
80100bc1:	e8 aa 63 00 00       	call   80106f70 <freevm>
80100bc6:	83 c4 10             	add    $0x10,%esp
  return -1;
80100bc9:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80100bce:	e9 a9 fe ff ff       	jmp    80100a7c <exec+0x6c>
    end_op();
80100bd3:	e8 38 24 00 00       	call   80103010 <end_op>
    cprintf("exec: fail\n");
80100bd8:	83 ec 0c             	sub    $0xc,%esp
80100bdb:	68 c1 77 10 80       	push   $0x801077c1
80100be0:	e8 7b fa ff ff       	call   80100660 <cprintf>
    return -1;
80100be5:	83 c4 10             	add    $0x10,%esp
80100be8:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80100bed:	e9 8a fe ff ff       	jmp    80100a7c <exec+0x6c>
  clearpteu(pgdir, (char*)(sz - 2*PGSIZE));
80100bf2:	8d 80 00 e0 ff ff    	lea    -0x2000(%eax),%eax
80100bf8:	83 ec 08             	sub    $0x8,%esp
  for(argc = 0; argv[argc]; argc++) {
80100bfb:	31 ff                	xor    %edi,%edi
80100bfd:	89 f3                	mov    %esi,%ebx
  clearpteu(pgdir, (char*)(sz - 2*PGSIZE));
80100bff:	50                   	push   %eax
80100c00:	ff b5 f0 fe ff ff    	pushl  -0x110(%ebp)
80100c06:	e8 85 64 00 00       	call   80107090 <clearpteu>
  for(argc = 0; argv[argc]; argc++) {
80100c0b:	8b 45 0c             	mov    0xc(%ebp),%eax
80100c0e:	83 c4 10             	add    $0x10,%esp
80100c11:	8d 95 58 ff ff ff    	lea    -0xa8(%ebp),%edx
80100c17:	8b 00                	mov    (%eax),%eax
80100c19:	85 c0                	test   %eax,%eax
80100c1b:	74 70                	je     80100c8d <exec+0x27d>
80100c1d:	89 b5 ec fe ff ff    	mov    %esi,-0x114(%ebp)
80100c23:	8b b5 f0 fe ff ff    	mov    -0x110(%ebp),%esi
80100c29:	eb 0a                	jmp    80100c35 <exec+0x225>
80100c2b:	90                   	nop
80100c2c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    if(argc >= MAXARG)
80100c30:	83 ff 20             	cmp    $0x20,%edi
80100c33:	74 83                	je     80100bb8 <exec+0x1a8>
    sp = (sp - (strlen(argv[argc]) + 1)) & ~3;
80100c35:	83 ec 0c             	sub    $0xc,%esp
80100c38:	50                   	push   %eax
80100c39:	e8 c2 3e 00 00       	call   80104b00 <strlen>
80100c3e:	f7 d0                	not    %eax
80100c40:	01 c3                	add    %eax,%ebx
    if(copyout(pgdir, sp, argv[argc], strlen(argv[argc]) + 1) < 0)
80100c42:	8b 45 0c             	mov    0xc(%ebp),%eax
80100c45:	5a                   	pop    %edx
    sp = (sp - (strlen(argv[argc]) + 1)) & ~3;
80100c46:	83 e3 fc             	and    $0xfffffffc,%ebx
    if(copyout(pgdir, sp, argv[argc], strlen(argv[argc]) + 1) < 0)
80100c49:	ff 34 b8             	pushl  (%eax,%edi,4)
80100c4c:	e8 af 3e 00 00       	call   80104b00 <strlen>
80100c51:	83 c0 01             	add    $0x1,%eax
80100c54:	50                   	push   %eax
80100c55:	8b 45 0c             	mov    0xc(%ebp),%eax
80100c58:	ff 34 b8             	pushl  (%eax,%edi,4)
80100c5b:	53                   	push   %ebx
80100c5c:	56                   	push   %esi
80100c5d:	e8 8e 65 00 00       	call   801071f0 <copyout>
80100c62:	83 c4 20             	add    $0x20,%esp
80100c65:	85 c0                	test   %eax,%eax
80100c67:	0f 88 4b ff ff ff    	js     80100bb8 <exec+0x1a8>
  for(argc = 0; argv[argc]; argc++) {
80100c6d:	8b 45 0c             	mov    0xc(%ebp),%eax
    ustack[3+argc] = sp;
80100c70:	89 9c bd 64 ff ff ff 	mov    %ebx,-0x9c(%ebp,%edi,4)
  for(argc = 0; argv[argc]; argc++) {
80100c77:	83 c7 01             	add    $0x1,%edi
    ustack[3+argc] = sp;
80100c7a:	8d 95 58 ff ff ff    	lea    -0xa8(%ebp),%edx
  for(argc = 0; argv[argc]; argc++) {
80100c80:	8b 04 b8             	mov    (%eax,%edi,4),%eax
80100c83:	85 c0                	test   %eax,%eax
80100c85:	75 a9                	jne    80100c30 <exec+0x220>
80100c87:	8b b5 ec fe ff ff    	mov    -0x114(%ebp),%esi
  ustack[2] = sp - (argc+1)*4;  // argv pointer
80100c8d:	8d 04 bd 04 00 00 00 	lea    0x4(,%edi,4),%eax
80100c94:	89 d9                	mov    %ebx,%ecx
  ustack[3+argc] = 0;
80100c96:	c7 84 bd 64 ff ff ff 	movl   $0x0,-0x9c(%ebp,%edi,4)
80100c9d:	00 00 00 00 
  ustack[0] = 0xffffffff;  // fake return PC
80100ca1:	c7 85 58 ff ff ff ff 	movl   $0xffffffff,-0xa8(%ebp)
80100ca8:	ff ff ff 
  ustack[1] = argc;
80100cab:	89 bd 5c ff ff ff    	mov    %edi,-0xa4(%ebp)
  ustack[2] = sp - (argc+1)*4;  // argv pointer
80100cb1:	29 c1                	sub    %eax,%ecx
  sp -= (3+argc+1) * 4;
80100cb3:	83 c0 0c             	add    $0xc,%eax
80100cb6:	29 c3                	sub    %eax,%ebx
  if(copyout(pgdir, sp, ustack, (3+argc+1)*4) < 0)
80100cb8:	50                   	push   %eax
80100cb9:	52                   	push   %edx
80100cba:	53                   	push   %ebx
80100cbb:	ff b5 f0 fe ff ff    	pushl  -0x110(%ebp)
  ustack[2] = sp - (argc+1)*4;  // argv pointer
80100cc1:	89 8d 60 ff ff ff    	mov    %ecx,-0xa0(%ebp)
  if(copyout(pgdir, sp, ustack, (3+argc+1)*4) < 0)
80100cc7:	e8 24 65 00 00       	call   801071f0 <copyout>
80100ccc:	83 c4 10             	add    $0x10,%esp
80100ccf:	85 c0                	test   %eax,%eax
80100cd1:	0f 88 e1 fe ff ff    	js     80100bb8 <exec+0x1a8>
  for(last=s=path; *s; s++)
80100cd7:	8b 45 08             	mov    0x8(%ebp),%eax
80100cda:	0f b6 00             	movzbl (%eax),%eax
80100cdd:	84 c0                	test   %al,%al
80100cdf:	74 17                	je     80100cf8 <exec+0x2e8>
80100ce1:	8b 55 08             	mov    0x8(%ebp),%edx
80100ce4:	89 d1                	mov    %edx,%ecx
80100ce6:	83 c1 01             	add    $0x1,%ecx
80100ce9:	3c 2f                	cmp    $0x2f,%al
80100ceb:	0f b6 01             	movzbl (%ecx),%eax
80100cee:	0f 44 d1             	cmove  %ecx,%edx
80100cf1:	84 c0                	test   %al,%al
80100cf3:	75 f1                	jne    80100ce6 <exec+0x2d6>
80100cf5:	89 55 08             	mov    %edx,0x8(%ebp)
  safestrcpy(curproc->name, last, sizeof(curproc->name));
80100cf8:	8b bd f4 fe ff ff    	mov    -0x10c(%ebp),%edi
80100cfe:	50                   	push   %eax
80100cff:	6a 10                	push   $0x10
80100d01:	ff 75 08             	pushl  0x8(%ebp)
80100d04:	89 f8                	mov    %edi,%eax
80100d06:	83 c0 6c             	add    $0x6c,%eax
80100d09:	50                   	push   %eax
80100d0a:	e8 b1 3d 00 00       	call   80104ac0 <safestrcpy>
  curproc->pgdir = pgdir;
80100d0f:	8b 95 f0 fe ff ff    	mov    -0x110(%ebp),%edx
  oldpgdir = curproc->pgdir;
80100d15:	89 f9                	mov    %edi,%ecx
80100d17:	8b 7f 04             	mov    0x4(%edi),%edi
  curproc->tf->eip = elf.entry;  // main
80100d1a:	8b 41 18             	mov    0x18(%ecx),%eax
  curproc->sz = sz;
80100d1d:	89 31                	mov    %esi,(%ecx)
  curproc->pgdir = pgdir;
80100d1f:	89 51 04             	mov    %edx,0x4(%ecx)
  curproc->tf->eip = elf.entry;  // main
80100d22:	8b 95 3c ff ff ff    	mov    -0xc4(%ebp),%edx
80100d28:	89 50 38             	mov    %edx,0x38(%eax)
  curproc->tf->esp = sp;
80100d2b:	8b 41 18             	mov    0x18(%ecx),%eax
80100d2e:	89 58 44             	mov    %ebx,0x44(%eax)
  switchuvm(curproc);
80100d31:	89 0c 24             	mov    %ecx,(%esp)
80100d34:	e8 e7 5e 00 00       	call   80106c20 <switchuvm>
  freevm(oldpgdir);
80100d39:	89 3c 24             	mov    %edi,(%esp)
80100d3c:	e8 2f 62 00 00       	call   80106f70 <freevm>
  return 0;
80100d41:	83 c4 10             	add    $0x10,%esp
80100d44:	31 c0                	xor    %eax,%eax
80100d46:	e9 31 fd ff ff       	jmp    80100a7c <exec+0x6c>
  for(i=0, off=elf.phoff; i<elf.phnum; i++, off+=sizeof(ph)){
80100d4b:	be 00 20 00 00       	mov    $0x2000,%esi
80100d50:	e9 3c fe ff ff       	jmp    80100b91 <exec+0x181>
80100d55:	66 90                	xchg   %ax,%ax
80100d57:	66 90                	xchg   %ax,%ax
80100d59:	66 90                	xchg   %ax,%ax
80100d5b:	66 90                	xchg   %ax,%ax
80100d5d:	66 90                	xchg   %ax,%ax
80100d5f:	90                   	nop

80100d60 <fileinit>:
  struct file file[NFILE];
} ftable;

void
fileinit(void)
{
80100d60:	55                   	push   %ebp
80100d61:	89 e5                	mov    %esp,%ebp
80100d63:	83 ec 10             	sub    $0x10,%esp
  initlock(&ftable.lock, "ftable");
80100d66:	68 cd 77 10 80       	push   $0x801077cd
80100d6b:	68 c0 0f 11 80       	push   $0x80110fc0
80100d70:	e8 1b 39 00 00       	call   80104690 <initlock>
}
80100d75:	83 c4 10             	add    $0x10,%esp
80100d78:	c9                   	leave  
80100d79:	c3                   	ret    
80100d7a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi

80100d80 <filealloc>:

// Allocate a file structure.
struct file*
filealloc(void)
{
80100d80:	55                   	push   %ebp
80100d81:	89 e5                	mov    %esp,%ebp
80100d83:	53                   	push   %ebx
  struct file *f;

  acquire(&ftable.lock);
  for(f = ftable.file; f < ftable.file + NFILE; f++){
80100d84:	bb f4 0f 11 80       	mov    $0x80110ff4,%ebx
{
80100d89:	83 ec 10             	sub    $0x10,%esp
  acquire(&ftable.lock);
80100d8c:	68 c0 0f 11 80       	push   $0x80110fc0
80100d91:	e8 3a 3a 00 00       	call   801047d0 <acquire>
80100d96:	83 c4 10             	add    $0x10,%esp
80100d99:	eb 10                	jmp    80100dab <filealloc+0x2b>
80100d9b:	90                   	nop
80100d9c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  for(f = ftable.file; f < ftable.file + NFILE; f++){
80100da0:	83 c3 18             	add    $0x18,%ebx
80100da3:	81 fb 54 19 11 80    	cmp    $0x80111954,%ebx
80100da9:	73 25                	jae    80100dd0 <filealloc+0x50>
    if(f->ref == 0){
80100dab:	8b 43 04             	mov    0x4(%ebx),%eax
80100dae:	85 c0                	test   %eax,%eax
80100db0:	75 ee                	jne    80100da0 <filealloc+0x20>
      f->ref = 1;
      release(&ftable.lock);
80100db2:	83 ec 0c             	sub    $0xc,%esp
      f->ref = 1;
80100db5:	c7 43 04 01 00 00 00 	movl   $0x1,0x4(%ebx)
      release(&ftable.lock);
80100dbc:	68 c0 0f 11 80       	push   $0x80110fc0
80100dc1:	e8 ca 3a 00 00       	call   80104890 <release>
      return f;
    }
  }
  release(&ftable.lock);
  return 0;
}
80100dc6:	89 d8                	mov    %ebx,%eax
      return f;
80100dc8:	83 c4 10             	add    $0x10,%esp
}
80100dcb:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80100dce:	c9                   	leave  
80100dcf:	c3                   	ret    
  release(&ftable.lock);
80100dd0:	83 ec 0c             	sub    $0xc,%esp
  return 0;
80100dd3:	31 db                	xor    %ebx,%ebx
  release(&ftable.lock);
80100dd5:	68 c0 0f 11 80       	push   $0x80110fc0
80100dda:	e8 b1 3a 00 00       	call   80104890 <release>
}
80100ddf:	89 d8                	mov    %ebx,%eax
  return 0;
80100de1:	83 c4 10             	add    $0x10,%esp
}
80100de4:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80100de7:	c9                   	leave  
80100de8:	c3                   	ret    
80100de9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

80100df0 <filedup>:

// Increment ref count for file f.
struct file*
filedup(struct file *f)
{
80100df0:	55                   	push   %ebp
80100df1:	89 e5                	mov    %esp,%ebp
80100df3:	53                   	push   %ebx
80100df4:	83 ec 10             	sub    $0x10,%esp
80100df7:	8b 5d 08             	mov    0x8(%ebp),%ebx
  acquire(&ftable.lock);
80100dfa:	68 c0 0f 11 80       	push   $0x80110fc0
80100dff:	e8 cc 39 00 00       	call   801047d0 <acquire>
  if(f->ref < 1)
80100e04:	8b 43 04             	mov    0x4(%ebx),%eax
80100e07:	83 c4 10             	add    $0x10,%esp
80100e0a:	85 c0                	test   %eax,%eax
80100e0c:	7e 1a                	jle    80100e28 <filedup+0x38>
    panic("filedup");
  f->ref++;
80100e0e:	83 c0 01             	add    $0x1,%eax
  release(&ftable.lock);
80100e11:	83 ec 0c             	sub    $0xc,%esp
  f->ref++;
80100e14:	89 43 04             	mov    %eax,0x4(%ebx)
  release(&ftable.lock);
80100e17:	68 c0 0f 11 80       	push   $0x80110fc0
80100e1c:	e8 6f 3a 00 00       	call   80104890 <release>
  return f;
}
80100e21:	89 d8                	mov    %ebx,%eax
80100e23:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80100e26:	c9                   	leave  
80100e27:	c3                   	ret    
    panic("filedup");
80100e28:	83 ec 0c             	sub    $0xc,%esp
80100e2b:	68 d4 77 10 80       	push   $0x801077d4
80100e30:	e8 5b f5 ff ff       	call   80100390 <panic>
80100e35:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80100e39:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80100e40 <fileclose>:

// Close file f.  (Decrement ref count, close when reaches 0.)
void
fileclose(struct file *f)
{
80100e40:	55                   	push   %ebp
80100e41:	89 e5                	mov    %esp,%ebp
80100e43:	57                   	push   %edi
80100e44:	56                   	push   %esi
80100e45:	53                   	push   %ebx
80100e46:	83 ec 28             	sub    $0x28,%esp
80100e49:	8b 5d 08             	mov    0x8(%ebp),%ebx
  struct file ff;

  acquire(&ftable.lock);
80100e4c:	68 c0 0f 11 80       	push   $0x80110fc0
80100e51:	e8 7a 39 00 00       	call   801047d0 <acquire>
  if(f->ref < 1)
80100e56:	8b 43 04             	mov    0x4(%ebx),%eax
80100e59:	83 c4 10             	add    $0x10,%esp
80100e5c:	85 c0                	test   %eax,%eax
80100e5e:	0f 8e 9b 00 00 00    	jle    80100eff <fileclose+0xbf>
    panic("fileclose");
  if(--f->ref > 0){
80100e64:	83 e8 01             	sub    $0x1,%eax
80100e67:	85 c0                	test   %eax,%eax
80100e69:	89 43 04             	mov    %eax,0x4(%ebx)
80100e6c:	74 1a                	je     80100e88 <fileclose+0x48>
    release(&ftable.lock);
80100e6e:	c7 45 08 c0 0f 11 80 	movl   $0x80110fc0,0x8(%ebp)
  else if(ff.type == FD_INODE){
    begin_op();
    iput(ff.ip);
    end_op();
  }
}
80100e75:	8d 65 f4             	lea    -0xc(%ebp),%esp
80100e78:	5b                   	pop    %ebx
80100e79:	5e                   	pop    %esi
80100e7a:	5f                   	pop    %edi
80100e7b:	5d                   	pop    %ebp
    release(&ftable.lock);
80100e7c:	e9 0f 3a 00 00       	jmp    80104890 <release>
80100e81:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
  ff = *f;
80100e88:	0f b6 43 09          	movzbl 0x9(%ebx),%eax
80100e8c:	8b 3b                	mov    (%ebx),%edi
  release(&ftable.lock);
80100e8e:	83 ec 0c             	sub    $0xc,%esp
  ff = *f;
80100e91:	8b 73 0c             	mov    0xc(%ebx),%esi
  f->type = FD_NONE;
80100e94:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  ff = *f;
80100e9a:	88 45 e7             	mov    %al,-0x19(%ebp)
80100e9d:	8b 43 10             	mov    0x10(%ebx),%eax
  release(&ftable.lock);
80100ea0:	68 c0 0f 11 80       	push   $0x80110fc0
  ff = *f;
80100ea5:	89 45 e0             	mov    %eax,-0x20(%ebp)
  release(&ftable.lock);
80100ea8:	e8 e3 39 00 00       	call   80104890 <release>
  if(ff.type == FD_PIPE)
80100ead:	83 c4 10             	add    $0x10,%esp
80100eb0:	83 ff 01             	cmp    $0x1,%edi
80100eb3:	74 13                	je     80100ec8 <fileclose+0x88>
  else if(ff.type == FD_INODE){
80100eb5:	83 ff 02             	cmp    $0x2,%edi
80100eb8:	74 26                	je     80100ee0 <fileclose+0xa0>
}
80100eba:	8d 65 f4             	lea    -0xc(%ebp),%esp
80100ebd:	5b                   	pop    %ebx
80100ebe:	5e                   	pop    %esi
80100ebf:	5f                   	pop    %edi
80100ec0:	5d                   	pop    %ebp
80100ec1:	c3                   	ret    
80100ec2:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    pipeclose(ff.pipe, ff.writable);
80100ec8:	0f be 5d e7          	movsbl -0x19(%ebp),%ebx
80100ecc:	83 ec 08             	sub    $0x8,%esp
80100ecf:	53                   	push   %ebx
80100ed0:	56                   	push   %esi
80100ed1:	e8 7a 28 00 00       	call   80103750 <pipeclose>
80100ed6:	83 c4 10             	add    $0x10,%esp
80100ed9:	eb df                	jmp    80100eba <fileclose+0x7a>
80100edb:	90                   	nop
80100edc:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    begin_op();
80100ee0:	e8 bb 20 00 00       	call   80102fa0 <begin_op>
    iput(ff.ip);
80100ee5:	83 ec 0c             	sub    $0xc,%esp
80100ee8:	ff 75 e0             	pushl  -0x20(%ebp)
80100eeb:	e8 c0 08 00 00       	call   801017b0 <iput>
    end_op();
80100ef0:	83 c4 10             	add    $0x10,%esp
}
80100ef3:	8d 65 f4             	lea    -0xc(%ebp),%esp
80100ef6:	5b                   	pop    %ebx
80100ef7:	5e                   	pop    %esi
80100ef8:	5f                   	pop    %edi
80100ef9:	5d                   	pop    %ebp
    end_op();
80100efa:	e9 11 21 00 00       	jmp    80103010 <end_op>
    panic("fileclose");
80100eff:	83 ec 0c             	sub    $0xc,%esp
80100f02:	68 dc 77 10 80       	push   $0x801077dc
80100f07:	e8 84 f4 ff ff       	call   80100390 <panic>
80100f0c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

80100f10 <filestat>:

// Get metadata about file f.
int
filestat(struct file *f, struct stat *st)
{
80100f10:	55                   	push   %ebp
80100f11:	89 e5                	mov    %esp,%ebp
80100f13:	53                   	push   %ebx
80100f14:	83 ec 04             	sub    $0x4,%esp
80100f17:	8b 5d 08             	mov    0x8(%ebp),%ebx
  if(f->type == FD_INODE){
80100f1a:	83 3b 02             	cmpl   $0x2,(%ebx)
80100f1d:	75 31                	jne    80100f50 <filestat+0x40>
    ilock(f->ip);
80100f1f:	83 ec 0c             	sub    $0xc,%esp
80100f22:	ff 73 10             	pushl  0x10(%ebx)
80100f25:	e8 56 07 00 00       	call   80101680 <ilock>
    stati(f->ip, st);
80100f2a:	58                   	pop    %eax
80100f2b:	5a                   	pop    %edx
80100f2c:	ff 75 0c             	pushl  0xc(%ebp)
80100f2f:	ff 73 10             	pushl  0x10(%ebx)
80100f32:	e8 f9 09 00 00       	call   80101930 <stati>
    iunlock(f->ip);
80100f37:	59                   	pop    %ecx
80100f38:	ff 73 10             	pushl  0x10(%ebx)
80100f3b:	e8 20 08 00 00       	call   80101760 <iunlock>
    return 0;
80100f40:	83 c4 10             	add    $0x10,%esp
80100f43:	31 c0                	xor    %eax,%eax
  }
  return -1;
}
80100f45:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80100f48:	c9                   	leave  
80100f49:	c3                   	ret    
80100f4a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
  return -1;
80100f50:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80100f55:	eb ee                	jmp    80100f45 <filestat+0x35>
80100f57:	89 f6                	mov    %esi,%esi
80100f59:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80100f60 <fileread>:

// Read from file f.
int
fileread(struct file *f, char *addr, int n)
{
80100f60:	55                   	push   %ebp
80100f61:	89 e5                	mov    %esp,%ebp
80100f63:	57                   	push   %edi
80100f64:	56                   	push   %esi
80100f65:	53                   	push   %ebx
80100f66:	83 ec 0c             	sub    $0xc,%esp
80100f69:	8b 5d 08             	mov    0x8(%ebp),%ebx
80100f6c:	8b 75 0c             	mov    0xc(%ebp),%esi
80100f6f:	8b 7d 10             	mov    0x10(%ebp),%edi
  int r;

  if(f->readable == 0)
80100f72:	80 7b 08 00          	cmpb   $0x0,0x8(%ebx)
80100f76:	74 60                	je     80100fd8 <fileread+0x78>
    return -1;
  if(f->type == FD_PIPE)
80100f78:	8b 03                	mov    (%ebx),%eax
80100f7a:	83 f8 01             	cmp    $0x1,%eax
80100f7d:	74 41                	je     80100fc0 <fileread+0x60>
    return piperead(f->pipe, addr, n);
  if(f->type == FD_INODE){
80100f7f:	83 f8 02             	cmp    $0x2,%eax
80100f82:	75 5b                	jne    80100fdf <fileread+0x7f>
    ilock(f->ip);
80100f84:	83 ec 0c             	sub    $0xc,%esp
80100f87:	ff 73 10             	pushl  0x10(%ebx)
80100f8a:	e8 f1 06 00 00       	call   80101680 <ilock>
    if((r = readi(f->ip, addr, f->off, n)) > 0)
80100f8f:	57                   	push   %edi
80100f90:	ff 73 14             	pushl  0x14(%ebx)
80100f93:	56                   	push   %esi
80100f94:	ff 73 10             	pushl  0x10(%ebx)
80100f97:	e8 c4 09 00 00       	call   80101960 <readi>
80100f9c:	83 c4 20             	add    $0x20,%esp
80100f9f:	85 c0                	test   %eax,%eax
80100fa1:	89 c6                	mov    %eax,%esi
80100fa3:	7e 03                	jle    80100fa8 <fileread+0x48>
      f->off += r;
80100fa5:	01 43 14             	add    %eax,0x14(%ebx)
    iunlock(f->ip);
80100fa8:	83 ec 0c             	sub    $0xc,%esp
80100fab:	ff 73 10             	pushl  0x10(%ebx)
80100fae:	e8 ad 07 00 00       	call   80101760 <iunlock>
    return r;
80100fb3:	83 c4 10             	add    $0x10,%esp
  }
  panic("fileread");
}
80100fb6:	8d 65 f4             	lea    -0xc(%ebp),%esp
80100fb9:	89 f0                	mov    %esi,%eax
80100fbb:	5b                   	pop    %ebx
80100fbc:	5e                   	pop    %esi
80100fbd:	5f                   	pop    %edi
80100fbe:	5d                   	pop    %ebp
80100fbf:	c3                   	ret    
    return piperead(f->pipe, addr, n);
80100fc0:	8b 43 0c             	mov    0xc(%ebx),%eax
80100fc3:	89 45 08             	mov    %eax,0x8(%ebp)
}
80100fc6:	8d 65 f4             	lea    -0xc(%ebp),%esp
80100fc9:	5b                   	pop    %ebx
80100fca:	5e                   	pop    %esi
80100fcb:	5f                   	pop    %edi
80100fcc:	5d                   	pop    %ebp
    return piperead(f->pipe, addr, n);
80100fcd:	e9 2e 29 00 00       	jmp    80103900 <piperead>
80100fd2:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    return -1;
80100fd8:	be ff ff ff ff       	mov    $0xffffffff,%esi
80100fdd:	eb d7                	jmp    80100fb6 <fileread+0x56>
  panic("fileread");
80100fdf:	83 ec 0c             	sub    $0xc,%esp
80100fe2:	68 e6 77 10 80       	push   $0x801077e6
80100fe7:	e8 a4 f3 ff ff       	call   80100390 <panic>
80100fec:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

80100ff0 <filewrite>:

//PAGEBREAK!
// Write to file f.
int
filewrite(struct file *f, char *addr, int n)
{
80100ff0:	55                   	push   %ebp
80100ff1:	89 e5                	mov    %esp,%ebp
80100ff3:	57                   	push   %edi
80100ff4:	56                   	push   %esi
80100ff5:	53                   	push   %ebx
80100ff6:	83 ec 1c             	sub    $0x1c,%esp
80100ff9:	8b 75 08             	mov    0x8(%ebp),%esi
80100ffc:	8b 45 0c             	mov    0xc(%ebp),%eax
  int r;

  if(f->writable == 0)
80100fff:	80 7e 09 00          	cmpb   $0x0,0x9(%esi)
{
80101003:	89 45 dc             	mov    %eax,-0x24(%ebp)
80101006:	8b 45 10             	mov    0x10(%ebp),%eax
80101009:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  if(f->writable == 0)
8010100c:	0f 84 aa 00 00 00    	je     801010bc <filewrite+0xcc>
    return -1;
  if(f->type == FD_PIPE)
80101012:	8b 06                	mov    (%esi),%eax
80101014:	83 f8 01             	cmp    $0x1,%eax
80101017:	0f 84 c3 00 00 00    	je     801010e0 <filewrite+0xf0>
    return pipewrite(f->pipe, addr, n);
  if(f->type == FD_INODE){
8010101d:	83 f8 02             	cmp    $0x2,%eax
80101020:	0f 85 d9 00 00 00    	jne    801010ff <filewrite+0x10f>
    // and 2 blocks of slop for non-aligned writes.
    // this really belongs lower down, since writei()
    // might be writing a device like the console.
    int max = ((MAXOPBLOCKS-1-1-2) / 2) * 512;
    int i = 0;
    while(i < n){
80101026:	8b 45 e4             	mov    -0x1c(%ebp),%eax
    int i = 0;
80101029:	31 ff                	xor    %edi,%edi
    while(i < n){
8010102b:	85 c0                	test   %eax,%eax
8010102d:	7f 34                	jg     80101063 <filewrite+0x73>
8010102f:	e9 9c 00 00 00       	jmp    801010d0 <filewrite+0xe0>
80101034:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
        n1 = max;

      begin_op();
      ilock(f->ip);
      if ((r = writei(f->ip, addr + i, f->off, n1)) > 0)
        f->off += r;
80101038:	01 46 14             	add    %eax,0x14(%esi)
      iunlock(f->ip);
8010103b:	83 ec 0c             	sub    $0xc,%esp
8010103e:	ff 76 10             	pushl  0x10(%esi)
        f->off += r;
80101041:	89 45 e0             	mov    %eax,-0x20(%ebp)
      iunlock(f->ip);
80101044:	e8 17 07 00 00       	call   80101760 <iunlock>
      end_op();
80101049:	e8 c2 1f 00 00       	call   80103010 <end_op>
8010104e:	8b 45 e0             	mov    -0x20(%ebp),%eax
80101051:	83 c4 10             	add    $0x10,%esp

      if(r < 0)
        break;
      if(r != n1)
80101054:	39 c3                	cmp    %eax,%ebx
80101056:	0f 85 96 00 00 00    	jne    801010f2 <filewrite+0x102>
        panic("short filewrite");
      i += r;
8010105c:	01 df                	add    %ebx,%edi
    while(i < n){
8010105e:	39 7d e4             	cmp    %edi,-0x1c(%ebp)
80101061:	7e 6d                	jle    801010d0 <filewrite+0xe0>
      int n1 = n - i;
80101063:	8b 5d e4             	mov    -0x1c(%ebp),%ebx
80101066:	b8 00 06 00 00       	mov    $0x600,%eax
8010106b:	29 fb                	sub    %edi,%ebx
8010106d:	81 fb 00 06 00 00    	cmp    $0x600,%ebx
80101073:	0f 4f d8             	cmovg  %eax,%ebx
      begin_op();
80101076:	e8 25 1f 00 00       	call   80102fa0 <begin_op>
      ilock(f->ip);
8010107b:	83 ec 0c             	sub    $0xc,%esp
8010107e:	ff 76 10             	pushl  0x10(%esi)
80101081:	e8 fa 05 00 00       	call   80101680 <ilock>
      if ((r = writei(f->ip, addr + i, f->off, n1)) > 0)
80101086:	8b 45 dc             	mov    -0x24(%ebp),%eax
80101089:	53                   	push   %ebx
8010108a:	ff 76 14             	pushl  0x14(%esi)
8010108d:	01 f8                	add    %edi,%eax
8010108f:	50                   	push   %eax
80101090:	ff 76 10             	pushl  0x10(%esi)
80101093:	e8 c8 09 00 00       	call   80101a60 <writei>
80101098:	83 c4 20             	add    $0x20,%esp
8010109b:	85 c0                	test   %eax,%eax
8010109d:	7f 99                	jg     80101038 <filewrite+0x48>
      iunlock(f->ip);
8010109f:	83 ec 0c             	sub    $0xc,%esp
801010a2:	ff 76 10             	pushl  0x10(%esi)
801010a5:	89 45 e0             	mov    %eax,-0x20(%ebp)
801010a8:	e8 b3 06 00 00       	call   80101760 <iunlock>
      end_op();
801010ad:	e8 5e 1f 00 00       	call   80103010 <end_op>
      if(r < 0)
801010b2:	8b 45 e0             	mov    -0x20(%ebp),%eax
801010b5:	83 c4 10             	add    $0x10,%esp
801010b8:	85 c0                	test   %eax,%eax
801010ba:	74 98                	je     80101054 <filewrite+0x64>
    }
    return i == n ? n : -1;
  }
  panic("filewrite");
}
801010bc:	8d 65 f4             	lea    -0xc(%ebp),%esp
    return -1;
801010bf:	bf ff ff ff ff       	mov    $0xffffffff,%edi
}
801010c4:	89 f8                	mov    %edi,%eax
801010c6:	5b                   	pop    %ebx
801010c7:	5e                   	pop    %esi
801010c8:	5f                   	pop    %edi
801010c9:	5d                   	pop    %ebp
801010ca:	c3                   	ret    
801010cb:	90                   	nop
801010cc:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    return i == n ? n : -1;
801010d0:	39 7d e4             	cmp    %edi,-0x1c(%ebp)
801010d3:	75 e7                	jne    801010bc <filewrite+0xcc>
}
801010d5:	8d 65 f4             	lea    -0xc(%ebp),%esp
801010d8:	89 f8                	mov    %edi,%eax
801010da:	5b                   	pop    %ebx
801010db:	5e                   	pop    %esi
801010dc:	5f                   	pop    %edi
801010dd:	5d                   	pop    %ebp
801010de:	c3                   	ret    
801010df:	90                   	nop
    return pipewrite(f->pipe, addr, n);
801010e0:	8b 46 0c             	mov    0xc(%esi),%eax
801010e3:	89 45 08             	mov    %eax,0x8(%ebp)
}
801010e6:	8d 65 f4             	lea    -0xc(%ebp),%esp
801010e9:	5b                   	pop    %ebx
801010ea:	5e                   	pop    %esi
801010eb:	5f                   	pop    %edi
801010ec:	5d                   	pop    %ebp
    return pipewrite(f->pipe, addr, n);
801010ed:	e9 fe 26 00 00       	jmp    801037f0 <pipewrite>
        panic("short filewrite");
801010f2:	83 ec 0c             	sub    $0xc,%esp
801010f5:	68 ef 77 10 80       	push   $0x801077ef
801010fa:	e8 91 f2 ff ff       	call   80100390 <panic>
  panic("filewrite");
801010ff:	83 ec 0c             	sub    $0xc,%esp
80101102:	68 f5 77 10 80       	push   $0x801077f5
80101107:	e8 84 f2 ff ff       	call   80100390 <panic>
8010110c:	66 90                	xchg   %ax,%ax
8010110e:	66 90                	xchg   %ax,%ax

80101110 <bfree>:
}

// Free a disk block.
static void
bfree(int dev, uint b)
{
80101110:	55                   	push   %ebp
80101111:	89 e5                	mov    %esp,%ebp
80101113:	56                   	push   %esi
80101114:	53                   	push   %ebx
80101115:	89 d3                	mov    %edx,%ebx
  struct buf *bp;
  int bi, m;

  bp = bread(dev, BBLOCK(b, sb));
80101117:	c1 ea 0c             	shr    $0xc,%edx
8010111a:	03 15 d8 19 11 80    	add    0x801119d8,%edx
80101120:	83 ec 08             	sub    $0x8,%esp
80101123:	52                   	push   %edx
80101124:	50                   	push   %eax
80101125:	e8 a6 ef ff ff       	call   801000d0 <bread>
  bi = b % BPB;
  m = 1 << (bi % 8);
8010112a:	89 d9                	mov    %ebx,%ecx
  if((bp->data[bi/8] & m) == 0)
8010112c:	c1 fb 03             	sar    $0x3,%ebx
  m = 1 << (bi % 8);
8010112f:	ba 01 00 00 00       	mov    $0x1,%edx
80101134:	83 e1 07             	and    $0x7,%ecx
  if((bp->data[bi/8] & m) == 0)
80101137:	81 e3 ff 01 00 00    	and    $0x1ff,%ebx
8010113d:	83 c4 10             	add    $0x10,%esp
  m = 1 << (bi % 8);
80101140:	d3 e2                	shl    %cl,%edx
  if((bp->data[bi/8] & m) == 0)
80101142:	0f b6 4c 18 5c       	movzbl 0x5c(%eax,%ebx,1),%ecx
80101147:	85 d1                	test   %edx,%ecx
80101149:	74 25                	je     80101170 <bfree+0x60>
    panic("freeing free block");
  bp->data[bi/8] &= ~m;
8010114b:	f7 d2                	not    %edx
8010114d:	89 c6                	mov    %eax,%esi
  log_write(bp);
8010114f:	83 ec 0c             	sub    $0xc,%esp
  bp->data[bi/8] &= ~m;
80101152:	21 ca                	and    %ecx,%edx
80101154:	88 54 1e 5c          	mov    %dl,0x5c(%esi,%ebx,1)
  log_write(bp);
80101158:	56                   	push   %esi
80101159:	e8 12 20 00 00       	call   80103170 <log_write>
  brelse(bp);
8010115e:	89 34 24             	mov    %esi,(%esp)
80101161:	e8 7a f0 ff ff       	call   801001e0 <brelse>
}
80101166:	83 c4 10             	add    $0x10,%esp
80101169:	8d 65 f8             	lea    -0x8(%ebp),%esp
8010116c:	5b                   	pop    %ebx
8010116d:	5e                   	pop    %esi
8010116e:	5d                   	pop    %ebp
8010116f:	c3                   	ret    
    panic("freeing free block");
80101170:	83 ec 0c             	sub    $0xc,%esp
80101173:	68 ff 77 10 80       	push   $0x801077ff
80101178:	e8 13 f2 ff ff       	call   80100390 <panic>
8010117d:	8d 76 00             	lea    0x0(%esi),%esi

80101180 <balloc>:
{
80101180:	55                   	push   %ebp
80101181:	89 e5                	mov    %esp,%ebp
80101183:	57                   	push   %edi
80101184:	56                   	push   %esi
80101185:	53                   	push   %ebx
80101186:	83 ec 1c             	sub    $0x1c,%esp
  for(b = 0; b < sb.size; b += BPB){
80101189:	8b 0d c0 19 11 80    	mov    0x801119c0,%ecx
{
8010118f:	89 45 d8             	mov    %eax,-0x28(%ebp)
  for(b = 0; b < sb.size; b += BPB){
80101192:	85 c9                	test   %ecx,%ecx
80101194:	0f 84 87 00 00 00    	je     80101221 <balloc+0xa1>
8010119a:	c7 45 dc 00 00 00 00 	movl   $0x0,-0x24(%ebp)
    bp = bread(dev, BBLOCK(b, sb));
801011a1:	8b 75 dc             	mov    -0x24(%ebp),%esi
801011a4:	83 ec 08             	sub    $0x8,%esp
801011a7:	89 f0                	mov    %esi,%eax
801011a9:	c1 f8 0c             	sar    $0xc,%eax
801011ac:	03 05 d8 19 11 80    	add    0x801119d8,%eax
801011b2:	50                   	push   %eax
801011b3:	ff 75 d8             	pushl  -0x28(%ebp)
801011b6:	e8 15 ef ff ff       	call   801000d0 <bread>
801011bb:	89 45 e4             	mov    %eax,-0x1c(%ebp)
    for(bi = 0; bi < BPB && b + bi < sb.size; bi++){
801011be:	a1 c0 19 11 80       	mov    0x801119c0,%eax
801011c3:	83 c4 10             	add    $0x10,%esp
801011c6:	89 45 e0             	mov    %eax,-0x20(%ebp)
801011c9:	31 c0                	xor    %eax,%eax
801011cb:	eb 2f                	jmp    801011fc <balloc+0x7c>
801011cd:	8d 76 00             	lea    0x0(%esi),%esi
      m = 1 << (bi % 8);
801011d0:	89 c1                	mov    %eax,%ecx
      if((bp->data[bi/8] & m) == 0){  // Is block free?
801011d2:	8b 55 e4             	mov    -0x1c(%ebp),%edx
      m = 1 << (bi % 8);
801011d5:	bb 01 00 00 00       	mov    $0x1,%ebx
801011da:	83 e1 07             	and    $0x7,%ecx
801011dd:	d3 e3                	shl    %cl,%ebx
      if((bp->data[bi/8] & m) == 0){  // Is block free?
801011df:	89 c1                	mov    %eax,%ecx
801011e1:	c1 f9 03             	sar    $0x3,%ecx
801011e4:	0f b6 7c 0a 5c       	movzbl 0x5c(%edx,%ecx,1),%edi
801011e9:	85 df                	test   %ebx,%edi
801011eb:	89 fa                	mov    %edi,%edx
801011ed:	74 41                	je     80101230 <balloc+0xb0>
    for(bi = 0; bi < BPB && b + bi < sb.size; bi++){
801011ef:	83 c0 01             	add    $0x1,%eax
801011f2:	83 c6 01             	add    $0x1,%esi
801011f5:	3d 00 10 00 00       	cmp    $0x1000,%eax
801011fa:	74 05                	je     80101201 <balloc+0x81>
801011fc:	39 75 e0             	cmp    %esi,-0x20(%ebp)
801011ff:	77 cf                	ja     801011d0 <balloc+0x50>
    brelse(bp);
80101201:	83 ec 0c             	sub    $0xc,%esp
80101204:	ff 75 e4             	pushl  -0x1c(%ebp)
80101207:	e8 d4 ef ff ff       	call   801001e0 <brelse>
  for(b = 0; b < sb.size; b += BPB){
8010120c:	81 45 dc 00 10 00 00 	addl   $0x1000,-0x24(%ebp)
80101213:	83 c4 10             	add    $0x10,%esp
80101216:	8b 45 dc             	mov    -0x24(%ebp),%eax
80101219:	39 05 c0 19 11 80    	cmp    %eax,0x801119c0
8010121f:	77 80                	ja     801011a1 <balloc+0x21>
  panic("balloc: out of blocks");
80101221:	83 ec 0c             	sub    $0xc,%esp
80101224:	68 12 78 10 80       	push   $0x80107812
80101229:	e8 62 f1 ff ff       	call   80100390 <panic>
8010122e:	66 90                	xchg   %ax,%ax
        bp->data[bi/8] |= m;  // Mark block in use.
80101230:	8b 7d e4             	mov    -0x1c(%ebp),%edi
        log_write(bp);
80101233:	83 ec 0c             	sub    $0xc,%esp
        bp->data[bi/8] |= m;  // Mark block in use.
80101236:	09 da                	or     %ebx,%edx
80101238:	88 54 0f 5c          	mov    %dl,0x5c(%edi,%ecx,1)
        log_write(bp);
8010123c:	57                   	push   %edi
8010123d:	e8 2e 1f 00 00       	call   80103170 <log_write>
        brelse(bp);
80101242:	89 3c 24             	mov    %edi,(%esp)
80101245:	e8 96 ef ff ff       	call   801001e0 <brelse>
  bp = bread(dev, bno);
8010124a:	58                   	pop    %eax
8010124b:	5a                   	pop    %edx
8010124c:	56                   	push   %esi
8010124d:	ff 75 d8             	pushl  -0x28(%ebp)
80101250:	e8 7b ee ff ff       	call   801000d0 <bread>
80101255:	89 c3                	mov    %eax,%ebx
  memset(bp->data, 0, BSIZE);
80101257:	8d 40 5c             	lea    0x5c(%eax),%eax
8010125a:	83 c4 0c             	add    $0xc,%esp
8010125d:	68 00 02 00 00       	push   $0x200
80101262:	6a 00                	push   $0x0
80101264:	50                   	push   %eax
80101265:	e8 76 36 00 00       	call   801048e0 <memset>
  log_write(bp);
8010126a:	89 1c 24             	mov    %ebx,(%esp)
8010126d:	e8 fe 1e 00 00       	call   80103170 <log_write>
  brelse(bp);
80101272:	89 1c 24             	mov    %ebx,(%esp)
80101275:	e8 66 ef ff ff       	call   801001e0 <brelse>
}
8010127a:	8d 65 f4             	lea    -0xc(%ebp),%esp
8010127d:	89 f0                	mov    %esi,%eax
8010127f:	5b                   	pop    %ebx
80101280:	5e                   	pop    %esi
80101281:	5f                   	pop    %edi
80101282:	5d                   	pop    %ebp
80101283:	c3                   	ret    
80101284:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
8010128a:	8d bf 00 00 00 00    	lea    0x0(%edi),%edi

80101290 <iget>:
// Find the inode with number inum on device dev
// and return the in-memory copy. Does not lock
// the inode and does not read it from disk.
static struct inode*
iget(uint dev, uint inum)
{
80101290:	55                   	push   %ebp
80101291:	89 e5                	mov    %esp,%ebp
80101293:	57                   	push   %edi
80101294:	56                   	push   %esi
80101295:	53                   	push   %ebx
80101296:	89 c7                	mov    %eax,%edi
  struct inode *ip, *empty;

  acquire(&icache.lock);

  // Is the inode already cached?
  empty = 0;
80101298:	31 f6                	xor    %esi,%esi
  for(ip = &icache.inode[0]; ip < &icache.inode[NINODE]; ip++){
8010129a:	bb 14 1a 11 80       	mov    $0x80111a14,%ebx
{
8010129f:	83 ec 28             	sub    $0x28,%esp
801012a2:	89 55 e4             	mov    %edx,-0x1c(%ebp)
  acquire(&icache.lock);
801012a5:	68 e0 19 11 80       	push   $0x801119e0
801012aa:	e8 21 35 00 00       	call   801047d0 <acquire>
801012af:	83 c4 10             	add    $0x10,%esp
  for(ip = &icache.inode[0]; ip < &icache.inode[NINODE]; ip++){
801012b2:	8b 55 e4             	mov    -0x1c(%ebp),%edx
801012b5:	eb 17                	jmp    801012ce <iget+0x3e>
801012b7:	89 f6                	mov    %esi,%esi
801012b9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi
801012c0:	81 c3 90 00 00 00    	add    $0x90,%ebx
801012c6:	81 fb 34 36 11 80    	cmp    $0x80113634,%ebx
801012cc:	73 22                	jae    801012f0 <iget+0x60>
    if(ip->ref > 0 && ip->dev == dev && ip->inum == inum){
801012ce:	8b 4b 08             	mov    0x8(%ebx),%ecx
801012d1:	85 c9                	test   %ecx,%ecx
801012d3:	7e 04                	jle    801012d9 <iget+0x49>
801012d5:	39 3b                	cmp    %edi,(%ebx)
801012d7:	74 4f                	je     80101328 <iget+0x98>
      ip->ref++;
      release(&icache.lock);
      return ip;
    }
    if(empty == 0 && ip->ref == 0)    // Remember empty slot.
801012d9:	85 f6                	test   %esi,%esi
801012db:	75 e3                	jne    801012c0 <iget+0x30>
801012dd:	85 c9                	test   %ecx,%ecx
801012df:	0f 44 f3             	cmove  %ebx,%esi
  for(ip = &icache.inode[0]; ip < &icache.inode[NINODE]; ip++){
801012e2:	81 c3 90 00 00 00    	add    $0x90,%ebx
801012e8:	81 fb 34 36 11 80    	cmp    $0x80113634,%ebx
801012ee:	72 de                	jb     801012ce <iget+0x3e>
      empty = ip;
  }

  // Recycle an inode cache entry.
  if(empty == 0)
801012f0:	85 f6                	test   %esi,%esi
801012f2:	74 5b                	je     8010134f <iget+0xbf>
  ip = empty;
  ip->dev = dev;
  ip->inum = inum;
  ip->ref = 1;
  ip->valid = 0;
  release(&icache.lock);
801012f4:	83 ec 0c             	sub    $0xc,%esp
  ip->dev = dev;
801012f7:	89 3e                	mov    %edi,(%esi)
  ip->inum = inum;
801012f9:	89 56 04             	mov    %edx,0x4(%esi)
  ip->ref = 1;
801012fc:	c7 46 08 01 00 00 00 	movl   $0x1,0x8(%esi)
  ip->valid = 0;
80101303:	c7 46 4c 00 00 00 00 	movl   $0x0,0x4c(%esi)
  release(&icache.lock);
8010130a:	68 e0 19 11 80       	push   $0x801119e0
8010130f:	e8 7c 35 00 00       	call   80104890 <release>

  return ip;
80101314:	83 c4 10             	add    $0x10,%esp
}
80101317:	8d 65 f4             	lea    -0xc(%ebp),%esp
8010131a:	89 f0                	mov    %esi,%eax
8010131c:	5b                   	pop    %ebx
8010131d:	5e                   	pop    %esi
8010131e:	5f                   	pop    %edi
8010131f:	5d                   	pop    %ebp
80101320:	c3                   	ret    
80101321:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    if(ip->ref > 0 && ip->dev == dev && ip->inum == inum){
80101328:	39 53 04             	cmp    %edx,0x4(%ebx)
8010132b:	75 ac                	jne    801012d9 <iget+0x49>
      release(&icache.lock);
8010132d:	83 ec 0c             	sub    $0xc,%esp
      ip->ref++;
80101330:	83 c1 01             	add    $0x1,%ecx
      return ip;
80101333:	89 de                	mov    %ebx,%esi
      release(&icache.lock);
80101335:	68 e0 19 11 80       	push   $0x801119e0
      ip->ref++;
8010133a:	89 4b 08             	mov    %ecx,0x8(%ebx)
      release(&icache.lock);
8010133d:	e8 4e 35 00 00       	call   80104890 <release>
      return ip;
80101342:	83 c4 10             	add    $0x10,%esp
}
80101345:	8d 65 f4             	lea    -0xc(%ebp),%esp
80101348:	89 f0                	mov    %esi,%eax
8010134a:	5b                   	pop    %ebx
8010134b:	5e                   	pop    %esi
8010134c:	5f                   	pop    %edi
8010134d:	5d                   	pop    %ebp
8010134e:	c3                   	ret    
    panic("iget: no inodes");
8010134f:	83 ec 0c             	sub    $0xc,%esp
80101352:	68 28 78 10 80       	push   $0x80107828
80101357:	e8 34 f0 ff ff       	call   80100390 <panic>
8010135c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

80101360 <bmap>:

// Return the disk block address of the nth block in inode ip.
// If there is no such block, bmap allocates one.
static uint
bmap(struct inode *ip, uint bn)
{
80101360:	55                   	push   %ebp
80101361:	89 e5                	mov    %esp,%ebp
80101363:	57                   	push   %edi
80101364:	56                   	push   %esi
80101365:	53                   	push   %ebx
80101366:	89 c6                	mov    %eax,%esi
80101368:	83 ec 1c             	sub    $0x1c,%esp
  uint addr, *a;
  struct buf *bp;

  if(bn < NDIRECT){
8010136b:	83 fa 0b             	cmp    $0xb,%edx
8010136e:	77 18                	ja     80101388 <bmap+0x28>
80101370:	8d 3c 90             	lea    (%eax,%edx,4),%edi
    if((addr = ip->addrs[bn]) == 0)
80101373:	8b 5f 5c             	mov    0x5c(%edi),%ebx
80101376:	85 db                	test   %ebx,%ebx
80101378:	74 76                	je     801013f0 <bmap+0x90>
    brelse(bp);
    return addr;
  }

  panic("bmap: out of range");
}
8010137a:	8d 65 f4             	lea    -0xc(%ebp),%esp
8010137d:	89 d8                	mov    %ebx,%eax
8010137f:	5b                   	pop    %ebx
80101380:	5e                   	pop    %esi
80101381:	5f                   	pop    %edi
80101382:	5d                   	pop    %ebp
80101383:	c3                   	ret    
80101384:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  bn -= NDIRECT;
80101388:	8d 5a f4             	lea    -0xc(%edx),%ebx
  if(bn < NINDIRECT){
8010138b:	83 fb 7f             	cmp    $0x7f,%ebx
8010138e:	0f 87 90 00 00 00    	ja     80101424 <bmap+0xc4>
    if((addr = ip->addrs[NDIRECT]) == 0)
80101394:	8b 90 8c 00 00 00    	mov    0x8c(%eax),%edx
8010139a:	8b 00                	mov    (%eax),%eax
8010139c:	85 d2                	test   %edx,%edx
8010139e:	74 70                	je     80101410 <bmap+0xb0>
    bp = bread(ip->dev, addr);
801013a0:	83 ec 08             	sub    $0x8,%esp
801013a3:	52                   	push   %edx
801013a4:	50                   	push   %eax
801013a5:	e8 26 ed ff ff       	call   801000d0 <bread>
    if((addr = a[bn]) == 0){
801013aa:	8d 54 98 5c          	lea    0x5c(%eax,%ebx,4),%edx
801013ae:	83 c4 10             	add    $0x10,%esp
    bp = bread(ip->dev, addr);
801013b1:	89 c7                	mov    %eax,%edi
    if((addr = a[bn]) == 0){
801013b3:	8b 1a                	mov    (%edx),%ebx
801013b5:	85 db                	test   %ebx,%ebx
801013b7:	75 1d                	jne    801013d6 <bmap+0x76>
      a[bn] = addr = balloc(ip->dev);
801013b9:	8b 06                	mov    (%esi),%eax
801013bb:	89 55 e4             	mov    %edx,-0x1c(%ebp)
801013be:	e8 bd fd ff ff       	call   80101180 <balloc>
801013c3:	8b 55 e4             	mov    -0x1c(%ebp),%edx
      log_write(bp);
801013c6:	83 ec 0c             	sub    $0xc,%esp
      a[bn] = addr = balloc(ip->dev);
801013c9:	89 c3                	mov    %eax,%ebx
801013cb:	89 02                	mov    %eax,(%edx)
      log_write(bp);
801013cd:	57                   	push   %edi
801013ce:	e8 9d 1d 00 00       	call   80103170 <log_write>
801013d3:	83 c4 10             	add    $0x10,%esp
    brelse(bp);
801013d6:	83 ec 0c             	sub    $0xc,%esp
801013d9:	57                   	push   %edi
801013da:	e8 01 ee ff ff       	call   801001e0 <brelse>
801013df:	83 c4 10             	add    $0x10,%esp
}
801013e2:	8d 65 f4             	lea    -0xc(%ebp),%esp
801013e5:	89 d8                	mov    %ebx,%eax
801013e7:	5b                   	pop    %ebx
801013e8:	5e                   	pop    %esi
801013e9:	5f                   	pop    %edi
801013ea:	5d                   	pop    %ebp
801013eb:	c3                   	ret    
801013ec:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
      ip->addrs[bn] = addr = balloc(ip->dev);
801013f0:	8b 00                	mov    (%eax),%eax
801013f2:	e8 89 fd ff ff       	call   80101180 <balloc>
801013f7:	89 47 5c             	mov    %eax,0x5c(%edi)
}
801013fa:	8d 65 f4             	lea    -0xc(%ebp),%esp
      ip->addrs[bn] = addr = balloc(ip->dev);
801013fd:	89 c3                	mov    %eax,%ebx
}
801013ff:	89 d8                	mov    %ebx,%eax
80101401:	5b                   	pop    %ebx
80101402:	5e                   	pop    %esi
80101403:	5f                   	pop    %edi
80101404:	5d                   	pop    %ebp
80101405:	c3                   	ret    
80101406:	8d 76 00             	lea    0x0(%esi),%esi
80101409:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi
      ip->addrs[NDIRECT] = addr = balloc(ip->dev);
80101410:	e8 6b fd ff ff       	call   80101180 <balloc>
80101415:	89 c2                	mov    %eax,%edx
80101417:	89 86 8c 00 00 00    	mov    %eax,0x8c(%esi)
8010141d:	8b 06                	mov    (%esi),%eax
8010141f:	e9 7c ff ff ff       	jmp    801013a0 <bmap+0x40>
  panic("bmap: out of range");
80101424:	83 ec 0c             	sub    $0xc,%esp
80101427:	68 38 78 10 80       	push   $0x80107838
8010142c:	e8 5f ef ff ff       	call   80100390 <panic>
80101431:	eb 0d                	jmp    80101440 <readsb>
80101433:	90                   	nop
80101434:	90                   	nop
80101435:	90                   	nop
80101436:	90                   	nop
80101437:	90                   	nop
80101438:	90                   	nop
80101439:	90                   	nop
8010143a:	90                   	nop
8010143b:	90                   	nop
8010143c:	90                   	nop
8010143d:	90                   	nop
8010143e:	90                   	nop
8010143f:	90                   	nop

80101440 <readsb>:
{
80101440:	55                   	push   %ebp
80101441:	89 e5                	mov    %esp,%ebp
80101443:	56                   	push   %esi
80101444:	53                   	push   %ebx
80101445:	8b 75 0c             	mov    0xc(%ebp),%esi
  bp = bread(dev, 1);
80101448:	83 ec 08             	sub    $0x8,%esp
8010144b:	6a 01                	push   $0x1
8010144d:	ff 75 08             	pushl  0x8(%ebp)
80101450:	e8 7b ec ff ff       	call   801000d0 <bread>
80101455:	89 c3                	mov    %eax,%ebx
  memmove(sb, bp->data, sizeof(*sb));
80101457:	8d 40 5c             	lea    0x5c(%eax),%eax
8010145a:	83 c4 0c             	add    $0xc,%esp
8010145d:	6a 1c                	push   $0x1c
8010145f:	50                   	push   %eax
80101460:	56                   	push   %esi
80101461:	e8 2a 35 00 00       	call   80104990 <memmove>
  brelse(bp);
80101466:	89 5d 08             	mov    %ebx,0x8(%ebp)
80101469:	83 c4 10             	add    $0x10,%esp
}
8010146c:	8d 65 f8             	lea    -0x8(%ebp),%esp
8010146f:	5b                   	pop    %ebx
80101470:	5e                   	pop    %esi
80101471:	5d                   	pop    %ebp
  brelse(bp);
80101472:	e9 69 ed ff ff       	jmp    801001e0 <brelse>
80101477:	89 f6                	mov    %esi,%esi
80101479:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80101480 <iinit>:
{
80101480:	55                   	push   %ebp
80101481:	89 e5                	mov    %esp,%ebp
80101483:	53                   	push   %ebx
80101484:	bb 20 1a 11 80       	mov    $0x80111a20,%ebx
80101489:	83 ec 0c             	sub    $0xc,%esp
  initlock(&icache.lock, "icache");
8010148c:	68 4b 78 10 80       	push   $0x8010784b
80101491:	68 e0 19 11 80       	push   $0x801119e0
80101496:	e8 f5 31 00 00       	call   80104690 <initlock>
8010149b:	83 c4 10             	add    $0x10,%esp
8010149e:	66 90                	xchg   %ax,%ax
    initsleeplock(&icache.inode[i].lock, "inode");
801014a0:	83 ec 08             	sub    $0x8,%esp
801014a3:	68 52 78 10 80       	push   $0x80107852
801014a8:	53                   	push   %ebx
801014a9:	81 c3 90 00 00 00    	add    $0x90,%ebx
801014af:	e8 ac 30 00 00       	call   80104560 <initsleeplock>
  for(i = 0; i < NINODE; i++) {
801014b4:	83 c4 10             	add    $0x10,%esp
801014b7:	81 fb 40 36 11 80    	cmp    $0x80113640,%ebx
801014bd:	75 e1                	jne    801014a0 <iinit+0x20>
  readsb(dev, &sb);
801014bf:	83 ec 08             	sub    $0x8,%esp
801014c2:	68 c0 19 11 80       	push   $0x801119c0
801014c7:	ff 75 08             	pushl  0x8(%ebp)
801014ca:	e8 71 ff ff ff       	call   80101440 <readsb>
  cprintf("sb: size %d nblocks %d ninodes %d nlog %d logstart %d\
801014cf:	ff 35 d8 19 11 80    	pushl  0x801119d8
801014d5:	ff 35 d4 19 11 80    	pushl  0x801119d4
801014db:	ff 35 d0 19 11 80    	pushl  0x801119d0
801014e1:	ff 35 cc 19 11 80    	pushl  0x801119cc
801014e7:	ff 35 c8 19 11 80    	pushl  0x801119c8
801014ed:	ff 35 c4 19 11 80    	pushl  0x801119c4
801014f3:	ff 35 c0 19 11 80    	pushl  0x801119c0
801014f9:	68 fc 78 10 80       	push   $0x801078fc
801014fe:	e8 5d f1 ff ff       	call   80100660 <cprintf>
}
80101503:	83 c4 30             	add    $0x30,%esp
80101506:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80101509:	c9                   	leave  
8010150a:	c3                   	ret    
8010150b:	90                   	nop
8010150c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

80101510 <ialloc>:
{
80101510:	55                   	push   %ebp
80101511:	89 e5                	mov    %esp,%ebp
80101513:	57                   	push   %edi
80101514:	56                   	push   %esi
80101515:	53                   	push   %ebx
80101516:	83 ec 1c             	sub    $0x1c,%esp
  for(inum = 1; inum < sb.ninodes; inum++){
80101519:	83 3d c8 19 11 80 01 	cmpl   $0x1,0x801119c8
{
80101520:	8b 45 0c             	mov    0xc(%ebp),%eax
80101523:	8b 75 08             	mov    0x8(%ebp),%esi
80101526:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  for(inum = 1; inum < sb.ninodes; inum++){
80101529:	0f 86 91 00 00 00    	jbe    801015c0 <ialloc+0xb0>
8010152f:	bb 01 00 00 00       	mov    $0x1,%ebx
80101534:	eb 21                	jmp    80101557 <ialloc+0x47>
80101536:	8d 76 00             	lea    0x0(%esi),%esi
80101539:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi
    brelse(bp);
80101540:	83 ec 0c             	sub    $0xc,%esp
  for(inum = 1; inum < sb.ninodes; inum++){
80101543:	83 c3 01             	add    $0x1,%ebx
    brelse(bp);
80101546:	57                   	push   %edi
80101547:	e8 94 ec ff ff       	call   801001e0 <brelse>
  for(inum = 1; inum < sb.ninodes; inum++){
8010154c:	83 c4 10             	add    $0x10,%esp
8010154f:	39 1d c8 19 11 80    	cmp    %ebx,0x801119c8
80101555:	76 69                	jbe    801015c0 <ialloc+0xb0>
    bp = bread(dev, IBLOCK(inum, sb));
80101557:	89 d8                	mov    %ebx,%eax
80101559:	83 ec 08             	sub    $0x8,%esp
8010155c:	c1 e8 03             	shr    $0x3,%eax
8010155f:	03 05 d4 19 11 80    	add    0x801119d4,%eax
80101565:	50                   	push   %eax
80101566:	56                   	push   %esi
80101567:	e8 64 eb ff ff       	call   801000d0 <bread>
8010156c:	89 c7                	mov    %eax,%edi
    dip = (struct dinode*)bp->data + inum%IPB;
8010156e:	89 d8                	mov    %ebx,%eax
    if(dip->type == 0){  // a free inode
80101570:	83 c4 10             	add    $0x10,%esp
    dip = (struct dinode*)bp->data + inum%IPB;
80101573:	83 e0 07             	and    $0x7,%eax
80101576:	c1 e0 06             	shl    $0x6,%eax
80101579:	8d 4c 07 5c          	lea    0x5c(%edi,%eax,1),%ecx
    if(dip->type == 0){  // a free inode
8010157d:	66 83 39 00          	cmpw   $0x0,(%ecx)
80101581:	75 bd                	jne    80101540 <ialloc+0x30>
      memset(dip, 0, sizeof(*dip));
80101583:	83 ec 04             	sub    $0x4,%esp
80101586:	89 4d e0             	mov    %ecx,-0x20(%ebp)
80101589:	6a 40                	push   $0x40
8010158b:	6a 00                	push   $0x0
8010158d:	51                   	push   %ecx
8010158e:	e8 4d 33 00 00       	call   801048e0 <memset>
      dip->type = type;
80101593:	0f b7 45 e4          	movzwl -0x1c(%ebp),%eax
80101597:	8b 4d e0             	mov    -0x20(%ebp),%ecx
8010159a:	66 89 01             	mov    %ax,(%ecx)
      log_write(bp);   // mark it allocated on the disk
8010159d:	89 3c 24             	mov    %edi,(%esp)
801015a0:	e8 cb 1b 00 00       	call   80103170 <log_write>
      brelse(bp);
801015a5:	89 3c 24             	mov    %edi,(%esp)
801015a8:	e8 33 ec ff ff       	call   801001e0 <brelse>
      return iget(dev, inum);
801015ad:	83 c4 10             	add    $0x10,%esp
}
801015b0:	8d 65 f4             	lea    -0xc(%ebp),%esp
      return iget(dev, inum);
801015b3:	89 da                	mov    %ebx,%edx
801015b5:	89 f0                	mov    %esi,%eax
}
801015b7:	5b                   	pop    %ebx
801015b8:	5e                   	pop    %esi
801015b9:	5f                   	pop    %edi
801015ba:	5d                   	pop    %ebp
      return iget(dev, inum);
801015bb:	e9 d0 fc ff ff       	jmp    80101290 <iget>
  panic("ialloc: no inodes");
801015c0:	83 ec 0c             	sub    $0xc,%esp
801015c3:	68 58 78 10 80       	push   $0x80107858
801015c8:	e8 c3 ed ff ff       	call   80100390 <panic>
801015cd:	8d 76 00             	lea    0x0(%esi),%esi

801015d0 <iupdate>:
{
801015d0:	55                   	push   %ebp
801015d1:	89 e5                	mov    %esp,%ebp
801015d3:	56                   	push   %esi
801015d4:	53                   	push   %ebx
801015d5:	8b 5d 08             	mov    0x8(%ebp),%ebx
  bp = bread(ip->dev, IBLOCK(ip->inum, sb));
801015d8:	83 ec 08             	sub    $0x8,%esp
801015db:	8b 43 04             	mov    0x4(%ebx),%eax
  memmove(dip->addrs, ip->addrs, sizeof(ip->addrs));
801015de:	83 c3 5c             	add    $0x5c,%ebx
  bp = bread(ip->dev, IBLOCK(ip->inum, sb));
801015e1:	c1 e8 03             	shr    $0x3,%eax
801015e4:	03 05 d4 19 11 80    	add    0x801119d4,%eax
801015ea:	50                   	push   %eax
801015eb:	ff 73 a4             	pushl  -0x5c(%ebx)
801015ee:	e8 dd ea ff ff       	call   801000d0 <bread>
801015f3:	89 c6                	mov    %eax,%esi
  dip = (struct dinode*)bp->data + ip->inum%IPB;
801015f5:	8b 43 a8             	mov    -0x58(%ebx),%eax
  dip->type = ip->type;
801015f8:	0f b7 53 f4          	movzwl -0xc(%ebx),%edx
  memmove(dip->addrs, ip->addrs, sizeof(ip->addrs));
801015fc:	83 c4 0c             	add    $0xc,%esp
  dip = (struct dinode*)bp->data + ip->inum%IPB;
801015ff:	83 e0 07             	and    $0x7,%eax
80101602:	c1 e0 06             	shl    $0x6,%eax
80101605:	8d 44 06 5c          	lea    0x5c(%esi,%eax,1),%eax
  dip->type = ip->type;
80101609:	66 89 10             	mov    %dx,(%eax)
  dip->major = ip->major;
8010160c:	0f b7 53 f6          	movzwl -0xa(%ebx),%edx
  memmove(dip->addrs, ip->addrs, sizeof(ip->addrs));
80101610:	83 c0 0c             	add    $0xc,%eax
  dip->major = ip->major;
80101613:	66 89 50 f6          	mov    %dx,-0xa(%eax)
  dip->minor = ip->minor;
80101617:	0f b7 53 f8          	movzwl -0x8(%ebx),%edx
8010161b:	66 89 50 f8          	mov    %dx,-0x8(%eax)
  dip->nlink = ip->nlink;
8010161f:	0f b7 53 fa          	movzwl -0x6(%ebx),%edx
80101623:	66 89 50 fa          	mov    %dx,-0x6(%eax)
  dip->size = ip->size;
80101627:	8b 53 fc             	mov    -0x4(%ebx),%edx
8010162a:	89 50 fc             	mov    %edx,-0x4(%eax)
  memmove(dip->addrs, ip->addrs, sizeof(ip->addrs));
8010162d:	6a 34                	push   $0x34
8010162f:	53                   	push   %ebx
80101630:	50                   	push   %eax
80101631:	e8 5a 33 00 00       	call   80104990 <memmove>
  log_write(bp);
80101636:	89 34 24             	mov    %esi,(%esp)
80101639:	e8 32 1b 00 00       	call   80103170 <log_write>
  brelse(bp);
8010163e:	89 75 08             	mov    %esi,0x8(%ebp)
80101641:	83 c4 10             	add    $0x10,%esp
}
80101644:	8d 65 f8             	lea    -0x8(%ebp),%esp
80101647:	5b                   	pop    %ebx
80101648:	5e                   	pop    %esi
80101649:	5d                   	pop    %ebp
  brelse(bp);
8010164a:	e9 91 eb ff ff       	jmp    801001e0 <brelse>
8010164f:	90                   	nop

80101650 <idup>:
{
80101650:	55                   	push   %ebp
80101651:	89 e5                	mov    %esp,%ebp
80101653:	53                   	push   %ebx
80101654:	83 ec 10             	sub    $0x10,%esp
80101657:	8b 5d 08             	mov    0x8(%ebp),%ebx
  acquire(&icache.lock);
8010165a:	68 e0 19 11 80       	push   $0x801119e0
8010165f:	e8 6c 31 00 00       	call   801047d0 <acquire>
  ip->ref++;
80101664:	83 43 08 01          	addl   $0x1,0x8(%ebx)
  release(&icache.lock);
80101668:	c7 04 24 e0 19 11 80 	movl   $0x801119e0,(%esp)
8010166f:	e8 1c 32 00 00       	call   80104890 <release>
}
80101674:	89 d8                	mov    %ebx,%eax
80101676:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80101679:	c9                   	leave  
8010167a:	c3                   	ret    
8010167b:	90                   	nop
8010167c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

80101680 <ilock>:
{
80101680:	55                   	push   %ebp
80101681:	89 e5                	mov    %esp,%ebp
80101683:	56                   	push   %esi
80101684:	53                   	push   %ebx
80101685:	8b 5d 08             	mov    0x8(%ebp),%ebx
  if(ip == 0 || ip->ref < 1)
80101688:	85 db                	test   %ebx,%ebx
8010168a:	0f 84 b7 00 00 00    	je     80101747 <ilock+0xc7>
80101690:	8b 53 08             	mov    0x8(%ebx),%edx
80101693:	85 d2                	test   %edx,%edx
80101695:	0f 8e ac 00 00 00    	jle    80101747 <ilock+0xc7>
  acquiresleep(&ip->lock);
8010169b:	8d 43 0c             	lea    0xc(%ebx),%eax
8010169e:	83 ec 0c             	sub    $0xc,%esp
801016a1:	50                   	push   %eax
801016a2:	e8 f9 2e 00 00       	call   801045a0 <acquiresleep>
  if(ip->valid == 0){
801016a7:	8b 43 4c             	mov    0x4c(%ebx),%eax
801016aa:	83 c4 10             	add    $0x10,%esp
801016ad:	85 c0                	test   %eax,%eax
801016af:	74 0f                	je     801016c0 <ilock+0x40>
}
801016b1:	8d 65 f8             	lea    -0x8(%ebp),%esp
801016b4:	5b                   	pop    %ebx
801016b5:	5e                   	pop    %esi
801016b6:	5d                   	pop    %ebp
801016b7:	c3                   	ret    
801016b8:	90                   	nop
801016b9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    bp = bread(ip->dev, IBLOCK(ip->inum, sb));
801016c0:	8b 43 04             	mov    0x4(%ebx),%eax
801016c3:	83 ec 08             	sub    $0x8,%esp
801016c6:	c1 e8 03             	shr    $0x3,%eax
801016c9:	03 05 d4 19 11 80    	add    0x801119d4,%eax
801016cf:	50                   	push   %eax
801016d0:	ff 33                	pushl  (%ebx)
801016d2:	e8 f9 e9 ff ff       	call   801000d0 <bread>
801016d7:	89 c6                	mov    %eax,%esi
    dip = (struct dinode*)bp->data + ip->inum%IPB;
801016d9:	8b 43 04             	mov    0x4(%ebx),%eax
    memmove(ip->addrs, dip->addrs, sizeof(ip->addrs));
801016dc:	83 c4 0c             	add    $0xc,%esp
    dip = (struct dinode*)bp->data + ip->inum%IPB;
801016df:	83 e0 07             	and    $0x7,%eax
801016e2:	c1 e0 06             	shl    $0x6,%eax
801016e5:	8d 44 06 5c          	lea    0x5c(%esi,%eax,1),%eax
    ip->type = dip->type;
801016e9:	0f b7 10             	movzwl (%eax),%edx
    memmove(ip->addrs, dip->addrs, sizeof(ip->addrs));
801016ec:	83 c0 0c             	add    $0xc,%eax
    ip->type = dip->type;
801016ef:	66 89 53 50          	mov    %dx,0x50(%ebx)
    ip->major = dip->major;
801016f3:	0f b7 50 f6          	movzwl -0xa(%eax),%edx
801016f7:	66 89 53 52          	mov    %dx,0x52(%ebx)
    ip->minor = dip->minor;
801016fb:	0f b7 50 f8          	movzwl -0x8(%eax),%edx
801016ff:	66 89 53 54          	mov    %dx,0x54(%ebx)
    ip->nlink = dip->nlink;
80101703:	0f b7 50 fa          	movzwl -0x6(%eax),%edx
80101707:	66 89 53 56          	mov    %dx,0x56(%ebx)
    ip->size = dip->size;
8010170b:	8b 50 fc             	mov    -0x4(%eax),%edx
8010170e:	89 53 58             	mov    %edx,0x58(%ebx)
    memmove(ip->addrs, dip->addrs, sizeof(ip->addrs));
80101711:	6a 34                	push   $0x34
80101713:	50                   	push   %eax
80101714:	8d 43 5c             	lea    0x5c(%ebx),%eax
80101717:	50                   	push   %eax
80101718:	e8 73 32 00 00       	call   80104990 <memmove>
    brelse(bp);
8010171d:	89 34 24             	mov    %esi,(%esp)
80101720:	e8 bb ea ff ff       	call   801001e0 <brelse>
    if(ip->type == 0)
80101725:	83 c4 10             	add    $0x10,%esp
80101728:	66 83 7b 50 00       	cmpw   $0x0,0x50(%ebx)
    ip->valid = 1;
8010172d:	c7 43 4c 01 00 00 00 	movl   $0x1,0x4c(%ebx)
    if(ip->type == 0)
80101734:	0f 85 77 ff ff ff    	jne    801016b1 <ilock+0x31>
      panic("ilock: no type");
8010173a:	83 ec 0c             	sub    $0xc,%esp
8010173d:	68 70 78 10 80       	push   $0x80107870
80101742:	e8 49 ec ff ff       	call   80100390 <panic>
    panic("ilock");
80101747:	83 ec 0c             	sub    $0xc,%esp
8010174a:	68 6a 78 10 80       	push   $0x8010786a
8010174f:	e8 3c ec ff ff       	call   80100390 <panic>
80101754:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
8010175a:	8d bf 00 00 00 00    	lea    0x0(%edi),%edi

80101760 <iunlock>:
{
80101760:	55                   	push   %ebp
80101761:	89 e5                	mov    %esp,%ebp
80101763:	56                   	push   %esi
80101764:	53                   	push   %ebx
80101765:	8b 5d 08             	mov    0x8(%ebp),%ebx
  if(ip == 0 || !holdingsleep(&ip->lock) || ip->ref < 1)
80101768:	85 db                	test   %ebx,%ebx
8010176a:	74 28                	je     80101794 <iunlock+0x34>
8010176c:	8d 73 0c             	lea    0xc(%ebx),%esi
8010176f:	83 ec 0c             	sub    $0xc,%esp
80101772:	56                   	push   %esi
80101773:	e8 c8 2e 00 00       	call   80104640 <holdingsleep>
80101778:	83 c4 10             	add    $0x10,%esp
8010177b:	85 c0                	test   %eax,%eax
8010177d:	74 15                	je     80101794 <iunlock+0x34>
8010177f:	8b 43 08             	mov    0x8(%ebx),%eax
80101782:	85 c0                	test   %eax,%eax
80101784:	7e 0e                	jle    80101794 <iunlock+0x34>
  releasesleep(&ip->lock);
80101786:	89 75 08             	mov    %esi,0x8(%ebp)
}
80101789:	8d 65 f8             	lea    -0x8(%ebp),%esp
8010178c:	5b                   	pop    %ebx
8010178d:	5e                   	pop    %esi
8010178e:	5d                   	pop    %ebp
  releasesleep(&ip->lock);
8010178f:	e9 6c 2e 00 00       	jmp    80104600 <releasesleep>
    panic("iunlock");
80101794:	83 ec 0c             	sub    $0xc,%esp
80101797:	68 7f 78 10 80       	push   $0x8010787f
8010179c:	e8 ef eb ff ff       	call   80100390 <panic>
801017a1:	eb 0d                	jmp    801017b0 <iput>
801017a3:	90                   	nop
801017a4:	90                   	nop
801017a5:	90                   	nop
801017a6:	90                   	nop
801017a7:	90                   	nop
801017a8:	90                   	nop
801017a9:	90                   	nop
801017aa:	90                   	nop
801017ab:	90                   	nop
801017ac:	90                   	nop
801017ad:	90                   	nop
801017ae:	90                   	nop
801017af:	90                   	nop

801017b0 <iput>:
{
801017b0:	55                   	push   %ebp
801017b1:	89 e5                	mov    %esp,%ebp
801017b3:	57                   	push   %edi
801017b4:	56                   	push   %esi
801017b5:	53                   	push   %ebx
801017b6:	83 ec 28             	sub    $0x28,%esp
801017b9:	8b 5d 08             	mov    0x8(%ebp),%ebx
  acquiresleep(&ip->lock);
801017bc:	8d 7b 0c             	lea    0xc(%ebx),%edi
801017bf:	57                   	push   %edi
801017c0:	e8 db 2d 00 00       	call   801045a0 <acquiresleep>
  if(ip->valid && ip->nlink == 0){
801017c5:	8b 53 4c             	mov    0x4c(%ebx),%edx
801017c8:	83 c4 10             	add    $0x10,%esp
801017cb:	85 d2                	test   %edx,%edx
801017cd:	74 07                	je     801017d6 <iput+0x26>
801017cf:	66 83 7b 56 00       	cmpw   $0x0,0x56(%ebx)
801017d4:	74 32                	je     80101808 <iput+0x58>
  releasesleep(&ip->lock);
801017d6:	83 ec 0c             	sub    $0xc,%esp
801017d9:	57                   	push   %edi
801017da:	e8 21 2e 00 00       	call   80104600 <releasesleep>
  acquire(&icache.lock);
801017df:	c7 04 24 e0 19 11 80 	movl   $0x801119e0,(%esp)
801017e6:	e8 e5 2f 00 00       	call   801047d0 <acquire>
  ip->ref--;
801017eb:	83 6b 08 01          	subl   $0x1,0x8(%ebx)
  release(&icache.lock);
801017ef:	83 c4 10             	add    $0x10,%esp
801017f2:	c7 45 08 e0 19 11 80 	movl   $0x801119e0,0x8(%ebp)
}
801017f9:	8d 65 f4             	lea    -0xc(%ebp),%esp
801017fc:	5b                   	pop    %ebx
801017fd:	5e                   	pop    %esi
801017fe:	5f                   	pop    %edi
801017ff:	5d                   	pop    %ebp
  release(&icache.lock);
80101800:	e9 8b 30 00 00       	jmp    80104890 <release>
80101805:	8d 76 00             	lea    0x0(%esi),%esi
    acquire(&icache.lock);
80101808:	83 ec 0c             	sub    $0xc,%esp
8010180b:	68 e0 19 11 80       	push   $0x801119e0
80101810:	e8 bb 2f 00 00       	call   801047d0 <acquire>
    int r = ip->ref;
80101815:	8b 73 08             	mov    0x8(%ebx),%esi
    release(&icache.lock);
80101818:	c7 04 24 e0 19 11 80 	movl   $0x801119e0,(%esp)
8010181f:	e8 6c 30 00 00       	call   80104890 <release>
    if(r == 1){
80101824:	83 c4 10             	add    $0x10,%esp
80101827:	83 fe 01             	cmp    $0x1,%esi
8010182a:	75 aa                	jne    801017d6 <iput+0x26>
8010182c:	8d 8b 8c 00 00 00    	lea    0x8c(%ebx),%ecx
80101832:	89 7d e4             	mov    %edi,-0x1c(%ebp)
80101835:	8d 73 5c             	lea    0x5c(%ebx),%esi
80101838:	89 cf                	mov    %ecx,%edi
8010183a:	eb 0b                	jmp    80101847 <iput+0x97>
8010183c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80101840:	83 c6 04             	add    $0x4,%esi
{
  int i, j;
  struct buf *bp;
  uint *a;

  for(i = 0; i < NDIRECT; i++){
80101843:	39 fe                	cmp    %edi,%esi
80101845:	74 19                	je     80101860 <iput+0xb0>
    if(ip->addrs[i]){
80101847:	8b 16                	mov    (%esi),%edx
80101849:	85 d2                	test   %edx,%edx
8010184b:	74 f3                	je     80101840 <iput+0x90>
      bfree(ip->dev, ip->addrs[i]);
8010184d:	8b 03                	mov    (%ebx),%eax
8010184f:	e8 bc f8 ff ff       	call   80101110 <bfree>
      ip->addrs[i] = 0;
80101854:	c7 06 00 00 00 00    	movl   $0x0,(%esi)
8010185a:	eb e4                	jmp    80101840 <iput+0x90>
8010185c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    }
  }

  if(ip->addrs[NDIRECT]){
80101860:	8b 83 8c 00 00 00    	mov    0x8c(%ebx),%eax
80101866:	8b 7d e4             	mov    -0x1c(%ebp),%edi
80101869:	85 c0                	test   %eax,%eax
8010186b:	75 33                	jne    801018a0 <iput+0xf0>
    bfree(ip->dev, ip->addrs[NDIRECT]);
    ip->addrs[NDIRECT] = 0;
  }

  ip->size = 0;
  iupdate(ip);
8010186d:	83 ec 0c             	sub    $0xc,%esp
  ip->size = 0;
80101870:	c7 43 58 00 00 00 00 	movl   $0x0,0x58(%ebx)
  iupdate(ip);
80101877:	53                   	push   %ebx
80101878:	e8 53 fd ff ff       	call   801015d0 <iupdate>
      ip->type = 0;
8010187d:	31 c0                	xor    %eax,%eax
8010187f:	66 89 43 50          	mov    %ax,0x50(%ebx)
      iupdate(ip);
80101883:	89 1c 24             	mov    %ebx,(%esp)
80101886:	e8 45 fd ff ff       	call   801015d0 <iupdate>
      ip->valid = 0;
8010188b:	c7 43 4c 00 00 00 00 	movl   $0x0,0x4c(%ebx)
80101892:	83 c4 10             	add    $0x10,%esp
80101895:	e9 3c ff ff ff       	jmp    801017d6 <iput+0x26>
8010189a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    bp = bread(ip->dev, ip->addrs[NDIRECT]);
801018a0:	83 ec 08             	sub    $0x8,%esp
801018a3:	50                   	push   %eax
801018a4:	ff 33                	pushl  (%ebx)
801018a6:	e8 25 e8 ff ff       	call   801000d0 <bread>
801018ab:	8d 88 5c 02 00 00    	lea    0x25c(%eax),%ecx
801018b1:	89 7d e0             	mov    %edi,-0x20(%ebp)
801018b4:	89 45 e4             	mov    %eax,-0x1c(%ebp)
    a = (uint*)bp->data;
801018b7:	8d 70 5c             	lea    0x5c(%eax),%esi
801018ba:	83 c4 10             	add    $0x10,%esp
801018bd:	89 cf                	mov    %ecx,%edi
801018bf:	eb 0e                	jmp    801018cf <iput+0x11f>
801018c1:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801018c8:	83 c6 04             	add    $0x4,%esi
    for(j = 0; j < NINDIRECT; j++){
801018cb:	39 fe                	cmp    %edi,%esi
801018cd:	74 0f                	je     801018de <iput+0x12e>
      if(a[j])
801018cf:	8b 16                	mov    (%esi),%edx
801018d1:	85 d2                	test   %edx,%edx
801018d3:	74 f3                	je     801018c8 <iput+0x118>
        bfree(ip->dev, a[j]);
801018d5:	8b 03                	mov    (%ebx),%eax
801018d7:	e8 34 f8 ff ff       	call   80101110 <bfree>
801018dc:	eb ea                	jmp    801018c8 <iput+0x118>
    brelse(bp);
801018de:	83 ec 0c             	sub    $0xc,%esp
801018e1:	ff 75 e4             	pushl  -0x1c(%ebp)
801018e4:	8b 7d e0             	mov    -0x20(%ebp),%edi
801018e7:	e8 f4 e8 ff ff       	call   801001e0 <brelse>
    bfree(ip->dev, ip->addrs[NDIRECT]);
801018ec:	8b 93 8c 00 00 00    	mov    0x8c(%ebx),%edx
801018f2:	8b 03                	mov    (%ebx),%eax
801018f4:	e8 17 f8 ff ff       	call   80101110 <bfree>
    ip->addrs[NDIRECT] = 0;
801018f9:	c7 83 8c 00 00 00 00 	movl   $0x0,0x8c(%ebx)
80101900:	00 00 00 
80101903:	83 c4 10             	add    $0x10,%esp
80101906:	e9 62 ff ff ff       	jmp    8010186d <iput+0xbd>
8010190b:	90                   	nop
8010190c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

80101910 <iunlockput>:
{
80101910:	55                   	push   %ebp
80101911:	89 e5                	mov    %esp,%ebp
80101913:	53                   	push   %ebx
80101914:	83 ec 10             	sub    $0x10,%esp
80101917:	8b 5d 08             	mov    0x8(%ebp),%ebx
  iunlock(ip);
8010191a:	53                   	push   %ebx
8010191b:	e8 40 fe ff ff       	call   80101760 <iunlock>
  iput(ip);
80101920:	89 5d 08             	mov    %ebx,0x8(%ebp)
80101923:	83 c4 10             	add    $0x10,%esp
}
80101926:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80101929:	c9                   	leave  
  iput(ip);
8010192a:	e9 81 fe ff ff       	jmp    801017b0 <iput>
8010192f:	90                   	nop

80101930 <stati>:

// Copy stat information from inode.
// Caller must hold ip->lock.
void
stati(struct inode *ip, struct stat *st)
{
80101930:	55                   	push   %ebp
80101931:	89 e5                	mov    %esp,%ebp
80101933:	8b 55 08             	mov    0x8(%ebp),%edx
80101936:	8b 45 0c             	mov    0xc(%ebp),%eax
  st->dev = ip->dev;
80101939:	8b 0a                	mov    (%edx),%ecx
8010193b:	89 48 04             	mov    %ecx,0x4(%eax)
  st->ino = ip->inum;
8010193e:	8b 4a 04             	mov    0x4(%edx),%ecx
80101941:	89 48 08             	mov    %ecx,0x8(%eax)
  st->type = ip->type;
80101944:	0f b7 4a 50          	movzwl 0x50(%edx),%ecx
80101948:	66 89 08             	mov    %cx,(%eax)
  st->nlink = ip->nlink;
8010194b:	0f b7 4a 56          	movzwl 0x56(%edx),%ecx
8010194f:	66 89 48 0c          	mov    %cx,0xc(%eax)
  st->size = ip->size;
80101953:	8b 52 58             	mov    0x58(%edx),%edx
80101956:	89 50 10             	mov    %edx,0x10(%eax)
}
80101959:	5d                   	pop    %ebp
8010195a:	c3                   	ret    
8010195b:	90                   	nop
8010195c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

80101960 <readi>:
//PAGEBREAK!
// Read data from inode.
// Caller must hold ip->lock.
int
readi(struct inode *ip, char *dst, uint off, uint n)
{
80101960:	55                   	push   %ebp
80101961:	89 e5                	mov    %esp,%ebp
80101963:	57                   	push   %edi
80101964:	56                   	push   %esi
80101965:	53                   	push   %ebx
80101966:	83 ec 1c             	sub    $0x1c,%esp
80101969:	8b 45 08             	mov    0x8(%ebp),%eax
8010196c:	8b 75 0c             	mov    0xc(%ebp),%esi
8010196f:	8b 7d 14             	mov    0x14(%ebp),%edi
  uint tot, m;
  struct buf *bp;

  if(ip->type == T_DEV){
80101972:	66 83 78 50 03       	cmpw   $0x3,0x50(%eax)
{
80101977:	89 75 e0             	mov    %esi,-0x20(%ebp)
8010197a:	89 45 d8             	mov    %eax,-0x28(%ebp)
8010197d:	8b 75 10             	mov    0x10(%ebp),%esi
80101980:	89 7d e4             	mov    %edi,-0x1c(%ebp)
  if(ip->type == T_DEV){
80101983:	0f 84 a7 00 00 00    	je     80101a30 <readi+0xd0>
    if(ip->major < 0 || ip->major >= NDEV || !devsw[ip->major].read)
      return -1;
    return devsw[ip->major].read(ip, dst, n);
  }

  if(off > ip->size || off + n < off)
80101989:	8b 45 d8             	mov    -0x28(%ebp),%eax
8010198c:	8b 40 58             	mov    0x58(%eax),%eax
8010198f:	39 c6                	cmp    %eax,%esi
80101991:	0f 87 ba 00 00 00    	ja     80101a51 <readi+0xf1>
80101997:	8b 7d e4             	mov    -0x1c(%ebp),%edi
8010199a:	89 f9                	mov    %edi,%ecx
8010199c:	01 f1                	add    %esi,%ecx
8010199e:	0f 82 ad 00 00 00    	jb     80101a51 <readi+0xf1>
    return -1;
  if(off + n > ip->size)
    n = ip->size - off;
801019a4:	89 c2                	mov    %eax,%edx
801019a6:	29 f2                	sub    %esi,%edx
801019a8:	39 c8                	cmp    %ecx,%eax
801019aa:	0f 43 d7             	cmovae %edi,%edx

  for(tot=0; tot<n; tot+=m, off+=m, dst+=m){
801019ad:	31 ff                	xor    %edi,%edi
801019af:	85 d2                	test   %edx,%edx
    n = ip->size - off;
801019b1:	89 55 e4             	mov    %edx,-0x1c(%ebp)
  for(tot=0; tot<n; tot+=m, off+=m, dst+=m){
801019b4:	74 6c                	je     80101a22 <readi+0xc2>
801019b6:	8d 76 00             	lea    0x0(%esi),%esi
801019b9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi
    bp = bread(ip->dev, bmap(ip, off/BSIZE));
801019c0:	8b 5d d8             	mov    -0x28(%ebp),%ebx
801019c3:	89 f2                	mov    %esi,%edx
801019c5:	c1 ea 09             	shr    $0x9,%edx
801019c8:	89 d8                	mov    %ebx,%eax
801019ca:	e8 91 f9 ff ff       	call   80101360 <bmap>
801019cf:	83 ec 08             	sub    $0x8,%esp
801019d2:	50                   	push   %eax
801019d3:	ff 33                	pushl  (%ebx)
801019d5:	e8 f6 e6 ff ff       	call   801000d0 <bread>
    m = min(n - tot, BSIZE - off%BSIZE);
801019da:	8b 5d e4             	mov    -0x1c(%ebp),%ebx
    bp = bread(ip->dev, bmap(ip, off/BSIZE));
801019dd:	89 c2                	mov    %eax,%edx
    m = min(n - tot, BSIZE - off%BSIZE);
801019df:	89 f0                	mov    %esi,%eax
801019e1:	25 ff 01 00 00       	and    $0x1ff,%eax
801019e6:	b9 00 02 00 00       	mov    $0x200,%ecx
801019eb:	83 c4 0c             	add    $0xc,%esp
801019ee:	29 c1                	sub    %eax,%ecx
    memmove(dst, bp->data + off%BSIZE, m);
801019f0:	8d 44 02 5c          	lea    0x5c(%edx,%eax,1),%eax
801019f4:	89 55 dc             	mov    %edx,-0x24(%ebp)
    m = min(n - tot, BSIZE - off%BSIZE);
801019f7:	29 fb                	sub    %edi,%ebx
801019f9:	39 d9                	cmp    %ebx,%ecx
801019fb:	0f 46 d9             	cmovbe %ecx,%ebx
    memmove(dst, bp->data + off%BSIZE, m);
801019fe:	53                   	push   %ebx
801019ff:	50                   	push   %eax
  for(tot=0; tot<n; tot+=m, off+=m, dst+=m){
80101a00:	01 df                	add    %ebx,%edi
    memmove(dst, bp->data + off%BSIZE, m);
80101a02:	ff 75 e0             	pushl  -0x20(%ebp)
  for(tot=0; tot<n; tot+=m, off+=m, dst+=m){
80101a05:	01 de                	add    %ebx,%esi
    memmove(dst, bp->data + off%BSIZE, m);
80101a07:	e8 84 2f 00 00       	call   80104990 <memmove>
    brelse(bp);
80101a0c:	8b 55 dc             	mov    -0x24(%ebp),%edx
80101a0f:	89 14 24             	mov    %edx,(%esp)
80101a12:	e8 c9 e7 ff ff       	call   801001e0 <brelse>
  for(tot=0; tot<n; tot+=m, off+=m, dst+=m){
80101a17:	01 5d e0             	add    %ebx,-0x20(%ebp)
80101a1a:	83 c4 10             	add    $0x10,%esp
80101a1d:	39 7d e4             	cmp    %edi,-0x1c(%ebp)
80101a20:	77 9e                	ja     801019c0 <readi+0x60>
  }
  return n;
80101a22:	8b 45 e4             	mov    -0x1c(%ebp),%eax
}
80101a25:	8d 65 f4             	lea    -0xc(%ebp),%esp
80101a28:	5b                   	pop    %ebx
80101a29:	5e                   	pop    %esi
80101a2a:	5f                   	pop    %edi
80101a2b:	5d                   	pop    %ebp
80101a2c:	c3                   	ret    
80101a2d:	8d 76 00             	lea    0x0(%esi),%esi
    if(ip->major < 0 || ip->major >= NDEV || !devsw[ip->major].read)
80101a30:	0f bf 40 52          	movswl 0x52(%eax),%eax
80101a34:	66 83 f8 09          	cmp    $0x9,%ax
80101a38:	77 17                	ja     80101a51 <readi+0xf1>
80101a3a:	8b 04 c5 60 19 11 80 	mov    -0x7feee6a0(,%eax,8),%eax
80101a41:	85 c0                	test   %eax,%eax
80101a43:	74 0c                	je     80101a51 <readi+0xf1>
    return devsw[ip->major].read(ip, dst, n);
80101a45:	89 7d 10             	mov    %edi,0x10(%ebp)
}
80101a48:	8d 65 f4             	lea    -0xc(%ebp),%esp
80101a4b:	5b                   	pop    %ebx
80101a4c:	5e                   	pop    %esi
80101a4d:	5f                   	pop    %edi
80101a4e:	5d                   	pop    %ebp
    return devsw[ip->major].read(ip, dst, n);
80101a4f:	ff e0                	jmp    *%eax
      return -1;
80101a51:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80101a56:	eb cd                	jmp    80101a25 <readi+0xc5>
80101a58:	90                   	nop
80101a59:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

80101a60 <writei>:
// PAGEBREAK!
// Write data to inode.
// Caller must hold ip->lock.
int
writei(struct inode *ip, char *src, uint off, uint n)
{
80101a60:	55                   	push   %ebp
80101a61:	89 e5                	mov    %esp,%ebp
80101a63:	57                   	push   %edi
80101a64:	56                   	push   %esi
80101a65:	53                   	push   %ebx
80101a66:	83 ec 1c             	sub    $0x1c,%esp
80101a69:	8b 45 08             	mov    0x8(%ebp),%eax
80101a6c:	8b 75 0c             	mov    0xc(%ebp),%esi
80101a6f:	8b 7d 14             	mov    0x14(%ebp),%edi
  uint tot, m;
  struct buf *bp;

  if(ip->type == T_DEV){
80101a72:	66 83 78 50 03       	cmpw   $0x3,0x50(%eax)
{
80101a77:	89 75 dc             	mov    %esi,-0x24(%ebp)
80101a7a:	89 45 d8             	mov    %eax,-0x28(%ebp)
80101a7d:	8b 75 10             	mov    0x10(%ebp),%esi
80101a80:	89 7d e0             	mov    %edi,-0x20(%ebp)
  if(ip->type == T_DEV){
80101a83:	0f 84 b7 00 00 00    	je     80101b40 <writei+0xe0>
    if(ip->major < 0 || ip->major >= NDEV || !devsw[ip->major].write)
      return -1;
    return devsw[ip->major].write(ip, src, n);
  }

  if(off > ip->size || off + n < off)
80101a89:	8b 45 d8             	mov    -0x28(%ebp),%eax
80101a8c:	39 70 58             	cmp    %esi,0x58(%eax)
80101a8f:	0f 82 eb 00 00 00    	jb     80101b80 <writei+0x120>
80101a95:	8b 7d e0             	mov    -0x20(%ebp),%edi
80101a98:	31 d2                	xor    %edx,%edx
80101a9a:	89 f8                	mov    %edi,%eax
80101a9c:	01 f0                	add    %esi,%eax
80101a9e:	0f 92 c2             	setb   %dl
    return -1;
  if(off + n > MAXFILE*BSIZE)
80101aa1:	3d 00 18 01 00       	cmp    $0x11800,%eax
80101aa6:	0f 87 d4 00 00 00    	ja     80101b80 <writei+0x120>
80101aac:	85 d2                	test   %edx,%edx
80101aae:	0f 85 cc 00 00 00    	jne    80101b80 <writei+0x120>
    return -1;

  for(tot=0; tot<n; tot+=m, off+=m, src+=m){
80101ab4:	85 ff                	test   %edi,%edi
80101ab6:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
80101abd:	74 72                	je     80101b31 <writei+0xd1>
80101abf:	90                   	nop
    bp = bread(ip->dev, bmap(ip, off/BSIZE));
80101ac0:	8b 7d d8             	mov    -0x28(%ebp),%edi
80101ac3:	89 f2                	mov    %esi,%edx
80101ac5:	c1 ea 09             	shr    $0x9,%edx
80101ac8:	89 f8                	mov    %edi,%eax
80101aca:	e8 91 f8 ff ff       	call   80101360 <bmap>
80101acf:	83 ec 08             	sub    $0x8,%esp
80101ad2:	50                   	push   %eax
80101ad3:	ff 37                	pushl  (%edi)
80101ad5:	e8 f6 e5 ff ff       	call   801000d0 <bread>
    m = min(n - tot, BSIZE - off%BSIZE);
80101ada:	8b 5d e0             	mov    -0x20(%ebp),%ebx
80101add:	2b 5d e4             	sub    -0x1c(%ebp),%ebx
    bp = bread(ip->dev, bmap(ip, off/BSIZE));
80101ae0:	89 c7                	mov    %eax,%edi
    m = min(n - tot, BSIZE - off%BSIZE);
80101ae2:	89 f0                	mov    %esi,%eax
80101ae4:	b9 00 02 00 00       	mov    $0x200,%ecx
80101ae9:	83 c4 0c             	add    $0xc,%esp
80101aec:	25 ff 01 00 00       	and    $0x1ff,%eax
80101af1:	29 c1                	sub    %eax,%ecx
    memmove(bp->data + off%BSIZE, src, m);
80101af3:	8d 44 07 5c          	lea    0x5c(%edi,%eax,1),%eax
    m = min(n - tot, BSIZE - off%BSIZE);
80101af7:	39 d9                	cmp    %ebx,%ecx
80101af9:	0f 46 d9             	cmovbe %ecx,%ebx
    memmove(bp->data + off%BSIZE, src, m);
80101afc:	53                   	push   %ebx
80101afd:	ff 75 dc             	pushl  -0x24(%ebp)
  for(tot=0; tot<n; tot+=m, off+=m, src+=m){
80101b00:	01 de                	add    %ebx,%esi
    memmove(bp->data + off%BSIZE, src, m);
80101b02:	50                   	push   %eax
80101b03:	e8 88 2e 00 00       	call   80104990 <memmove>
    log_write(bp);
80101b08:	89 3c 24             	mov    %edi,(%esp)
80101b0b:	e8 60 16 00 00       	call   80103170 <log_write>
    brelse(bp);
80101b10:	89 3c 24             	mov    %edi,(%esp)
80101b13:	e8 c8 e6 ff ff       	call   801001e0 <brelse>
  for(tot=0; tot<n; tot+=m, off+=m, src+=m){
80101b18:	01 5d e4             	add    %ebx,-0x1c(%ebp)
80101b1b:	01 5d dc             	add    %ebx,-0x24(%ebp)
80101b1e:	83 c4 10             	add    $0x10,%esp
80101b21:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80101b24:	39 45 e0             	cmp    %eax,-0x20(%ebp)
80101b27:	77 97                	ja     80101ac0 <writei+0x60>
  }

  if(n > 0 && off > ip->size){
80101b29:	8b 45 d8             	mov    -0x28(%ebp),%eax
80101b2c:	3b 70 58             	cmp    0x58(%eax),%esi
80101b2f:	77 37                	ja     80101b68 <writei+0x108>
    ip->size = off;
    iupdate(ip);
  }
  return n;
80101b31:	8b 45 e0             	mov    -0x20(%ebp),%eax
}
80101b34:	8d 65 f4             	lea    -0xc(%ebp),%esp
80101b37:	5b                   	pop    %ebx
80101b38:	5e                   	pop    %esi
80101b39:	5f                   	pop    %edi
80101b3a:	5d                   	pop    %ebp
80101b3b:	c3                   	ret    
80101b3c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    if(ip->major < 0 || ip->major >= NDEV || !devsw[ip->major].write)
80101b40:	0f bf 40 52          	movswl 0x52(%eax),%eax
80101b44:	66 83 f8 09          	cmp    $0x9,%ax
80101b48:	77 36                	ja     80101b80 <writei+0x120>
80101b4a:	8b 04 c5 64 19 11 80 	mov    -0x7feee69c(,%eax,8),%eax
80101b51:	85 c0                	test   %eax,%eax
80101b53:	74 2b                	je     80101b80 <writei+0x120>
    return devsw[ip->major].write(ip, src, n);
80101b55:	89 7d 10             	mov    %edi,0x10(%ebp)
}
80101b58:	8d 65 f4             	lea    -0xc(%ebp),%esp
80101b5b:	5b                   	pop    %ebx
80101b5c:	5e                   	pop    %esi
80101b5d:	5f                   	pop    %edi
80101b5e:	5d                   	pop    %ebp
    return devsw[ip->major].write(ip, src, n);
80101b5f:	ff e0                	jmp    *%eax
80101b61:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    ip->size = off;
80101b68:	8b 45 d8             	mov    -0x28(%ebp),%eax
    iupdate(ip);
80101b6b:	83 ec 0c             	sub    $0xc,%esp
    ip->size = off;
80101b6e:	89 70 58             	mov    %esi,0x58(%eax)
    iupdate(ip);
80101b71:	50                   	push   %eax
80101b72:	e8 59 fa ff ff       	call   801015d0 <iupdate>
80101b77:	83 c4 10             	add    $0x10,%esp
80101b7a:	eb b5                	jmp    80101b31 <writei+0xd1>
80101b7c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
      return -1;
80101b80:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80101b85:	eb ad                	jmp    80101b34 <writei+0xd4>
80101b87:	89 f6                	mov    %esi,%esi
80101b89:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80101b90 <namecmp>:
//PAGEBREAK!
// Directories

int
namecmp(const char *s, const char *t)
{
80101b90:	55                   	push   %ebp
80101b91:	89 e5                	mov    %esp,%ebp
80101b93:	83 ec 0c             	sub    $0xc,%esp
  return strncmp(s, t, DIRSIZ);
80101b96:	6a 0e                	push   $0xe
80101b98:	ff 75 0c             	pushl  0xc(%ebp)
80101b9b:	ff 75 08             	pushl  0x8(%ebp)
80101b9e:	e8 5d 2e 00 00       	call   80104a00 <strncmp>
}
80101ba3:	c9                   	leave  
80101ba4:	c3                   	ret    
80101ba5:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80101ba9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80101bb0 <dirlookup>:

// Look for a directory entry in a directory.
// If found, set *poff to byte offset of entry.
struct inode*
dirlookup(struct inode *dp, char *name, uint *poff)
{
80101bb0:	55                   	push   %ebp
80101bb1:	89 e5                	mov    %esp,%ebp
80101bb3:	57                   	push   %edi
80101bb4:	56                   	push   %esi
80101bb5:	53                   	push   %ebx
80101bb6:	83 ec 1c             	sub    $0x1c,%esp
80101bb9:	8b 5d 08             	mov    0x8(%ebp),%ebx
  uint off, inum;
  struct dirent de;

  if(dp->type != T_DIR)
80101bbc:	66 83 7b 50 01       	cmpw   $0x1,0x50(%ebx)
80101bc1:	0f 85 85 00 00 00    	jne    80101c4c <dirlookup+0x9c>
    panic("dirlookup not DIR");

  for(off = 0; off < dp->size; off += sizeof(de)){
80101bc7:	8b 53 58             	mov    0x58(%ebx),%edx
80101bca:	31 ff                	xor    %edi,%edi
80101bcc:	8d 75 d8             	lea    -0x28(%ebp),%esi
80101bcf:	85 d2                	test   %edx,%edx
80101bd1:	74 3e                	je     80101c11 <dirlookup+0x61>
80101bd3:	90                   	nop
80101bd4:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    if(readi(dp, (char*)&de, off, sizeof(de)) != sizeof(de))
80101bd8:	6a 10                	push   $0x10
80101bda:	57                   	push   %edi
80101bdb:	56                   	push   %esi
80101bdc:	53                   	push   %ebx
80101bdd:	e8 7e fd ff ff       	call   80101960 <readi>
80101be2:	83 c4 10             	add    $0x10,%esp
80101be5:	83 f8 10             	cmp    $0x10,%eax
80101be8:	75 55                	jne    80101c3f <dirlookup+0x8f>
      panic("dirlookup read");
    if(de.inum == 0)
80101bea:	66 83 7d d8 00       	cmpw   $0x0,-0x28(%ebp)
80101bef:	74 18                	je     80101c09 <dirlookup+0x59>
  return strncmp(s, t, DIRSIZ);
80101bf1:	8d 45 da             	lea    -0x26(%ebp),%eax
80101bf4:	83 ec 04             	sub    $0x4,%esp
80101bf7:	6a 0e                	push   $0xe
80101bf9:	50                   	push   %eax
80101bfa:	ff 75 0c             	pushl  0xc(%ebp)
80101bfd:	e8 fe 2d 00 00       	call   80104a00 <strncmp>
      continue;
    if(namecmp(name, de.name) == 0){
80101c02:	83 c4 10             	add    $0x10,%esp
80101c05:	85 c0                	test   %eax,%eax
80101c07:	74 17                	je     80101c20 <dirlookup+0x70>
  for(off = 0; off < dp->size; off += sizeof(de)){
80101c09:	83 c7 10             	add    $0x10,%edi
80101c0c:	3b 7b 58             	cmp    0x58(%ebx),%edi
80101c0f:	72 c7                	jb     80101bd8 <dirlookup+0x28>
      return iget(dp->dev, inum);
    }
  }

  return 0;
}
80101c11:	8d 65 f4             	lea    -0xc(%ebp),%esp
  return 0;
80101c14:	31 c0                	xor    %eax,%eax
}
80101c16:	5b                   	pop    %ebx
80101c17:	5e                   	pop    %esi
80101c18:	5f                   	pop    %edi
80101c19:	5d                   	pop    %ebp
80101c1a:	c3                   	ret    
80101c1b:	90                   	nop
80101c1c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
      if(poff)
80101c20:	8b 45 10             	mov    0x10(%ebp),%eax
80101c23:	85 c0                	test   %eax,%eax
80101c25:	74 05                	je     80101c2c <dirlookup+0x7c>
        *poff = off;
80101c27:	8b 45 10             	mov    0x10(%ebp),%eax
80101c2a:	89 38                	mov    %edi,(%eax)
      inum = de.inum;
80101c2c:	0f b7 55 d8          	movzwl -0x28(%ebp),%edx
      return iget(dp->dev, inum);
80101c30:	8b 03                	mov    (%ebx),%eax
80101c32:	e8 59 f6 ff ff       	call   80101290 <iget>
}
80101c37:	8d 65 f4             	lea    -0xc(%ebp),%esp
80101c3a:	5b                   	pop    %ebx
80101c3b:	5e                   	pop    %esi
80101c3c:	5f                   	pop    %edi
80101c3d:	5d                   	pop    %ebp
80101c3e:	c3                   	ret    
      panic("dirlookup read");
80101c3f:	83 ec 0c             	sub    $0xc,%esp
80101c42:	68 99 78 10 80       	push   $0x80107899
80101c47:	e8 44 e7 ff ff       	call   80100390 <panic>
    panic("dirlookup not DIR");
80101c4c:	83 ec 0c             	sub    $0xc,%esp
80101c4f:	68 87 78 10 80       	push   $0x80107887
80101c54:	e8 37 e7 ff ff       	call   80100390 <panic>
80101c59:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

80101c60 <namex>:
// If parent != 0, return the inode for the parent and copy the final
// path element into name, which must have room for DIRSIZ bytes.
// Must be called inside a transaction since it calls iput().
static struct inode*
namex(char *path, int nameiparent, char *name)
{
80101c60:	55                   	push   %ebp
80101c61:	89 e5                	mov    %esp,%ebp
80101c63:	57                   	push   %edi
80101c64:	56                   	push   %esi
80101c65:	53                   	push   %ebx
80101c66:	89 cf                	mov    %ecx,%edi
80101c68:	89 c3                	mov    %eax,%ebx
80101c6a:	83 ec 1c             	sub    $0x1c,%esp
  struct inode *ip, *next;

  if(*path == '/')
80101c6d:	80 38 2f             	cmpb   $0x2f,(%eax)
{
80101c70:	89 55 e0             	mov    %edx,-0x20(%ebp)
  if(*path == '/')
80101c73:	0f 84 67 01 00 00    	je     80101de0 <namex+0x180>
    ip = iget(ROOTDEV, ROOTINO);
  else
    ip = idup(myproc()->cwd);
80101c79:	e8 b2 1f 00 00       	call   80103c30 <myproc>
  acquire(&icache.lock);
80101c7e:	83 ec 0c             	sub    $0xc,%esp
    ip = idup(myproc()->cwd);
80101c81:	8b 70 68             	mov    0x68(%eax),%esi
  acquire(&icache.lock);
80101c84:	68 e0 19 11 80       	push   $0x801119e0
80101c89:	e8 42 2b 00 00       	call   801047d0 <acquire>
  ip->ref++;
80101c8e:	83 46 08 01          	addl   $0x1,0x8(%esi)
  release(&icache.lock);
80101c92:	c7 04 24 e0 19 11 80 	movl   $0x801119e0,(%esp)
80101c99:	e8 f2 2b 00 00       	call   80104890 <release>
80101c9e:	83 c4 10             	add    $0x10,%esp
80101ca1:	eb 08                	jmp    80101cab <namex+0x4b>
80101ca3:	90                   	nop
80101ca4:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    path++;
80101ca8:	83 c3 01             	add    $0x1,%ebx
  while(*path == '/')
80101cab:	0f b6 03             	movzbl (%ebx),%eax
80101cae:	3c 2f                	cmp    $0x2f,%al
80101cb0:	74 f6                	je     80101ca8 <namex+0x48>
  if(*path == 0)
80101cb2:	84 c0                	test   %al,%al
80101cb4:	0f 84 ee 00 00 00    	je     80101da8 <namex+0x148>
  while(*path != '/' && *path != 0)
80101cba:	0f b6 03             	movzbl (%ebx),%eax
80101cbd:	3c 2f                	cmp    $0x2f,%al
80101cbf:	0f 84 b3 00 00 00    	je     80101d78 <namex+0x118>
80101cc5:	84 c0                	test   %al,%al
80101cc7:	89 da                	mov    %ebx,%edx
80101cc9:	75 09                	jne    80101cd4 <namex+0x74>
80101ccb:	e9 a8 00 00 00       	jmp    80101d78 <namex+0x118>
80101cd0:	84 c0                	test   %al,%al
80101cd2:	74 0a                	je     80101cde <namex+0x7e>
    path++;
80101cd4:	83 c2 01             	add    $0x1,%edx
  while(*path != '/' && *path != 0)
80101cd7:	0f b6 02             	movzbl (%edx),%eax
80101cda:	3c 2f                	cmp    $0x2f,%al
80101cdc:	75 f2                	jne    80101cd0 <namex+0x70>
80101cde:	89 d1                	mov    %edx,%ecx
80101ce0:	29 d9                	sub    %ebx,%ecx
  if(len >= DIRSIZ)
80101ce2:	83 f9 0d             	cmp    $0xd,%ecx
80101ce5:	0f 8e 91 00 00 00    	jle    80101d7c <namex+0x11c>
    memmove(name, s, DIRSIZ);
80101ceb:	83 ec 04             	sub    $0x4,%esp
80101cee:	89 55 e4             	mov    %edx,-0x1c(%ebp)
80101cf1:	6a 0e                	push   $0xe
80101cf3:	53                   	push   %ebx
80101cf4:	57                   	push   %edi
80101cf5:	e8 96 2c 00 00       	call   80104990 <memmove>
    path++;
80101cfa:	8b 55 e4             	mov    -0x1c(%ebp),%edx
    memmove(name, s, DIRSIZ);
80101cfd:	83 c4 10             	add    $0x10,%esp
    path++;
80101d00:	89 d3                	mov    %edx,%ebx
  while(*path == '/')
80101d02:	80 3a 2f             	cmpb   $0x2f,(%edx)
80101d05:	75 11                	jne    80101d18 <namex+0xb8>
80101d07:	89 f6                	mov    %esi,%esi
80101d09:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi
    path++;
80101d10:	83 c3 01             	add    $0x1,%ebx
  while(*path == '/')
80101d13:	80 3b 2f             	cmpb   $0x2f,(%ebx)
80101d16:	74 f8                	je     80101d10 <namex+0xb0>

  while((path = skipelem(path, name)) != 0){
    ilock(ip);
80101d18:	83 ec 0c             	sub    $0xc,%esp
80101d1b:	56                   	push   %esi
80101d1c:	e8 5f f9 ff ff       	call   80101680 <ilock>
    if(ip->type != T_DIR){
80101d21:	83 c4 10             	add    $0x10,%esp
80101d24:	66 83 7e 50 01       	cmpw   $0x1,0x50(%esi)
80101d29:	0f 85 91 00 00 00    	jne    80101dc0 <namex+0x160>
      iunlockput(ip);
      return 0;
    }
    if(nameiparent && *path == '\0'){
80101d2f:	8b 55 e0             	mov    -0x20(%ebp),%edx
80101d32:	85 d2                	test   %edx,%edx
80101d34:	74 09                	je     80101d3f <namex+0xdf>
80101d36:	80 3b 00             	cmpb   $0x0,(%ebx)
80101d39:	0f 84 b7 00 00 00    	je     80101df6 <namex+0x196>
      // Stop one level early.
      iunlock(ip);
      return ip;
    }
    if((next = dirlookup(ip, name, 0)) == 0){
80101d3f:	83 ec 04             	sub    $0x4,%esp
80101d42:	6a 00                	push   $0x0
80101d44:	57                   	push   %edi
80101d45:	56                   	push   %esi
80101d46:	e8 65 fe ff ff       	call   80101bb0 <dirlookup>
80101d4b:	83 c4 10             	add    $0x10,%esp
80101d4e:	85 c0                	test   %eax,%eax
80101d50:	74 6e                	je     80101dc0 <namex+0x160>
  iunlock(ip);
80101d52:	83 ec 0c             	sub    $0xc,%esp
80101d55:	89 45 e4             	mov    %eax,-0x1c(%ebp)
80101d58:	56                   	push   %esi
80101d59:	e8 02 fa ff ff       	call   80101760 <iunlock>
  iput(ip);
80101d5e:	89 34 24             	mov    %esi,(%esp)
80101d61:	e8 4a fa ff ff       	call   801017b0 <iput>
80101d66:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80101d69:	83 c4 10             	add    $0x10,%esp
80101d6c:	89 c6                	mov    %eax,%esi
80101d6e:	e9 38 ff ff ff       	jmp    80101cab <namex+0x4b>
80101d73:	90                   	nop
80101d74:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  while(*path != '/' && *path != 0)
80101d78:	89 da                	mov    %ebx,%edx
80101d7a:	31 c9                	xor    %ecx,%ecx
    memmove(name, s, len);
80101d7c:	83 ec 04             	sub    $0x4,%esp
80101d7f:	89 55 dc             	mov    %edx,-0x24(%ebp)
80101d82:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
80101d85:	51                   	push   %ecx
80101d86:	53                   	push   %ebx
80101d87:	57                   	push   %edi
80101d88:	e8 03 2c 00 00       	call   80104990 <memmove>
    name[len] = 0;
80101d8d:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
80101d90:	8b 55 dc             	mov    -0x24(%ebp),%edx
80101d93:	83 c4 10             	add    $0x10,%esp
80101d96:	c6 04 0f 00          	movb   $0x0,(%edi,%ecx,1)
80101d9a:	89 d3                	mov    %edx,%ebx
80101d9c:	e9 61 ff ff ff       	jmp    80101d02 <namex+0xa2>
80101da1:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
      return 0;
    }
    iunlockput(ip);
    ip = next;
  }
  if(nameiparent){
80101da8:	8b 45 e0             	mov    -0x20(%ebp),%eax
80101dab:	85 c0                	test   %eax,%eax
80101dad:	75 5d                	jne    80101e0c <namex+0x1ac>
    iput(ip);
    return 0;
  }
  return ip;
}
80101daf:	8d 65 f4             	lea    -0xc(%ebp),%esp
80101db2:	89 f0                	mov    %esi,%eax
80101db4:	5b                   	pop    %ebx
80101db5:	5e                   	pop    %esi
80101db6:	5f                   	pop    %edi
80101db7:	5d                   	pop    %ebp
80101db8:	c3                   	ret    
80101db9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
  iunlock(ip);
80101dc0:	83 ec 0c             	sub    $0xc,%esp
80101dc3:	56                   	push   %esi
80101dc4:	e8 97 f9 ff ff       	call   80101760 <iunlock>
  iput(ip);
80101dc9:	89 34 24             	mov    %esi,(%esp)
      return 0;
80101dcc:	31 f6                	xor    %esi,%esi
  iput(ip);
80101dce:	e8 dd f9 ff ff       	call   801017b0 <iput>
      return 0;
80101dd3:	83 c4 10             	add    $0x10,%esp
}
80101dd6:	8d 65 f4             	lea    -0xc(%ebp),%esp
80101dd9:	89 f0                	mov    %esi,%eax
80101ddb:	5b                   	pop    %ebx
80101ddc:	5e                   	pop    %esi
80101ddd:	5f                   	pop    %edi
80101dde:	5d                   	pop    %ebp
80101ddf:	c3                   	ret    
    ip = iget(ROOTDEV, ROOTINO);
80101de0:	ba 01 00 00 00       	mov    $0x1,%edx
80101de5:	b8 01 00 00 00       	mov    $0x1,%eax
80101dea:	e8 a1 f4 ff ff       	call   80101290 <iget>
80101def:	89 c6                	mov    %eax,%esi
80101df1:	e9 b5 fe ff ff       	jmp    80101cab <namex+0x4b>
      iunlock(ip);
80101df6:	83 ec 0c             	sub    $0xc,%esp
80101df9:	56                   	push   %esi
80101dfa:	e8 61 f9 ff ff       	call   80101760 <iunlock>
      return ip;
80101dff:	83 c4 10             	add    $0x10,%esp
}
80101e02:	8d 65 f4             	lea    -0xc(%ebp),%esp
80101e05:	89 f0                	mov    %esi,%eax
80101e07:	5b                   	pop    %ebx
80101e08:	5e                   	pop    %esi
80101e09:	5f                   	pop    %edi
80101e0a:	5d                   	pop    %ebp
80101e0b:	c3                   	ret    
    iput(ip);
80101e0c:	83 ec 0c             	sub    $0xc,%esp
80101e0f:	56                   	push   %esi
    return 0;
80101e10:	31 f6                	xor    %esi,%esi
    iput(ip);
80101e12:	e8 99 f9 ff ff       	call   801017b0 <iput>
    return 0;
80101e17:	83 c4 10             	add    $0x10,%esp
80101e1a:	eb 93                	jmp    80101daf <namex+0x14f>
80101e1c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

80101e20 <dirlink>:
{
80101e20:	55                   	push   %ebp
80101e21:	89 e5                	mov    %esp,%ebp
80101e23:	57                   	push   %edi
80101e24:	56                   	push   %esi
80101e25:	53                   	push   %ebx
80101e26:	83 ec 20             	sub    $0x20,%esp
80101e29:	8b 5d 08             	mov    0x8(%ebp),%ebx
  if((ip = dirlookup(dp, name, 0)) != 0){
80101e2c:	6a 00                	push   $0x0
80101e2e:	ff 75 0c             	pushl  0xc(%ebp)
80101e31:	53                   	push   %ebx
80101e32:	e8 79 fd ff ff       	call   80101bb0 <dirlookup>
80101e37:	83 c4 10             	add    $0x10,%esp
80101e3a:	85 c0                	test   %eax,%eax
80101e3c:	75 67                	jne    80101ea5 <dirlink+0x85>
  for(off = 0; off < dp->size; off += sizeof(de)){
80101e3e:	8b 7b 58             	mov    0x58(%ebx),%edi
80101e41:	8d 75 d8             	lea    -0x28(%ebp),%esi
80101e44:	85 ff                	test   %edi,%edi
80101e46:	74 29                	je     80101e71 <dirlink+0x51>
80101e48:	31 ff                	xor    %edi,%edi
80101e4a:	8d 75 d8             	lea    -0x28(%ebp),%esi
80101e4d:	eb 09                	jmp    80101e58 <dirlink+0x38>
80101e4f:	90                   	nop
80101e50:	83 c7 10             	add    $0x10,%edi
80101e53:	3b 7b 58             	cmp    0x58(%ebx),%edi
80101e56:	73 19                	jae    80101e71 <dirlink+0x51>
    if(readi(dp, (char*)&de, off, sizeof(de)) != sizeof(de))
80101e58:	6a 10                	push   $0x10
80101e5a:	57                   	push   %edi
80101e5b:	56                   	push   %esi
80101e5c:	53                   	push   %ebx
80101e5d:	e8 fe fa ff ff       	call   80101960 <readi>
80101e62:	83 c4 10             	add    $0x10,%esp
80101e65:	83 f8 10             	cmp    $0x10,%eax
80101e68:	75 4e                	jne    80101eb8 <dirlink+0x98>
    if(de.inum == 0)
80101e6a:	66 83 7d d8 00       	cmpw   $0x0,-0x28(%ebp)
80101e6f:	75 df                	jne    80101e50 <dirlink+0x30>
  strncpy(de.name, name, DIRSIZ);
80101e71:	8d 45 da             	lea    -0x26(%ebp),%eax
80101e74:	83 ec 04             	sub    $0x4,%esp
80101e77:	6a 0e                	push   $0xe
80101e79:	ff 75 0c             	pushl  0xc(%ebp)
80101e7c:	50                   	push   %eax
80101e7d:	e8 de 2b 00 00       	call   80104a60 <strncpy>
  de.inum = inum;
80101e82:	8b 45 10             	mov    0x10(%ebp),%eax
  if(writei(dp, (char*)&de, off, sizeof(de)) != sizeof(de))
80101e85:	6a 10                	push   $0x10
80101e87:	57                   	push   %edi
80101e88:	56                   	push   %esi
80101e89:	53                   	push   %ebx
  de.inum = inum;
80101e8a:	66 89 45 d8          	mov    %ax,-0x28(%ebp)
  if(writei(dp, (char*)&de, off, sizeof(de)) != sizeof(de))
80101e8e:	e8 cd fb ff ff       	call   80101a60 <writei>
80101e93:	83 c4 20             	add    $0x20,%esp
80101e96:	83 f8 10             	cmp    $0x10,%eax
80101e99:	75 2a                	jne    80101ec5 <dirlink+0xa5>
  return 0;
80101e9b:	31 c0                	xor    %eax,%eax
}
80101e9d:	8d 65 f4             	lea    -0xc(%ebp),%esp
80101ea0:	5b                   	pop    %ebx
80101ea1:	5e                   	pop    %esi
80101ea2:	5f                   	pop    %edi
80101ea3:	5d                   	pop    %ebp
80101ea4:	c3                   	ret    
    iput(ip);
80101ea5:	83 ec 0c             	sub    $0xc,%esp
80101ea8:	50                   	push   %eax
80101ea9:	e8 02 f9 ff ff       	call   801017b0 <iput>
    return -1;
80101eae:	83 c4 10             	add    $0x10,%esp
80101eb1:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80101eb6:	eb e5                	jmp    80101e9d <dirlink+0x7d>
      panic("dirlink read");
80101eb8:	83 ec 0c             	sub    $0xc,%esp
80101ebb:	68 a8 78 10 80       	push   $0x801078a8
80101ec0:	e8 cb e4 ff ff       	call   80100390 <panic>
    panic("dirlink");
80101ec5:	83 ec 0c             	sub    $0xc,%esp
80101ec8:	68 2d 7f 10 80       	push   $0x80107f2d
80101ecd:	e8 be e4 ff ff       	call   80100390 <panic>
80101ed2:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80101ed9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80101ee0 <namei>:

struct inode*
namei(char *path)
{
80101ee0:	55                   	push   %ebp
  char name[DIRSIZ];
  return namex(path, 0, name);
80101ee1:	31 d2                	xor    %edx,%edx
{
80101ee3:	89 e5                	mov    %esp,%ebp
80101ee5:	83 ec 18             	sub    $0x18,%esp
  return namex(path, 0, name);
80101ee8:	8b 45 08             	mov    0x8(%ebp),%eax
80101eeb:	8d 4d ea             	lea    -0x16(%ebp),%ecx
80101eee:	e8 6d fd ff ff       	call   80101c60 <namex>
}
80101ef3:	c9                   	leave  
80101ef4:	c3                   	ret    
80101ef5:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80101ef9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80101f00 <nameiparent>:

struct inode*
nameiparent(char *path, char *name)
{
80101f00:	55                   	push   %ebp
  return namex(path, 1, name);
80101f01:	ba 01 00 00 00       	mov    $0x1,%edx
{
80101f06:	89 e5                	mov    %esp,%ebp
  return namex(path, 1, name);
80101f08:	8b 4d 0c             	mov    0xc(%ebp),%ecx
80101f0b:	8b 45 08             	mov    0x8(%ebp),%eax
}
80101f0e:	5d                   	pop    %ebp
  return namex(path, 1, name);
80101f0f:	e9 4c fd ff ff       	jmp    80101c60 <namex>
80101f14:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
80101f1a:	8d bf 00 00 00 00    	lea    0x0(%edi),%edi

80101f20 <itoa>:

#include "fcntl.h"
#define DIGITS 14

char* itoa(int i, char b[]){
80101f20:	55                   	push   %ebp
    char const digit[] = "0123456789";
80101f21:	b8 38 39 00 00       	mov    $0x3938,%eax
char* itoa(int i, char b[]){
80101f26:	89 e5                	mov    %esp,%ebp
80101f28:	57                   	push   %edi
80101f29:	56                   	push   %esi
80101f2a:	53                   	push   %ebx
80101f2b:	83 ec 10             	sub    $0x10,%esp
80101f2e:	8b 4d 08             	mov    0x8(%ebp),%ecx
    char const digit[] = "0123456789";
80101f31:	c7 45 e9 30 31 32 33 	movl   $0x33323130,-0x17(%ebp)
80101f38:	c7 45 ed 34 35 36 37 	movl   $0x37363534,-0x13(%ebp)
80101f3f:	66 89 45 f1          	mov    %ax,-0xf(%ebp)
80101f43:	c6 45 f3 00          	movb   $0x0,-0xd(%ebp)
80101f47:	8b 75 0c             	mov    0xc(%ebp),%esi
    char* p = b;
    if(i<0){
80101f4a:	85 c9                	test   %ecx,%ecx
80101f4c:	79 0a                	jns    80101f58 <itoa+0x38>
80101f4e:	89 f0                	mov    %esi,%eax
80101f50:	8d 76 01             	lea    0x1(%esi),%esi
        *p++ = '-';
        i *= -1;
80101f53:	f7 d9                	neg    %ecx
        *p++ = '-';
80101f55:	c6 00 2d             	movb   $0x2d,(%eax)
    }
    int shifter = i;
80101f58:	89 cb                	mov    %ecx,%ebx
    do{ //Move to where representation ends
        ++p;
        shifter = shifter/10;
80101f5a:	bf 67 66 66 66       	mov    $0x66666667,%edi
80101f5f:	90                   	nop
80101f60:	89 d8                	mov    %ebx,%eax
80101f62:	c1 fb 1f             	sar    $0x1f,%ebx
        ++p;
80101f65:	83 c6 01             	add    $0x1,%esi
        shifter = shifter/10;
80101f68:	f7 ef                	imul   %edi
80101f6a:	c1 fa 02             	sar    $0x2,%edx
    }while(shifter);
80101f6d:	29 da                	sub    %ebx,%edx
80101f6f:	89 d3                	mov    %edx,%ebx
80101f71:	75 ed                	jne    80101f60 <itoa+0x40>
    *p = '\0';
80101f73:	c6 06 00             	movb   $0x0,(%esi)
    do{ //Move back, inserting digits as u go
        *--p = digit[i%10];
80101f76:	bb 67 66 66 66       	mov    $0x66666667,%ebx
80101f7b:	90                   	nop
80101f7c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80101f80:	89 c8                	mov    %ecx,%eax
80101f82:	83 ee 01             	sub    $0x1,%esi
80101f85:	f7 eb                	imul   %ebx
80101f87:	89 c8                	mov    %ecx,%eax
80101f89:	c1 f8 1f             	sar    $0x1f,%eax
80101f8c:	c1 fa 02             	sar    $0x2,%edx
80101f8f:	29 c2                	sub    %eax,%edx
80101f91:	8d 04 92             	lea    (%edx,%edx,4),%eax
80101f94:	01 c0                	add    %eax,%eax
80101f96:	29 c1                	sub    %eax,%ecx
        i = i/10;
    }while(i);
80101f98:	85 d2                	test   %edx,%edx
        *--p = digit[i%10];
80101f9a:	0f b6 44 0d e9       	movzbl -0x17(%ebp,%ecx,1),%eax
        i = i/10;
80101f9f:	89 d1                	mov    %edx,%ecx
        *--p = digit[i%10];
80101fa1:	88 06                	mov    %al,(%esi)
    }while(i);
80101fa3:	75 db                	jne    80101f80 <itoa+0x60>
    return b;
}
80101fa5:	8b 45 0c             	mov    0xc(%ebp),%eax
80101fa8:	83 c4 10             	add    $0x10,%esp
80101fab:	5b                   	pop    %ebx
80101fac:	5e                   	pop    %esi
80101fad:	5f                   	pop    %edi
80101fae:	5d                   	pop    %ebp
80101faf:	c3                   	ret    

80101fb0 <removeSwapFile>:
//remove swap file of proc p;
int
removeSwapFile(struct proc* p)
{
80101fb0:	55                   	push   %ebp
80101fb1:	89 e5                	mov    %esp,%ebp
80101fb3:	57                   	push   %edi
80101fb4:	56                   	push   %esi
80101fb5:	53                   	push   %ebx
  //path of proccess
  char path[DIGITS];
  memmove(path,"/.swap", 6);
80101fb6:	8d 75 bc             	lea    -0x44(%ebp),%esi
{
80101fb9:	83 ec 40             	sub    $0x40,%esp
80101fbc:	8b 5d 08             	mov    0x8(%ebp),%ebx
  memmove(path,"/.swap", 6);
80101fbf:	6a 06                	push   $0x6
80101fc1:	68 b5 78 10 80       	push   $0x801078b5
80101fc6:	56                   	push   %esi
80101fc7:	e8 c4 29 00 00       	call   80104990 <memmove>
  itoa(p->pid, path+ 6);
80101fcc:	58                   	pop    %eax
80101fcd:	8d 45 c2             	lea    -0x3e(%ebp),%eax
80101fd0:	5a                   	pop    %edx
80101fd1:	50                   	push   %eax
80101fd2:	ff 73 10             	pushl  0x10(%ebx)
80101fd5:	e8 46 ff ff ff       	call   80101f20 <itoa>
  struct inode *ip, *dp;
  struct dirent de;
  char name[DIRSIZ];
  uint off;

  if(0 == p->swapFile)
80101fda:	8b 43 7c             	mov    0x7c(%ebx),%eax
80101fdd:	83 c4 10             	add    $0x10,%esp
80101fe0:	85 c0                	test   %eax,%eax
80101fe2:	0f 84 88 01 00 00    	je     80102170 <removeSwapFile+0x1c0>
  {
    return -1;
  }
  fileclose(p->swapFile);
80101fe8:	83 ec 0c             	sub    $0xc,%esp
  return namex(path, 1, name);
80101feb:	8d 5d ca             	lea    -0x36(%ebp),%ebx
  fileclose(p->swapFile);
80101fee:	50                   	push   %eax
80101fef:	e8 4c ee ff ff       	call   80100e40 <fileclose>

  begin_op();
80101ff4:	e8 a7 0f 00 00       	call   80102fa0 <begin_op>
  return namex(path, 1, name);
80101ff9:	89 f0                	mov    %esi,%eax
80101ffb:	89 d9                	mov    %ebx,%ecx
80101ffd:	ba 01 00 00 00       	mov    $0x1,%edx
80102002:	e8 59 fc ff ff       	call   80101c60 <namex>
  if((dp = nameiparent(path, name)) == 0)
80102007:	83 c4 10             	add    $0x10,%esp
8010200a:	85 c0                	test   %eax,%eax
  return namex(path, 1, name);
8010200c:	89 c6                	mov    %eax,%esi
  if((dp = nameiparent(path, name)) == 0)
8010200e:	0f 84 66 01 00 00    	je     8010217a <removeSwapFile+0x1ca>
  {
    end_op();
    return -1;
  }

  ilock(dp);
80102014:	83 ec 0c             	sub    $0xc,%esp
80102017:	50                   	push   %eax
80102018:	e8 63 f6 ff ff       	call   80101680 <ilock>
  return strncmp(s, t, DIRSIZ);
8010201d:	83 c4 0c             	add    $0xc,%esp
80102020:	6a 0e                	push   $0xe
80102022:	68 bd 78 10 80       	push   $0x801078bd
80102027:	53                   	push   %ebx
80102028:	e8 d3 29 00 00       	call   80104a00 <strncmp>

    // Cannot unlink "." or "..".
  if(namecmp(name, ".") == 0 || namecmp(name, "..") == 0)
8010202d:	83 c4 10             	add    $0x10,%esp
80102030:	85 c0                	test   %eax,%eax
80102032:	0f 84 f8 00 00 00    	je     80102130 <removeSwapFile+0x180>
  return strncmp(s, t, DIRSIZ);
80102038:	83 ec 04             	sub    $0x4,%esp
8010203b:	6a 0e                	push   $0xe
8010203d:	68 bc 78 10 80       	push   $0x801078bc
80102042:	53                   	push   %ebx
80102043:	e8 b8 29 00 00       	call   80104a00 <strncmp>
  if(namecmp(name, ".") == 0 || namecmp(name, "..") == 0)
80102048:	83 c4 10             	add    $0x10,%esp
8010204b:	85 c0                	test   %eax,%eax
8010204d:	0f 84 dd 00 00 00    	je     80102130 <removeSwapFile+0x180>
     goto bad;

  if((ip = dirlookup(dp, name, &off)) == 0)
80102053:	8d 45 b8             	lea    -0x48(%ebp),%eax
80102056:	83 ec 04             	sub    $0x4,%esp
80102059:	50                   	push   %eax
8010205a:	53                   	push   %ebx
8010205b:	56                   	push   %esi
8010205c:	e8 4f fb ff ff       	call   80101bb0 <dirlookup>
80102061:	83 c4 10             	add    $0x10,%esp
80102064:	85 c0                	test   %eax,%eax
80102066:	89 c3                	mov    %eax,%ebx
80102068:	0f 84 c2 00 00 00    	je     80102130 <removeSwapFile+0x180>
    goto bad;
  ilock(ip);
8010206e:	83 ec 0c             	sub    $0xc,%esp
80102071:	50                   	push   %eax
80102072:	e8 09 f6 ff ff       	call   80101680 <ilock>

  if(ip->nlink < 1)
80102077:	83 c4 10             	add    $0x10,%esp
8010207a:	66 83 7b 56 00       	cmpw   $0x0,0x56(%ebx)
8010207f:	0f 8e 11 01 00 00    	jle    80102196 <removeSwapFile+0x1e6>
    panic("unlink: nlink < 1");
  if(ip->type == T_DIR && !isdirempty(ip)){
80102085:	66 83 7b 50 01       	cmpw   $0x1,0x50(%ebx)
8010208a:	74 74                	je     80102100 <removeSwapFile+0x150>
    iunlockput(ip);
    goto bad;
  }

  memset(&de, 0, sizeof(de));
8010208c:	8d 7d d8             	lea    -0x28(%ebp),%edi
8010208f:	83 ec 04             	sub    $0x4,%esp
80102092:	6a 10                	push   $0x10
80102094:	6a 00                	push   $0x0
80102096:	57                   	push   %edi
80102097:	e8 44 28 00 00       	call   801048e0 <memset>
  if(writei(dp, (char*)&de, off, sizeof(de)) != sizeof(de))
8010209c:	6a 10                	push   $0x10
8010209e:	ff 75 b8             	pushl  -0x48(%ebp)
801020a1:	57                   	push   %edi
801020a2:	56                   	push   %esi
801020a3:	e8 b8 f9 ff ff       	call   80101a60 <writei>
801020a8:	83 c4 20             	add    $0x20,%esp
801020ab:	83 f8 10             	cmp    $0x10,%eax
801020ae:	0f 85 d5 00 00 00    	jne    80102189 <removeSwapFile+0x1d9>
    panic("unlink: writei");
  if(ip->type == T_DIR){
801020b4:	66 83 7b 50 01       	cmpw   $0x1,0x50(%ebx)
801020b9:	0f 84 91 00 00 00    	je     80102150 <removeSwapFile+0x1a0>
  iunlock(ip);
801020bf:	83 ec 0c             	sub    $0xc,%esp
801020c2:	56                   	push   %esi
801020c3:	e8 98 f6 ff ff       	call   80101760 <iunlock>
  iput(ip);
801020c8:	89 34 24             	mov    %esi,(%esp)
801020cb:	e8 e0 f6 ff ff       	call   801017b0 <iput>
    dp->nlink--;
    iupdate(dp);
  }
  iunlockput(dp);

  ip->nlink--;
801020d0:	66 83 6b 56 01       	subw   $0x1,0x56(%ebx)
  iupdate(ip);
801020d5:	89 1c 24             	mov    %ebx,(%esp)
801020d8:	e8 f3 f4 ff ff       	call   801015d0 <iupdate>
  iunlock(ip);
801020dd:	89 1c 24             	mov    %ebx,(%esp)
801020e0:	e8 7b f6 ff ff       	call   80101760 <iunlock>
  iput(ip);
801020e5:	89 1c 24             	mov    %ebx,(%esp)
801020e8:	e8 c3 f6 ff ff       	call   801017b0 <iput>
  iunlockput(ip);

  end_op();
801020ed:	e8 1e 0f 00 00       	call   80103010 <end_op>

  return 0;
801020f2:	83 c4 10             	add    $0x10,%esp
801020f5:	31 c0                	xor    %eax,%eax
  bad:
    iunlockput(dp);
    end_op();
    return -1;

}
801020f7:	8d 65 f4             	lea    -0xc(%ebp),%esp
801020fa:	5b                   	pop    %ebx
801020fb:	5e                   	pop    %esi
801020fc:	5f                   	pop    %edi
801020fd:	5d                   	pop    %ebp
801020fe:	c3                   	ret    
801020ff:	90                   	nop
  if(ip->type == T_DIR && !isdirempty(ip)){
80102100:	83 ec 0c             	sub    $0xc,%esp
80102103:	53                   	push   %ebx
80102104:	e8 b7 2f 00 00       	call   801050c0 <isdirempty>
80102109:	83 c4 10             	add    $0x10,%esp
8010210c:	85 c0                	test   %eax,%eax
8010210e:	0f 85 78 ff ff ff    	jne    8010208c <removeSwapFile+0xdc>
  iunlock(ip);
80102114:	83 ec 0c             	sub    $0xc,%esp
80102117:	53                   	push   %ebx
80102118:	e8 43 f6 ff ff       	call   80101760 <iunlock>
  iput(ip);
8010211d:	89 1c 24             	mov    %ebx,(%esp)
80102120:	e8 8b f6 ff ff       	call   801017b0 <iput>
80102125:	83 c4 10             	add    $0x10,%esp
80102128:	90                   	nop
80102129:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
  iunlock(ip);
80102130:	83 ec 0c             	sub    $0xc,%esp
80102133:	56                   	push   %esi
80102134:	e8 27 f6 ff ff       	call   80101760 <iunlock>
  iput(ip);
80102139:	89 34 24             	mov    %esi,(%esp)
8010213c:	e8 6f f6 ff ff       	call   801017b0 <iput>
    end_op();
80102141:	e8 ca 0e 00 00       	call   80103010 <end_op>
    return -1;
80102146:	83 c4 10             	add    $0x10,%esp
80102149:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
8010214e:	eb a7                	jmp    801020f7 <removeSwapFile+0x147>
    dp->nlink--;
80102150:	66 83 6e 56 01       	subw   $0x1,0x56(%esi)
    iupdate(dp);
80102155:	83 ec 0c             	sub    $0xc,%esp
80102158:	56                   	push   %esi
80102159:	e8 72 f4 ff ff       	call   801015d0 <iupdate>
8010215e:	83 c4 10             	add    $0x10,%esp
80102161:	e9 59 ff ff ff       	jmp    801020bf <removeSwapFile+0x10f>
80102166:	8d 76 00             	lea    0x0(%esi),%esi
80102169:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi
    return -1;
80102170:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80102175:	e9 7d ff ff ff       	jmp    801020f7 <removeSwapFile+0x147>
    end_op();
8010217a:	e8 91 0e 00 00       	call   80103010 <end_op>
    return -1;
8010217f:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80102184:	e9 6e ff ff ff       	jmp    801020f7 <removeSwapFile+0x147>
    panic("unlink: writei");
80102189:	83 ec 0c             	sub    $0xc,%esp
8010218c:	68 d1 78 10 80       	push   $0x801078d1
80102191:	e8 fa e1 ff ff       	call   80100390 <panic>
    panic("unlink: nlink < 1");
80102196:	83 ec 0c             	sub    $0xc,%esp
80102199:	68 bf 78 10 80       	push   $0x801078bf
8010219e:	e8 ed e1 ff ff       	call   80100390 <panic>
801021a3:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
801021a9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

801021b0 <createSwapFile>:


//return 0 on success
int
createSwapFile(struct proc* p)
{
801021b0:	55                   	push   %ebp
801021b1:	89 e5                	mov    %esp,%ebp
801021b3:	56                   	push   %esi
801021b4:	53                   	push   %ebx

  char path[DIGITS];
  memmove(path,"/.swap", 6);
801021b5:	8d 75 ea             	lea    -0x16(%ebp),%esi
{
801021b8:	83 ec 14             	sub    $0x14,%esp
801021bb:	8b 5d 08             	mov    0x8(%ebp),%ebx
  memmove(path,"/.swap", 6);
801021be:	6a 06                	push   $0x6
801021c0:	68 b5 78 10 80       	push   $0x801078b5
801021c5:	56                   	push   %esi
801021c6:	e8 c5 27 00 00       	call   80104990 <memmove>
  itoa(p->pid, path+ 6);
801021cb:	58                   	pop    %eax
801021cc:	8d 45 f0             	lea    -0x10(%ebp),%eax
801021cf:	5a                   	pop    %edx
801021d0:	50                   	push   %eax
801021d1:	ff 73 10             	pushl  0x10(%ebx)
801021d4:	e8 47 fd ff ff       	call   80101f20 <itoa>

    begin_op();
801021d9:	e8 c2 0d 00 00       	call   80102fa0 <begin_op>
    struct inode * in = create(path, T_FILE, 0, 0);
801021de:	6a 00                	push   $0x0
801021e0:	6a 00                	push   $0x0
801021e2:	6a 02                	push   $0x2
801021e4:	56                   	push   %esi
801021e5:	e8 e6 30 00 00       	call   801052d0 <create>
  iunlock(in);
801021ea:	83 c4 14             	add    $0x14,%esp
    struct inode * in = create(path, T_FILE, 0, 0);
801021ed:	89 c6                	mov    %eax,%esi
  iunlock(in);
801021ef:	50                   	push   %eax
801021f0:	e8 6b f5 ff ff       	call   80101760 <iunlock>

  p->swapFile = filealloc();
801021f5:	e8 86 eb ff ff       	call   80100d80 <filealloc>
  if (p->swapFile == 0)
801021fa:	83 c4 10             	add    $0x10,%esp
801021fd:	85 c0                	test   %eax,%eax
  p->swapFile = filealloc();
801021ff:	89 43 7c             	mov    %eax,0x7c(%ebx)
  if (p->swapFile == 0)
80102202:	74 32                	je     80102236 <createSwapFile+0x86>
    panic("no slot for files on /store");

  p->swapFile->ip = in;
80102204:	89 70 10             	mov    %esi,0x10(%eax)
  p->swapFile->type = FD_INODE;
80102207:	8b 43 7c             	mov    0x7c(%ebx),%eax
8010220a:	c7 00 02 00 00 00    	movl   $0x2,(%eax)
  p->swapFile->off = 0;
80102210:	8b 43 7c             	mov    0x7c(%ebx),%eax
80102213:	c7 40 14 00 00 00 00 	movl   $0x0,0x14(%eax)
  p->swapFile->readable = O_WRONLY;
8010221a:	8b 43 7c             	mov    0x7c(%ebx),%eax
8010221d:	c6 40 08 01          	movb   $0x1,0x8(%eax)
  p->swapFile->writable = O_RDWR;
80102221:	8b 43 7c             	mov    0x7c(%ebx),%eax
80102224:	c6 40 09 02          	movb   $0x2,0x9(%eax)
    end_op();
80102228:	e8 e3 0d 00 00       	call   80103010 <end_op>

    return 0;
}
8010222d:	8d 65 f8             	lea    -0x8(%ebp),%esp
80102230:	31 c0                	xor    %eax,%eax
80102232:	5b                   	pop    %ebx
80102233:	5e                   	pop    %esi
80102234:	5d                   	pop    %ebp
80102235:	c3                   	ret    
    panic("no slot for files on /store");
80102236:	83 ec 0c             	sub    $0xc,%esp
80102239:	68 e0 78 10 80       	push   $0x801078e0
8010223e:	e8 4d e1 ff ff       	call   80100390 <panic>
80102243:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
80102249:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80102250 <writeToSwapFile>:

//return as sys_write (-1 when error)
int
writeToSwapFile(struct proc * p, char* buffer, uint placeOnFile, uint size)
{
80102250:	55                   	push   %ebp
80102251:	89 e5                	mov    %esp,%ebp
80102253:	8b 45 08             	mov    0x8(%ebp),%eax
  p->swapFile->off = placeOnFile;
80102256:	8b 4d 10             	mov    0x10(%ebp),%ecx
80102259:	8b 50 7c             	mov    0x7c(%eax),%edx
8010225c:	89 4a 14             	mov    %ecx,0x14(%edx)

  return filewrite(p->swapFile, buffer, size);
8010225f:	8b 55 14             	mov    0x14(%ebp),%edx
80102262:	89 55 10             	mov    %edx,0x10(%ebp)
80102265:	8b 40 7c             	mov    0x7c(%eax),%eax
80102268:	89 45 08             	mov    %eax,0x8(%ebp)

}
8010226b:	5d                   	pop    %ebp
  return filewrite(p->swapFile, buffer, size);
8010226c:	e9 7f ed ff ff       	jmp    80100ff0 <filewrite>
80102271:	eb 0d                	jmp    80102280 <readFromSwapFile>
80102273:	90                   	nop
80102274:	90                   	nop
80102275:	90                   	nop
80102276:	90                   	nop
80102277:	90                   	nop
80102278:	90                   	nop
80102279:	90                   	nop
8010227a:	90                   	nop
8010227b:	90                   	nop
8010227c:	90                   	nop
8010227d:	90                   	nop
8010227e:	90                   	nop
8010227f:	90                   	nop

80102280 <readFromSwapFile>:

//return as sys_read (-1 when error)
int
readFromSwapFile(struct proc * p, char* buffer, uint placeOnFile, uint size)
{
80102280:	55                   	push   %ebp
80102281:	89 e5                	mov    %esp,%ebp
80102283:	8b 45 08             	mov    0x8(%ebp),%eax
  p->swapFile->off = placeOnFile;
80102286:	8b 4d 10             	mov    0x10(%ebp),%ecx
80102289:	8b 50 7c             	mov    0x7c(%eax),%edx
8010228c:	89 4a 14             	mov    %ecx,0x14(%edx)

  return fileread(p->swapFile, buffer,  size);
8010228f:	8b 55 14             	mov    0x14(%ebp),%edx
80102292:	89 55 10             	mov    %edx,0x10(%ebp)
80102295:	8b 40 7c             	mov    0x7c(%eax),%eax
80102298:	89 45 08             	mov    %eax,0x8(%ebp)
}
8010229b:	5d                   	pop    %ebp
  return fileread(p->swapFile, buffer,  size);
8010229c:	e9 bf ec ff ff       	jmp    80100f60 <fileread>
801022a1:	66 90                	xchg   %ax,%ax
801022a3:	66 90                	xchg   %ax,%ax
801022a5:	66 90                	xchg   %ax,%ax
801022a7:	66 90                	xchg   %ax,%ax
801022a9:	66 90                	xchg   %ax,%ax
801022ab:	66 90                	xchg   %ax,%ax
801022ad:	66 90                	xchg   %ax,%ax
801022af:	90                   	nop

801022b0 <idestart>:
}

// Start the request for b.  Caller must hold idelock.
static void
idestart(struct buf *b)
{
801022b0:	55                   	push   %ebp
801022b1:	89 e5                	mov    %esp,%ebp
801022b3:	57                   	push   %edi
801022b4:	56                   	push   %esi
801022b5:	53                   	push   %ebx
801022b6:	83 ec 0c             	sub    $0xc,%esp
  if(b == 0)
801022b9:	85 c0                	test   %eax,%eax
801022bb:	0f 84 b4 00 00 00    	je     80102375 <idestart+0xc5>
    panic("idestart");
  if(b->blockno >= FSSIZE)
801022c1:	8b 58 08             	mov    0x8(%eax),%ebx
801022c4:	89 c6                	mov    %eax,%esi
801022c6:	81 fb e7 03 00 00    	cmp    $0x3e7,%ebx
801022cc:	0f 87 96 00 00 00    	ja     80102368 <idestart+0xb8>
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
801022d2:	b9 f7 01 00 00       	mov    $0x1f7,%ecx
801022d7:	89 f6                	mov    %esi,%esi
801022d9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi
801022e0:	89 ca                	mov    %ecx,%edx
801022e2:	ec                   	in     (%dx),%al
  while(((r = inb(0x1f7)) & (IDE_BSY|IDE_DRDY)) != IDE_DRDY)
801022e3:	83 e0 c0             	and    $0xffffffc0,%eax
801022e6:	3c 40                	cmp    $0x40,%al
801022e8:	75 f6                	jne    801022e0 <idestart+0x30>
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
801022ea:	31 ff                	xor    %edi,%edi
801022ec:	ba f6 03 00 00       	mov    $0x3f6,%edx
801022f1:	89 f8                	mov    %edi,%eax
801022f3:	ee                   	out    %al,(%dx)
801022f4:	b8 01 00 00 00       	mov    $0x1,%eax
801022f9:	ba f2 01 00 00       	mov    $0x1f2,%edx
801022fe:	ee                   	out    %al,(%dx)
801022ff:	ba f3 01 00 00       	mov    $0x1f3,%edx
80102304:	89 d8                	mov    %ebx,%eax
80102306:	ee                   	out    %al,(%dx)

  idewait(0);
  outb(0x3f6, 0);  // generate interrupt
  outb(0x1f2, sector_per_block);  // number of sectors
  outb(0x1f3, sector & 0xff);
  outb(0x1f4, (sector >> 8) & 0xff);
80102307:	89 d8                	mov    %ebx,%eax
80102309:	ba f4 01 00 00       	mov    $0x1f4,%edx
8010230e:	c1 f8 08             	sar    $0x8,%eax
80102311:	ee                   	out    %al,(%dx)
80102312:	ba f5 01 00 00       	mov    $0x1f5,%edx
80102317:	89 f8                	mov    %edi,%eax
80102319:	ee                   	out    %al,(%dx)
  outb(0x1f5, (sector >> 16) & 0xff);
  outb(0x1f6, 0xe0 | ((b->dev&1)<<4) | ((sector>>24)&0x0f));
8010231a:	0f b6 46 04          	movzbl 0x4(%esi),%eax
8010231e:	ba f6 01 00 00       	mov    $0x1f6,%edx
80102323:	c1 e0 04             	shl    $0x4,%eax
80102326:	83 e0 10             	and    $0x10,%eax
80102329:	83 c8 e0             	or     $0xffffffe0,%eax
8010232c:	ee                   	out    %al,(%dx)
  if(b->flags & B_DIRTY){
8010232d:	f6 06 04             	testb  $0x4,(%esi)
80102330:	75 16                	jne    80102348 <idestart+0x98>
80102332:	b8 20 00 00 00       	mov    $0x20,%eax
80102337:	89 ca                	mov    %ecx,%edx
80102339:	ee                   	out    %al,(%dx)
    outb(0x1f7, write_cmd);
    outsl(0x1f0, b->data, BSIZE/4);
  } else {
    outb(0x1f7, read_cmd);
  }
}
8010233a:	8d 65 f4             	lea    -0xc(%ebp),%esp
8010233d:	5b                   	pop    %ebx
8010233e:	5e                   	pop    %esi
8010233f:	5f                   	pop    %edi
80102340:	5d                   	pop    %ebp
80102341:	c3                   	ret    
80102342:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
80102348:	b8 30 00 00 00       	mov    $0x30,%eax
8010234d:	89 ca                	mov    %ecx,%edx
8010234f:	ee                   	out    %al,(%dx)
  asm volatile("cld; rep outsl" :
80102350:	b9 80 00 00 00       	mov    $0x80,%ecx
    outsl(0x1f0, b->data, BSIZE/4);
80102355:	83 c6 5c             	add    $0x5c,%esi
80102358:	ba f0 01 00 00       	mov    $0x1f0,%edx
8010235d:	fc                   	cld    
8010235e:	f3 6f                	rep outsl %ds:(%esi),(%dx)
}
80102360:	8d 65 f4             	lea    -0xc(%ebp),%esp
80102363:	5b                   	pop    %ebx
80102364:	5e                   	pop    %esi
80102365:	5f                   	pop    %edi
80102366:	5d                   	pop    %ebp
80102367:	c3                   	ret    
    panic("incorrect blockno");
80102368:	83 ec 0c             	sub    $0xc,%esp
8010236b:	68 58 79 10 80       	push   $0x80107958
80102370:	e8 1b e0 ff ff       	call   80100390 <panic>
    panic("idestart");
80102375:	83 ec 0c             	sub    $0xc,%esp
80102378:	68 4f 79 10 80       	push   $0x8010794f
8010237d:	e8 0e e0 ff ff       	call   80100390 <panic>
80102382:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80102389:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80102390 <ideinit>:
{
80102390:	55                   	push   %ebp
80102391:	89 e5                	mov    %esp,%ebp
80102393:	83 ec 10             	sub    $0x10,%esp
  initlock(&idelock, "ide");
80102396:	68 6a 79 10 80       	push   $0x8010796a
8010239b:	68 80 b5 10 80       	push   $0x8010b580
801023a0:	e8 eb 22 00 00       	call   80104690 <initlock>
  ioapicenable(IRQ_IDE, ncpu - 1);
801023a5:	58                   	pop    %eax
801023a6:	a1 00 3d 11 80       	mov    0x80113d00,%eax
801023ab:	5a                   	pop    %edx
801023ac:	83 e8 01             	sub    $0x1,%eax
801023af:	50                   	push   %eax
801023b0:	6a 0e                	push   $0xe
801023b2:	e8 a9 02 00 00       	call   80102660 <ioapicenable>
801023b7:	83 c4 10             	add    $0x10,%esp
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
801023ba:	ba f7 01 00 00       	mov    $0x1f7,%edx
801023bf:	90                   	nop
801023c0:	ec                   	in     (%dx),%al
  while(((r = inb(0x1f7)) & (IDE_BSY|IDE_DRDY)) != IDE_DRDY)
801023c1:	83 e0 c0             	and    $0xffffffc0,%eax
801023c4:	3c 40                	cmp    $0x40,%al
801023c6:	75 f8                	jne    801023c0 <ideinit+0x30>
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
801023c8:	b8 f0 ff ff ff       	mov    $0xfffffff0,%eax
801023cd:	ba f6 01 00 00       	mov    $0x1f6,%edx
801023d2:	ee                   	out    %al,(%dx)
801023d3:	b9 e8 03 00 00       	mov    $0x3e8,%ecx
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
801023d8:	ba f7 01 00 00       	mov    $0x1f7,%edx
801023dd:	eb 06                	jmp    801023e5 <ideinit+0x55>
801023df:	90                   	nop
  for(i=0; i<1000; i++){
801023e0:	83 e9 01             	sub    $0x1,%ecx
801023e3:	74 0f                	je     801023f4 <ideinit+0x64>
801023e5:	ec                   	in     (%dx),%al
    if(inb(0x1f7) != 0){
801023e6:	84 c0                	test   %al,%al
801023e8:	74 f6                	je     801023e0 <ideinit+0x50>
      havedisk1 = 1;
801023ea:	c7 05 60 b5 10 80 01 	movl   $0x1,0x8010b560
801023f1:	00 00 00 
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
801023f4:	b8 e0 ff ff ff       	mov    $0xffffffe0,%eax
801023f9:	ba f6 01 00 00       	mov    $0x1f6,%edx
801023fe:	ee                   	out    %al,(%dx)
}
801023ff:	c9                   	leave  
80102400:	c3                   	ret    
80102401:	eb 0d                	jmp    80102410 <ideintr>
80102403:	90                   	nop
80102404:	90                   	nop
80102405:	90                   	nop
80102406:	90                   	nop
80102407:	90                   	nop
80102408:	90                   	nop
80102409:	90                   	nop
8010240a:	90                   	nop
8010240b:	90                   	nop
8010240c:	90                   	nop
8010240d:	90                   	nop
8010240e:	90                   	nop
8010240f:	90                   	nop

80102410 <ideintr>:

// Interrupt handler.
void
ideintr(void)
{
80102410:	55                   	push   %ebp
80102411:	89 e5                	mov    %esp,%ebp
80102413:	57                   	push   %edi
80102414:	56                   	push   %esi
80102415:	53                   	push   %ebx
80102416:	83 ec 18             	sub    $0x18,%esp
  struct buf *b;

  // First queued buffer is the active request.
  acquire(&idelock);
80102419:	68 80 b5 10 80       	push   $0x8010b580
8010241e:	e8 ad 23 00 00       	call   801047d0 <acquire>

  if((b = idequeue) == 0){
80102423:	8b 1d 64 b5 10 80    	mov    0x8010b564,%ebx
80102429:	83 c4 10             	add    $0x10,%esp
8010242c:	85 db                	test   %ebx,%ebx
8010242e:	74 67                	je     80102497 <ideintr+0x87>
    release(&idelock);
    return;
  }
  idequeue = b->qnext;
80102430:	8b 43 58             	mov    0x58(%ebx),%eax
80102433:	a3 64 b5 10 80       	mov    %eax,0x8010b564

  // Read data if needed.
  if(!(b->flags & B_DIRTY) && idewait(1) >= 0)
80102438:	8b 3b                	mov    (%ebx),%edi
8010243a:	f7 c7 04 00 00 00    	test   $0x4,%edi
80102440:	75 31                	jne    80102473 <ideintr+0x63>
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80102442:	ba f7 01 00 00       	mov    $0x1f7,%edx
80102447:	89 f6                	mov    %esi,%esi
80102449:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi
80102450:	ec                   	in     (%dx),%al
  while(((r = inb(0x1f7)) & (IDE_BSY|IDE_DRDY)) != IDE_DRDY)
80102451:	89 c6                	mov    %eax,%esi
80102453:	83 e6 c0             	and    $0xffffffc0,%esi
80102456:	89 f1                	mov    %esi,%ecx
80102458:	80 f9 40             	cmp    $0x40,%cl
8010245b:	75 f3                	jne    80102450 <ideintr+0x40>
  if(checkerr && (r & (IDE_DF|IDE_ERR)) != 0)
8010245d:	a8 21                	test   $0x21,%al
8010245f:	75 12                	jne    80102473 <ideintr+0x63>
    insl(0x1f0, b->data, BSIZE/4);
80102461:	8d 7b 5c             	lea    0x5c(%ebx),%edi
  asm volatile("cld; rep insl" :
80102464:	b9 80 00 00 00       	mov    $0x80,%ecx
80102469:	ba f0 01 00 00       	mov    $0x1f0,%edx
8010246e:	fc                   	cld    
8010246f:	f3 6d                	rep insl (%dx),%es:(%edi)
80102471:	8b 3b                	mov    (%ebx),%edi

  // Wake process waiting for this buf.
  b->flags |= B_VALID;
  b->flags &= ~B_DIRTY;
80102473:	83 e7 fb             	and    $0xfffffffb,%edi
  wakeup(b);
80102476:	83 ec 0c             	sub    $0xc,%esp
  b->flags &= ~B_DIRTY;
80102479:	89 f9                	mov    %edi,%ecx
8010247b:	83 c9 02             	or     $0x2,%ecx
8010247e:	89 0b                	mov    %ecx,(%ebx)
  wakeup(b);
80102480:	53                   	push   %ebx
80102481:	e8 2a 1f 00 00       	call   801043b0 <wakeup>

  // Start disk on next buf in queue.
  if(idequeue != 0)
80102486:	a1 64 b5 10 80       	mov    0x8010b564,%eax
8010248b:	83 c4 10             	add    $0x10,%esp
8010248e:	85 c0                	test   %eax,%eax
80102490:	74 05                	je     80102497 <ideintr+0x87>
    idestart(idequeue);
80102492:	e8 19 fe ff ff       	call   801022b0 <idestart>
    release(&idelock);
80102497:	83 ec 0c             	sub    $0xc,%esp
8010249a:	68 80 b5 10 80       	push   $0x8010b580
8010249f:	e8 ec 23 00 00       	call   80104890 <release>

  release(&idelock);
}
801024a4:	8d 65 f4             	lea    -0xc(%ebp),%esp
801024a7:	5b                   	pop    %ebx
801024a8:	5e                   	pop    %esi
801024a9:	5f                   	pop    %edi
801024aa:	5d                   	pop    %ebp
801024ab:	c3                   	ret    
801024ac:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

801024b0 <iderw>:
// Sync buf with disk.
// If B_DIRTY is set, write buf to disk, clear B_DIRTY, set B_VALID.
// Else if B_VALID is not set, read buf from disk, set B_VALID.
void
iderw(struct buf *b)
{
801024b0:	55                   	push   %ebp
801024b1:	89 e5                	mov    %esp,%ebp
801024b3:	53                   	push   %ebx
801024b4:	83 ec 10             	sub    $0x10,%esp
801024b7:	8b 5d 08             	mov    0x8(%ebp),%ebx
  struct buf **pp;

  if(!holdingsleep(&b->lock))
801024ba:	8d 43 0c             	lea    0xc(%ebx),%eax
801024bd:	50                   	push   %eax
801024be:	e8 7d 21 00 00       	call   80104640 <holdingsleep>
801024c3:	83 c4 10             	add    $0x10,%esp
801024c6:	85 c0                	test   %eax,%eax
801024c8:	0f 84 c6 00 00 00    	je     80102594 <iderw+0xe4>
    panic("iderw: buf not locked");
  if((b->flags & (B_VALID|B_DIRTY)) == B_VALID)
801024ce:	8b 03                	mov    (%ebx),%eax
801024d0:	83 e0 06             	and    $0x6,%eax
801024d3:	83 f8 02             	cmp    $0x2,%eax
801024d6:	0f 84 ab 00 00 00    	je     80102587 <iderw+0xd7>
    panic("iderw: nothing to do");
  if(b->dev != 0 && !havedisk1)
801024dc:	8b 53 04             	mov    0x4(%ebx),%edx
801024df:	85 d2                	test   %edx,%edx
801024e1:	74 0d                	je     801024f0 <iderw+0x40>
801024e3:	a1 60 b5 10 80       	mov    0x8010b560,%eax
801024e8:	85 c0                	test   %eax,%eax
801024ea:	0f 84 b1 00 00 00    	je     801025a1 <iderw+0xf1>
    panic("iderw: ide disk 1 not present");

  acquire(&idelock);  //DOC:acquire-lock
801024f0:	83 ec 0c             	sub    $0xc,%esp
801024f3:	68 80 b5 10 80       	push   $0x8010b580
801024f8:	e8 d3 22 00 00       	call   801047d0 <acquire>

  // Append b to idequeue.
  b->qnext = 0;
  for(pp=&idequeue; *pp; pp=&(*pp)->qnext)  //DOC:insert-queue
801024fd:	8b 15 64 b5 10 80    	mov    0x8010b564,%edx
80102503:	83 c4 10             	add    $0x10,%esp
  b->qnext = 0;
80102506:	c7 43 58 00 00 00 00 	movl   $0x0,0x58(%ebx)
  for(pp=&idequeue; *pp; pp=&(*pp)->qnext)  //DOC:insert-queue
8010250d:	85 d2                	test   %edx,%edx
8010250f:	75 09                	jne    8010251a <iderw+0x6a>
80102511:	eb 6d                	jmp    80102580 <iderw+0xd0>
80102513:	90                   	nop
80102514:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80102518:	89 c2                	mov    %eax,%edx
8010251a:	8b 42 58             	mov    0x58(%edx),%eax
8010251d:	85 c0                	test   %eax,%eax
8010251f:	75 f7                	jne    80102518 <iderw+0x68>
80102521:	83 c2 58             	add    $0x58,%edx
    ;
  *pp = b;
80102524:	89 1a                	mov    %ebx,(%edx)

  // Start disk if necessary.
  if(idequeue == b)
80102526:	39 1d 64 b5 10 80    	cmp    %ebx,0x8010b564
8010252c:	74 42                	je     80102570 <iderw+0xc0>
    idestart(b);

  // Wait for request to finish.
  while((b->flags & (B_VALID|B_DIRTY)) != B_VALID){
8010252e:	8b 03                	mov    (%ebx),%eax
80102530:	83 e0 06             	and    $0x6,%eax
80102533:	83 f8 02             	cmp    $0x2,%eax
80102536:	74 23                	je     8010255b <iderw+0xab>
80102538:	90                   	nop
80102539:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    sleep(b, &idelock);
80102540:	83 ec 08             	sub    $0x8,%esp
80102543:	68 80 b5 10 80       	push   $0x8010b580
80102548:	53                   	push   %ebx
80102549:	e8 a2 1c 00 00       	call   801041f0 <sleep>
  while((b->flags & (B_VALID|B_DIRTY)) != B_VALID){
8010254e:	8b 03                	mov    (%ebx),%eax
80102550:	83 c4 10             	add    $0x10,%esp
80102553:	83 e0 06             	and    $0x6,%eax
80102556:	83 f8 02             	cmp    $0x2,%eax
80102559:	75 e5                	jne    80102540 <iderw+0x90>
  }


  release(&idelock);
8010255b:	c7 45 08 80 b5 10 80 	movl   $0x8010b580,0x8(%ebp)
}
80102562:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80102565:	c9                   	leave  
  release(&idelock);
80102566:	e9 25 23 00 00       	jmp    80104890 <release>
8010256b:	90                   	nop
8010256c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    idestart(b);
80102570:	89 d8                	mov    %ebx,%eax
80102572:	e8 39 fd ff ff       	call   801022b0 <idestart>
80102577:	eb b5                	jmp    8010252e <iderw+0x7e>
80102579:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
  for(pp=&idequeue; *pp; pp=&(*pp)->qnext)  //DOC:insert-queue
80102580:	ba 64 b5 10 80       	mov    $0x8010b564,%edx
80102585:	eb 9d                	jmp    80102524 <iderw+0x74>
    panic("iderw: nothing to do");
80102587:	83 ec 0c             	sub    $0xc,%esp
8010258a:	68 84 79 10 80       	push   $0x80107984
8010258f:	e8 fc dd ff ff       	call   80100390 <panic>
    panic("iderw: buf not locked");
80102594:	83 ec 0c             	sub    $0xc,%esp
80102597:	68 6e 79 10 80       	push   $0x8010796e
8010259c:	e8 ef dd ff ff       	call   80100390 <panic>
    panic("iderw: ide disk 1 not present");
801025a1:	83 ec 0c             	sub    $0xc,%esp
801025a4:	68 99 79 10 80       	push   $0x80107999
801025a9:	e8 e2 dd ff ff       	call   80100390 <panic>
801025ae:	66 90                	xchg   %ax,%ax

801025b0 <ioapicinit>:
  ioapic->data = data;
}

void
ioapicinit(void)
{
801025b0:	55                   	push   %ebp
  int i, id, maxintr;

  ioapic = (volatile struct ioapic*)IOAPIC;
801025b1:	c7 05 34 36 11 80 00 	movl   $0xfec00000,0x80113634
801025b8:	00 c0 fe 
{
801025bb:	89 e5                	mov    %esp,%ebp
801025bd:	56                   	push   %esi
801025be:	53                   	push   %ebx
  ioapic->reg = reg;
801025bf:	c7 05 00 00 c0 fe 01 	movl   $0x1,0xfec00000
801025c6:	00 00 00 
  return ioapic->data;
801025c9:	a1 34 36 11 80       	mov    0x80113634,%eax
801025ce:	8b 58 10             	mov    0x10(%eax),%ebx
  ioapic->reg = reg;
801025d1:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
  return ioapic->data;
801025d7:	8b 0d 34 36 11 80    	mov    0x80113634,%ecx
  maxintr = (ioapicread(REG_VER) >> 16) & 0xFF;
  id = ioapicread(REG_ID) >> 24;
  if(id != ioapicid)
801025dd:	0f b6 15 60 37 11 80 	movzbl 0x80113760,%edx
  maxintr = (ioapicread(REG_VER) >> 16) & 0xFF;
801025e4:	c1 eb 10             	shr    $0x10,%ebx
  return ioapic->data;
801025e7:	8b 41 10             	mov    0x10(%ecx),%eax
  maxintr = (ioapicread(REG_VER) >> 16) & 0xFF;
801025ea:	0f b6 db             	movzbl %bl,%ebx
  id = ioapicread(REG_ID) >> 24;
801025ed:	c1 e8 18             	shr    $0x18,%eax
  if(id != ioapicid)
801025f0:	39 c2                	cmp    %eax,%edx
801025f2:	74 16                	je     8010260a <ioapicinit+0x5a>
    cprintf("ioapicinit: id isn't equal to ioapicid; not a MP\n");
801025f4:	83 ec 0c             	sub    $0xc,%esp
801025f7:	68 b8 79 10 80       	push   $0x801079b8
801025fc:	e8 5f e0 ff ff       	call   80100660 <cprintf>
80102601:	8b 0d 34 36 11 80    	mov    0x80113634,%ecx
80102607:	83 c4 10             	add    $0x10,%esp
8010260a:	83 c3 21             	add    $0x21,%ebx
{
8010260d:	ba 10 00 00 00       	mov    $0x10,%edx
80102612:	b8 20 00 00 00       	mov    $0x20,%eax
80102617:	89 f6                	mov    %esi,%esi
80102619:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi
  ioapic->reg = reg;
80102620:	89 11                	mov    %edx,(%ecx)
  ioapic->data = data;
80102622:	8b 0d 34 36 11 80    	mov    0x80113634,%ecx

  // Mark all interrupts edge-triggered, active high, disabled,
  // and not routed to any CPUs.
  for(i = 0; i <= maxintr; i++){
    ioapicwrite(REG_TABLE+2*i, INT_DISABLED | (T_IRQ0 + i));
80102628:	89 c6                	mov    %eax,%esi
8010262a:	81 ce 00 00 01 00    	or     $0x10000,%esi
80102630:	83 c0 01             	add    $0x1,%eax
  ioapic->data = data;
80102633:	89 71 10             	mov    %esi,0x10(%ecx)
80102636:	8d 72 01             	lea    0x1(%edx),%esi
80102639:	83 c2 02             	add    $0x2,%edx
  for(i = 0; i <= maxintr; i++){
8010263c:	39 d8                	cmp    %ebx,%eax
  ioapic->reg = reg;
8010263e:	89 31                	mov    %esi,(%ecx)
  ioapic->data = data;
80102640:	8b 0d 34 36 11 80    	mov    0x80113634,%ecx
80102646:	c7 41 10 00 00 00 00 	movl   $0x0,0x10(%ecx)
  for(i = 0; i <= maxintr; i++){
8010264d:	75 d1                	jne    80102620 <ioapicinit+0x70>
    ioapicwrite(REG_TABLE+2*i+1, 0);
  }
}
8010264f:	8d 65 f8             	lea    -0x8(%ebp),%esp
80102652:	5b                   	pop    %ebx
80102653:	5e                   	pop    %esi
80102654:	5d                   	pop    %ebp
80102655:	c3                   	ret    
80102656:	8d 76 00             	lea    0x0(%esi),%esi
80102659:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80102660 <ioapicenable>:

void
ioapicenable(int irq, int cpunum)
{
80102660:	55                   	push   %ebp
  ioapic->reg = reg;
80102661:	8b 0d 34 36 11 80    	mov    0x80113634,%ecx
{
80102667:	89 e5                	mov    %esp,%ebp
80102669:	8b 45 08             	mov    0x8(%ebp),%eax
  // Mark interrupt edge-triggered, active high,
  // enabled, and routed to the given cpunum,
  // which happens to be that cpu's APIC ID.
  ioapicwrite(REG_TABLE+2*irq, T_IRQ0 + irq);
8010266c:	8d 50 20             	lea    0x20(%eax),%edx
8010266f:	8d 44 00 10          	lea    0x10(%eax,%eax,1),%eax
  ioapic->reg = reg;
80102673:	89 01                	mov    %eax,(%ecx)
  ioapic->data = data;
80102675:	8b 0d 34 36 11 80    	mov    0x80113634,%ecx
  ioapicwrite(REG_TABLE+2*irq+1, cpunum << 24);
8010267b:	83 c0 01             	add    $0x1,%eax
  ioapic->data = data;
8010267e:	89 51 10             	mov    %edx,0x10(%ecx)
  ioapicwrite(REG_TABLE+2*irq+1, cpunum << 24);
80102681:	8b 55 0c             	mov    0xc(%ebp),%edx
  ioapic->reg = reg;
80102684:	89 01                	mov    %eax,(%ecx)
  ioapic->data = data;
80102686:	a1 34 36 11 80       	mov    0x80113634,%eax
  ioapicwrite(REG_TABLE+2*irq+1, cpunum << 24);
8010268b:	c1 e2 18             	shl    $0x18,%edx
  ioapic->data = data;
8010268e:	89 50 10             	mov    %edx,0x10(%eax)
}
80102691:	5d                   	pop    %ebp
80102692:	c3                   	ret    
80102693:	66 90                	xchg   %ax,%ax
80102695:	66 90                	xchg   %ax,%ax
80102697:	66 90                	xchg   %ax,%ax
80102699:	66 90                	xchg   %ax,%ax
8010269b:	66 90                	xchg   %ax,%ax
8010269d:	66 90                	xchg   %ax,%ax
8010269f:	90                   	nop

801026a0 <kfree>:
// which normally should have been returned by a
// call to kalloc().  (The exception is when
// initializing the allocator; see kinit above.)
void
kfree(char *v)
{
801026a0:	55                   	push   %ebp
801026a1:	89 e5                	mov    %esp,%ebp
801026a3:	57                   	push   %edi
801026a4:	56                   	push   %esi
801026a5:	53                   	push   %ebx
801026a6:	83 ec 0c             	sub    $0xc,%esp
801026a9:	8b 5d 08             	mov    0x8(%ebp),%ebx
  struct run *r;

  if((uint)v % PGSIZE)
801026ac:	89 df                	mov    %ebx,%edi
801026ae:	81 e7 ff 0f 00 00    	and    $0xfff,%edi
801026b4:	0f 85 b6 00 00 00    	jne    80102770 <kfree+0xd0>
    cprintf("(uint)v % PGSIZE\n");
  
  if(v < end)
801026ba:	81 fb a8 98 11 80    	cmp    $0x801198a8,%ebx
801026c0:	8d b3 00 00 00 80    	lea    -0x80000000(%ebx),%esi
801026c6:	72 70                	jb     80102738 <kfree+0x98>
    cprintf("v < end\n");

  if(V2P(v) >= PHYSTOP)
801026c8:	81 fe ff ff ff 0d    	cmp    $0xdffffff,%esi
801026ce:	0f 87 7c 00 00 00    	ja     80102750 <kfree+0xb0>
    cprintf("V2P(v) >= PHYSTOP\n");


  if((uint)v % PGSIZE || v < end || V2P(v) >= PHYSTOP)
801026d4:	85 ff                	test   %edi,%edi
801026d6:	0f 85 84 00 00 00    	jne    80102760 <kfree+0xc0>
    panic("kfreeeeee");

  // Fill with junk to catch dangling refs.
  memset(v, 1, PGSIZE);
801026dc:	83 ec 04             	sub    $0x4,%esp
801026df:	68 00 10 00 00       	push   $0x1000
801026e4:	6a 01                	push   $0x1
801026e6:	53                   	push   %ebx
801026e7:	e8 f4 21 00 00       	call   801048e0 <memset>

  if(kmem.use_lock)
801026ec:	8b 15 74 36 11 80    	mov    0x80113674,%edx
801026f2:	83 c4 10             	add    $0x10,%esp
801026f5:	85 d2                	test   %edx,%edx
801026f7:	0f 85 8b 00 00 00    	jne    80102788 <kfree+0xe8>
    acquire(&kmem.lock);
  r = (struct run*)v;
  r->next = kmem.freelist;
801026fd:	a1 78 36 11 80       	mov    0x80113678,%eax
80102702:	89 03                	mov    %eax,(%ebx)
  kmem.freelist = r;
  if(kmem.use_lock)
80102704:	a1 74 36 11 80       	mov    0x80113674,%eax
  kmem.freelist = r;
80102709:	89 1d 78 36 11 80    	mov    %ebx,0x80113678
  if(kmem.use_lock)
8010270f:	85 c0                	test   %eax,%eax
80102711:	75 0d                	jne    80102720 <kfree+0x80>
    release(&kmem.lock);
}
80102713:	8d 65 f4             	lea    -0xc(%ebp),%esp
80102716:	5b                   	pop    %ebx
80102717:	5e                   	pop    %esi
80102718:	5f                   	pop    %edi
80102719:	5d                   	pop    %ebp
8010271a:	c3                   	ret    
8010271b:	90                   	nop
8010271c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    release(&kmem.lock);
80102720:	c7 45 08 40 36 11 80 	movl   $0x80113640,0x8(%ebp)
}
80102727:	8d 65 f4             	lea    -0xc(%ebp),%esp
8010272a:	5b                   	pop    %ebx
8010272b:	5e                   	pop    %esi
8010272c:	5f                   	pop    %edi
8010272d:	5d                   	pop    %ebp
    release(&kmem.lock);
8010272e:	e9 5d 21 00 00       	jmp    80104890 <release>
80102733:	90                   	nop
80102734:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    cprintf("v < end\n");
80102738:	83 ec 0c             	sub    $0xc,%esp
8010273b:	68 fc 79 10 80       	push   $0x801079fc
80102740:	e8 1b df ff ff       	call   80100660 <cprintf>
  if(V2P(v) >= PHYSTOP)
80102745:	83 c4 10             	add    $0x10,%esp
80102748:	81 fe ff ff ff 0d    	cmp    $0xdffffff,%esi
8010274e:	76 10                	jbe    80102760 <kfree+0xc0>
    cprintf("V2P(v) >= PHYSTOP\n");
80102750:	83 ec 0c             	sub    $0xc,%esp
80102753:	68 05 7a 10 80       	push   $0x80107a05
80102758:	e8 03 df ff ff       	call   80100660 <cprintf>
8010275d:	83 c4 10             	add    $0x10,%esp
    panic("kfreeeeee");
80102760:	83 ec 0c             	sub    $0xc,%esp
80102763:	68 18 7a 10 80       	push   $0x80107a18
80102768:	e8 23 dc ff ff       	call   80100390 <panic>
8010276d:	8d 76 00             	lea    0x0(%esi),%esi
    cprintf("(uint)v % PGSIZE\n");
80102770:	83 ec 0c             	sub    $0xc,%esp
80102773:	68 ea 79 10 80       	push   $0x801079ea
80102778:	e8 e3 de ff ff       	call   80100660 <cprintf>
8010277d:	83 c4 10             	add    $0x10,%esp
80102780:	e9 35 ff ff ff       	jmp    801026ba <kfree+0x1a>
80102785:	8d 76 00             	lea    0x0(%esi),%esi
    acquire(&kmem.lock);
80102788:	83 ec 0c             	sub    $0xc,%esp
8010278b:	68 40 36 11 80       	push   $0x80113640
80102790:	e8 3b 20 00 00       	call   801047d0 <acquire>
80102795:	83 c4 10             	add    $0x10,%esp
80102798:	e9 60 ff ff ff       	jmp    801026fd <kfree+0x5d>
8010279d:	8d 76 00             	lea    0x0(%esi),%esi

801027a0 <freerange>:
{
801027a0:	55                   	push   %ebp
801027a1:	89 e5                	mov    %esp,%ebp
801027a3:	56                   	push   %esi
801027a4:	53                   	push   %ebx
  p = (char*)PGROUNDUP((uint)vstart);
801027a5:	8b 45 08             	mov    0x8(%ebp),%eax
{
801027a8:	8b 75 0c             	mov    0xc(%ebp),%esi
  p = (char*)PGROUNDUP((uint)vstart);
801027ab:	8d 98 ff 0f 00 00    	lea    0xfff(%eax),%ebx
801027b1:	81 e3 00 f0 ff ff    	and    $0xfffff000,%ebx
  for(; p + PGSIZE <= (char*)vend; p += PGSIZE)
801027b7:	81 c3 00 10 00 00    	add    $0x1000,%ebx
801027bd:	39 de                	cmp    %ebx,%esi
801027bf:	72 23                	jb     801027e4 <freerange+0x44>
801027c1:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    kfree(p);
801027c8:	8d 83 00 f0 ff ff    	lea    -0x1000(%ebx),%eax
801027ce:	83 ec 0c             	sub    $0xc,%esp
  for(; p + PGSIZE <= (char*)vend; p += PGSIZE)
801027d1:	81 c3 00 10 00 00    	add    $0x1000,%ebx
    kfree(p);
801027d7:	50                   	push   %eax
801027d8:	e8 c3 fe ff ff       	call   801026a0 <kfree>
  for(; p + PGSIZE <= (char*)vend; p += PGSIZE)
801027dd:	83 c4 10             	add    $0x10,%esp
801027e0:	39 f3                	cmp    %esi,%ebx
801027e2:	76 e4                	jbe    801027c8 <freerange+0x28>
}
801027e4:	8d 65 f8             	lea    -0x8(%ebp),%esp
801027e7:	5b                   	pop    %ebx
801027e8:	5e                   	pop    %esi
801027e9:	5d                   	pop    %ebp
801027ea:	c3                   	ret    
801027eb:	90                   	nop
801027ec:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

801027f0 <kinit1>:
{
801027f0:	55                   	push   %ebp
801027f1:	89 e5                	mov    %esp,%ebp
801027f3:	56                   	push   %esi
801027f4:	53                   	push   %ebx
801027f5:	8b 75 0c             	mov    0xc(%ebp),%esi
  initlock(&kmem.lock, "kmem");
801027f8:	83 ec 08             	sub    $0x8,%esp
801027fb:	68 22 7a 10 80       	push   $0x80107a22
80102800:	68 40 36 11 80       	push   $0x80113640
80102805:	e8 86 1e 00 00       	call   80104690 <initlock>
  p = (char*)PGROUNDUP((uint)vstart);
8010280a:	8b 45 08             	mov    0x8(%ebp),%eax
  for(; p + PGSIZE <= (char*)vend; p += PGSIZE)
8010280d:	83 c4 10             	add    $0x10,%esp
  kmem.use_lock = 0;
80102810:	c7 05 74 36 11 80 00 	movl   $0x0,0x80113674
80102817:	00 00 00 
  p = (char*)PGROUNDUP((uint)vstart);
8010281a:	8d 98 ff 0f 00 00    	lea    0xfff(%eax),%ebx
80102820:	81 e3 00 f0 ff ff    	and    $0xfffff000,%ebx
  for(; p + PGSIZE <= (char*)vend; p += PGSIZE)
80102826:	81 c3 00 10 00 00    	add    $0x1000,%ebx
8010282c:	39 de                	cmp    %ebx,%esi
8010282e:	72 1c                	jb     8010284c <kinit1+0x5c>
    kfree(p);
80102830:	8d 83 00 f0 ff ff    	lea    -0x1000(%ebx),%eax
80102836:	83 ec 0c             	sub    $0xc,%esp
  for(; p + PGSIZE <= (char*)vend; p += PGSIZE)
80102839:	81 c3 00 10 00 00    	add    $0x1000,%ebx
    kfree(p);
8010283f:	50                   	push   %eax
80102840:	e8 5b fe ff ff       	call   801026a0 <kfree>
  for(; p + PGSIZE <= (char*)vend; p += PGSIZE)
80102845:	83 c4 10             	add    $0x10,%esp
80102848:	39 de                	cmp    %ebx,%esi
8010284a:	73 e4                	jae    80102830 <kinit1+0x40>
}
8010284c:	8d 65 f8             	lea    -0x8(%ebp),%esp
8010284f:	5b                   	pop    %ebx
80102850:	5e                   	pop    %esi
80102851:	5d                   	pop    %ebp
80102852:	c3                   	ret    
80102853:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
80102859:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80102860 <kinit2>:
{
80102860:	55                   	push   %ebp
80102861:	89 e5                	mov    %esp,%ebp
80102863:	56                   	push   %esi
80102864:	53                   	push   %ebx
  p = (char*)PGROUNDUP((uint)vstart);
80102865:	8b 45 08             	mov    0x8(%ebp),%eax
{
80102868:	8b 75 0c             	mov    0xc(%ebp),%esi
  p = (char*)PGROUNDUP((uint)vstart);
8010286b:	8d 98 ff 0f 00 00    	lea    0xfff(%eax),%ebx
80102871:	81 e3 00 f0 ff ff    	and    $0xfffff000,%ebx
  for(; p + PGSIZE <= (char*)vend; p += PGSIZE)
80102877:	81 c3 00 10 00 00    	add    $0x1000,%ebx
8010287d:	39 de                	cmp    %ebx,%esi
8010287f:	72 23                	jb     801028a4 <kinit2+0x44>
80102881:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    kfree(p);
80102888:	8d 83 00 f0 ff ff    	lea    -0x1000(%ebx),%eax
8010288e:	83 ec 0c             	sub    $0xc,%esp
  for(; p + PGSIZE <= (char*)vend; p += PGSIZE)
80102891:	81 c3 00 10 00 00    	add    $0x1000,%ebx
    kfree(p);
80102897:	50                   	push   %eax
80102898:	e8 03 fe ff ff       	call   801026a0 <kfree>
  for(; p + PGSIZE <= (char*)vend; p += PGSIZE)
8010289d:	83 c4 10             	add    $0x10,%esp
801028a0:	39 de                	cmp    %ebx,%esi
801028a2:	73 e4                	jae    80102888 <kinit2+0x28>
  kmem.use_lock = 1;
801028a4:	c7 05 74 36 11 80 01 	movl   $0x1,0x80113674
801028ab:	00 00 00 
}
801028ae:	8d 65 f8             	lea    -0x8(%ebp),%esp
801028b1:	5b                   	pop    %ebx
801028b2:	5e                   	pop    %esi
801028b3:	5d                   	pop    %ebp
801028b4:	c3                   	ret    
801028b5:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
801028b9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

801028c0 <kalloc>:
char*
kalloc(void)
{
  struct run *r;

  if(kmem.use_lock)
801028c0:	a1 74 36 11 80       	mov    0x80113674,%eax
801028c5:	85 c0                	test   %eax,%eax
801028c7:	75 1f                	jne    801028e8 <kalloc+0x28>
    acquire(&kmem.lock);
  r = kmem.freelist;
801028c9:	a1 78 36 11 80       	mov    0x80113678,%eax
  if(r)
801028ce:	85 c0                	test   %eax,%eax
801028d0:	74 0e                	je     801028e0 <kalloc+0x20>
    kmem.freelist = r->next;
801028d2:	8b 10                	mov    (%eax),%edx
801028d4:	89 15 78 36 11 80    	mov    %edx,0x80113678
801028da:	c3                   	ret    
801028db:	90                   	nop
801028dc:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  if(kmem.use_lock)
    release(&kmem.lock);
  return (char*)r;
}
801028e0:	f3 c3                	repz ret 
801028e2:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
{
801028e8:	55                   	push   %ebp
801028e9:	89 e5                	mov    %esp,%ebp
801028eb:	83 ec 24             	sub    $0x24,%esp
    acquire(&kmem.lock);
801028ee:	68 40 36 11 80       	push   $0x80113640
801028f3:	e8 d8 1e 00 00       	call   801047d0 <acquire>
  r = kmem.freelist;
801028f8:	a1 78 36 11 80       	mov    0x80113678,%eax
  if(r)
801028fd:	83 c4 10             	add    $0x10,%esp
80102900:	8b 15 74 36 11 80    	mov    0x80113674,%edx
80102906:	85 c0                	test   %eax,%eax
80102908:	74 08                	je     80102912 <kalloc+0x52>
    kmem.freelist = r->next;
8010290a:	8b 08                	mov    (%eax),%ecx
8010290c:	89 0d 78 36 11 80    	mov    %ecx,0x80113678
  if(kmem.use_lock)
80102912:	85 d2                	test   %edx,%edx
80102914:	74 16                	je     8010292c <kalloc+0x6c>
    release(&kmem.lock);
80102916:	83 ec 0c             	sub    $0xc,%esp
80102919:	89 45 f4             	mov    %eax,-0xc(%ebp)
8010291c:	68 40 36 11 80       	push   $0x80113640
80102921:	e8 6a 1f 00 00       	call   80104890 <release>
  return (char*)r;
80102926:	8b 45 f4             	mov    -0xc(%ebp),%eax
    release(&kmem.lock);
80102929:	83 c4 10             	add    $0x10,%esp
}
8010292c:	c9                   	leave  
8010292d:	c3                   	ret    
8010292e:	66 90                	xchg   %ax,%ax

80102930 <kbdgetc>:
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80102930:	ba 64 00 00 00       	mov    $0x64,%edx
80102935:	ec                   	in     (%dx),%al
    normalmap, shiftmap, ctlmap, ctlmap
  };
  uint st, data, c;

  st = inb(KBSTATP);
  if((st & KBS_DIB) == 0)
80102936:	a8 01                	test   $0x1,%al
80102938:	0f 84 c2 00 00 00    	je     80102a00 <kbdgetc+0xd0>
8010293e:	ba 60 00 00 00       	mov    $0x60,%edx
80102943:	ec                   	in     (%dx),%al
    return -1;
  data = inb(KBDATAP);
80102944:	0f b6 d0             	movzbl %al,%edx
80102947:	8b 0d b4 b5 10 80    	mov    0x8010b5b4,%ecx

  if(data == 0xE0){
8010294d:	81 fa e0 00 00 00    	cmp    $0xe0,%edx
80102953:	0f 84 7f 00 00 00    	je     801029d8 <kbdgetc+0xa8>
{
80102959:	55                   	push   %ebp
8010295a:	89 e5                	mov    %esp,%ebp
8010295c:	53                   	push   %ebx
8010295d:	89 cb                	mov    %ecx,%ebx
8010295f:	83 e3 40             	and    $0x40,%ebx
    shift |= E0ESC;
    return 0;
  } else if(data & 0x80){
80102962:	84 c0                	test   %al,%al
80102964:	78 4a                	js     801029b0 <kbdgetc+0x80>
    // Key released
    data = (shift & E0ESC ? data : data & 0x7F);
    shift &= ~(shiftcode[data] | E0ESC);
    return 0;
  } else if(shift & E0ESC){
80102966:	85 db                	test   %ebx,%ebx
80102968:	74 09                	je     80102973 <kbdgetc+0x43>
    // Last character was an E0 escape; or with 0x80
    data |= 0x80;
8010296a:	83 c8 80             	or     $0xffffff80,%eax
    shift &= ~E0ESC;
8010296d:	83 e1 bf             	and    $0xffffffbf,%ecx
    data |= 0x80;
80102970:	0f b6 d0             	movzbl %al,%edx
  }

  shift |= shiftcode[data];
80102973:	0f b6 82 60 7b 10 80 	movzbl -0x7fef84a0(%edx),%eax
8010297a:	09 c1                	or     %eax,%ecx
  shift ^= togglecode[data];
8010297c:	0f b6 82 60 7a 10 80 	movzbl -0x7fef85a0(%edx),%eax
80102983:	31 c1                	xor    %eax,%ecx
  c = charcode[shift & (CTL | SHIFT)][data];
80102985:	89 c8                	mov    %ecx,%eax
  shift ^= togglecode[data];
80102987:	89 0d b4 b5 10 80    	mov    %ecx,0x8010b5b4
  c = charcode[shift & (CTL | SHIFT)][data];
8010298d:	83 e0 03             	and    $0x3,%eax
  if(shift & CAPSLOCK){
80102990:	83 e1 08             	and    $0x8,%ecx
  c = charcode[shift & (CTL | SHIFT)][data];
80102993:	8b 04 85 40 7a 10 80 	mov    -0x7fef85c0(,%eax,4),%eax
8010299a:	0f b6 04 10          	movzbl (%eax,%edx,1),%eax
  if(shift & CAPSLOCK){
8010299e:	74 31                	je     801029d1 <kbdgetc+0xa1>
    if('a' <= c && c <= 'z')
801029a0:	8d 50 9f             	lea    -0x61(%eax),%edx
801029a3:	83 fa 19             	cmp    $0x19,%edx
801029a6:	77 40                	ja     801029e8 <kbdgetc+0xb8>
      c += 'A' - 'a';
801029a8:	83 e8 20             	sub    $0x20,%eax
    else if('A' <= c && c <= 'Z')
      c += 'a' - 'A';
  }
  return c;
}
801029ab:	5b                   	pop    %ebx
801029ac:	5d                   	pop    %ebp
801029ad:	c3                   	ret    
801029ae:	66 90                	xchg   %ax,%ax
    data = (shift & E0ESC ? data : data & 0x7F);
801029b0:	83 e0 7f             	and    $0x7f,%eax
801029b3:	85 db                	test   %ebx,%ebx
801029b5:	0f 44 d0             	cmove  %eax,%edx
    shift &= ~(shiftcode[data] | E0ESC);
801029b8:	0f b6 82 60 7b 10 80 	movzbl -0x7fef84a0(%edx),%eax
801029bf:	83 c8 40             	or     $0x40,%eax
801029c2:	0f b6 c0             	movzbl %al,%eax
801029c5:	f7 d0                	not    %eax
801029c7:	21 c1                	and    %eax,%ecx
    return 0;
801029c9:	31 c0                	xor    %eax,%eax
    shift &= ~(shiftcode[data] | E0ESC);
801029cb:	89 0d b4 b5 10 80    	mov    %ecx,0x8010b5b4
}
801029d1:	5b                   	pop    %ebx
801029d2:	5d                   	pop    %ebp
801029d3:	c3                   	ret    
801029d4:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    shift |= E0ESC;
801029d8:	83 c9 40             	or     $0x40,%ecx
    return 0;
801029db:	31 c0                	xor    %eax,%eax
    shift |= E0ESC;
801029dd:	89 0d b4 b5 10 80    	mov    %ecx,0x8010b5b4
    return 0;
801029e3:	c3                   	ret    
801029e4:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    else if('A' <= c && c <= 'Z')
801029e8:	8d 48 bf             	lea    -0x41(%eax),%ecx
      c += 'a' - 'A';
801029eb:	8d 50 20             	lea    0x20(%eax),%edx
}
801029ee:	5b                   	pop    %ebx
      c += 'a' - 'A';
801029ef:	83 f9 1a             	cmp    $0x1a,%ecx
801029f2:	0f 42 c2             	cmovb  %edx,%eax
}
801029f5:	5d                   	pop    %ebp
801029f6:	c3                   	ret    
801029f7:	89 f6                	mov    %esi,%esi
801029f9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi
    return -1;
80102a00:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80102a05:	c3                   	ret    
80102a06:	8d 76 00             	lea    0x0(%esi),%esi
80102a09:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80102a10 <kbdintr>:

void
kbdintr(void)
{
80102a10:	55                   	push   %ebp
80102a11:	89 e5                	mov    %esp,%ebp
80102a13:	83 ec 14             	sub    $0x14,%esp
  consoleintr(kbdgetc);
80102a16:	68 30 29 10 80       	push   $0x80102930
80102a1b:	e8 f0 dd ff ff       	call   80100810 <consoleintr>
}
80102a20:	83 c4 10             	add    $0x10,%esp
80102a23:	c9                   	leave  
80102a24:	c3                   	ret    
80102a25:	66 90                	xchg   %ax,%ax
80102a27:	66 90                	xchg   %ax,%ax
80102a29:	66 90                	xchg   %ax,%ax
80102a2b:	66 90                	xchg   %ax,%ax
80102a2d:	66 90                	xchg   %ax,%ax
80102a2f:	90                   	nop

80102a30 <lapicinit>:
}

void
lapicinit(void)
{
  if(!lapic)
80102a30:	a1 7c 36 11 80       	mov    0x8011367c,%eax
{
80102a35:	55                   	push   %ebp
80102a36:	89 e5                	mov    %esp,%ebp
  if(!lapic)
80102a38:	85 c0                	test   %eax,%eax
80102a3a:	0f 84 c8 00 00 00    	je     80102b08 <lapicinit+0xd8>
  lapic[index] = value;
80102a40:	c7 80 f0 00 00 00 3f 	movl   $0x13f,0xf0(%eax)
80102a47:	01 00 00 
  lapic[ID];  // wait for write to finish, by reading
80102a4a:	8b 50 20             	mov    0x20(%eax),%edx
  lapic[index] = value;
80102a4d:	c7 80 e0 03 00 00 0b 	movl   $0xb,0x3e0(%eax)
80102a54:	00 00 00 
  lapic[ID];  // wait for write to finish, by reading
80102a57:	8b 50 20             	mov    0x20(%eax),%edx
  lapic[index] = value;
80102a5a:	c7 80 20 03 00 00 20 	movl   $0x20020,0x320(%eax)
80102a61:	00 02 00 
  lapic[ID];  // wait for write to finish, by reading
80102a64:	8b 50 20             	mov    0x20(%eax),%edx
  lapic[index] = value;
80102a67:	c7 80 80 03 00 00 80 	movl   $0x989680,0x380(%eax)
80102a6e:	96 98 00 
  lapic[ID];  // wait for write to finish, by reading
80102a71:	8b 50 20             	mov    0x20(%eax),%edx
  lapic[index] = value;
80102a74:	c7 80 50 03 00 00 00 	movl   $0x10000,0x350(%eax)
80102a7b:	00 01 00 
  lapic[ID];  // wait for write to finish, by reading
80102a7e:	8b 50 20             	mov    0x20(%eax),%edx
  lapic[index] = value;
80102a81:	c7 80 60 03 00 00 00 	movl   $0x10000,0x360(%eax)
80102a88:	00 01 00 
  lapic[ID];  // wait for write to finish, by reading
80102a8b:	8b 50 20             	mov    0x20(%eax),%edx
  lapicw(LINT0, MASKED);
  lapicw(LINT1, MASKED);

  // Disable performance counter overflow interrupts
  // on machines that provide that interrupt entry.
  if(((lapic[VER]>>16) & 0xFF) >= 4)
80102a8e:	8b 50 30             	mov    0x30(%eax),%edx
80102a91:	c1 ea 10             	shr    $0x10,%edx
80102a94:	80 fa 03             	cmp    $0x3,%dl
80102a97:	77 77                	ja     80102b10 <lapicinit+0xe0>
  lapic[index] = value;
80102a99:	c7 80 70 03 00 00 33 	movl   $0x33,0x370(%eax)
80102aa0:	00 00 00 
  lapic[ID];  // wait for write to finish, by reading
80102aa3:	8b 50 20             	mov    0x20(%eax),%edx
  lapic[index] = value;
80102aa6:	c7 80 80 02 00 00 00 	movl   $0x0,0x280(%eax)
80102aad:	00 00 00 
  lapic[ID];  // wait for write to finish, by reading
80102ab0:	8b 50 20             	mov    0x20(%eax),%edx
  lapic[index] = value;
80102ab3:	c7 80 80 02 00 00 00 	movl   $0x0,0x280(%eax)
80102aba:	00 00 00 
  lapic[ID];  // wait for write to finish, by reading
80102abd:	8b 50 20             	mov    0x20(%eax),%edx
  lapic[index] = value;
80102ac0:	c7 80 b0 00 00 00 00 	movl   $0x0,0xb0(%eax)
80102ac7:	00 00 00 
  lapic[ID];  // wait for write to finish, by reading
80102aca:	8b 50 20             	mov    0x20(%eax),%edx
  lapic[index] = value;
80102acd:	c7 80 10 03 00 00 00 	movl   $0x0,0x310(%eax)
80102ad4:	00 00 00 
  lapic[ID];  // wait for write to finish, by reading
80102ad7:	8b 50 20             	mov    0x20(%eax),%edx
  lapic[index] = value;
80102ada:	c7 80 00 03 00 00 00 	movl   $0x88500,0x300(%eax)
80102ae1:	85 08 00 
  lapic[ID];  // wait for write to finish, by reading
80102ae4:	8b 50 20             	mov    0x20(%eax),%edx
80102ae7:	89 f6                	mov    %esi,%esi
80102ae9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi
  lapicw(EOI, 0);

  // Send an Init Level De-Assert to synchronise arbitration ID's.
  lapicw(ICRHI, 0);
  lapicw(ICRLO, BCAST | INIT | LEVEL);
  while(lapic[ICRLO] & DELIVS)
80102af0:	8b 90 00 03 00 00    	mov    0x300(%eax),%edx
80102af6:	80 e6 10             	and    $0x10,%dh
80102af9:	75 f5                	jne    80102af0 <lapicinit+0xc0>
  lapic[index] = value;
80102afb:	c7 80 80 00 00 00 00 	movl   $0x0,0x80(%eax)
80102b02:	00 00 00 
  lapic[ID];  // wait for write to finish, by reading
80102b05:	8b 40 20             	mov    0x20(%eax),%eax
    ;

  // Enable interrupts on the APIC (but not on the processor).
  lapicw(TPR, 0);
}
80102b08:	5d                   	pop    %ebp
80102b09:	c3                   	ret    
80102b0a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
  lapic[index] = value;
80102b10:	c7 80 40 03 00 00 00 	movl   $0x10000,0x340(%eax)
80102b17:	00 01 00 
  lapic[ID];  // wait for write to finish, by reading
80102b1a:	8b 50 20             	mov    0x20(%eax),%edx
80102b1d:	e9 77 ff ff ff       	jmp    80102a99 <lapicinit+0x69>
80102b22:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80102b29:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80102b30 <lapicid>:

int
lapicid(void)
{
  if (!lapic)
80102b30:	8b 15 7c 36 11 80    	mov    0x8011367c,%edx
{
80102b36:	55                   	push   %ebp
80102b37:	31 c0                	xor    %eax,%eax
80102b39:	89 e5                	mov    %esp,%ebp
  if (!lapic)
80102b3b:	85 d2                	test   %edx,%edx
80102b3d:	74 06                	je     80102b45 <lapicid+0x15>
    return 0;
  return lapic[ID] >> 24;
80102b3f:	8b 42 20             	mov    0x20(%edx),%eax
80102b42:	c1 e8 18             	shr    $0x18,%eax
}
80102b45:	5d                   	pop    %ebp
80102b46:	c3                   	ret    
80102b47:	89 f6                	mov    %esi,%esi
80102b49:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80102b50 <lapiceoi>:

// Acknowledge interrupt.
void
lapiceoi(void)
{
  if(lapic)
80102b50:	a1 7c 36 11 80       	mov    0x8011367c,%eax
{
80102b55:	55                   	push   %ebp
80102b56:	89 e5                	mov    %esp,%ebp
  if(lapic)
80102b58:	85 c0                	test   %eax,%eax
80102b5a:	74 0d                	je     80102b69 <lapiceoi+0x19>
  lapic[index] = value;
80102b5c:	c7 80 b0 00 00 00 00 	movl   $0x0,0xb0(%eax)
80102b63:	00 00 00 
  lapic[ID];  // wait for write to finish, by reading
80102b66:	8b 40 20             	mov    0x20(%eax),%eax
    lapicw(EOI, 0);
}
80102b69:	5d                   	pop    %ebp
80102b6a:	c3                   	ret    
80102b6b:	90                   	nop
80102b6c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

80102b70 <microdelay>:

// Spin for a given number of microseconds.
// On real hardware would want to tune this dynamically.
void
microdelay(int us)
{
80102b70:	55                   	push   %ebp
80102b71:	89 e5                	mov    %esp,%ebp
}
80102b73:	5d                   	pop    %ebp
80102b74:	c3                   	ret    
80102b75:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80102b79:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80102b80 <lapicstartap>:

// Start additional processor running entry code at addr.
// See Appendix B of MultiProcessor Specification.
void
lapicstartap(uchar apicid, uint addr)
{
80102b80:	55                   	push   %ebp
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80102b81:	b8 0f 00 00 00       	mov    $0xf,%eax
80102b86:	ba 70 00 00 00       	mov    $0x70,%edx
80102b8b:	89 e5                	mov    %esp,%ebp
80102b8d:	53                   	push   %ebx
80102b8e:	8b 4d 0c             	mov    0xc(%ebp),%ecx
80102b91:	8b 5d 08             	mov    0x8(%ebp),%ebx
80102b94:	ee                   	out    %al,(%dx)
80102b95:	b8 0a 00 00 00       	mov    $0xa,%eax
80102b9a:	ba 71 00 00 00       	mov    $0x71,%edx
80102b9f:	ee                   	out    %al,(%dx)
  // and the warm reset vector (DWORD based at 40:67) to point at
  // the AP startup code prior to the [universal startup algorithm]."
  outb(CMOS_PORT, 0xF);  // offset 0xF is shutdown code
  outb(CMOS_PORT+1, 0x0A);
  wrv = (ushort*)P2V((0x40<<4 | 0x67));  // Warm reset vector
  wrv[0] = 0;
80102ba0:	31 c0                	xor    %eax,%eax
  wrv[1] = addr >> 4;

  // "Universal startup algorithm."
  // Send INIT (level-triggered) interrupt to reset other CPU.
  lapicw(ICRHI, apicid<<24);
80102ba2:	c1 e3 18             	shl    $0x18,%ebx
  wrv[0] = 0;
80102ba5:	66 a3 67 04 00 80    	mov    %ax,0x80000467
  wrv[1] = addr >> 4;
80102bab:	89 c8                	mov    %ecx,%eax
  // when it is in the halted state due to an INIT.  So the second
  // should be ignored, but it is part of the official Intel algorithm.
  // Bochs complains about the second one.  Too bad for Bochs.
  for(i = 0; i < 2; i++){
    lapicw(ICRHI, apicid<<24);
    lapicw(ICRLO, STARTUP | (addr>>12));
80102bad:	c1 e9 0c             	shr    $0xc,%ecx
  wrv[1] = addr >> 4;
80102bb0:	c1 e8 04             	shr    $0x4,%eax
  lapicw(ICRHI, apicid<<24);
80102bb3:	89 da                	mov    %ebx,%edx
    lapicw(ICRLO, STARTUP | (addr>>12));
80102bb5:	80 cd 06             	or     $0x6,%ch
  wrv[1] = addr >> 4;
80102bb8:	66 a3 69 04 00 80    	mov    %ax,0x80000469
  lapic[index] = value;
80102bbe:	a1 7c 36 11 80       	mov    0x8011367c,%eax
80102bc3:	89 98 10 03 00 00    	mov    %ebx,0x310(%eax)
  lapic[ID];  // wait for write to finish, by reading
80102bc9:	8b 58 20             	mov    0x20(%eax),%ebx
  lapic[index] = value;
80102bcc:	c7 80 00 03 00 00 00 	movl   $0xc500,0x300(%eax)
80102bd3:	c5 00 00 
  lapic[ID];  // wait for write to finish, by reading
80102bd6:	8b 58 20             	mov    0x20(%eax),%ebx
  lapic[index] = value;
80102bd9:	c7 80 00 03 00 00 00 	movl   $0x8500,0x300(%eax)
80102be0:	85 00 00 
  lapic[ID];  // wait for write to finish, by reading
80102be3:	8b 58 20             	mov    0x20(%eax),%ebx
  lapic[index] = value;
80102be6:	89 90 10 03 00 00    	mov    %edx,0x310(%eax)
  lapic[ID];  // wait for write to finish, by reading
80102bec:	8b 58 20             	mov    0x20(%eax),%ebx
  lapic[index] = value;
80102bef:	89 88 00 03 00 00    	mov    %ecx,0x300(%eax)
  lapic[ID];  // wait for write to finish, by reading
80102bf5:	8b 58 20             	mov    0x20(%eax),%ebx
  lapic[index] = value;
80102bf8:	89 90 10 03 00 00    	mov    %edx,0x310(%eax)
  lapic[ID];  // wait for write to finish, by reading
80102bfe:	8b 50 20             	mov    0x20(%eax),%edx
  lapic[index] = value;
80102c01:	89 88 00 03 00 00    	mov    %ecx,0x300(%eax)
  lapic[ID];  // wait for write to finish, by reading
80102c07:	8b 40 20             	mov    0x20(%eax),%eax
    microdelay(200);
  }
}
80102c0a:	5b                   	pop    %ebx
80102c0b:	5d                   	pop    %ebp
80102c0c:	c3                   	ret    
80102c0d:	8d 76 00             	lea    0x0(%esi),%esi

80102c10 <cmostime>:
}

// qemu seems to use 24-hour GWT and the values are BCD encoded
void
cmostime(struct rtcdate *r)
{
80102c10:	55                   	push   %ebp
80102c11:	b8 0b 00 00 00       	mov    $0xb,%eax
80102c16:	ba 70 00 00 00       	mov    $0x70,%edx
80102c1b:	89 e5                	mov    %esp,%ebp
80102c1d:	57                   	push   %edi
80102c1e:	56                   	push   %esi
80102c1f:	53                   	push   %ebx
80102c20:	83 ec 4c             	sub    $0x4c,%esp
80102c23:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80102c24:	ba 71 00 00 00       	mov    $0x71,%edx
80102c29:	ec                   	in     (%dx),%al
80102c2a:	83 e0 04             	and    $0x4,%eax
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80102c2d:	bb 70 00 00 00       	mov    $0x70,%ebx
80102c32:	88 45 b3             	mov    %al,-0x4d(%ebp)
80102c35:	8d 76 00             	lea    0x0(%esi),%esi
80102c38:	31 c0                	xor    %eax,%eax
80102c3a:	89 da                	mov    %ebx,%edx
80102c3c:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80102c3d:	b9 71 00 00 00       	mov    $0x71,%ecx
80102c42:	89 ca                	mov    %ecx,%edx
80102c44:	ec                   	in     (%dx),%al
80102c45:	88 45 b7             	mov    %al,-0x49(%ebp)
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80102c48:	89 da                	mov    %ebx,%edx
80102c4a:	b8 02 00 00 00       	mov    $0x2,%eax
80102c4f:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80102c50:	89 ca                	mov    %ecx,%edx
80102c52:	ec                   	in     (%dx),%al
80102c53:	88 45 b6             	mov    %al,-0x4a(%ebp)
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80102c56:	89 da                	mov    %ebx,%edx
80102c58:	b8 04 00 00 00       	mov    $0x4,%eax
80102c5d:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80102c5e:	89 ca                	mov    %ecx,%edx
80102c60:	ec                   	in     (%dx),%al
80102c61:	88 45 b5             	mov    %al,-0x4b(%ebp)
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80102c64:	89 da                	mov    %ebx,%edx
80102c66:	b8 07 00 00 00       	mov    $0x7,%eax
80102c6b:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80102c6c:	89 ca                	mov    %ecx,%edx
80102c6e:	ec                   	in     (%dx),%al
80102c6f:	88 45 b4             	mov    %al,-0x4c(%ebp)
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80102c72:	89 da                	mov    %ebx,%edx
80102c74:	b8 08 00 00 00       	mov    $0x8,%eax
80102c79:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80102c7a:	89 ca                	mov    %ecx,%edx
80102c7c:	ec                   	in     (%dx),%al
80102c7d:	89 c7                	mov    %eax,%edi
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80102c7f:	89 da                	mov    %ebx,%edx
80102c81:	b8 09 00 00 00       	mov    $0x9,%eax
80102c86:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80102c87:	89 ca                	mov    %ecx,%edx
80102c89:	ec                   	in     (%dx),%al
80102c8a:	89 c6                	mov    %eax,%esi
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80102c8c:	89 da                	mov    %ebx,%edx
80102c8e:	b8 0a 00 00 00       	mov    $0xa,%eax
80102c93:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80102c94:	89 ca                	mov    %ecx,%edx
80102c96:	ec                   	in     (%dx),%al
  bcd = (sb & (1 << 2)) == 0;

  // make sure CMOS doesn't modify time while we read it
  for(;;) {
    fill_rtcdate(&t1);
    if(cmos_read(CMOS_STATA) & CMOS_UIP)
80102c97:	84 c0                	test   %al,%al
80102c99:	78 9d                	js     80102c38 <cmostime+0x28>
  return inb(CMOS_RETURN);
80102c9b:	0f b6 45 b7          	movzbl -0x49(%ebp),%eax
80102c9f:	89 fa                	mov    %edi,%edx
80102ca1:	0f b6 fa             	movzbl %dl,%edi
80102ca4:	89 f2                	mov    %esi,%edx
80102ca6:	0f b6 f2             	movzbl %dl,%esi
80102ca9:	89 7d c8             	mov    %edi,-0x38(%ebp)
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80102cac:	89 da                	mov    %ebx,%edx
80102cae:	89 75 cc             	mov    %esi,-0x34(%ebp)
80102cb1:	89 45 b8             	mov    %eax,-0x48(%ebp)
80102cb4:	0f b6 45 b6          	movzbl -0x4a(%ebp),%eax
80102cb8:	89 45 bc             	mov    %eax,-0x44(%ebp)
80102cbb:	0f b6 45 b5          	movzbl -0x4b(%ebp),%eax
80102cbf:	89 45 c0             	mov    %eax,-0x40(%ebp)
80102cc2:	0f b6 45 b4          	movzbl -0x4c(%ebp),%eax
80102cc6:	89 45 c4             	mov    %eax,-0x3c(%ebp)
80102cc9:	31 c0                	xor    %eax,%eax
80102ccb:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80102ccc:	89 ca                	mov    %ecx,%edx
80102cce:	ec                   	in     (%dx),%al
80102ccf:	0f b6 c0             	movzbl %al,%eax
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80102cd2:	89 da                	mov    %ebx,%edx
80102cd4:	89 45 d0             	mov    %eax,-0x30(%ebp)
80102cd7:	b8 02 00 00 00       	mov    $0x2,%eax
80102cdc:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80102cdd:	89 ca                	mov    %ecx,%edx
80102cdf:	ec                   	in     (%dx),%al
80102ce0:	0f b6 c0             	movzbl %al,%eax
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80102ce3:	89 da                	mov    %ebx,%edx
80102ce5:	89 45 d4             	mov    %eax,-0x2c(%ebp)
80102ce8:	b8 04 00 00 00       	mov    $0x4,%eax
80102ced:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80102cee:	89 ca                	mov    %ecx,%edx
80102cf0:	ec                   	in     (%dx),%al
80102cf1:	0f b6 c0             	movzbl %al,%eax
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80102cf4:	89 da                	mov    %ebx,%edx
80102cf6:	89 45 d8             	mov    %eax,-0x28(%ebp)
80102cf9:	b8 07 00 00 00       	mov    $0x7,%eax
80102cfe:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80102cff:	89 ca                	mov    %ecx,%edx
80102d01:	ec                   	in     (%dx),%al
80102d02:	0f b6 c0             	movzbl %al,%eax
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80102d05:	89 da                	mov    %ebx,%edx
80102d07:	89 45 dc             	mov    %eax,-0x24(%ebp)
80102d0a:	b8 08 00 00 00       	mov    $0x8,%eax
80102d0f:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80102d10:	89 ca                	mov    %ecx,%edx
80102d12:	ec                   	in     (%dx),%al
80102d13:	0f b6 c0             	movzbl %al,%eax
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80102d16:	89 da                	mov    %ebx,%edx
80102d18:	89 45 e0             	mov    %eax,-0x20(%ebp)
80102d1b:	b8 09 00 00 00       	mov    $0x9,%eax
80102d20:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80102d21:	89 ca                	mov    %ecx,%edx
80102d23:	ec                   	in     (%dx),%al
80102d24:	0f b6 c0             	movzbl %al,%eax
        continue;
    fill_rtcdate(&t2);
    if(memcmp(&t1, &t2, sizeof(t1)) == 0)
80102d27:	83 ec 04             	sub    $0x4,%esp
  return inb(CMOS_RETURN);
80102d2a:	89 45 e4             	mov    %eax,-0x1c(%ebp)
    if(memcmp(&t1, &t2, sizeof(t1)) == 0)
80102d2d:	8d 45 d0             	lea    -0x30(%ebp),%eax
80102d30:	6a 18                	push   $0x18
80102d32:	50                   	push   %eax
80102d33:	8d 45 b8             	lea    -0x48(%ebp),%eax
80102d36:	50                   	push   %eax
80102d37:	e8 f4 1b 00 00       	call   80104930 <memcmp>
80102d3c:	83 c4 10             	add    $0x10,%esp
80102d3f:	85 c0                	test   %eax,%eax
80102d41:	0f 85 f1 fe ff ff    	jne    80102c38 <cmostime+0x28>
      break;
  }

  // convert
  if(bcd) {
80102d47:	80 7d b3 00          	cmpb   $0x0,-0x4d(%ebp)
80102d4b:	75 78                	jne    80102dc5 <cmostime+0x1b5>
#define    CONV(x)     (t1.x = ((t1.x >> 4) * 10) + (t1.x & 0xf))
    CONV(second);
80102d4d:	8b 45 b8             	mov    -0x48(%ebp),%eax
80102d50:	89 c2                	mov    %eax,%edx
80102d52:	83 e0 0f             	and    $0xf,%eax
80102d55:	c1 ea 04             	shr    $0x4,%edx
80102d58:	8d 14 92             	lea    (%edx,%edx,4),%edx
80102d5b:	8d 04 50             	lea    (%eax,%edx,2),%eax
80102d5e:	89 45 b8             	mov    %eax,-0x48(%ebp)
    CONV(minute);
80102d61:	8b 45 bc             	mov    -0x44(%ebp),%eax
80102d64:	89 c2                	mov    %eax,%edx
80102d66:	83 e0 0f             	and    $0xf,%eax
80102d69:	c1 ea 04             	shr    $0x4,%edx
80102d6c:	8d 14 92             	lea    (%edx,%edx,4),%edx
80102d6f:	8d 04 50             	lea    (%eax,%edx,2),%eax
80102d72:	89 45 bc             	mov    %eax,-0x44(%ebp)
    CONV(hour  );
80102d75:	8b 45 c0             	mov    -0x40(%ebp),%eax
80102d78:	89 c2                	mov    %eax,%edx
80102d7a:	83 e0 0f             	and    $0xf,%eax
80102d7d:	c1 ea 04             	shr    $0x4,%edx
80102d80:	8d 14 92             	lea    (%edx,%edx,4),%edx
80102d83:	8d 04 50             	lea    (%eax,%edx,2),%eax
80102d86:	89 45 c0             	mov    %eax,-0x40(%ebp)
    CONV(day   );
80102d89:	8b 45 c4             	mov    -0x3c(%ebp),%eax
80102d8c:	89 c2                	mov    %eax,%edx
80102d8e:	83 e0 0f             	and    $0xf,%eax
80102d91:	c1 ea 04             	shr    $0x4,%edx
80102d94:	8d 14 92             	lea    (%edx,%edx,4),%edx
80102d97:	8d 04 50             	lea    (%eax,%edx,2),%eax
80102d9a:	89 45 c4             	mov    %eax,-0x3c(%ebp)
    CONV(month );
80102d9d:	8b 45 c8             	mov    -0x38(%ebp),%eax
80102da0:	89 c2                	mov    %eax,%edx
80102da2:	83 e0 0f             	and    $0xf,%eax
80102da5:	c1 ea 04             	shr    $0x4,%edx
80102da8:	8d 14 92             	lea    (%edx,%edx,4),%edx
80102dab:	8d 04 50             	lea    (%eax,%edx,2),%eax
80102dae:	89 45 c8             	mov    %eax,-0x38(%ebp)
    CONV(year  );
80102db1:	8b 45 cc             	mov    -0x34(%ebp),%eax
80102db4:	89 c2                	mov    %eax,%edx
80102db6:	83 e0 0f             	and    $0xf,%eax
80102db9:	c1 ea 04             	shr    $0x4,%edx
80102dbc:	8d 14 92             	lea    (%edx,%edx,4),%edx
80102dbf:	8d 04 50             	lea    (%eax,%edx,2),%eax
80102dc2:	89 45 cc             	mov    %eax,-0x34(%ebp)
#undef     CONV
  }

  *r = t1;
80102dc5:	8b 75 08             	mov    0x8(%ebp),%esi
80102dc8:	8b 45 b8             	mov    -0x48(%ebp),%eax
80102dcb:	89 06                	mov    %eax,(%esi)
80102dcd:	8b 45 bc             	mov    -0x44(%ebp),%eax
80102dd0:	89 46 04             	mov    %eax,0x4(%esi)
80102dd3:	8b 45 c0             	mov    -0x40(%ebp),%eax
80102dd6:	89 46 08             	mov    %eax,0x8(%esi)
80102dd9:	8b 45 c4             	mov    -0x3c(%ebp),%eax
80102ddc:	89 46 0c             	mov    %eax,0xc(%esi)
80102ddf:	8b 45 c8             	mov    -0x38(%ebp),%eax
80102de2:	89 46 10             	mov    %eax,0x10(%esi)
80102de5:	8b 45 cc             	mov    -0x34(%ebp),%eax
80102de8:	89 46 14             	mov    %eax,0x14(%esi)
  r->year += 2000;
80102deb:	81 46 14 d0 07 00 00 	addl   $0x7d0,0x14(%esi)
}
80102df2:	8d 65 f4             	lea    -0xc(%ebp),%esp
80102df5:	5b                   	pop    %ebx
80102df6:	5e                   	pop    %esi
80102df7:	5f                   	pop    %edi
80102df8:	5d                   	pop    %ebp
80102df9:	c3                   	ret    
80102dfa:	66 90                	xchg   %ax,%ax
80102dfc:	66 90                	xchg   %ax,%ax
80102dfe:	66 90                	xchg   %ax,%ax

80102e00 <install_trans>:
static void
install_trans(void)
{
  int tail;

  for (tail = 0; tail < log.lh.n; tail++) {
80102e00:	8b 0d c8 36 11 80    	mov    0x801136c8,%ecx
80102e06:	85 c9                	test   %ecx,%ecx
80102e08:	0f 8e 8a 00 00 00    	jle    80102e98 <install_trans+0x98>
{
80102e0e:	55                   	push   %ebp
80102e0f:	89 e5                	mov    %esp,%ebp
80102e11:	57                   	push   %edi
80102e12:	56                   	push   %esi
80102e13:	53                   	push   %ebx
  for (tail = 0; tail < log.lh.n; tail++) {
80102e14:	31 db                	xor    %ebx,%ebx
{
80102e16:	83 ec 0c             	sub    $0xc,%esp
80102e19:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    struct buf *lbuf = bread(log.dev, log.start+tail+1); // read log block
80102e20:	a1 b4 36 11 80       	mov    0x801136b4,%eax
80102e25:	83 ec 08             	sub    $0x8,%esp
80102e28:	01 d8                	add    %ebx,%eax
80102e2a:	83 c0 01             	add    $0x1,%eax
80102e2d:	50                   	push   %eax
80102e2e:	ff 35 c4 36 11 80    	pushl  0x801136c4
80102e34:	e8 97 d2 ff ff       	call   801000d0 <bread>
80102e39:	89 c7                	mov    %eax,%edi
    struct buf *dbuf = bread(log.dev, log.lh.block[tail]); // read dst
80102e3b:	58                   	pop    %eax
80102e3c:	5a                   	pop    %edx
80102e3d:	ff 34 9d cc 36 11 80 	pushl  -0x7feec934(,%ebx,4)
80102e44:	ff 35 c4 36 11 80    	pushl  0x801136c4
  for (tail = 0; tail < log.lh.n; tail++) {
80102e4a:	83 c3 01             	add    $0x1,%ebx
    struct buf *dbuf = bread(log.dev, log.lh.block[tail]); // read dst
80102e4d:	e8 7e d2 ff ff       	call   801000d0 <bread>
80102e52:	89 c6                	mov    %eax,%esi
    memmove(dbuf->data, lbuf->data, BSIZE);  // copy block to dst
80102e54:	8d 47 5c             	lea    0x5c(%edi),%eax
80102e57:	83 c4 0c             	add    $0xc,%esp
80102e5a:	68 00 02 00 00       	push   $0x200
80102e5f:	50                   	push   %eax
80102e60:	8d 46 5c             	lea    0x5c(%esi),%eax
80102e63:	50                   	push   %eax
80102e64:	e8 27 1b 00 00       	call   80104990 <memmove>
    bwrite(dbuf);  // write dst to disk
80102e69:	89 34 24             	mov    %esi,(%esp)
80102e6c:	e8 2f d3 ff ff       	call   801001a0 <bwrite>
    brelse(lbuf);
80102e71:	89 3c 24             	mov    %edi,(%esp)
80102e74:	e8 67 d3 ff ff       	call   801001e0 <brelse>
    brelse(dbuf);
80102e79:	89 34 24             	mov    %esi,(%esp)
80102e7c:	e8 5f d3 ff ff       	call   801001e0 <brelse>
  for (tail = 0; tail < log.lh.n; tail++) {
80102e81:	83 c4 10             	add    $0x10,%esp
80102e84:	39 1d c8 36 11 80    	cmp    %ebx,0x801136c8
80102e8a:	7f 94                	jg     80102e20 <install_trans+0x20>
  }
}
80102e8c:	8d 65 f4             	lea    -0xc(%ebp),%esp
80102e8f:	5b                   	pop    %ebx
80102e90:	5e                   	pop    %esi
80102e91:	5f                   	pop    %edi
80102e92:	5d                   	pop    %ebp
80102e93:	c3                   	ret    
80102e94:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80102e98:	f3 c3                	repz ret 
80102e9a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi

80102ea0 <write_head>:
// Write in-memory log header to disk.
// This is the true point at which the
// current transaction commits.
static void
write_head(void)
{
80102ea0:	55                   	push   %ebp
80102ea1:	89 e5                	mov    %esp,%ebp
80102ea3:	56                   	push   %esi
80102ea4:	53                   	push   %ebx
  struct buf *buf = bread(log.dev, log.start);
80102ea5:	83 ec 08             	sub    $0x8,%esp
80102ea8:	ff 35 b4 36 11 80    	pushl  0x801136b4
80102eae:	ff 35 c4 36 11 80    	pushl  0x801136c4
80102eb4:	e8 17 d2 ff ff       	call   801000d0 <bread>
  struct logheader *hb = (struct logheader *) (buf->data);
  int i;
  hb->n = log.lh.n;
80102eb9:	8b 1d c8 36 11 80    	mov    0x801136c8,%ebx
  for (i = 0; i < log.lh.n; i++) {
80102ebf:	83 c4 10             	add    $0x10,%esp
  struct buf *buf = bread(log.dev, log.start);
80102ec2:	89 c6                	mov    %eax,%esi
  for (i = 0; i < log.lh.n; i++) {
80102ec4:	85 db                	test   %ebx,%ebx
  hb->n = log.lh.n;
80102ec6:	89 58 5c             	mov    %ebx,0x5c(%eax)
  for (i = 0; i < log.lh.n; i++) {
80102ec9:	7e 16                	jle    80102ee1 <write_head+0x41>
80102ecb:	c1 e3 02             	shl    $0x2,%ebx
80102ece:	31 d2                	xor    %edx,%edx
    hb->block[i] = log.lh.block[i];
80102ed0:	8b 8a cc 36 11 80    	mov    -0x7feec934(%edx),%ecx
80102ed6:	89 4c 16 60          	mov    %ecx,0x60(%esi,%edx,1)
80102eda:	83 c2 04             	add    $0x4,%edx
  for (i = 0; i < log.lh.n; i++) {
80102edd:	39 da                	cmp    %ebx,%edx
80102edf:	75 ef                	jne    80102ed0 <write_head+0x30>
  }
  bwrite(buf);
80102ee1:	83 ec 0c             	sub    $0xc,%esp
80102ee4:	56                   	push   %esi
80102ee5:	e8 b6 d2 ff ff       	call   801001a0 <bwrite>
  brelse(buf);
80102eea:	89 34 24             	mov    %esi,(%esp)
80102eed:	e8 ee d2 ff ff       	call   801001e0 <brelse>
}
80102ef2:	83 c4 10             	add    $0x10,%esp
80102ef5:	8d 65 f8             	lea    -0x8(%ebp),%esp
80102ef8:	5b                   	pop    %ebx
80102ef9:	5e                   	pop    %esi
80102efa:	5d                   	pop    %ebp
80102efb:	c3                   	ret    
80102efc:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

80102f00 <initlog>:
{
80102f00:	55                   	push   %ebp
80102f01:	89 e5                	mov    %esp,%ebp
80102f03:	53                   	push   %ebx
80102f04:	83 ec 2c             	sub    $0x2c,%esp
80102f07:	8b 5d 08             	mov    0x8(%ebp),%ebx
  initlock(&log.lock, "log");
80102f0a:	68 60 7c 10 80       	push   $0x80107c60
80102f0f:	68 80 36 11 80       	push   $0x80113680
80102f14:	e8 77 17 00 00       	call   80104690 <initlock>
  readsb(dev, &sb);
80102f19:	58                   	pop    %eax
80102f1a:	8d 45 dc             	lea    -0x24(%ebp),%eax
80102f1d:	5a                   	pop    %edx
80102f1e:	50                   	push   %eax
80102f1f:	53                   	push   %ebx
80102f20:	e8 1b e5 ff ff       	call   80101440 <readsb>
  log.size = sb.nlog;
80102f25:	8b 55 e8             	mov    -0x18(%ebp),%edx
  log.start = sb.logstart;
80102f28:	8b 45 ec             	mov    -0x14(%ebp),%eax
  struct buf *buf = bread(log.dev, log.start);
80102f2b:	59                   	pop    %ecx
  log.dev = dev;
80102f2c:	89 1d c4 36 11 80    	mov    %ebx,0x801136c4
  log.size = sb.nlog;
80102f32:	89 15 b8 36 11 80    	mov    %edx,0x801136b8
  log.start = sb.logstart;
80102f38:	a3 b4 36 11 80       	mov    %eax,0x801136b4
  struct buf *buf = bread(log.dev, log.start);
80102f3d:	5a                   	pop    %edx
80102f3e:	50                   	push   %eax
80102f3f:	53                   	push   %ebx
80102f40:	e8 8b d1 ff ff       	call   801000d0 <bread>
  log.lh.n = lh->n;
80102f45:	8b 58 5c             	mov    0x5c(%eax),%ebx
  for (i = 0; i < log.lh.n; i++) {
80102f48:	83 c4 10             	add    $0x10,%esp
80102f4b:	85 db                	test   %ebx,%ebx
  log.lh.n = lh->n;
80102f4d:	89 1d c8 36 11 80    	mov    %ebx,0x801136c8
  for (i = 0; i < log.lh.n; i++) {
80102f53:	7e 1c                	jle    80102f71 <initlog+0x71>
80102f55:	c1 e3 02             	shl    $0x2,%ebx
80102f58:	31 d2                	xor    %edx,%edx
80102f5a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    log.lh.block[i] = lh->block[i];
80102f60:	8b 4c 10 60          	mov    0x60(%eax,%edx,1),%ecx
80102f64:	83 c2 04             	add    $0x4,%edx
80102f67:	89 8a c8 36 11 80    	mov    %ecx,-0x7feec938(%edx)
  for (i = 0; i < log.lh.n; i++) {
80102f6d:	39 d3                	cmp    %edx,%ebx
80102f6f:	75 ef                	jne    80102f60 <initlog+0x60>
  brelse(buf);
80102f71:	83 ec 0c             	sub    $0xc,%esp
80102f74:	50                   	push   %eax
80102f75:	e8 66 d2 ff ff       	call   801001e0 <brelse>

static void
recover_from_log(void)
{
  read_head();
  install_trans(); // if committed, copy from log to disk
80102f7a:	e8 81 fe ff ff       	call   80102e00 <install_trans>
  log.lh.n = 0;
80102f7f:	c7 05 c8 36 11 80 00 	movl   $0x0,0x801136c8
80102f86:	00 00 00 
  write_head(); // clear the log
80102f89:	e8 12 ff ff ff       	call   80102ea0 <write_head>
}
80102f8e:	83 c4 10             	add    $0x10,%esp
80102f91:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80102f94:	c9                   	leave  
80102f95:	c3                   	ret    
80102f96:	8d 76 00             	lea    0x0(%esi),%esi
80102f99:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80102fa0 <begin_op>:
}

// called at the start of each FS system call.
void
begin_op(void)
{
80102fa0:	55                   	push   %ebp
80102fa1:	89 e5                	mov    %esp,%ebp
80102fa3:	83 ec 14             	sub    $0x14,%esp
  acquire(&log.lock);
80102fa6:	68 80 36 11 80       	push   $0x80113680
80102fab:	e8 20 18 00 00       	call   801047d0 <acquire>
80102fb0:	83 c4 10             	add    $0x10,%esp
80102fb3:	eb 18                	jmp    80102fcd <begin_op+0x2d>
80102fb5:	8d 76 00             	lea    0x0(%esi),%esi
  while(1){
    if(log.committing){
      sleep(&log, &log.lock);
80102fb8:	83 ec 08             	sub    $0x8,%esp
80102fbb:	68 80 36 11 80       	push   $0x80113680
80102fc0:	68 80 36 11 80       	push   $0x80113680
80102fc5:	e8 26 12 00 00       	call   801041f0 <sleep>
80102fca:	83 c4 10             	add    $0x10,%esp
    if(log.committing){
80102fcd:	a1 c0 36 11 80       	mov    0x801136c0,%eax
80102fd2:	85 c0                	test   %eax,%eax
80102fd4:	75 e2                	jne    80102fb8 <begin_op+0x18>
    } else if(log.lh.n + (log.outstanding+1)*MAXOPBLOCKS > LOGSIZE){
80102fd6:	a1 bc 36 11 80       	mov    0x801136bc,%eax
80102fdb:	8b 15 c8 36 11 80    	mov    0x801136c8,%edx
80102fe1:	83 c0 01             	add    $0x1,%eax
80102fe4:	8d 0c 80             	lea    (%eax,%eax,4),%ecx
80102fe7:	8d 14 4a             	lea    (%edx,%ecx,2),%edx
80102fea:	83 fa 1e             	cmp    $0x1e,%edx
80102fed:	7f c9                	jg     80102fb8 <begin_op+0x18>
      // this op might exhaust log space; wait for commit.
      sleep(&log, &log.lock);
    } else {
      log.outstanding += 1;
      release(&log.lock);
80102fef:	83 ec 0c             	sub    $0xc,%esp
      log.outstanding += 1;
80102ff2:	a3 bc 36 11 80       	mov    %eax,0x801136bc
      release(&log.lock);
80102ff7:	68 80 36 11 80       	push   $0x80113680
80102ffc:	e8 8f 18 00 00       	call   80104890 <release>
      break;
    }
  }
}
80103001:	83 c4 10             	add    $0x10,%esp
80103004:	c9                   	leave  
80103005:	c3                   	ret    
80103006:	8d 76 00             	lea    0x0(%esi),%esi
80103009:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80103010 <end_op>:

// called at the end of each FS system call.
// commits if this was the last outstanding operation.
void
end_op(void)
{
80103010:	55                   	push   %ebp
80103011:	89 e5                	mov    %esp,%ebp
80103013:	57                   	push   %edi
80103014:	56                   	push   %esi
80103015:	53                   	push   %ebx
80103016:	83 ec 18             	sub    $0x18,%esp
  int do_commit = 0;

  acquire(&log.lock);
80103019:	68 80 36 11 80       	push   $0x80113680
8010301e:	e8 ad 17 00 00       	call   801047d0 <acquire>
  log.outstanding -= 1;
80103023:	a1 bc 36 11 80       	mov    0x801136bc,%eax
  if(log.committing)
80103028:	8b 35 c0 36 11 80    	mov    0x801136c0,%esi
8010302e:	83 c4 10             	add    $0x10,%esp
  log.outstanding -= 1;
80103031:	8d 58 ff             	lea    -0x1(%eax),%ebx
  if(log.committing)
80103034:	85 f6                	test   %esi,%esi
  log.outstanding -= 1;
80103036:	89 1d bc 36 11 80    	mov    %ebx,0x801136bc
  if(log.committing)
8010303c:	0f 85 1a 01 00 00    	jne    8010315c <end_op+0x14c>
    panic("log.committing");
  if(log.outstanding == 0){
80103042:	85 db                	test   %ebx,%ebx
80103044:	0f 85 ee 00 00 00    	jne    80103138 <end_op+0x128>
    // begin_op() may be waiting for log space,
    // and decrementing log.outstanding has decreased
    // the amount of reserved space.
    wakeup(&log);
  }
  release(&log.lock);
8010304a:	83 ec 0c             	sub    $0xc,%esp
    log.committing = 1;
8010304d:	c7 05 c0 36 11 80 01 	movl   $0x1,0x801136c0
80103054:	00 00 00 
  release(&log.lock);
80103057:	68 80 36 11 80       	push   $0x80113680
8010305c:	e8 2f 18 00 00       	call   80104890 <release>
}

static void
commit()
{
  if (log.lh.n > 0) {
80103061:	8b 0d c8 36 11 80    	mov    0x801136c8,%ecx
80103067:	83 c4 10             	add    $0x10,%esp
8010306a:	85 c9                	test   %ecx,%ecx
8010306c:	0f 8e 85 00 00 00    	jle    801030f7 <end_op+0xe7>
    struct buf *to = bread(log.dev, log.start+tail+1); // log block
80103072:	a1 b4 36 11 80       	mov    0x801136b4,%eax
80103077:	83 ec 08             	sub    $0x8,%esp
8010307a:	01 d8                	add    %ebx,%eax
8010307c:	83 c0 01             	add    $0x1,%eax
8010307f:	50                   	push   %eax
80103080:	ff 35 c4 36 11 80    	pushl  0x801136c4
80103086:	e8 45 d0 ff ff       	call   801000d0 <bread>
8010308b:	89 c6                	mov    %eax,%esi
    struct buf *from = bread(log.dev, log.lh.block[tail]); // cache block
8010308d:	58                   	pop    %eax
8010308e:	5a                   	pop    %edx
8010308f:	ff 34 9d cc 36 11 80 	pushl  -0x7feec934(,%ebx,4)
80103096:	ff 35 c4 36 11 80    	pushl  0x801136c4
  for (tail = 0; tail < log.lh.n; tail++) {
8010309c:	83 c3 01             	add    $0x1,%ebx
    struct buf *from = bread(log.dev, log.lh.block[tail]); // cache block
8010309f:	e8 2c d0 ff ff       	call   801000d0 <bread>
801030a4:	89 c7                	mov    %eax,%edi
    memmove(to->data, from->data, BSIZE);
801030a6:	8d 40 5c             	lea    0x5c(%eax),%eax
801030a9:	83 c4 0c             	add    $0xc,%esp
801030ac:	68 00 02 00 00       	push   $0x200
801030b1:	50                   	push   %eax
801030b2:	8d 46 5c             	lea    0x5c(%esi),%eax
801030b5:	50                   	push   %eax
801030b6:	e8 d5 18 00 00       	call   80104990 <memmove>
    bwrite(to);  // write the log
801030bb:	89 34 24             	mov    %esi,(%esp)
801030be:	e8 dd d0 ff ff       	call   801001a0 <bwrite>
    brelse(from);
801030c3:	89 3c 24             	mov    %edi,(%esp)
801030c6:	e8 15 d1 ff ff       	call   801001e0 <brelse>
    brelse(to);
801030cb:	89 34 24             	mov    %esi,(%esp)
801030ce:	e8 0d d1 ff ff       	call   801001e0 <brelse>
  for (tail = 0; tail < log.lh.n; tail++) {
801030d3:	83 c4 10             	add    $0x10,%esp
801030d6:	3b 1d c8 36 11 80    	cmp    0x801136c8,%ebx
801030dc:	7c 94                	jl     80103072 <end_op+0x62>
    write_log();     // Write modified blocks from cache to log
    write_head();    // Write header to disk -- the real commit
801030de:	e8 bd fd ff ff       	call   80102ea0 <write_head>
    install_trans(); // Now install writes to home locations
801030e3:	e8 18 fd ff ff       	call   80102e00 <install_trans>
    log.lh.n = 0;
801030e8:	c7 05 c8 36 11 80 00 	movl   $0x0,0x801136c8
801030ef:	00 00 00 
    write_head();    // Erase the transaction from the log
801030f2:	e8 a9 fd ff ff       	call   80102ea0 <write_head>
    acquire(&log.lock);
801030f7:	83 ec 0c             	sub    $0xc,%esp
801030fa:	68 80 36 11 80       	push   $0x80113680
801030ff:	e8 cc 16 00 00       	call   801047d0 <acquire>
    wakeup(&log);
80103104:	c7 04 24 80 36 11 80 	movl   $0x80113680,(%esp)
    log.committing = 0;
8010310b:	c7 05 c0 36 11 80 00 	movl   $0x0,0x801136c0
80103112:	00 00 00 
    wakeup(&log);
80103115:	e8 96 12 00 00       	call   801043b0 <wakeup>
    release(&log.lock);
8010311a:	c7 04 24 80 36 11 80 	movl   $0x80113680,(%esp)
80103121:	e8 6a 17 00 00       	call   80104890 <release>
80103126:	83 c4 10             	add    $0x10,%esp
}
80103129:	8d 65 f4             	lea    -0xc(%ebp),%esp
8010312c:	5b                   	pop    %ebx
8010312d:	5e                   	pop    %esi
8010312e:	5f                   	pop    %edi
8010312f:	5d                   	pop    %ebp
80103130:	c3                   	ret    
80103131:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    wakeup(&log);
80103138:	83 ec 0c             	sub    $0xc,%esp
8010313b:	68 80 36 11 80       	push   $0x80113680
80103140:	e8 6b 12 00 00       	call   801043b0 <wakeup>
  release(&log.lock);
80103145:	c7 04 24 80 36 11 80 	movl   $0x80113680,(%esp)
8010314c:	e8 3f 17 00 00       	call   80104890 <release>
80103151:	83 c4 10             	add    $0x10,%esp
}
80103154:	8d 65 f4             	lea    -0xc(%ebp),%esp
80103157:	5b                   	pop    %ebx
80103158:	5e                   	pop    %esi
80103159:	5f                   	pop    %edi
8010315a:	5d                   	pop    %ebp
8010315b:	c3                   	ret    
    panic("log.committing");
8010315c:	83 ec 0c             	sub    $0xc,%esp
8010315f:	68 64 7c 10 80       	push   $0x80107c64
80103164:	e8 27 d2 ff ff       	call   80100390 <panic>
80103169:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

80103170 <log_write>:
//   modify bp->data[]
//   log_write(bp)
//   brelse(bp)
void
log_write(struct buf *b)
{
80103170:	55                   	push   %ebp
80103171:	89 e5                	mov    %esp,%ebp
80103173:	53                   	push   %ebx
80103174:	83 ec 04             	sub    $0x4,%esp
  int i;

  if (log.lh.n >= LOGSIZE || log.lh.n >= log.size - 1)
80103177:	8b 15 c8 36 11 80    	mov    0x801136c8,%edx
{
8010317d:	8b 5d 08             	mov    0x8(%ebp),%ebx
  if (log.lh.n >= LOGSIZE || log.lh.n >= log.size - 1)
80103180:	83 fa 1d             	cmp    $0x1d,%edx
80103183:	0f 8f 9d 00 00 00    	jg     80103226 <log_write+0xb6>
80103189:	a1 b8 36 11 80       	mov    0x801136b8,%eax
8010318e:	83 e8 01             	sub    $0x1,%eax
80103191:	39 c2                	cmp    %eax,%edx
80103193:	0f 8d 8d 00 00 00    	jge    80103226 <log_write+0xb6>
    panic("too big a transaction");
  if (log.outstanding < 1)
80103199:	a1 bc 36 11 80       	mov    0x801136bc,%eax
8010319e:	85 c0                	test   %eax,%eax
801031a0:	0f 8e 8d 00 00 00    	jle    80103233 <log_write+0xc3>
    panic("log_write outside of trans");

  acquire(&log.lock);
801031a6:	83 ec 0c             	sub    $0xc,%esp
801031a9:	68 80 36 11 80       	push   $0x80113680
801031ae:	e8 1d 16 00 00       	call   801047d0 <acquire>
  for (i = 0; i < log.lh.n; i++) {
801031b3:	8b 0d c8 36 11 80    	mov    0x801136c8,%ecx
801031b9:	83 c4 10             	add    $0x10,%esp
801031bc:	83 f9 00             	cmp    $0x0,%ecx
801031bf:	7e 57                	jle    80103218 <log_write+0xa8>
    if (log.lh.block[i] == b->blockno)   // log absorbtion
801031c1:	8b 53 08             	mov    0x8(%ebx),%edx
  for (i = 0; i < log.lh.n; i++) {
801031c4:	31 c0                	xor    %eax,%eax
    if (log.lh.block[i] == b->blockno)   // log absorbtion
801031c6:	3b 15 cc 36 11 80    	cmp    0x801136cc,%edx
801031cc:	75 0b                	jne    801031d9 <log_write+0x69>
801031ce:	eb 38                	jmp    80103208 <log_write+0x98>
801031d0:	39 14 85 cc 36 11 80 	cmp    %edx,-0x7feec934(,%eax,4)
801031d7:	74 2f                	je     80103208 <log_write+0x98>
  for (i = 0; i < log.lh.n; i++) {
801031d9:	83 c0 01             	add    $0x1,%eax
801031dc:	39 c1                	cmp    %eax,%ecx
801031de:	75 f0                	jne    801031d0 <log_write+0x60>
      break;
  }
  log.lh.block[i] = b->blockno;
801031e0:	89 14 85 cc 36 11 80 	mov    %edx,-0x7feec934(,%eax,4)
  if (i == log.lh.n)
    log.lh.n++;
801031e7:	83 c0 01             	add    $0x1,%eax
801031ea:	a3 c8 36 11 80       	mov    %eax,0x801136c8
  b->flags |= B_DIRTY; // prevent eviction
801031ef:	83 0b 04             	orl    $0x4,(%ebx)
  release(&log.lock);
801031f2:	c7 45 08 80 36 11 80 	movl   $0x80113680,0x8(%ebp)
}
801031f9:	8b 5d fc             	mov    -0x4(%ebp),%ebx
801031fc:	c9                   	leave  
  release(&log.lock);
801031fd:	e9 8e 16 00 00       	jmp    80104890 <release>
80103202:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
  log.lh.block[i] = b->blockno;
80103208:	89 14 85 cc 36 11 80 	mov    %edx,-0x7feec934(,%eax,4)
8010320f:	eb de                	jmp    801031ef <log_write+0x7f>
80103211:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80103218:	8b 43 08             	mov    0x8(%ebx),%eax
8010321b:	a3 cc 36 11 80       	mov    %eax,0x801136cc
  if (i == log.lh.n)
80103220:	75 cd                	jne    801031ef <log_write+0x7f>
80103222:	31 c0                	xor    %eax,%eax
80103224:	eb c1                	jmp    801031e7 <log_write+0x77>
    panic("too big a transaction");
80103226:	83 ec 0c             	sub    $0xc,%esp
80103229:	68 73 7c 10 80       	push   $0x80107c73
8010322e:	e8 5d d1 ff ff       	call   80100390 <panic>
    panic("log_write outside of trans");
80103233:	83 ec 0c             	sub    $0xc,%esp
80103236:	68 89 7c 10 80       	push   $0x80107c89
8010323b:	e8 50 d1 ff ff       	call   80100390 <panic>

80103240 <mpmain>:
}

// Common CPU setup code.
static void
mpmain(void)
{
80103240:	55                   	push   %ebp
80103241:	89 e5                	mov    %esp,%ebp
80103243:	53                   	push   %ebx
80103244:	83 ec 04             	sub    $0x4,%esp
  cprintf("cpu%d: starting %d\n", cpuid(), cpuid());
80103247:	e8 c4 09 00 00       	call   80103c10 <cpuid>
8010324c:	89 c3                	mov    %eax,%ebx
8010324e:	e8 bd 09 00 00       	call   80103c10 <cpuid>
80103253:	83 ec 04             	sub    $0x4,%esp
80103256:	53                   	push   %ebx
80103257:	50                   	push   %eax
80103258:	68 a4 7c 10 80       	push   $0x80107ca4
8010325d:	e8 fe d3 ff ff       	call   80100660 <cprintf>
  idtinit();       // load idt register
80103262:	e8 39 29 00 00       	call   80105ba0 <idtinit>
  xchg(&(mycpu()->started), 1); // tell startothers() we're up
80103267:	e8 24 09 00 00       	call   80103b90 <mycpu>
8010326c:	89 c2                	mov    %eax,%edx
xchg(volatile uint *addr, uint newval)
{
  uint result;

  // The + in "+m" denotes a read-modify-write operand.
  asm volatile("lock; xchgl %0, %1" :
8010326e:	b8 01 00 00 00       	mov    $0x1,%eax
80103273:	f0 87 82 a0 00 00 00 	lock xchg %eax,0xa0(%edx)
  scheduler();     // start running processes
8010327a:	e8 71 0c 00 00       	call   80103ef0 <scheduler>
8010327f:	90                   	nop

80103280 <mpenter>:
{
80103280:	55                   	push   %ebp
80103281:	89 e5                	mov    %esp,%ebp
80103283:	83 ec 08             	sub    $0x8,%esp
  switchkvm();
80103286:	e8 75 39 00 00       	call   80106c00 <switchkvm>
  seginit();
8010328b:	e8 e0 38 00 00       	call   80106b70 <seginit>
  lapicinit();
80103290:	e8 9b f7 ff ff       	call   80102a30 <lapicinit>
  mpmain();
80103295:	e8 a6 ff ff ff       	call   80103240 <mpmain>
8010329a:	66 90                	xchg   %ax,%ax
8010329c:	66 90                	xchg   %ax,%ax
8010329e:	66 90                	xchg   %ax,%ax

801032a0 <main>:
{
801032a0:	8d 4c 24 04          	lea    0x4(%esp),%ecx
801032a4:	83 e4 f0             	and    $0xfffffff0,%esp
801032a7:	ff 71 fc             	pushl  -0x4(%ecx)
801032aa:	55                   	push   %ebp
801032ab:	89 e5                	mov    %esp,%ebp
801032ad:	53                   	push   %ebx
801032ae:	51                   	push   %ecx
  kinit1(end, P2V(4*1024*1024)); // phys page allocator
801032af:	83 ec 08             	sub    $0x8,%esp
801032b2:	68 00 00 40 80       	push   $0x80400000
801032b7:	68 a8 98 11 80       	push   $0x801198a8
801032bc:	e8 2f f5 ff ff       	call   801027f0 <kinit1>
  kvmalloc();      // kernel page table
801032c1:	e8 aa 3d 00 00       	call   80107070 <kvmalloc>
  mpinit();        // detect other processors
801032c6:	e8 75 01 00 00       	call   80103440 <mpinit>
  lapicinit();     // interrupt controller
801032cb:	e8 60 f7 ff ff       	call   80102a30 <lapicinit>
  seginit();       // segment descriptors
801032d0:	e8 9b 38 00 00       	call   80106b70 <seginit>
  picinit();       // disable pic
801032d5:	e8 46 03 00 00       	call   80103620 <picinit>
  ioapicinit();    // another interrupt controller
801032da:	e8 d1 f2 ff ff       	call   801025b0 <ioapicinit>
  consoleinit();   // console hardware
801032df:	e8 dc d6 ff ff       	call   801009c0 <consoleinit>
  uartinit();      // serial port
801032e4:	e8 f7 2b 00 00       	call   80105ee0 <uartinit>
  pinit();         // process table
801032e9:	e8 82 08 00 00       	call   80103b70 <pinit>
  tvinit();        // trap vectors
801032ee:	e8 2d 28 00 00       	call   80105b20 <tvinit>
  binit();         // buffer cache
801032f3:	e8 48 cd ff ff       	call   80100040 <binit>
  fileinit();      // file table
801032f8:	e8 63 da ff ff       	call   80100d60 <fileinit>
  ideinit();       // disk 
801032fd:	e8 8e f0 ff ff       	call   80102390 <ideinit>

  // Write entry code to unused memory at 0x7000.
  // The linker has placed the image of entryother.S in
  // _binary_entryother_start.
  code = P2V(0x7000);
  memmove(code, _binary_entryother_start, (uint)_binary_entryother_size);
80103302:	83 c4 0c             	add    $0xc,%esp
80103305:	68 8a 00 00 00       	push   $0x8a
8010330a:	68 8c b4 10 80       	push   $0x8010b48c
8010330f:	68 00 70 00 80       	push   $0x80007000
80103314:	e8 77 16 00 00       	call   80104990 <memmove>

  for(c = cpus; c < cpus+ncpu; c++){
80103319:	69 05 00 3d 11 80 b0 	imul   $0xb0,0x80113d00,%eax
80103320:	00 00 00 
80103323:	83 c4 10             	add    $0x10,%esp
80103326:	05 80 37 11 80       	add    $0x80113780,%eax
8010332b:	3d 80 37 11 80       	cmp    $0x80113780,%eax
80103330:	76 71                	jbe    801033a3 <main+0x103>
80103332:	bb 80 37 11 80       	mov    $0x80113780,%ebx
80103337:	89 f6                	mov    %esi,%esi
80103339:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi
    if(c == mycpu())  // We've started already.
80103340:	e8 4b 08 00 00       	call   80103b90 <mycpu>
80103345:	39 d8                	cmp    %ebx,%eax
80103347:	74 41                	je     8010338a <main+0xea>
      continue;

    // Tell entryother.S what stack to use, where to enter, and what
    // pgdir to use. We cannot use kpgdir yet, because the AP processor
    // is running in low  memory, so we use entrypgdir for the APs too.
    stack = kalloc();
80103349:	e8 72 f5 ff ff       	call   801028c0 <kalloc>
    *(void**)(code-4) = stack + KSTACKSIZE;
8010334e:	05 00 10 00 00       	add    $0x1000,%eax
    *(void(**)(void))(code-8) = mpenter;
80103353:	c7 05 f8 6f 00 80 80 	movl   $0x80103280,0x80006ff8
8010335a:	32 10 80 
    *(int**)(code-12) = (void *) V2P(entrypgdir);
8010335d:	c7 05 f4 6f 00 80 00 	movl   $0x10a000,0x80006ff4
80103364:	a0 10 00 
    *(void**)(code-4) = stack + KSTACKSIZE;
80103367:	a3 fc 6f 00 80       	mov    %eax,0x80006ffc

    lapicstartap(c->apicid, V2P(code));
8010336c:	0f b6 03             	movzbl (%ebx),%eax
8010336f:	83 ec 08             	sub    $0x8,%esp
80103372:	68 00 70 00 00       	push   $0x7000
80103377:	50                   	push   %eax
80103378:	e8 03 f8 ff ff       	call   80102b80 <lapicstartap>
8010337d:	83 c4 10             	add    $0x10,%esp

    // wait for cpu to finish mpmain()
    while(c->started == 0)
80103380:	8b 83 a0 00 00 00    	mov    0xa0(%ebx),%eax
80103386:	85 c0                	test   %eax,%eax
80103388:	74 f6                	je     80103380 <main+0xe0>
  for(c = cpus; c < cpus+ncpu; c++){
8010338a:	69 05 00 3d 11 80 b0 	imul   $0xb0,0x80113d00,%eax
80103391:	00 00 00 
80103394:	81 c3 b0 00 00 00    	add    $0xb0,%ebx
8010339a:	05 80 37 11 80       	add    $0x80113780,%eax
8010339f:	39 c3                	cmp    %eax,%ebx
801033a1:	72 9d                	jb     80103340 <main+0xa0>
  kinit2(P2V(4*1024*1024), P2V(PHYSTOP)); // must come after startothers()
801033a3:	83 ec 08             	sub    $0x8,%esp
801033a6:	68 00 00 00 8e       	push   $0x8e000000
801033ab:	68 00 00 40 80       	push   $0x80400000
801033b0:	e8 ab f4 ff ff       	call   80102860 <kinit2>
  userinit();      // first user process
801033b5:	e8 a6 08 00 00       	call   80103c60 <userinit>
  mpmain();        // finish this processor's setup
801033ba:	e8 81 fe ff ff       	call   80103240 <mpmain>
801033bf:	90                   	nop

801033c0 <mpsearch1>:
}

// Look for an MP structure in the len bytes at addr.
static struct mp*
mpsearch1(uint a, int len)
{
801033c0:	55                   	push   %ebp
801033c1:	89 e5                	mov    %esp,%ebp
801033c3:	57                   	push   %edi
801033c4:	56                   	push   %esi
  uchar *e, *p, *addr;

  addr = P2V(a);
801033c5:	8d b0 00 00 00 80    	lea    -0x80000000(%eax),%esi
{
801033cb:	53                   	push   %ebx
  e = addr+len;
801033cc:	8d 1c 16             	lea    (%esi,%edx,1),%ebx
{
801033cf:	83 ec 0c             	sub    $0xc,%esp
  for(p = addr; p < e; p += sizeof(struct mp))
801033d2:	39 de                	cmp    %ebx,%esi
801033d4:	72 10                	jb     801033e6 <mpsearch1+0x26>
801033d6:	eb 50                	jmp    80103428 <mpsearch1+0x68>
801033d8:	90                   	nop
801033d9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801033e0:	39 fb                	cmp    %edi,%ebx
801033e2:	89 fe                	mov    %edi,%esi
801033e4:	76 42                	jbe    80103428 <mpsearch1+0x68>
    if(memcmp(p, "_MP_", 4) == 0 && sum(p, sizeof(struct mp)) == 0)
801033e6:	83 ec 04             	sub    $0x4,%esp
801033e9:	8d 7e 10             	lea    0x10(%esi),%edi
801033ec:	6a 04                	push   $0x4
801033ee:	68 b8 7c 10 80       	push   $0x80107cb8
801033f3:	56                   	push   %esi
801033f4:	e8 37 15 00 00       	call   80104930 <memcmp>
801033f9:	83 c4 10             	add    $0x10,%esp
801033fc:	85 c0                	test   %eax,%eax
801033fe:	75 e0                	jne    801033e0 <mpsearch1+0x20>
80103400:	89 f1                	mov    %esi,%ecx
80103402:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    sum += addr[i];
80103408:	0f b6 11             	movzbl (%ecx),%edx
8010340b:	83 c1 01             	add    $0x1,%ecx
8010340e:	01 d0                	add    %edx,%eax
  for(i=0; i<len; i++)
80103410:	39 f9                	cmp    %edi,%ecx
80103412:	75 f4                	jne    80103408 <mpsearch1+0x48>
    if(memcmp(p, "_MP_", 4) == 0 && sum(p, sizeof(struct mp)) == 0)
80103414:	84 c0                	test   %al,%al
80103416:	75 c8                	jne    801033e0 <mpsearch1+0x20>
      return (struct mp*)p;
  return 0;
}
80103418:	8d 65 f4             	lea    -0xc(%ebp),%esp
8010341b:	89 f0                	mov    %esi,%eax
8010341d:	5b                   	pop    %ebx
8010341e:	5e                   	pop    %esi
8010341f:	5f                   	pop    %edi
80103420:	5d                   	pop    %ebp
80103421:	c3                   	ret    
80103422:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
80103428:	8d 65 f4             	lea    -0xc(%ebp),%esp
  return 0;
8010342b:	31 f6                	xor    %esi,%esi
}
8010342d:	89 f0                	mov    %esi,%eax
8010342f:	5b                   	pop    %ebx
80103430:	5e                   	pop    %esi
80103431:	5f                   	pop    %edi
80103432:	5d                   	pop    %ebp
80103433:	c3                   	ret    
80103434:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
8010343a:	8d bf 00 00 00 00    	lea    0x0(%edi),%edi

80103440 <mpinit>:
  return conf;
}

void
mpinit(void)
{
80103440:	55                   	push   %ebp
80103441:	89 e5                	mov    %esp,%ebp
80103443:	57                   	push   %edi
80103444:	56                   	push   %esi
80103445:	53                   	push   %ebx
80103446:	83 ec 1c             	sub    $0x1c,%esp
  if((p = ((bda[0x0F]<<8)| bda[0x0E]) << 4)){
80103449:	0f b6 05 0f 04 00 80 	movzbl 0x8000040f,%eax
80103450:	0f b6 15 0e 04 00 80 	movzbl 0x8000040e,%edx
80103457:	c1 e0 08             	shl    $0x8,%eax
8010345a:	09 d0                	or     %edx,%eax
8010345c:	c1 e0 04             	shl    $0x4,%eax
8010345f:	85 c0                	test   %eax,%eax
80103461:	75 1b                	jne    8010347e <mpinit+0x3e>
    p = ((bda[0x14]<<8)|bda[0x13])*1024;
80103463:	0f b6 05 14 04 00 80 	movzbl 0x80000414,%eax
8010346a:	0f b6 15 13 04 00 80 	movzbl 0x80000413,%edx
80103471:	c1 e0 08             	shl    $0x8,%eax
80103474:	09 d0                	or     %edx,%eax
80103476:	c1 e0 0a             	shl    $0xa,%eax
    if((mp = mpsearch1(p-1024, 1024)))
80103479:	2d 00 04 00 00       	sub    $0x400,%eax
    if((mp = mpsearch1(p, 1024)))
8010347e:	ba 00 04 00 00       	mov    $0x400,%edx
80103483:	e8 38 ff ff ff       	call   801033c0 <mpsearch1>
80103488:	85 c0                	test   %eax,%eax
8010348a:	89 45 e4             	mov    %eax,-0x1c(%ebp)
8010348d:	0f 84 3d 01 00 00    	je     801035d0 <mpinit+0x190>
  if((mp = mpsearch()) == 0 || mp->physaddr == 0)
80103493:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80103496:	8b 58 04             	mov    0x4(%eax),%ebx
80103499:	85 db                	test   %ebx,%ebx
8010349b:	0f 84 4f 01 00 00    	je     801035f0 <mpinit+0x1b0>
  conf = (struct mpconf*) P2V((uint) mp->physaddr);
801034a1:	8d b3 00 00 00 80    	lea    -0x80000000(%ebx),%esi
  if(memcmp(conf, "PCMP", 4) != 0)
801034a7:	83 ec 04             	sub    $0x4,%esp
801034aa:	6a 04                	push   $0x4
801034ac:	68 d5 7c 10 80       	push   $0x80107cd5
801034b1:	56                   	push   %esi
801034b2:	e8 79 14 00 00       	call   80104930 <memcmp>
801034b7:	83 c4 10             	add    $0x10,%esp
801034ba:	85 c0                	test   %eax,%eax
801034bc:	0f 85 2e 01 00 00    	jne    801035f0 <mpinit+0x1b0>
  if(conf->version != 1 && conf->version != 4)
801034c2:	0f b6 83 06 00 00 80 	movzbl -0x7ffffffa(%ebx),%eax
801034c9:	3c 01                	cmp    $0x1,%al
801034cb:	0f 95 c2             	setne  %dl
801034ce:	3c 04                	cmp    $0x4,%al
801034d0:	0f 95 c0             	setne  %al
801034d3:	20 c2                	and    %al,%dl
801034d5:	0f 85 15 01 00 00    	jne    801035f0 <mpinit+0x1b0>
  if(sum((uchar*)conf, conf->length) != 0)
801034db:	0f b7 bb 04 00 00 80 	movzwl -0x7ffffffc(%ebx),%edi
  for(i=0; i<len; i++)
801034e2:	66 85 ff             	test   %di,%di
801034e5:	74 1a                	je     80103501 <mpinit+0xc1>
801034e7:	89 f0                	mov    %esi,%eax
801034e9:	01 f7                	add    %esi,%edi
  sum = 0;
801034eb:	31 d2                	xor    %edx,%edx
801034ed:	8d 76 00             	lea    0x0(%esi),%esi
    sum += addr[i];
801034f0:	0f b6 08             	movzbl (%eax),%ecx
801034f3:	83 c0 01             	add    $0x1,%eax
801034f6:	01 ca                	add    %ecx,%edx
  for(i=0; i<len; i++)
801034f8:	39 c7                	cmp    %eax,%edi
801034fa:	75 f4                	jne    801034f0 <mpinit+0xb0>
801034fc:	84 d2                	test   %dl,%dl
801034fe:	0f 95 c2             	setne  %dl
  struct mp *mp;
  struct mpconf *conf;
  struct mpproc *proc;
  struct mpioapic *ioapic;

  if((conf = mpconfig(&mp)) == 0)
80103501:	85 f6                	test   %esi,%esi
80103503:	0f 84 e7 00 00 00    	je     801035f0 <mpinit+0x1b0>
80103509:	84 d2                	test   %dl,%dl
8010350b:	0f 85 df 00 00 00    	jne    801035f0 <mpinit+0x1b0>
    panic("Expect to run on an SMP");
  ismp = 1;
  lapic = (uint*)conf->lapicaddr;
80103511:	8b 83 24 00 00 80    	mov    -0x7fffffdc(%ebx),%eax
80103517:	a3 7c 36 11 80       	mov    %eax,0x8011367c
  for(p=(uchar*)(conf+1), e=(uchar*)conf+conf->length; p<e; ){
8010351c:	0f b7 93 04 00 00 80 	movzwl -0x7ffffffc(%ebx),%edx
80103523:	8d 83 2c 00 00 80    	lea    -0x7fffffd4(%ebx),%eax
  ismp = 1;
80103529:	bb 01 00 00 00       	mov    $0x1,%ebx
  for(p=(uchar*)(conf+1), e=(uchar*)conf+conf->length; p<e; ){
8010352e:	01 d6                	add    %edx,%esi
80103530:	39 c6                	cmp    %eax,%esi
80103532:	76 23                	jbe    80103557 <mpinit+0x117>
    switch(*p){
80103534:	0f b6 10             	movzbl (%eax),%edx
80103537:	80 fa 04             	cmp    $0x4,%dl
8010353a:	0f 87 ca 00 00 00    	ja     8010360a <mpinit+0x1ca>
80103540:	ff 24 95 fc 7c 10 80 	jmp    *-0x7fef8304(,%edx,4)
80103547:	89 f6                	mov    %esi,%esi
80103549:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi
      p += sizeof(struct mpioapic);
      continue;
    case MPBUS:
    case MPIOINTR:
    case MPLINTR:
      p += 8;
80103550:	83 c0 08             	add    $0x8,%eax
  for(p=(uchar*)(conf+1), e=(uchar*)conf+conf->length; p<e; ){
80103553:	39 c6                	cmp    %eax,%esi
80103555:	77 dd                	ja     80103534 <mpinit+0xf4>
    default:
      ismp = 0;
      break;
    }
  }
  if(!ismp)
80103557:	85 db                	test   %ebx,%ebx
80103559:	0f 84 9e 00 00 00    	je     801035fd <mpinit+0x1bd>
    panic("Didn't find a suitable machine");

  if(mp->imcrp){
8010355f:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80103562:	80 78 0c 00          	cmpb   $0x0,0xc(%eax)
80103566:	74 15                	je     8010357d <mpinit+0x13d>
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80103568:	b8 70 00 00 00       	mov    $0x70,%eax
8010356d:	ba 22 00 00 00       	mov    $0x22,%edx
80103572:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80103573:	ba 23 00 00 00       	mov    $0x23,%edx
80103578:	ec                   	in     (%dx),%al
    // Bochs doesn't support IMCR, so this doesn't run on Bochs.
    // But it would on real hardware.
    outb(0x22, 0x70);   // Select IMCR
    outb(0x23, inb(0x23) | 1);  // Mask external interrupts.
80103579:	83 c8 01             	or     $0x1,%eax
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
8010357c:	ee                   	out    %al,(%dx)
  }
}
8010357d:	8d 65 f4             	lea    -0xc(%ebp),%esp
80103580:	5b                   	pop    %ebx
80103581:	5e                   	pop    %esi
80103582:	5f                   	pop    %edi
80103583:	5d                   	pop    %ebp
80103584:	c3                   	ret    
80103585:	8d 76 00             	lea    0x0(%esi),%esi
      if(ncpu < NCPU) {
80103588:	8b 0d 00 3d 11 80    	mov    0x80113d00,%ecx
8010358e:	83 f9 07             	cmp    $0x7,%ecx
80103591:	7f 19                	jg     801035ac <mpinit+0x16c>
        cpus[ncpu].apicid = proc->apicid;  // apicid may differ from ncpu
80103593:	0f b6 50 01          	movzbl 0x1(%eax),%edx
80103597:	69 f9 b0 00 00 00    	imul   $0xb0,%ecx,%edi
        ncpu++;
8010359d:	83 c1 01             	add    $0x1,%ecx
801035a0:	89 0d 00 3d 11 80    	mov    %ecx,0x80113d00
        cpus[ncpu].apicid = proc->apicid;  // apicid may differ from ncpu
801035a6:	88 97 80 37 11 80    	mov    %dl,-0x7feec880(%edi)
      p += sizeof(struct mpproc);
801035ac:	83 c0 14             	add    $0x14,%eax
      continue;
801035af:	e9 7c ff ff ff       	jmp    80103530 <mpinit+0xf0>
801035b4:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
      ioapicid = ioapic->apicno;
801035b8:	0f b6 50 01          	movzbl 0x1(%eax),%edx
      p += sizeof(struct mpioapic);
801035bc:	83 c0 08             	add    $0x8,%eax
      ioapicid = ioapic->apicno;
801035bf:	88 15 60 37 11 80    	mov    %dl,0x80113760
      continue;
801035c5:	e9 66 ff ff ff       	jmp    80103530 <mpinit+0xf0>
801035ca:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
  return mpsearch1(0xF0000, 0x10000);
801035d0:	ba 00 00 01 00       	mov    $0x10000,%edx
801035d5:	b8 00 00 0f 00       	mov    $0xf0000,%eax
801035da:	e8 e1 fd ff ff       	call   801033c0 <mpsearch1>
  if((mp = mpsearch()) == 0 || mp->physaddr == 0)
801035df:	85 c0                	test   %eax,%eax
  return mpsearch1(0xF0000, 0x10000);
801035e1:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  if((mp = mpsearch()) == 0 || mp->physaddr == 0)
801035e4:	0f 85 a9 fe ff ff    	jne    80103493 <mpinit+0x53>
801035ea:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    panic("Expect to run on an SMP");
801035f0:	83 ec 0c             	sub    $0xc,%esp
801035f3:	68 bd 7c 10 80       	push   $0x80107cbd
801035f8:	e8 93 cd ff ff       	call   80100390 <panic>
    panic("Didn't find a suitable machine");
801035fd:	83 ec 0c             	sub    $0xc,%esp
80103600:	68 dc 7c 10 80       	push   $0x80107cdc
80103605:	e8 86 cd ff ff       	call   80100390 <panic>
      ismp = 0;
8010360a:	31 db                	xor    %ebx,%ebx
8010360c:	e9 26 ff ff ff       	jmp    80103537 <mpinit+0xf7>
80103611:	66 90                	xchg   %ax,%ax
80103613:	66 90                	xchg   %ax,%ax
80103615:	66 90                	xchg   %ax,%ax
80103617:	66 90                	xchg   %ax,%ax
80103619:	66 90                	xchg   %ax,%ax
8010361b:	66 90                	xchg   %ax,%ax
8010361d:	66 90                	xchg   %ax,%ax
8010361f:	90                   	nop

80103620 <picinit>:
#define IO_PIC2         0xA0    // Slave (IRQs 8-15)

// Don't use the 8259A interrupt controllers.  Xv6 assumes SMP hardware.
void
picinit(void)
{
80103620:	55                   	push   %ebp
80103621:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80103626:	ba 21 00 00 00       	mov    $0x21,%edx
8010362b:	89 e5                	mov    %esp,%ebp
8010362d:	ee                   	out    %al,(%dx)
8010362e:	ba a1 00 00 00       	mov    $0xa1,%edx
80103633:	ee                   	out    %al,(%dx)
  // mask all interrupts
  outb(IO_PIC1+1, 0xFF);
  outb(IO_PIC2+1, 0xFF);
}
80103634:	5d                   	pop    %ebp
80103635:	c3                   	ret    
80103636:	66 90                	xchg   %ax,%ax
80103638:	66 90                	xchg   %ax,%ax
8010363a:	66 90                	xchg   %ax,%ax
8010363c:	66 90                	xchg   %ax,%ax
8010363e:	66 90                	xchg   %ax,%ax

80103640 <pipealloc>:
  int writeopen;  // write fd is still open
};

int
pipealloc(struct file **f0, struct file **f1)
{
80103640:	55                   	push   %ebp
80103641:	89 e5                	mov    %esp,%ebp
80103643:	57                   	push   %edi
80103644:	56                   	push   %esi
80103645:	53                   	push   %ebx
80103646:	83 ec 0c             	sub    $0xc,%esp
80103649:	8b 5d 08             	mov    0x8(%ebp),%ebx
8010364c:	8b 75 0c             	mov    0xc(%ebp),%esi
  struct pipe *p;

  p = 0;
  *f0 = *f1 = 0;
8010364f:	c7 06 00 00 00 00    	movl   $0x0,(%esi)
80103655:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  if((*f0 = filealloc()) == 0 || (*f1 = filealloc()) == 0)
8010365b:	e8 20 d7 ff ff       	call   80100d80 <filealloc>
80103660:	85 c0                	test   %eax,%eax
80103662:	89 03                	mov    %eax,(%ebx)
80103664:	74 22                	je     80103688 <pipealloc+0x48>
80103666:	e8 15 d7 ff ff       	call   80100d80 <filealloc>
8010366b:	85 c0                	test   %eax,%eax
8010366d:	89 06                	mov    %eax,(%esi)
8010366f:	74 3f                	je     801036b0 <pipealloc+0x70>
    goto bad;
  if((p = (struct pipe*)kalloc()) == 0)
80103671:	e8 4a f2 ff ff       	call   801028c0 <kalloc>
80103676:	85 c0                	test   %eax,%eax
80103678:	89 c7                	mov    %eax,%edi
8010367a:	75 54                	jne    801036d0 <pipealloc+0x90>

//PAGEBREAK: 20
 bad:
  if(p)
    kfree((char*)p);
  if(*f0)
8010367c:	8b 03                	mov    (%ebx),%eax
8010367e:	85 c0                	test   %eax,%eax
80103680:	75 34                	jne    801036b6 <pipealloc+0x76>
80103682:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    fileclose(*f0);
  if(*f1)
80103688:	8b 06                	mov    (%esi),%eax
8010368a:	85 c0                	test   %eax,%eax
8010368c:	74 0c                	je     8010369a <pipealloc+0x5a>
    fileclose(*f1);
8010368e:	83 ec 0c             	sub    $0xc,%esp
80103691:	50                   	push   %eax
80103692:	e8 a9 d7 ff ff       	call   80100e40 <fileclose>
80103697:	83 c4 10             	add    $0x10,%esp
  return -1;
}
8010369a:	8d 65 f4             	lea    -0xc(%ebp),%esp
  return -1;
8010369d:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
801036a2:	5b                   	pop    %ebx
801036a3:	5e                   	pop    %esi
801036a4:	5f                   	pop    %edi
801036a5:	5d                   	pop    %ebp
801036a6:	c3                   	ret    
801036a7:	89 f6                	mov    %esi,%esi
801036a9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi
  if(*f0)
801036b0:	8b 03                	mov    (%ebx),%eax
801036b2:	85 c0                	test   %eax,%eax
801036b4:	74 e4                	je     8010369a <pipealloc+0x5a>
    fileclose(*f0);
801036b6:	83 ec 0c             	sub    $0xc,%esp
801036b9:	50                   	push   %eax
801036ba:	e8 81 d7 ff ff       	call   80100e40 <fileclose>
  if(*f1)
801036bf:	8b 06                	mov    (%esi),%eax
    fileclose(*f0);
801036c1:	83 c4 10             	add    $0x10,%esp
  if(*f1)
801036c4:	85 c0                	test   %eax,%eax
801036c6:	75 c6                	jne    8010368e <pipealloc+0x4e>
801036c8:	eb d0                	jmp    8010369a <pipealloc+0x5a>
801036ca:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
  initlock(&p->lock, "pipe");
801036d0:	83 ec 08             	sub    $0x8,%esp
  p->readopen = 1;
801036d3:	c7 80 3c 02 00 00 01 	movl   $0x1,0x23c(%eax)
801036da:	00 00 00 
  p->writeopen = 1;
801036dd:	c7 80 40 02 00 00 01 	movl   $0x1,0x240(%eax)
801036e4:	00 00 00 
  p->nwrite = 0;
801036e7:	c7 80 38 02 00 00 00 	movl   $0x0,0x238(%eax)
801036ee:	00 00 00 
  p->nread = 0;
801036f1:	c7 80 34 02 00 00 00 	movl   $0x0,0x234(%eax)
801036f8:	00 00 00 
  initlock(&p->lock, "pipe");
801036fb:	68 10 7d 10 80       	push   $0x80107d10
80103700:	50                   	push   %eax
80103701:	e8 8a 0f 00 00       	call   80104690 <initlock>
  (*f0)->type = FD_PIPE;
80103706:	8b 03                	mov    (%ebx),%eax
  return 0;
80103708:	83 c4 10             	add    $0x10,%esp
  (*f0)->type = FD_PIPE;
8010370b:	c7 00 01 00 00 00    	movl   $0x1,(%eax)
  (*f0)->readable = 1;
80103711:	8b 03                	mov    (%ebx),%eax
80103713:	c6 40 08 01          	movb   $0x1,0x8(%eax)
  (*f0)->writable = 0;
80103717:	8b 03                	mov    (%ebx),%eax
80103719:	c6 40 09 00          	movb   $0x0,0x9(%eax)
  (*f0)->pipe = p;
8010371d:	8b 03                	mov    (%ebx),%eax
8010371f:	89 78 0c             	mov    %edi,0xc(%eax)
  (*f1)->type = FD_PIPE;
80103722:	8b 06                	mov    (%esi),%eax
80103724:	c7 00 01 00 00 00    	movl   $0x1,(%eax)
  (*f1)->readable = 0;
8010372a:	8b 06                	mov    (%esi),%eax
8010372c:	c6 40 08 00          	movb   $0x0,0x8(%eax)
  (*f1)->writable = 1;
80103730:	8b 06                	mov    (%esi),%eax
80103732:	c6 40 09 01          	movb   $0x1,0x9(%eax)
  (*f1)->pipe = p;
80103736:	8b 06                	mov    (%esi),%eax
80103738:	89 78 0c             	mov    %edi,0xc(%eax)
}
8010373b:	8d 65 f4             	lea    -0xc(%ebp),%esp
  return 0;
8010373e:	31 c0                	xor    %eax,%eax
}
80103740:	5b                   	pop    %ebx
80103741:	5e                   	pop    %esi
80103742:	5f                   	pop    %edi
80103743:	5d                   	pop    %ebp
80103744:	c3                   	ret    
80103745:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80103749:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80103750 <pipeclose>:

void
pipeclose(struct pipe *p, int writable)
{
80103750:	55                   	push   %ebp
80103751:	89 e5                	mov    %esp,%ebp
80103753:	56                   	push   %esi
80103754:	53                   	push   %ebx
80103755:	8b 5d 08             	mov    0x8(%ebp),%ebx
80103758:	8b 75 0c             	mov    0xc(%ebp),%esi
  acquire(&p->lock);
8010375b:	83 ec 0c             	sub    $0xc,%esp
8010375e:	53                   	push   %ebx
8010375f:	e8 6c 10 00 00       	call   801047d0 <acquire>
  if(writable){
80103764:	83 c4 10             	add    $0x10,%esp
80103767:	85 f6                	test   %esi,%esi
80103769:	74 45                	je     801037b0 <pipeclose+0x60>
    p->writeopen = 0;
    wakeup(&p->nread);
8010376b:	8d 83 34 02 00 00    	lea    0x234(%ebx),%eax
80103771:	83 ec 0c             	sub    $0xc,%esp
    p->writeopen = 0;
80103774:	c7 83 40 02 00 00 00 	movl   $0x0,0x240(%ebx)
8010377b:	00 00 00 
    wakeup(&p->nread);
8010377e:	50                   	push   %eax
8010377f:	e8 2c 0c 00 00       	call   801043b0 <wakeup>
80103784:	83 c4 10             	add    $0x10,%esp
  } else {
    p->readopen = 0;
    wakeup(&p->nwrite);
  }
  if(p->readopen == 0 && p->writeopen == 0){
80103787:	8b 93 3c 02 00 00    	mov    0x23c(%ebx),%edx
8010378d:	85 d2                	test   %edx,%edx
8010378f:	75 0a                	jne    8010379b <pipeclose+0x4b>
80103791:	8b 83 40 02 00 00    	mov    0x240(%ebx),%eax
80103797:	85 c0                	test   %eax,%eax
80103799:	74 35                	je     801037d0 <pipeclose+0x80>
    release(&p->lock);
    kfree((char*)p);
  } else
    release(&p->lock);
8010379b:	89 5d 08             	mov    %ebx,0x8(%ebp)
}
8010379e:	8d 65 f8             	lea    -0x8(%ebp),%esp
801037a1:	5b                   	pop    %ebx
801037a2:	5e                   	pop    %esi
801037a3:	5d                   	pop    %ebp
    release(&p->lock);
801037a4:	e9 e7 10 00 00       	jmp    80104890 <release>
801037a9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    wakeup(&p->nwrite);
801037b0:	8d 83 38 02 00 00    	lea    0x238(%ebx),%eax
801037b6:	83 ec 0c             	sub    $0xc,%esp
    p->readopen = 0;
801037b9:	c7 83 3c 02 00 00 00 	movl   $0x0,0x23c(%ebx)
801037c0:	00 00 00 
    wakeup(&p->nwrite);
801037c3:	50                   	push   %eax
801037c4:	e8 e7 0b 00 00       	call   801043b0 <wakeup>
801037c9:	83 c4 10             	add    $0x10,%esp
801037cc:	eb b9                	jmp    80103787 <pipeclose+0x37>
801037ce:	66 90                	xchg   %ax,%ax
    release(&p->lock);
801037d0:	83 ec 0c             	sub    $0xc,%esp
801037d3:	53                   	push   %ebx
801037d4:	e8 b7 10 00 00       	call   80104890 <release>
    kfree((char*)p);
801037d9:	89 5d 08             	mov    %ebx,0x8(%ebp)
801037dc:	83 c4 10             	add    $0x10,%esp
}
801037df:	8d 65 f8             	lea    -0x8(%ebp),%esp
801037e2:	5b                   	pop    %ebx
801037e3:	5e                   	pop    %esi
801037e4:	5d                   	pop    %ebp
    kfree((char*)p);
801037e5:	e9 b6 ee ff ff       	jmp    801026a0 <kfree>
801037ea:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi

801037f0 <pipewrite>:

//PAGEBREAK: 40
int
pipewrite(struct pipe *p, char *addr, int n)
{
801037f0:	55                   	push   %ebp
801037f1:	89 e5                	mov    %esp,%ebp
801037f3:	57                   	push   %edi
801037f4:	56                   	push   %esi
801037f5:	53                   	push   %ebx
801037f6:	83 ec 28             	sub    $0x28,%esp
801037f9:	8b 5d 08             	mov    0x8(%ebp),%ebx
  int i;

  acquire(&p->lock);
801037fc:	53                   	push   %ebx
801037fd:	e8 ce 0f 00 00       	call   801047d0 <acquire>
  for(i = 0; i < n; i++){
80103802:	8b 45 10             	mov    0x10(%ebp),%eax
80103805:	83 c4 10             	add    $0x10,%esp
80103808:	85 c0                	test   %eax,%eax
8010380a:	0f 8e c9 00 00 00    	jle    801038d9 <pipewrite+0xe9>
80103810:	8b 4d 0c             	mov    0xc(%ebp),%ecx
80103813:	8b 83 38 02 00 00    	mov    0x238(%ebx),%eax
    while(p->nwrite == p->nread + PIPESIZE){  //DOC: pipewrite-full
      if(p->readopen == 0 || myproc()->killed){
        release(&p->lock);
        return -1;
      }
      wakeup(&p->nread);
80103819:	8d bb 34 02 00 00    	lea    0x234(%ebx),%edi
8010381f:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
80103822:	03 4d 10             	add    0x10(%ebp),%ecx
80103825:	89 4d e0             	mov    %ecx,-0x20(%ebp)
    while(p->nwrite == p->nread + PIPESIZE){  //DOC: pipewrite-full
80103828:	8b 8b 34 02 00 00    	mov    0x234(%ebx),%ecx
8010382e:	8d 91 00 02 00 00    	lea    0x200(%ecx),%edx
80103834:	39 d0                	cmp    %edx,%eax
80103836:	75 71                	jne    801038a9 <pipewrite+0xb9>
      if(p->readopen == 0 || myproc()->killed){
80103838:	8b 83 3c 02 00 00    	mov    0x23c(%ebx),%eax
8010383e:	85 c0                	test   %eax,%eax
80103840:	74 4e                	je     80103890 <pipewrite+0xa0>
      sleep(&p->nwrite, &p->lock);  //DOC: pipewrite-sleep
80103842:	8d b3 38 02 00 00    	lea    0x238(%ebx),%esi
80103848:	eb 3a                	jmp    80103884 <pipewrite+0x94>
8010384a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
      wakeup(&p->nread);
80103850:	83 ec 0c             	sub    $0xc,%esp
80103853:	57                   	push   %edi
80103854:	e8 57 0b 00 00       	call   801043b0 <wakeup>
      sleep(&p->nwrite, &p->lock);  //DOC: pipewrite-sleep
80103859:	5a                   	pop    %edx
8010385a:	59                   	pop    %ecx
8010385b:	53                   	push   %ebx
8010385c:	56                   	push   %esi
8010385d:	e8 8e 09 00 00       	call   801041f0 <sleep>
    while(p->nwrite == p->nread + PIPESIZE){  //DOC: pipewrite-full
80103862:	8b 83 34 02 00 00    	mov    0x234(%ebx),%eax
80103868:	8b 93 38 02 00 00    	mov    0x238(%ebx),%edx
8010386e:	83 c4 10             	add    $0x10,%esp
80103871:	05 00 02 00 00       	add    $0x200,%eax
80103876:	39 c2                	cmp    %eax,%edx
80103878:	75 36                	jne    801038b0 <pipewrite+0xc0>
      if(p->readopen == 0 || myproc()->killed){
8010387a:	8b 83 3c 02 00 00    	mov    0x23c(%ebx),%eax
80103880:	85 c0                	test   %eax,%eax
80103882:	74 0c                	je     80103890 <pipewrite+0xa0>
80103884:	e8 a7 03 00 00       	call   80103c30 <myproc>
80103889:	8b 40 24             	mov    0x24(%eax),%eax
8010388c:	85 c0                	test   %eax,%eax
8010388e:	74 c0                	je     80103850 <pipewrite+0x60>
        release(&p->lock);
80103890:	83 ec 0c             	sub    $0xc,%esp
80103893:	53                   	push   %ebx
80103894:	e8 f7 0f 00 00       	call   80104890 <release>
        return -1;
80103899:	83 c4 10             	add    $0x10,%esp
8010389c:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
    p->data[p->nwrite++ % PIPESIZE] = addr[i];
  }
  wakeup(&p->nread);  //DOC: pipewrite-wakeup1
  release(&p->lock);
  return n;
}
801038a1:	8d 65 f4             	lea    -0xc(%ebp),%esp
801038a4:	5b                   	pop    %ebx
801038a5:	5e                   	pop    %esi
801038a6:	5f                   	pop    %edi
801038a7:	5d                   	pop    %ebp
801038a8:	c3                   	ret    
    while(p->nwrite == p->nread + PIPESIZE){  //DOC: pipewrite-full
801038a9:	89 c2                	mov    %eax,%edx
801038ab:	90                   	nop
801038ac:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    p->data[p->nwrite++ % PIPESIZE] = addr[i];
801038b0:	8b 75 e4             	mov    -0x1c(%ebp),%esi
801038b3:	8d 42 01             	lea    0x1(%edx),%eax
801038b6:	81 e2 ff 01 00 00    	and    $0x1ff,%edx
801038bc:	89 83 38 02 00 00    	mov    %eax,0x238(%ebx)
801038c2:	83 c6 01             	add    $0x1,%esi
801038c5:	0f b6 4e ff          	movzbl -0x1(%esi),%ecx
  for(i = 0; i < n; i++){
801038c9:	3b 75 e0             	cmp    -0x20(%ebp),%esi
801038cc:	89 75 e4             	mov    %esi,-0x1c(%ebp)
    p->data[p->nwrite++ % PIPESIZE] = addr[i];
801038cf:	88 4c 13 34          	mov    %cl,0x34(%ebx,%edx,1)
  for(i = 0; i < n; i++){
801038d3:	0f 85 4f ff ff ff    	jne    80103828 <pipewrite+0x38>
  wakeup(&p->nread);  //DOC: pipewrite-wakeup1
801038d9:	8d 83 34 02 00 00    	lea    0x234(%ebx),%eax
801038df:	83 ec 0c             	sub    $0xc,%esp
801038e2:	50                   	push   %eax
801038e3:	e8 c8 0a 00 00       	call   801043b0 <wakeup>
  release(&p->lock);
801038e8:	89 1c 24             	mov    %ebx,(%esp)
801038eb:	e8 a0 0f 00 00       	call   80104890 <release>
  return n;
801038f0:	83 c4 10             	add    $0x10,%esp
801038f3:	8b 45 10             	mov    0x10(%ebp),%eax
801038f6:	eb a9                	jmp    801038a1 <pipewrite+0xb1>
801038f8:	90                   	nop
801038f9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

80103900 <piperead>:

int
piperead(struct pipe *p, char *addr, int n)
{
80103900:	55                   	push   %ebp
80103901:	89 e5                	mov    %esp,%ebp
80103903:	57                   	push   %edi
80103904:	56                   	push   %esi
80103905:	53                   	push   %ebx
80103906:	83 ec 18             	sub    $0x18,%esp
80103909:	8b 75 08             	mov    0x8(%ebp),%esi
8010390c:	8b 7d 0c             	mov    0xc(%ebp),%edi
  int i;

  acquire(&p->lock);
8010390f:	56                   	push   %esi
80103910:	e8 bb 0e 00 00       	call   801047d0 <acquire>
  while(p->nread == p->nwrite && p->writeopen){  //DOC: pipe-empty
80103915:	83 c4 10             	add    $0x10,%esp
80103918:	8b 8e 34 02 00 00    	mov    0x234(%esi),%ecx
8010391e:	3b 8e 38 02 00 00    	cmp    0x238(%esi),%ecx
80103924:	75 6a                	jne    80103990 <piperead+0x90>
80103926:	8b 9e 40 02 00 00    	mov    0x240(%esi),%ebx
8010392c:	85 db                	test   %ebx,%ebx
8010392e:	0f 84 c4 00 00 00    	je     801039f8 <piperead+0xf8>
    if(myproc()->killed){
      release(&p->lock);
      return -1;
    }
    sleep(&p->nread, &p->lock); //DOC: piperead-sleep
80103934:	8d 9e 34 02 00 00    	lea    0x234(%esi),%ebx
8010393a:	eb 2d                	jmp    80103969 <piperead+0x69>
8010393c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80103940:	83 ec 08             	sub    $0x8,%esp
80103943:	56                   	push   %esi
80103944:	53                   	push   %ebx
80103945:	e8 a6 08 00 00       	call   801041f0 <sleep>
  while(p->nread == p->nwrite && p->writeopen){  //DOC: pipe-empty
8010394a:	83 c4 10             	add    $0x10,%esp
8010394d:	8b 8e 34 02 00 00    	mov    0x234(%esi),%ecx
80103953:	3b 8e 38 02 00 00    	cmp    0x238(%esi),%ecx
80103959:	75 35                	jne    80103990 <piperead+0x90>
8010395b:	8b 96 40 02 00 00    	mov    0x240(%esi),%edx
80103961:	85 d2                	test   %edx,%edx
80103963:	0f 84 8f 00 00 00    	je     801039f8 <piperead+0xf8>
    if(myproc()->killed){
80103969:	e8 c2 02 00 00       	call   80103c30 <myproc>
8010396e:	8b 48 24             	mov    0x24(%eax),%ecx
80103971:	85 c9                	test   %ecx,%ecx
80103973:	74 cb                	je     80103940 <piperead+0x40>
      release(&p->lock);
80103975:	83 ec 0c             	sub    $0xc,%esp
      return -1;
80103978:	bb ff ff ff ff       	mov    $0xffffffff,%ebx
      release(&p->lock);
8010397d:	56                   	push   %esi
8010397e:	e8 0d 0f 00 00       	call   80104890 <release>
      return -1;
80103983:	83 c4 10             	add    $0x10,%esp
    addr[i] = p->data[p->nread++ % PIPESIZE];
  }
  wakeup(&p->nwrite);  //DOC: piperead-wakeup
  release(&p->lock);
  return i;
}
80103986:	8d 65 f4             	lea    -0xc(%ebp),%esp
80103989:	89 d8                	mov    %ebx,%eax
8010398b:	5b                   	pop    %ebx
8010398c:	5e                   	pop    %esi
8010398d:	5f                   	pop    %edi
8010398e:	5d                   	pop    %ebp
8010398f:	c3                   	ret    
  for(i = 0; i < n; i++){  //DOC: piperead-copy
80103990:	8b 45 10             	mov    0x10(%ebp),%eax
80103993:	85 c0                	test   %eax,%eax
80103995:	7e 61                	jle    801039f8 <piperead+0xf8>
    if(p->nread == p->nwrite)
80103997:	31 db                	xor    %ebx,%ebx
80103999:	eb 13                	jmp    801039ae <piperead+0xae>
8010399b:	90                   	nop
8010399c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
801039a0:	8b 8e 34 02 00 00    	mov    0x234(%esi),%ecx
801039a6:	3b 8e 38 02 00 00    	cmp    0x238(%esi),%ecx
801039ac:	74 1f                	je     801039cd <piperead+0xcd>
    addr[i] = p->data[p->nread++ % PIPESIZE];
801039ae:	8d 41 01             	lea    0x1(%ecx),%eax
801039b1:	81 e1 ff 01 00 00    	and    $0x1ff,%ecx
801039b7:	89 86 34 02 00 00    	mov    %eax,0x234(%esi)
801039bd:	0f b6 44 0e 34       	movzbl 0x34(%esi,%ecx,1),%eax
801039c2:	88 04 1f             	mov    %al,(%edi,%ebx,1)
  for(i = 0; i < n; i++){  //DOC: piperead-copy
801039c5:	83 c3 01             	add    $0x1,%ebx
801039c8:	39 5d 10             	cmp    %ebx,0x10(%ebp)
801039cb:	75 d3                	jne    801039a0 <piperead+0xa0>
  wakeup(&p->nwrite);  //DOC: piperead-wakeup
801039cd:	8d 86 38 02 00 00    	lea    0x238(%esi),%eax
801039d3:	83 ec 0c             	sub    $0xc,%esp
801039d6:	50                   	push   %eax
801039d7:	e8 d4 09 00 00       	call   801043b0 <wakeup>
  release(&p->lock);
801039dc:	89 34 24             	mov    %esi,(%esp)
801039df:	e8 ac 0e 00 00       	call   80104890 <release>
  return i;
801039e4:	83 c4 10             	add    $0x10,%esp
}
801039e7:	8d 65 f4             	lea    -0xc(%ebp),%esp
801039ea:	89 d8                	mov    %ebx,%eax
801039ec:	5b                   	pop    %ebx
801039ed:	5e                   	pop    %esi
801039ee:	5f                   	pop    %edi
801039ef:	5d                   	pop    %ebp
801039f0:	c3                   	ret    
801039f1:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801039f8:	31 db                	xor    %ebx,%ebx
801039fa:	eb d1                	jmp    801039cd <piperead+0xcd>
801039fc:	66 90                	xchg   %ax,%ax
801039fe:	66 90                	xchg   %ax,%ax

80103a00 <allocproc>:
// If found, change state to EMBRYO and initialize
// state required to run in the kernel.
// Otherwise return 0.
static struct proc*
allocproc(void)
{
80103a00:	55                   	push   %ebp
80103a01:	89 e5                	mov    %esp,%ebp
80103a03:	53                   	push   %ebx
  struct proc *p;
  char *sp;

  acquire(&ptable.lock);

  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
80103a04:	bb 54 3d 11 80       	mov    $0x80113d54,%ebx
{
80103a09:	83 ec 10             	sub    $0x10,%esp
  acquire(&ptable.lock);
80103a0c:	68 20 3d 11 80       	push   $0x80113d20
80103a11:	e8 ba 0d 00 00       	call   801047d0 <acquire>
80103a16:	83 c4 10             	add    $0x10,%esp
80103a19:	eb 17                	jmp    80103a32 <allocproc+0x32>
80103a1b:	90                   	nop
80103a1c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
80103a20:	81 c3 4c 01 00 00    	add    $0x14c,%ebx
80103a26:	81 fb 54 90 11 80    	cmp    $0x80119054,%ebx
80103a2c:	0f 83 be 00 00 00    	jae    80103af0 <allocproc+0xf0>
    if(p->state == UNUSED)
80103a32:	8b 43 0c             	mov    0xc(%ebx),%eax
80103a35:	85 c0                	test   %eax,%eax
80103a37:	75 e7                	jne    80103a20 <allocproc+0x20>
  release(&ptable.lock);
  return 0;

found:
  p->state = EMBRYO;
  p->pid = nextpid++;
80103a39:	a1 04 b0 10 80       	mov    0x8010b004,%eax
  p->state = EMBRYO;
80103a3e:	c7 43 0c 01 00 00 00 	movl   $0x1,0xc(%ebx)

  //////////////////////////
  p->numOfPhysPages=0;
80103a45:	c7 83 40 01 00 00 00 	movl   $0x0,0x140(%ebx)
80103a4c:	00 00 00 
  p->lastSpotInFile=0;
80103a4f:	c7 83 44 01 00 00 00 	movl   $0x0,0x144(%ebx)
80103a56:	00 00 00 
  p->pageToSwap=0;
80103a59:	c7 83 48 01 00 00 00 	movl   $0x0,0x148(%ebx)
80103a60:	00 00 00 
  p->swapFile=0;
80103a63:	c7 43 7c 00 00 00 00 	movl   $0x0,0x7c(%ebx)
  p->pid = nextpid++;
80103a6a:	8d 50 01             	lea    0x1(%eax),%edx
80103a6d:	89 43 10             	mov    %eax,0x10(%ebx)
80103a70:	8d 83 00 01 00 00    	lea    0x100(%ebx),%eax
80103a76:	89 15 04 b0 10 80    	mov    %edx,0x8010b004
80103a7c:	8d 93 40 01 00 00    	lea    0x140(%ebx),%edx
80103a82:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi

  int i;
  for(i=0;i<MAX_PSYC_PAGES;i++)
    p->physPages[i] = (char*)-1;
80103a88:	c7 00 ff ff ff ff    	movl   $0xffffffff,(%eax)
80103a8e:	83 c0 04             	add    $0x4,%eax
  for(i=0;i<MAX_PSYC_PAGES;i++)
80103a91:	39 c2                	cmp    %eax,%edx
80103a93:	75 f3                	jne    80103a88 <allocproc+0x88>
  //////////////////////////

  release(&ptable.lock);
80103a95:	83 ec 0c             	sub    $0xc,%esp
80103a98:	68 20 3d 11 80       	push   $0x80113d20
80103a9d:	e8 ee 0d 00 00       	call   80104890 <release>

  // Allocate kernel stack.
  if((p->kstack = kalloc()) == 0){
80103aa2:	e8 19 ee ff ff       	call   801028c0 <kalloc>
80103aa7:	83 c4 10             	add    $0x10,%esp
80103aaa:	85 c0                	test   %eax,%eax
80103aac:	89 43 08             	mov    %eax,0x8(%ebx)
80103aaf:	74 58                	je     80103b09 <allocproc+0x109>
    return 0;
  }
  sp = p->kstack + KSTACKSIZE;

  // Leave room for trap frame.
  sp -= sizeof *p->tf;
80103ab1:	8d 90 b4 0f 00 00    	lea    0xfb4(%eax),%edx
  sp -= 4;
  *(uint*)sp = (uint)trapret;

  sp -= sizeof *p->context;
  p->context = (struct context*)sp;
  memset(p->context, 0, sizeof *p->context);
80103ab7:	83 ec 04             	sub    $0x4,%esp
  sp -= sizeof *p->context;
80103aba:	05 9c 0f 00 00       	add    $0xf9c,%eax
  sp -= sizeof *p->tf;
80103abf:	89 53 18             	mov    %edx,0x18(%ebx)
  *(uint*)sp = (uint)trapret;
80103ac2:	c7 40 14 12 5b 10 80 	movl   $0x80105b12,0x14(%eax)
  p->context = (struct context*)sp;
80103ac9:	89 43 1c             	mov    %eax,0x1c(%ebx)
  memset(p->context, 0, sizeof *p->context);
80103acc:	6a 14                	push   $0x14
80103ace:	6a 00                	push   $0x0
80103ad0:	50                   	push   %eax
80103ad1:	e8 0a 0e 00 00       	call   801048e0 <memset>
  p->context->eip = (uint)forkret;
80103ad6:	8b 43 1c             	mov    0x1c(%ebx),%eax

  return p;
80103ad9:	83 c4 10             	add    $0x10,%esp
  p->context->eip = (uint)forkret;
80103adc:	c7 40 10 20 3b 10 80 	movl   $0x80103b20,0x10(%eax)
}
80103ae3:	89 d8                	mov    %ebx,%eax
80103ae5:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80103ae8:	c9                   	leave  
80103ae9:	c3                   	ret    
80103aea:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
  release(&ptable.lock);
80103af0:	83 ec 0c             	sub    $0xc,%esp
  return 0;
80103af3:	31 db                	xor    %ebx,%ebx
  release(&ptable.lock);
80103af5:	68 20 3d 11 80       	push   $0x80113d20
80103afa:	e8 91 0d 00 00       	call   80104890 <release>
}
80103aff:	89 d8                	mov    %ebx,%eax
  return 0;
80103b01:	83 c4 10             	add    $0x10,%esp
}
80103b04:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80103b07:	c9                   	leave  
80103b08:	c3                   	ret    
    p->state = UNUSED;
80103b09:	c7 43 0c 00 00 00 00 	movl   $0x0,0xc(%ebx)
    return 0;
80103b10:	31 db                	xor    %ebx,%ebx
80103b12:	eb cf                	jmp    80103ae3 <allocproc+0xe3>
80103b14:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
80103b1a:	8d bf 00 00 00 00    	lea    0x0(%edi),%edi

80103b20 <forkret>:

// A fork child's very first scheduling by scheduler()
// will swtch here.  "Return" to user space.
void
forkret(void)
{
80103b20:	55                   	push   %ebp
80103b21:	89 e5                	mov    %esp,%ebp
80103b23:	83 ec 14             	sub    $0x14,%esp
  static int first = 1;
  // Still holding ptable.lock from scheduler.
  release(&ptable.lock);
80103b26:	68 20 3d 11 80       	push   $0x80113d20
80103b2b:	e8 60 0d 00 00       	call   80104890 <release>

  if (first) {
80103b30:	a1 00 b0 10 80       	mov    0x8010b000,%eax
80103b35:	83 c4 10             	add    $0x10,%esp
80103b38:	85 c0                	test   %eax,%eax
80103b3a:	75 04                	jne    80103b40 <forkret+0x20>
    iinit(ROOTDEV);
    initlog(ROOTDEV);
  }

  // Return to "caller", actually trapret (see allocproc).
}
80103b3c:	c9                   	leave  
80103b3d:	c3                   	ret    
80103b3e:	66 90                	xchg   %ax,%ax
    iinit(ROOTDEV);
80103b40:	83 ec 0c             	sub    $0xc,%esp
    first = 0;
80103b43:	c7 05 00 b0 10 80 00 	movl   $0x0,0x8010b000
80103b4a:	00 00 00 
    iinit(ROOTDEV);
80103b4d:	6a 01                	push   $0x1
80103b4f:	e8 2c d9 ff ff       	call   80101480 <iinit>
    initlog(ROOTDEV);
80103b54:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
80103b5b:	e8 a0 f3 ff ff       	call   80102f00 <initlog>
80103b60:	83 c4 10             	add    $0x10,%esp
}
80103b63:	c9                   	leave  
80103b64:	c3                   	ret    
80103b65:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80103b69:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80103b70 <pinit>:
{
80103b70:	55                   	push   %ebp
80103b71:	89 e5                	mov    %esp,%ebp
80103b73:	83 ec 10             	sub    $0x10,%esp
  initlock(&ptable.lock, "ptable");
80103b76:	68 15 7d 10 80       	push   $0x80107d15
80103b7b:	68 20 3d 11 80       	push   $0x80113d20
80103b80:	e8 0b 0b 00 00       	call   80104690 <initlock>
}
80103b85:	83 c4 10             	add    $0x10,%esp
80103b88:	c9                   	leave  
80103b89:	c3                   	ret    
80103b8a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi

80103b90 <mycpu>:
{
80103b90:	55                   	push   %ebp
80103b91:	89 e5                	mov    %esp,%ebp
80103b93:	56                   	push   %esi
80103b94:	53                   	push   %ebx
  asm volatile("pushfl; popl %0" : "=r" (eflags));
80103b95:	9c                   	pushf  
80103b96:	58                   	pop    %eax
  if(readeflags()&FL_IF)
80103b97:	f6 c4 02             	test   $0x2,%ah
80103b9a:	75 5e                	jne    80103bfa <mycpu+0x6a>
  apicid = lapicid();
80103b9c:	e8 8f ef ff ff       	call   80102b30 <lapicid>
  for (i = 0; i < ncpu; ++i) {
80103ba1:	8b 35 00 3d 11 80    	mov    0x80113d00,%esi
80103ba7:	85 f6                	test   %esi,%esi
80103ba9:	7e 42                	jle    80103bed <mycpu+0x5d>
    if (cpus[i].apicid == apicid)
80103bab:	0f b6 15 80 37 11 80 	movzbl 0x80113780,%edx
80103bb2:	39 d0                	cmp    %edx,%eax
80103bb4:	74 30                	je     80103be6 <mycpu+0x56>
80103bb6:	b9 30 38 11 80       	mov    $0x80113830,%ecx
  for (i = 0; i < ncpu; ++i) {
80103bbb:	31 d2                	xor    %edx,%edx
80103bbd:	8d 76 00             	lea    0x0(%esi),%esi
80103bc0:	83 c2 01             	add    $0x1,%edx
80103bc3:	39 f2                	cmp    %esi,%edx
80103bc5:	74 26                	je     80103bed <mycpu+0x5d>
    if (cpus[i].apicid == apicid)
80103bc7:	0f b6 19             	movzbl (%ecx),%ebx
80103bca:	81 c1 b0 00 00 00    	add    $0xb0,%ecx
80103bd0:	39 c3                	cmp    %eax,%ebx
80103bd2:	75 ec                	jne    80103bc0 <mycpu+0x30>
80103bd4:	69 c2 b0 00 00 00    	imul   $0xb0,%edx,%eax
80103bda:	05 80 37 11 80       	add    $0x80113780,%eax
}
80103bdf:	8d 65 f8             	lea    -0x8(%ebp),%esp
80103be2:	5b                   	pop    %ebx
80103be3:	5e                   	pop    %esi
80103be4:	5d                   	pop    %ebp
80103be5:	c3                   	ret    
    if (cpus[i].apicid == apicid)
80103be6:	b8 80 37 11 80       	mov    $0x80113780,%eax
      return &cpus[i];
80103beb:	eb f2                	jmp    80103bdf <mycpu+0x4f>
  panic("unknown apicid\n");
80103bed:	83 ec 0c             	sub    $0xc,%esp
80103bf0:	68 1c 7d 10 80       	push   $0x80107d1c
80103bf5:	e8 96 c7 ff ff       	call   80100390 <panic>
    panic("mycpu called with interrupts enabled\n");
80103bfa:	83 ec 0c             	sub    $0xc,%esp
80103bfd:	68 f8 7d 10 80       	push   $0x80107df8
80103c02:	e8 89 c7 ff ff       	call   80100390 <panic>
80103c07:	89 f6                	mov    %esi,%esi
80103c09:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80103c10 <cpuid>:
cpuid() {
80103c10:	55                   	push   %ebp
80103c11:	89 e5                	mov    %esp,%ebp
80103c13:	83 ec 08             	sub    $0x8,%esp
  return mycpu()-cpus;
80103c16:	e8 75 ff ff ff       	call   80103b90 <mycpu>
80103c1b:	2d 80 37 11 80       	sub    $0x80113780,%eax
}
80103c20:	c9                   	leave  
  return mycpu()-cpus;
80103c21:	c1 f8 04             	sar    $0x4,%eax
80103c24:	69 c0 a3 8b 2e ba    	imul   $0xba2e8ba3,%eax,%eax
}
80103c2a:	c3                   	ret    
80103c2b:	90                   	nop
80103c2c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

80103c30 <myproc>:
myproc(void) {
80103c30:	55                   	push   %ebp
80103c31:	89 e5                	mov    %esp,%ebp
80103c33:	53                   	push   %ebx
80103c34:	83 ec 04             	sub    $0x4,%esp
  pushcli();
80103c37:	e8 c4 0a 00 00       	call   80104700 <pushcli>
  c = mycpu();
80103c3c:	e8 4f ff ff ff       	call   80103b90 <mycpu>
  p = c->proc;
80103c41:	8b 98 ac 00 00 00    	mov    0xac(%eax),%ebx
  popcli();
80103c47:	e8 f4 0a 00 00       	call   80104740 <popcli>
}
80103c4c:	83 c4 04             	add    $0x4,%esp
80103c4f:	89 d8                	mov    %ebx,%eax
80103c51:	5b                   	pop    %ebx
80103c52:	5d                   	pop    %ebp
80103c53:	c3                   	ret    
80103c54:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
80103c5a:	8d bf 00 00 00 00    	lea    0x0(%edi),%edi

80103c60 <userinit>:
{
80103c60:	55                   	push   %ebp
80103c61:	89 e5                	mov    %esp,%ebp
80103c63:	53                   	push   %ebx
80103c64:	83 ec 04             	sub    $0x4,%esp
  p = allocproc();
80103c67:	e8 94 fd ff ff       	call   80103a00 <allocproc>
80103c6c:	89 c3                	mov    %eax,%ebx
  initproc = p;
80103c6e:	a3 b8 b5 10 80       	mov    %eax,0x8010b5b8
  if((p->pgdir = setupkvm()) == 0)
80103c73:	e8 78 33 00 00       	call   80106ff0 <setupkvm>
80103c78:	85 c0                	test   %eax,%eax
80103c7a:	89 43 04             	mov    %eax,0x4(%ebx)
80103c7d:	0f 84 bd 00 00 00    	je     80103d40 <userinit+0xe0>
  inituvm(p->pgdir, _binary_initcode_start, (int)_binary_initcode_size);
80103c83:	83 ec 04             	sub    $0x4,%esp
80103c86:	68 2c 00 00 00       	push   $0x2c
80103c8b:	68 60 b4 10 80       	push   $0x8010b460
80103c90:	50                   	push   %eax
80103c91:	e8 9a 30 00 00       	call   80106d30 <inituvm>
  memset(p->tf, 0, sizeof(*p->tf));
80103c96:	83 c4 0c             	add    $0xc,%esp
  p->sz = PGSIZE;
80103c99:	c7 03 00 10 00 00    	movl   $0x1000,(%ebx)
  memset(p->tf, 0, sizeof(*p->tf));
80103c9f:	6a 4c                	push   $0x4c
80103ca1:	6a 00                	push   $0x0
80103ca3:	ff 73 18             	pushl  0x18(%ebx)
80103ca6:	e8 35 0c 00 00       	call   801048e0 <memset>
  p->tf->cs = (SEG_UCODE << 3) | DPL_USER;
80103cab:	8b 43 18             	mov    0x18(%ebx),%eax
80103cae:	ba 1b 00 00 00       	mov    $0x1b,%edx
  p->tf->ds = (SEG_UDATA << 3) | DPL_USER;
80103cb3:	b9 23 00 00 00       	mov    $0x23,%ecx
  safestrcpy(p->name, "initcode", sizeof(p->name));
80103cb8:	83 c4 0c             	add    $0xc,%esp
  p->tf->cs = (SEG_UCODE << 3) | DPL_USER;
80103cbb:	66 89 50 3c          	mov    %dx,0x3c(%eax)
  p->tf->ds = (SEG_UDATA << 3) | DPL_USER;
80103cbf:	8b 43 18             	mov    0x18(%ebx),%eax
80103cc2:	66 89 48 2c          	mov    %cx,0x2c(%eax)
  p->tf->es = p->tf->ds;
80103cc6:	8b 43 18             	mov    0x18(%ebx),%eax
80103cc9:	0f b7 50 2c          	movzwl 0x2c(%eax),%edx
80103ccd:	66 89 50 28          	mov    %dx,0x28(%eax)
  p->tf->ss = p->tf->ds;
80103cd1:	8b 43 18             	mov    0x18(%ebx),%eax
80103cd4:	0f b7 50 2c          	movzwl 0x2c(%eax),%edx
80103cd8:	66 89 50 48          	mov    %dx,0x48(%eax)
  p->tf->eflags = FL_IF;
80103cdc:	8b 43 18             	mov    0x18(%ebx),%eax
80103cdf:	c7 40 40 00 02 00 00 	movl   $0x200,0x40(%eax)
  p->tf->esp = PGSIZE;
80103ce6:	8b 43 18             	mov    0x18(%ebx),%eax
80103ce9:	c7 40 44 00 10 00 00 	movl   $0x1000,0x44(%eax)
  p->tf->eip = 0;  // beginning of initcode.S
80103cf0:	8b 43 18             	mov    0x18(%ebx),%eax
80103cf3:	c7 40 38 00 00 00 00 	movl   $0x0,0x38(%eax)
  safestrcpy(p->name, "initcode", sizeof(p->name));
80103cfa:	8d 43 6c             	lea    0x6c(%ebx),%eax
80103cfd:	6a 10                	push   $0x10
80103cff:	68 45 7d 10 80       	push   $0x80107d45
80103d04:	50                   	push   %eax
80103d05:	e8 b6 0d 00 00       	call   80104ac0 <safestrcpy>
  p->cwd = namei("/");
80103d0a:	c7 04 24 4e 7d 10 80 	movl   $0x80107d4e,(%esp)
80103d11:	e8 ca e1 ff ff       	call   80101ee0 <namei>
80103d16:	89 43 68             	mov    %eax,0x68(%ebx)
  acquire(&ptable.lock);
80103d19:	c7 04 24 20 3d 11 80 	movl   $0x80113d20,(%esp)
80103d20:	e8 ab 0a 00 00       	call   801047d0 <acquire>
  p->state = RUNNABLE;
80103d25:	c7 43 0c 03 00 00 00 	movl   $0x3,0xc(%ebx)
  release(&ptable.lock);
80103d2c:	c7 04 24 20 3d 11 80 	movl   $0x80113d20,(%esp)
80103d33:	e8 58 0b 00 00       	call   80104890 <release>
}
80103d38:	83 c4 10             	add    $0x10,%esp
80103d3b:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80103d3e:	c9                   	leave  
80103d3f:	c3                   	ret    
    panic("userinit: out of memory?");
80103d40:	83 ec 0c             	sub    $0xc,%esp
80103d43:	68 2c 7d 10 80       	push   $0x80107d2c
80103d48:	e8 43 c6 ff ff       	call   80100390 <panic>
80103d4d:	8d 76 00             	lea    0x0(%esi),%esi

80103d50 <growproc>:
{
80103d50:	55                   	push   %ebp
80103d51:	89 e5                	mov    %esp,%ebp
80103d53:	56                   	push   %esi
80103d54:	53                   	push   %ebx
80103d55:	8b 75 08             	mov    0x8(%ebp),%esi
  pushcli();
80103d58:	e8 a3 09 00 00       	call   80104700 <pushcli>
  c = mycpu();
80103d5d:	e8 2e fe ff ff       	call   80103b90 <mycpu>
  p = c->proc;
80103d62:	8b 98 ac 00 00 00    	mov    0xac(%eax),%ebx
  popcli();
80103d68:	e8 d3 09 00 00       	call   80104740 <popcli>
  if(n > 0){
80103d6d:	83 fe 00             	cmp    $0x0,%esi
  sz = curproc->sz;
80103d70:	8b 03                	mov    (%ebx),%eax
  if(n > 0){
80103d72:	7f 1c                	jg     80103d90 <growproc+0x40>
  } else if(n < 0){
80103d74:	75 3a                	jne    80103db0 <growproc+0x60>
  switchuvm(curproc);
80103d76:	83 ec 0c             	sub    $0xc,%esp
  curproc->sz = sz;
80103d79:	89 03                	mov    %eax,(%ebx)
  switchuvm(curproc);
80103d7b:	53                   	push   %ebx
80103d7c:	e8 9f 2e 00 00       	call   80106c20 <switchuvm>
  return 0;
80103d81:	83 c4 10             	add    $0x10,%esp
80103d84:	31 c0                	xor    %eax,%eax
}
80103d86:	8d 65 f8             	lea    -0x8(%ebp),%esp
80103d89:	5b                   	pop    %ebx
80103d8a:	5e                   	pop    %esi
80103d8b:	5d                   	pop    %ebp
80103d8c:	c3                   	ret    
80103d8d:	8d 76 00             	lea    0x0(%esi),%esi
    if((sz = allocuvm(curproc->pgdir, sz, sz + n)) == 0)
80103d90:	83 ec 04             	sub    $0x4,%esp
80103d93:	01 c6                	add    %eax,%esi
80103d95:	56                   	push   %esi
80103d96:	50                   	push   %eax
80103d97:	ff 73 04             	pushl  0x4(%ebx)
80103d9a:	e8 71 37 00 00       	call   80107510 <allocuvm>
80103d9f:	83 c4 10             	add    $0x10,%esp
80103da2:	85 c0                	test   %eax,%eax
80103da4:	75 d0                	jne    80103d76 <growproc+0x26>
      return -1;
80103da6:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80103dab:	eb d9                	jmp    80103d86 <growproc+0x36>
80103dad:	8d 76 00             	lea    0x0(%esi),%esi
    if((sz = deallocuvm(curproc->pgdir, sz, sz + n)) == 0)
80103db0:	83 ec 04             	sub    $0x4,%esp
80103db3:	01 c6                	add    %eax,%esi
80103db5:	56                   	push   %esi
80103db6:	50                   	push   %eax
80103db7:	ff 73 04             	pushl  0x4(%ebx)
80103dba:	e8 b1 30 00 00       	call   80106e70 <deallocuvm>
80103dbf:	83 c4 10             	add    $0x10,%esp
80103dc2:	85 c0                	test   %eax,%eax
80103dc4:	75 b0                	jne    80103d76 <growproc+0x26>
80103dc6:	eb de                	jmp    80103da6 <growproc+0x56>
80103dc8:	90                   	nop
80103dc9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

80103dd0 <fork>:
{
80103dd0:	55                   	push   %ebp
80103dd1:	89 e5                	mov    %esp,%ebp
80103dd3:	57                   	push   %edi
80103dd4:	56                   	push   %esi
80103dd5:	53                   	push   %ebx
80103dd6:	83 ec 1c             	sub    $0x1c,%esp
  pushcli();
80103dd9:	e8 22 09 00 00       	call   80104700 <pushcli>
  c = mycpu();
80103dde:	e8 ad fd ff ff       	call   80103b90 <mycpu>
  p = c->proc;
80103de3:	8b 98 ac 00 00 00    	mov    0xac(%eax),%ebx
  popcli();
80103de9:	e8 52 09 00 00       	call   80104740 <popcli>
  if((np = allocproc()) == 0){
80103dee:	e8 0d fc ff ff       	call   80103a00 <allocproc>
80103df3:	85 c0                	test   %eax,%eax
80103df5:	89 45 e4             	mov    %eax,-0x1c(%ebp)
80103df8:	0f 84 b7 00 00 00    	je     80103eb5 <fork+0xe5>
  if((np->pgdir = copyuvm(curproc->pgdir, curproc->sz)) == 0){
80103dfe:	83 ec 08             	sub    $0x8,%esp
80103e01:	ff 33                	pushl  (%ebx)
80103e03:	ff 73 04             	pushl  0x4(%ebx)
80103e06:	89 c7                	mov    %eax,%edi
80103e08:	e8 b3 32 00 00       	call   801070c0 <copyuvm>
80103e0d:	83 c4 10             	add    $0x10,%esp
80103e10:	85 c0                	test   %eax,%eax
80103e12:	89 47 04             	mov    %eax,0x4(%edi)
80103e15:	0f 84 a1 00 00 00    	je     80103ebc <fork+0xec>
  np->sz = curproc->sz;
80103e1b:	8b 03                	mov    (%ebx),%eax
80103e1d:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
80103e20:	89 01                	mov    %eax,(%ecx)
  np->parent = curproc;
80103e22:	89 59 14             	mov    %ebx,0x14(%ecx)
80103e25:	89 c8                	mov    %ecx,%eax
  *np->tf = *curproc->tf;
80103e27:	8b 79 18             	mov    0x18(%ecx),%edi
80103e2a:	8b 73 18             	mov    0x18(%ebx),%esi
80103e2d:	b9 13 00 00 00       	mov    $0x13,%ecx
80103e32:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  for(i = 0; i < NOFILE; i++)
80103e34:	31 f6                	xor    %esi,%esi
  np->tf->eax = 0;
80103e36:	8b 40 18             	mov    0x18(%eax),%eax
80103e39:	c7 40 1c 00 00 00 00 	movl   $0x0,0x1c(%eax)
    if(curproc->ofile[i])
80103e40:	8b 44 b3 28          	mov    0x28(%ebx,%esi,4),%eax
80103e44:	85 c0                	test   %eax,%eax
80103e46:	74 13                	je     80103e5b <fork+0x8b>
      np->ofile[i] = filedup(curproc->ofile[i]);
80103e48:	83 ec 0c             	sub    $0xc,%esp
80103e4b:	50                   	push   %eax
80103e4c:	e8 9f cf ff ff       	call   80100df0 <filedup>
80103e51:	8b 55 e4             	mov    -0x1c(%ebp),%edx
80103e54:	83 c4 10             	add    $0x10,%esp
80103e57:	89 44 b2 28          	mov    %eax,0x28(%edx,%esi,4)
  for(i = 0; i < NOFILE; i++)
80103e5b:	83 c6 01             	add    $0x1,%esi
80103e5e:	83 fe 10             	cmp    $0x10,%esi
80103e61:	75 dd                	jne    80103e40 <fork+0x70>
  np->cwd = idup(curproc->cwd);
80103e63:	83 ec 0c             	sub    $0xc,%esp
80103e66:	ff 73 68             	pushl  0x68(%ebx)
  safestrcpy(np->name, curproc->name, sizeof(curproc->name));
80103e69:	83 c3 6c             	add    $0x6c,%ebx
  np->cwd = idup(curproc->cwd);
80103e6c:	e8 df d7 ff ff       	call   80101650 <idup>
80103e71:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  safestrcpy(np->name, curproc->name, sizeof(curproc->name));
80103e74:	83 c4 0c             	add    $0xc,%esp
  np->cwd = idup(curproc->cwd);
80103e77:	89 47 68             	mov    %eax,0x68(%edi)
  safestrcpy(np->name, curproc->name, sizeof(curproc->name));
80103e7a:	8d 47 6c             	lea    0x6c(%edi),%eax
80103e7d:	6a 10                	push   $0x10
80103e7f:	53                   	push   %ebx
80103e80:	50                   	push   %eax
80103e81:	e8 3a 0c 00 00       	call   80104ac0 <safestrcpy>
  pid = np->pid;
80103e86:	8b 5f 10             	mov    0x10(%edi),%ebx
  acquire(&ptable.lock);
80103e89:	c7 04 24 20 3d 11 80 	movl   $0x80113d20,(%esp)
80103e90:	e8 3b 09 00 00       	call   801047d0 <acquire>
  np->state = RUNNABLE;
80103e95:	c7 47 0c 03 00 00 00 	movl   $0x3,0xc(%edi)
  release(&ptable.lock);
80103e9c:	c7 04 24 20 3d 11 80 	movl   $0x80113d20,(%esp)
80103ea3:	e8 e8 09 00 00       	call   80104890 <release>
  return pid;
80103ea8:	83 c4 10             	add    $0x10,%esp
}
80103eab:	8d 65 f4             	lea    -0xc(%ebp),%esp
80103eae:	89 d8                	mov    %ebx,%eax
80103eb0:	5b                   	pop    %ebx
80103eb1:	5e                   	pop    %esi
80103eb2:	5f                   	pop    %edi
80103eb3:	5d                   	pop    %ebp
80103eb4:	c3                   	ret    
    return -1;
80103eb5:	bb ff ff ff ff       	mov    $0xffffffff,%ebx
80103eba:	eb ef                	jmp    80103eab <fork+0xdb>
    kfree(np->kstack);
80103ebc:	8b 5d e4             	mov    -0x1c(%ebp),%ebx
80103ebf:	83 ec 0c             	sub    $0xc,%esp
80103ec2:	ff 73 08             	pushl  0x8(%ebx)
80103ec5:	e8 d6 e7 ff ff       	call   801026a0 <kfree>
    np->kstack = 0;
80103eca:	c7 43 08 00 00 00 00 	movl   $0x0,0x8(%ebx)
    np->state = UNUSED;
80103ed1:	c7 43 0c 00 00 00 00 	movl   $0x0,0xc(%ebx)
    return -1;
80103ed8:	83 c4 10             	add    $0x10,%esp
80103edb:	bb ff ff ff ff       	mov    $0xffffffff,%ebx
80103ee0:	eb c9                	jmp    80103eab <fork+0xdb>
80103ee2:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80103ee9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80103ef0 <scheduler>:
{
80103ef0:	55                   	push   %ebp
80103ef1:	89 e5                	mov    %esp,%ebp
80103ef3:	57                   	push   %edi
80103ef4:	56                   	push   %esi
80103ef5:	53                   	push   %ebx
80103ef6:	83 ec 0c             	sub    $0xc,%esp
  struct cpu *c = mycpu();
80103ef9:	e8 92 fc ff ff       	call   80103b90 <mycpu>
80103efe:	8d 78 04             	lea    0x4(%eax),%edi
80103f01:	89 c6                	mov    %eax,%esi
  c->proc = 0;
80103f03:	c7 80 ac 00 00 00 00 	movl   $0x0,0xac(%eax)
80103f0a:	00 00 00 
80103f0d:	8d 76 00             	lea    0x0(%esi),%esi
  asm volatile("sti");
80103f10:	fb                   	sti    
    acquire(&ptable.lock);
80103f11:	83 ec 0c             	sub    $0xc,%esp
    for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
80103f14:	bb 54 3d 11 80       	mov    $0x80113d54,%ebx
    acquire(&ptable.lock);
80103f19:	68 20 3d 11 80       	push   $0x80113d20
80103f1e:	e8 ad 08 00 00       	call   801047d0 <acquire>
80103f23:	83 c4 10             	add    $0x10,%esp
80103f26:	8d 76 00             	lea    0x0(%esi),%esi
80103f29:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi
      if(p->state != RUNNABLE)
80103f30:	83 7b 0c 03          	cmpl   $0x3,0xc(%ebx)
80103f34:	75 33                	jne    80103f69 <scheduler+0x79>
      switchuvm(p);
80103f36:	83 ec 0c             	sub    $0xc,%esp
      c->proc = p;
80103f39:	89 9e ac 00 00 00    	mov    %ebx,0xac(%esi)
      switchuvm(p);
80103f3f:	53                   	push   %ebx
80103f40:	e8 db 2c 00 00       	call   80106c20 <switchuvm>
      swtch(&(c->scheduler), p->context);
80103f45:	58                   	pop    %eax
80103f46:	5a                   	pop    %edx
80103f47:	ff 73 1c             	pushl  0x1c(%ebx)
80103f4a:	57                   	push   %edi
      p->state = RUNNING;
80103f4b:	c7 43 0c 04 00 00 00 	movl   $0x4,0xc(%ebx)
      swtch(&(c->scheduler), p->context);
80103f52:	e8 c4 0b 00 00       	call   80104b1b <swtch>
      switchkvm();
80103f57:	e8 a4 2c 00 00       	call   80106c00 <switchkvm>
      c->proc = 0;
80103f5c:	c7 86 ac 00 00 00 00 	movl   $0x0,0xac(%esi)
80103f63:	00 00 00 
80103f66:	83 c4 10             	add    $0x10,%esp
    for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
80103f69:	81 c3 4c 01 00 00    	add    $0x14c,%ebx
80103f6f:	81 fb 54 90 11 80    	cmp    $0x80119054,%ebx
80103f75:	72 b9                	jb     80103f30 <scheduler+0x40>
    release(&ptable.lock);
80103f77:	83 ec 0c             	sub    $0xc,%esp
80103f7a:	68 20 3d 11 80       	push   $0x80113d20
80103f7f:	e8 0c 09 00 00       	call   80104890 <release>
    sti();
80103f84:	83 c4 10             	add    $0x10,%esp
80103f87:	eb 87                	jmp    80103f10 <scheduler+0x20>
80103f89:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

80103f90 <sched>:
{
80103f90:	55                   	push   %ebp
80103f91:	89 e5                	mov    %esp,%ebp
80103f93:	56                   	push   %esi
80103f94:	53                   	push   %ebx
  pushcli();
80103f95:	e8 66 07 00 00       	call   80104700 <pushcli>
  c = mycpu();
80103f9a:	e8 f1 fb ff ff       	call   80103b90 <mycpu>
  p = c->proc;
80103f9f:	8b 98 ac 00 00 00    	mov    0xac(%eax),%ebx
  popcli();
80103fa5:	e8 96 07 00 00       	call   80104740 <popcli>
  if(!holding(&ptable.lock))
80103faa:	83 ec 0c             	sub    $0xc,%esp
80103fad:	68 20 3d 11 80       	push   $0x80113d20
80103fb2:	e8 e9 07 00 00       	call   801047a0 <holding>
80103fb7:	83 c4 10             	add    $0x10,%esp
80103fba:	85 c0                	test   %eax,%eax
80103fbc:	74 4f                	je     8010400d <sched+0x7d>
  if(mycpu()->ncli != 1)
80103fbe:	e8 cd fb ff ff       	call   80103b90 <mycpu>
80103fc3:	83 b8 a4 00 00 00 01 	cmpl   $0x1,0xa4(%eax)
80103fca:	75 68                	jne    80104034 <sched+0xa4>
  if(p->state == RUNNING)
80103fcc:	83 7b 0c 04          	cmpl   $0x4,0xc(%ebx)
80103fd0:	74 55                	je     80104027 <sched+0x97>
  asm volatile("pushfl; popl %0" : "=r" (eflags));
80103fd2:	9c                   	pushf  
80103fd3:	58                   	pop    %eax
  if(readeflags()&FL_IF)
80103fd4:	f6 c4 02             	test   $0x2,%ah
80103fd7:	75 41                	jne    8010401a <sched+0x8a>
  intena = mycpu()->intena;
80103fd9:	e8 b2 fb ff ff       	call   80103b90 <mycpu>
  swtch(&p->context, mycpu()->scheduler);
80103fde:	83 c3 1c             	add    $0x1c,%ebx
  intena = mycpu()->intena;
80103fe1:	8b b0 a8 00 00 00    	mov    0xa8(%eax),%esi
  swtch(&p->context, mycpu()->scheduler);
80103fe7:	e8 a4 fb ff ff       	call   80103b90 <mycpu>
80103fec:	83 ec 08             	sub    $0x8,%esp
80103fef:	ff 70 04             	pushl  0x4(%eax)
80103ff2:	53                   	push   %ebx
80103ff3:	e8 23 0b 00 00       	call   80104b1b <swtch>
  mycpu()->intena = intena;
80103ff8:	e8 93 fb ff ff       	call   80103b90 <mycpu>
}
80103ffd:	83 c4 10             	add    $0x10,%esp
  mycpu()->intena = intena;
80104000:	89 b0 a8 00 00 00    	mov    %esi,0xa8(%eax)
}
80104006:	8d 65 f8             	lea    -0x8(%ebp),%esp
80104009:	5b                   	pop    %ebx
8010400a:	5e                   	pop    %esi
8010400b:	5d                   	pop    %ebp
8010400c:	c3                   	ret    
    panic("sched ptable.lock");
8010400d:	83 ec 0c             	sub    $0xc,%esp
80104010:	68 50 7d 10 80       	push   $0x80107d50
80104015:	e8 76 c3 ff ff       	call   80100390 <panic>
    panic("sched interruptible");
8010401a:	83 ec 0c             	sub    $0xc,%esp
8010401d:	68 7c 7d 10 80       	push   $0x80107d7c
80104022:	e8 69 c3 ff ff       	call   80100390 <panic>
    panic("sched running");
80104027:	83 ec 0c             	sub    $0xc,%esp
8010402a:	68 6e 7d 10 80       	push   $0x80107d6e
8010402f:	e8 5c c3 ff ff       	call   80100390 <panic>
    panic("sched locks");
80104034:	83 ec 0c             	sub    $0xc,%esp
80104037:	68 62 7d 10 80       	push   $0x80107d62
8010403c:	e8 4f c3 ff ff       	call   80100390 <panic>
80104041:	eb 0d                	jmp    80104050 <exit>
80104043:	90                   	nop
80104044:	90                   	nop
80104045:	90                   	nop
80104046:	90                   	nop
80104047:	90                   	nop
80104048:	90                   	nop
80104049:	90                   	nop
8010404a:	90                   	nop
8010404b:	90                   	nop
8010404c:	90                   	nop
8010404d:	90                   	nop
8010404e:	90                   	nop
8010404f:	90                   	nop

80104050 <exit>:
{
80104050:	55                   	push   %ebp
80104051:	89 e5                	mov    %esp,%ebp
80104053:	57                   	push   %edi
80104054:	56                   	push   %esi
80104055:	53                   	push   %ebx
80104056:	83 ec 0c             	sub    $0xc,%esp
  pushcli();
80104059:	e8 a2 06 00 00       	call   80104700 <pushcli>
  c = mycpu();
8010405e:	e8 2d fb ff ff       	call   80103b90 <mycpu>
  p = c->proc;
80104063:	8b 98 ac 00 00 00    	mov    0xac(%eax),%ebx
  popcli();
80104069:	e8 d2 06 00 00       	call   80104740 <popcli>
  if(curproc == initproc)
8010406e:	39 1d b8 b5 10 80    	cmp    %ebx,0x8010b5b8
80104074:	0f 84 07 01 00 00    	je     80104181 <exit+0x131>
  if(curproc->numOfPhysPages==17){ // we opened the swapped file
8010407a:	83 bb 40 01 00 00 11 	cmpl   $0x11,0x140(%ebx)
80104081:	0f 84 07 01 00 00    	je     8010418e <exit+0x13e>
80104087:	8d 73 28             	lea    0x28(%ebx),%esi
8010408a:	8d 7b 68             	lea    0x68(%ebx),%edi
8010408d:	8d 76 00             	lea    0x0(%esi),%esi
    if(curproc->ofile[fd]){
80104090:	8b 06                	mov    (%esi),%eax
80104092:	85 c0                	test   %eax,%eax
80104094:	74 12                	je     801040a8 <exit+0x58>
      fileclose(curproc->ofile[fd]);
80104096:	83 ec 0c             	sub    $0xc,%esp
80104099:	50                   	push   %eax
8010409a:	e8 a1 cd ff ff       	call   80100e40 <fileclose>
      curproc->ofile[fd] = 0;
8010409f:	c7 06 00 00 00 00    	movl   $0x0,(%esi)
801040a5:	83 c4 10             	add    $0x10,%esp
801040a8:	83 c6 04             	add    $0x4,%esi
  for(fd = 0; fd < NOFILE; fd++){
801040ab:	39 fe                	cmp    %edi,%esi
801040ad:	75 e1                	jne    80104090 <exit+0x40>
  begin_op();
801040af:	e8 ec ee ff ff       	call   80102fa0 <begin_op>
  iput(curproc->cwd);
801040b4:	83 ec 0c             	sub    $0xc,%esp
801040b7:	ff 73 68             	pushl  0x68(%ebx)
801040ba:	e8 f1 d6 ff ff       	call   801017b0 <iput>
  end_op();
801040bf:	e8 4c ef ff ff       	call   80103010 <end_op>
  curproc->cwd = 0;
801040c4:	c7 43 68 00 00 00 00 	movl   $0x0,0x68(%ebx)
  acquire(&ptable.lock);
801040cb:	c7 04 24 20 3d 11 80 	movl   $0x80113d20,(%esp)
801040d2:	e8 f9 06 00 00       	call   801047d0 <acquire>
  wakeup1(curproc->parent);
801040d7:	8b 53 14             	mov    0x14(%ebx),%edx
801040da:	83 c4 10             	add    $0x10,%esp
static void
wakeup1(void *chan)
{
  struct proc *p;

  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
801040dd:	b8 54 3d 11 80       	mov    $0x80113d54,%eax
801040e2:	eb 10                	jmp    801040f4 <exit+0xa4>
801040e4:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
801040e8:	05 4c 01 00 00       	add    $0x14c,%eax
801040ed:	3d 54 90 11 80       	cmp    $0x80119054,%eax
801040f2:	73 1e                	jae    80104112 <exit+0xc2>
    if(p->state == SLEEPING && p->chan == chan)
801040f4:	83 78 0c 02          	cmpl   $0x2,0xc(%eax)
801040f8:	75 ee                	jne    801040e8 <exit+0x98>
801040fa:	3b 50 20             	cmp    0x20(%eax),%edx
801040fd:	75 e9                	jne    801040e8 <exit+0x98>
      p->state = RUNNABLE;
801040ff:	c7 40 0c 03 00 00 00 	movl   $0x3,0xc(%eax)
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
80104106:	05 4c 01 00 00       	add    $0x14c,%eax
8010410b:	3d 54 90 11 80       	cmp    $0x80119054,%eax
80104110:	72 e2                	jb     801040f4 <exit+0xa4>
      p->parent = initproc;
80104112:	8b 0d b8 b5 10 80    	mov    0x8010b5b8,%ecx
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
80104118:	ba 54 3d 11 80       	mov    $0x80113d54,%edx
8010411d:	eb 0f                	jmp    8010412e <exit+0xde>
8010411f:	90                   	nop
80104120:	81 c2 4c 01 00 00    	add    $0x14c,%edx
80104126:	81 fa 54 90 11 80    	cmp    $0x80119054,%edx
8010412c:	73 3a                	jae    80104168 <exit+0x118>
    if(p->parent == curproc){
8010412e:	39 5a 14             	cmp    %ebx,0x14(%edx)
80104131:	75 ed                	jne    80104120 <exit+0xd0>
      if(p->state == ZOMBIE)
80104133:	83 7a 0c 05          	cmpl   $0x5,0xc(%edx)
      p->parent = initproc;
80104137:	89 4a 14             	mov    %ecx,0x14(%edx)
      if(p->state == ZOMBIE)
8010413a:	75 e4                	jne    80104120 <exit+0xd0>
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
8010413c:	b8 54 3d 11 80       	mov    $0x80113d54,%eax
80104141:	eb 11                	jmp    80104154 <exit+0x104>
80104143:	90                   	nop
80104144:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80104148:	05 4c 01 00 00       	add    $0x14c,%eax
8010414d:	3d 54 90 11 80       	cmp    $0x80119054,%eax
80104152:	73 cc                	jae    80104120 <exit+0xd0>
    if(p->state == SLEEPING && p->chan == chan)
80104154:	83 78 0c 02          	cmpl   $0x2,0xc(%eax)
80104158:	75 ee                	jne    80104148 <exit+0xf8>
8010415a:	3b 48 20             	cmp    0x20(%eax),%ecx
8010415d:	75 e9                	jne    80104148 <exit+0xf8>
      p->state = RUNNABLE;
8010415f:	c7 40 0c 03 00 00 00 	movl   $0x3,0xc(%eax)
80104166:	eb e0                	jmp    80104148 <exit+0xf8>
  curproc->state = ZOMBIE;
80104168:	c7 43 0c 05 00 00 00 	movl   $0x5,0xc(%ebx)
  sched();
8010416f:	e8 1c fe ff ff       	call   80103f90 <sched>
  panic("zombie exit");
80104174:	83 ec 0c             	sub    $0xc,%esp
80104177:	68 9d 7d 10 80       	push   $0x80107d9d
8010417c:	e8 0f c2 ff ff       	call   80100390 <panic>
    panic("init exiting");
80104181:	83 ec 0c             	sub    $0xc,%esp
80104184:	68 90 7d 10 80       	push   $0x80107d90
80104189:	e8 02 c2 ff ff       	call   80100390 <panic>
    removeSwapFile(curproc);
8010418e:	83 ec 0c             	sub    $0xc,%esp
80104191:	53                   	push   %ebx
80104192:	e8 19 de ff ff       	call   80101fb0 <removeSwapFile>
80104197:	83 c4 10             	add    $0x10,%esp
8010419a:	e9 e8 fe ff ff       	jmp    80104087 <exit+0x37>
8010419f:	90                   	nop

801041a0 <yield>:
{
801041a0:	55                   	push   %ebp
801041a1:	89 e5                	mov    %esp,%ebp
801041a3:	53                   	push   %ebx
801041a4:	83 ec 10             	sub    $0x10,%esp
  acquire(&ptable.lock);  //DOC: yieldlock
801041a7:	68 20 3d 11 80       	push   $0x80113d20
801041ac:	e8 1f 06 00 00       	call   801047d0 <acquire>
  pushcli();
801041b1:	e8 4a 05 00 00       	call   80104700 <pushcli>
  c = mycpu();
801041b6:	e8 d5 f9 ff ff       	call   80103b90 <mycpu>
  p = c->proc;
801041bb:	8b 98 ac 00 00 00    	mov    0xac(%eax),%ebx
  popcli();
801041c1:	e8 7a 05 00 00       	call   80104740 <popcli>
  myproc()->state = RUNNABLE;
801041c6:	c7 43 0c 03 00 00 00 	movl   $0x3,0xc(%ebx)
  sched();
801041cd:	e8 be fd ff ff       	call   80103f90 <sched>
  release(&ptable.lock);
801041d2:	c7 04 24 20 3d 11 80 	movl   $0x80113d20,(%esp)
801041d9:	e8 b2 06 00 00       	call   80104890 <release>
}
801041de:	83 c4 10             	add    $0x10,%esp
801041e1:	8b 5d fc             	mov    -0x4(%ebp),%ebx
801041e4:	c9                   	leave  
801041e5:	c3                   	ret    
801041e6:	8d 76 00             	lea    0x0(%esi),%esi
801041e9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

801041f0 <sleep>:
{
801041f0:	55                   	push   %ebp
801041f1:	89 e5                	mov    %esp,%ebp
801041f3:	57                   	push   %edi
801041f4:	56                   	push   %esi
801041f5:	53                   	push   %ebx
801041f6:	83 ec 0c             	sub    $0xc,%esp
801041f9:	8b 7d 08             	mov    0x8(%ebp),%edi
801041fc:	8b 75 0c             	mov    0xc(%ebp),%esi
  pushcli();
801041ff:	e8 fc 04 00 00       	call   80104700 <pushcli>
  c = mycpu();
80104204:	e8 87 f9 ff ff       	call   80103b90 <mycpu>
  p = c->proc;
80104209:	8b 98 ac 00 00 00    	mov    0xac(%eax),%ebx
  popcli();
8010420f:	e8 2c 05 00 00       	call   80104740 <popcli>
  if(p == 0)
80104214:	85 db                	test   %ebx,%ebx
80104216:	0f 84 87 00 00 00    	je     801042a3 <sleep+0xb3>
  if(lk == 0)
8010421c:	85 f6                	test   %esi,%esi
8010421e:	74 76                	je     80104296 <sleep+0xa6>
  if(lk != &ptable.lock){  //DOC: sleeplock0
80104220:	81 fe 20 3d 11 80    	cmp    $0x80113d20,%esi
80104226:	74 50                	je     80104278 <sleep+0x88>
    acquire(&ptable.lock);  //DOC: sleeplock1
80104228:	83 ec 0c             	sub    $0xc,%esp
8010422b:	68 20 3d 11 80       	push   $0x80113d20
80104230:	e8 9b 05 00 00       	call   801047d0 <acquire>
    release(lk);
80104235:	89 34 24             	mov    %esi,(%esp)
80104238:	e8 53 06 00 00       	call   80104890 <release>
  p->chan = chan;
8010423d:	89 7b 20             	mov    %edi,0x20(%ebx)
  p->state = SLEEPING;
80104240:	c7 43 0c 02 00 00 00 	movl   $0x2,0xc(%ebx)
  sched();
80104247:	e8 44 fd ff ff       	call   80103f90 <sched>
  p->chan = 0;
8010424c:	c7 43 20 00 00 00 00 	movl   $0x0,0x20(%ebx)
    release(&ptable.lock);
80104253:	c7 04 24 20 3d 11 80 	movl   $0x80113d20,(%esp)
8010425a:	e8 31 06 00 00       	call   80104890 <release>
    acquire(lk);
8010425f:	89 75 08             	mov    %esi,0x8(%ebp)
80104262:	83 c4 10             	add    $0x10,%esp
}
80104265:	8d 65 f4             	lea    -0xc(%ebp),%esp
80104268:	5b                   	pop    %ebx
80104269:	5e                   	pop    %esi
8010426a:	5f                   	pop    %edi
8010426b:	5d                   	pop    %ebp
    acquire(lk);
8010426c:	e9 5f 05 00 00       	jmp    801047d0 <acquire>
80104271:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
  p->chan = chan;
80104278:	89 7b 20             	mov    %edi,0x20(%ebx)
  p->state = SLEEPING;
8010427b:	c7 43 0c 02 00 00 00 	movl   $0x2,0xc(%ebx)
  sched();
80104282:	e8 09 fd ff ff       	call   80103f90 <sched>
  p->chan = 0;
80104287:	c7 43 20 00 00 00 00 	movl   $0x0,0x20(%ebx)
}
8010428e:	8d 65 f4             	lea    -0xc(%ebp),%esp
80104291:	5b                   	pop    %ebx
80104292:	5e                   	pop    %esi
80104293:	5f                   	pop    %edi
80104294:	5d                   	pop    %ebp
80104295:	c3                   	ret    
    panic("sleep without lk");
80104296:	83 ec 0c             	sub    $0xc,%esp
80104299:	68 af 7d 10 80       	push   $0x80107daf
8010429e:	e8 ed c0 ff ff       	call   80100390 <panic>
    panic("sleep");
801042a3:	83 ec 0c             	sub    $0xc,%esp
801042a6:	68 a9 7d 10 80       	push   $0x80107da9
801042ab:	e8 e0 c0 ff ff       	call   80100390 <panic>

801042b0 <wait>:
{
801042b0:	55                   	push   %ebp
801042b1:	89 e5                	mov    %esp,%ebp
801042b3:	56                   	push   %esi
801042b4:	53                   	push   %ebx
  pushcli();
801042b5:	e8 46 04 00 00       	call   80104700 <pushcli>
  c = mycpu();
801042ba:	e8 d1 f8 ff ff       	call   80103b90 <mycpu>
  p = c->proc;
801042bf:	8b b0 ac 00 00 00    	mov    0xac(%eax),%esi
  popcli();
801042c5:	e8 76 04 00 00       	call   80104740 <popcli>
  acquire(&ptable.lock);
801042ca:	83 ec 0c             	sub    $0xc,%esp
801042cd:	68 20 3d 11 80       	push   $0x80113d20
801042d2:	e8 f9 04 00 00       	call   801047d0 <acquire>
801042d7:	83 c4 10             	add    $0x10,%esp
    havekids = 0;
801042da:	31 c0                	xor    %eax,%eax
    for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
801042dc:	bb 54 3d 11 80       	mov    $0x80113d54,%ebx
801042e1:	eb 13                	jmp    801042f6 <wait+0x46>
801042e3:	90                   	nop
801042e4:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
801042e8:	81 c3 4c 01 00 00    	add    $0x14c,%ebx
801042ee:	81 fb 54 90 11 80    	cmp    $0x80119054,%ebx
801042f4:	73 1e                	jae    80104314 <wait+0x64>
      if(p->parent != curproc)
801042f6:	39 73 14             	cmp    %esi,0x14(%ebx)
801042f9:	75 ed                	jne    801042e8 <wait+0x38>
      if(p->state == ZOMBIE){
801042fb:	83 7b 0c 05          	cmpl   $0x5,0xc(%ebx)
801042ff:	74 37                	je     80104338 <wait+0x88>
    for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
80104301:	81 c3 4c 01 00 00    	add    $0x14c,%ebx
      havekids = 1;
80104307:	b8 01 00 00 00       	mov    $0x1,%eax
    for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
8010430c:	81 fb 54 90 11 80    	cmp    $0x80119054,%ebx
80104312:	72 e2                	jb     801042f6 <wait+0x46>
    if(!havekids || curproc->killed){
80104314:	85 c0                	test   %eax,%eax
80104316:	74 76                	je     8010438e <wait+0xde>
80104318:	8b 46 24             	mov    0x24(%esi),%eax
8010431b:	85 c0                	test   %eax,%eax
8010431d:	75 6f                	jne    8010438e <wait+0xde>
    sleep(curproc, &ptable.lock);  //DOC: wait-sleep
8010431f:	83 ec 08             	sub    $0x8,%esp
80104322:	68 20 3d 11 80       	push   $0x80113d20
80104327:	56                   	push   %esi
80104328:	e8 c3 fe ff ff       	call   801041f0 <sleep>
    havekids = 0;
8010432d:	83 c4 10             	add    $0x10,%esp
80104330:	eb a8                	jmp    801042da <wait+0x2a>
80104332:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
        kfree(p->kstack);
80104338:	83 ec 0c             	sub    $0xc,%esp
8010433b:	ff 73 08             	pushl  0x8(%ebx)
        pid = p->pid;
8010433e:	8b 73 10             	mov    0x10(%ebx),%esi
        kfree(p->kstack);
80104341:	e8 5a e3 ff ff       	call   801026a0 <kfree>
        freevm(p->pgdir);
80104346:	5a                   	pop    %edx
80104347:	ff 73 04             	pushl  0x4(%ebx)
        p->kstack = 0;
8010434a:	c7 43 08 00 00 00 00 	movl   $0x0,0x8(%ebx)
        freevm(p->pgdir);
80104351:	e8 1a 2c 00 00       	call   80106f70 <freevm>
        release(&ptable.lock);
80104356:	c7 04 24 20 3d 11 80 	movl   $0x80113d20,(%esp)
        p->pid = 0;
8010435d:	c7 43 10 00 00 00 00 	movl   $0x0,0x10(%ebx)
        p->parent = 0;
80104364:	c7 43 14 00 00 00 00 	movl   $0x0,0x14(%ebx)
        p->name[0] = 0;
8010436b:	c6 43 6c 00          	movb   $0x0,0x6c(%ebx)
        p->killed = 0;
8010436f:	c7 43 24 00 00 00 00 	movl   $0x0,0x24(%ebx)
        p->state = UNUSED;
80104376:	c7 43 0c 00 00 00 00 	movl   $0x0,0xc(%ebx)
        release(&ptable.lock);
8010437d:	e8 0e 05 00 00       	call   80104890 <release>
        return pid;
80104382:	83 c4 10             	add    $0x10,%esp
}
80104385:	8d 65 f8             	lea    -0x8(%ebp),%esp
80104388:	89 f0                	mov    %esi,%eax
8010438a:	5b                   	pop    %ebx
8010438b:	5e                   	pop    %esi
8010438c:	5d                   	pop    %ebp
8010438d:	c3                   	ret    
      release(&ptable.lock);
8010438e:	83 ec 0c             	sub    $0xc,%esp
      return -1;
80104391:	be ff ff ff ff       	mov    $0xffffffff,%esi
      release(&ptable.lock);
80104396:	68 20 3d 11 80       	push   $0x80113d20
8010439b:	e8 f0 04 00 00       	call   80104890 <release>
      return -1;
801043a0:	83 c4 10             	add    $0x10,%esp
801043a3:	eb e0                	jmp    80104385 <wait+0xd5>
801043a5:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
801043a9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

801043b0 <wakeup>:
}

// Wake up all processes sleeping on chan.
void
wakeup(void *chan)
{
801043b0:	55                   	push   %ebp
801043b1:	89 e5                	mov    %esp,%ebp
801043b3:	53                   	push   %ebx
801043b4:	83 ec 10             	sub    $0x10,%esp
801043b7:	8b 5d 08             	mov    0x8(%ebp),%ebx
  acquire(&ptable.lock);
801043ba:	68 20 3d 11 80       	push   $0x80113d20
801043bf:	e8 0c 04 00 00       	call   801047d0 <acquire>
801043c4:	83 c4 10             	add    $0x10,%esp
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
801043c7:	b8 54 3d 11 80       	mov    $0x80113d54,%eax
801043cc:	eb 0e                	jmp    801043dc <wakeup+0x2c>
801043ce:	66 90                	xchg   %ax,%ax
801043d0:	05 4c 01 00 00       	add    $0x14c,%eax
801043d5:	3d 54 90 11 80       	cmp    $0x80119054,%eax
801043da:	73 1e                	jae    801043fa <wakeup+0x4a>
    if(p->state == SLEEPING && p->chan == chan)
801043dc:	83 78 0c 02          	cmpl   $0x2,0xc(%eax)
801043e0:	75 ee                	jne    801043d0 <wakeup+0x20>
801043e2:	3b 58 20             	cmp    0x20(%eax),%ebx
801043e5:	75 e9                	jne    801043d0 <wakeup+0x20>
      p->state = RUNNABLE;
801043e7:	c7 40 0c 03 00 00 00 	movl   $0x3,0xc(%eax)
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
801043ee:	05 4c 01 00 00       	add    $0x14c,%eax
801043f3:	3d 54 90 11 80       	cmp    $0x80119054,%eax
801043f8:	72 e2                	jb     801043dc <wakeup+0x2c>
  wakeup1(chan);
  release(&ptable.lock);
801043fa:	c7 45 08 20 3d 11 80 	movl   $0x80113d20,0x8(%ebp)
}
80104401:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80104404:	c9                   	leave  
  release(&ptable.lock);
80104405:	e9 86 04 00 00       	jmp    80104890 <release>
8010440a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi

80104410 <kill>:
// Kill the process with the given pid.
// Process won't exit until it returns
// to user space (see trap in trap.c).
int
kill(int pid)
{
80104410:	55                   	push   %ebp
80104411:	89 e5                	mov    %esp,%ebp
80104413:	53                   	push   %ebx
80104414:	83 ec 10             	sub    $0x10,%esp
80104417:	8b 5d 08             	mov    0x8(%ebp),%ebx
  struct proc *p;

  acquire(&ptable.lock);
8010441a:	68 20 3d 11 80       	push   $0x80113d20
8010441f:	e8 ac 03 00 00       	call   801047d0 <acquire>
80104424:	83 c4 10             	add    $0x10,%esp
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
80104427:	b8 54 3d 11 80       	mov    $0x80113d54,%eax
8010442c:	eb 0e                	jmp    8010443c <kill+0x2c>
8010442e:	66 90                	xchg   %ax,%ax
80104430:	05 4c 01 00 00       	add    $0x14c,%eax
80104435:	3d 54 90 11 80       	cmp    $0x80119054,%eax
8010443a:	73 34                	jae    80104470 <kill+0x60>
    if(p->pid == pid){
8010443c:	39 58 10             	cmp    %ebx,0x10(%eax)
8010443f:	75 ef                	jne    80104430 <kill+0x20>
      p->killed = 1;
      // Wake process from sleep if necessary.
      if(p->state == SLEEPING)
80104441:	83 78 0c 02          	cmpl   $0x2,0xc(%eax)
      p->killed = 1;
80104445:	c7 40 24 01 00 00 00 	movl   $0x1,0x24(%eax)
      if(p->state == SLEEPING)
8010444c:	75 07                	jne    80104455 <kill+0x45>
        p->state = RUNNABLE;
8010444e:	c7 40 0c 03 00 00 00 	movl   $0x3,0xc(%eax)
      release(&ptable.lock);
80104455:	83 ec 0c             	sub    $0xc,%esp
80104458:	68 20 3d 11 80       	push   $0x80113d20
8010445d:	e8 2e 04 00 00       	call   80104890 <release>
      return 0;
80104462:	83 c4 10             	add    $0x10,%esp
80104465:	31 c0                	xor    %eax,%eax
    }
  }
  release(&ptable.lock);
  return -1;
}
80104467:	8b 5d fc             	mov    -0x4(%ebp),%ebx
8010446a:	c9                   	leave  
8010446b:	c3                   	ret    
8010446c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  release(&ptable.lock);
80104470:	83 ec 0c             	sub    $0xc,%esp
80104473:	68 20 3d 11 80       	push   $0x80113d20
80104478:	e8 13 04 00 00       	call   80104890 <release>
  return -1;
8010447d:	83 c4 10             	add    $0x10,%esp
80104480:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80104485:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80104488:	c9                   	leave  
80104489:	c3                   	ret    
8010448a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi

80104490 <procdump>:
// Print a process listing to console.  For debugging.
// Runs when user types ^P on console.
// No lock to avoid wedging a stuck machine further.
void
procdump(void)
{
80104490:	55                   	push   %ebp
80104491:	89 e5                	mov    %esp,%ebp
80104493:	57                   	push   %edi
80104494:	56                   	push   %esi
80104495:	53                   	push   %ebx
80104496:	8d 75 e8             	lea    -0x18(%ebp),%esi
  int i;
  struct proc *p;
  char *state;
  uint pc[10];

  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
80104499:	bb 54 3d 11 80       	mov    $0x80113d54,%ebx
{
8010449e:	83 ec 3c             	sub    $0x3c,%esp
801044a1:	eb 27                	jmp    801044ca <procdump+0x3a>
801044a3:	90                   	nop
801044a4:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    if(p->state == SLEEPING){
      getcallerpcs((uint*)p->context->ebp+2, pc);
      for(i=0; i<10 && pc[i] != 0; i++)
        cprintf(" %p", pc[i]);
    }
    cprintf("\n");
801044a8:	83 ec 0c             	sub    $0xc,%esp
801044ab:	68 d1 81 10 80       	push   $0x801081d1
801044b0:	e8 ab c1 ff ff       	call   80100660 <cprintf>
801044b5:	83 c4 10             	add    $0x10,%esp
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
801044b8:	81 c3 4c 01 00 00    	add    $0x14c,%ebx
801044be:	81 fb 54 90 11 80    	cmp    $0x80119054,%ebx
801044c4:	0f 83 86 00 00 00    	jae    80104550 <procdump+0xc0>
    if(p->state == UNUSED)
801044ca:	8b 43 0c             	mov    0xc(%ebx),%eax
801044cd:	85 c0                	test   %eax,%eax
801044cf:	74 e7                	je     801044b8 <procdump+0x28>
    if(p->state >= 0 && p->state < NELEM(states) && states[p->state])
801044d1:	83 f8 05             	cmp    $0x5,%eax
      state = "???";
801044d4:	ba c0 7d 10 80       	mov    $0x80107dc0,%edx
    if(p->state >= 0 && p->state < NELEM(states) && states[p->state])
801044d9:	77 11                	ja     801044ec <procdump+0x5c>
801044db:	8b 14 85 20 7e 10 80 	mov    -0x7fef81e0(,%eax,4),%edx
      state = "???";
801044e2:	b8 c0 7d 10 80       	mov    $0x80107dc0,%eax
801044e7:	85 d2                	test   %edx,%edx
801044e9:	0f 44 d0             	cmove  %eax,%edx
    cprintf("%d %s %s", p->pid, state, p->name);
801044ec:	8d 43 6c             	lea    0x6c(%ebx),%eax
801044ef:	50                   	push   %eax
801044f0:	52                   	push   %edx
801044f1:	ff 73 10             	pushl  0x10(%ebx)
801044f4:	68 c4 7d 10 80       	push   $0x80107dc4
801044f9:	e8 62 c1 ff ff       	call   80100660 <cprintf>
    if(p->state == SLEEPING){
801044fe:	83 c4 10             	add    $0x10,%esp
80104501:	83 7b 0c 02          	cmpl   $0x2,0xc(%ebx)
80104505:	75 a1                	jne    801044a8 <procdump+0x18>
      getcallerpcs((uint*)p->context->ebp+2, pc);
80104507:	8d 45 c0             	lea    -0x40(%ebp),%eax
8010450a:	83 ec 08             	sub    $0x8,%esp
8010450d:	8d 7d c0             	lea    -0x40(%ebp),%edi
80104510:	50                   	push   %eax
80104511:	8b 43 1c             	mov    0x1c(%ebx),%eax
80104514:	8b 40 0c             	mov    0xc(%eax),%eax
80104517:	83 c0 08             	add    $0x8,%eax
8010451a:	50                   	push   %eax
8010451b:	e8 90 01 00 00       	call   801046b0 <getcallerpcs>
80104520:	83 c4 10             	add    $0x10,%esp
80104523:	90                   	nop
80104524:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
      for(i=0; i<10 && pc[i] != 0; i++)
80104528:	8b 17                	mov    (%edi),%edx
8010452a:	85 d2                	test   %edx,%edx
8010452c:	0f 84 76 ff ff ff    	je     801044a8 <procdump+0x18>
        cprintf(" %p", pc[i]);
80104532:	83 ec 08             	sub    $0x8,%esp
80104535:	83 c7 04             	add    $0x4,%edi
80104538:	52                   	push   %edx
80104539:	68 81 77 10 80       	push   $0x80107781
8010453e:	e8 1d c1 ff ff       	call   80100660 <cprintf>
      for(i=0; i<10 && pc[i] != 0; i++)
80104543:	83 c4 10             	add    $0x10,%esp
80104546:	39 fe                	cmp    %edi,%esi
80104548:	75 de                	jne    80104528 <procdump+0x98>
8010454a:	e9 59 ff ff ff       	jmp    801044a8 <procdump+0x18>
8010454f:	90                   	nop
  }
}
80104550:	8d 65 f4             	lea    -0xc(%ebp),%esp
80104553:	5b                   	pop    %ebx
80104554:	5e                   	pop    %esi
80104555:	5f                   	pop    %edi
80104556:	5d                   	pop    %ebp
80104557:	c3                   	ret    
80104558:	66 90                	xchg   %ax,%ax
8010455a:	66 90                	xchg   %ax,%ax
8010455c:	66 90                	xchg   %ax,%ax
8010455e:	66 90                	xchg   %ax,%ax

80104560 <initsleeplock>:
#include "spinlock.h"
#include "sleeplock.h"

void
initsleeplock(struct sleeplock *lk, char *name)
{
80104560:	55                   	push   %ebp
80104561:	89 e5                	mov    %esp,%ebp
80104563:	53                   	push   %ebx
80104564:	83 ec 0c             	sub    $0xc,%esp
80104567:	8b 5d 08             	mov    0x8(%ebp),%ebx
  initlock(&lk->lk, "sleep lock");
8010456a:	68 38 7e 10 80       	push   $0x80107e38
8010456f:	8d 43 04             	lea    0x4(%ebx),%eax
80104572:	50                   	push   %eax
80104573:	e8 18 01 00 00       	call   80104690 <initlock>
  lk->name = name;
80104578:	8b 45 0c             	mov    0xc(%ebp),%eax
  lk->locked = 0;
8010457b:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  lk->pid = 0;
}
80104581:	83 c4 10             	add    $0x10,%esp
  lk->pid = 0;
80104584:	c7 43 3c 00 00 00 00 	movl   $0x0,0x3c(%ebx)
  lk->name = name;
8010458b:	89 43 38             	mov    %eax,0x38(%ebx)
}
8010458e:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80104591:	c9                   	leave  
80104592:	c3                   	ret    
80104593:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
80104599:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

801045a0 <acquiresleep>:

void
acquiresleep(struct sleeplock *lk)
{
801045a0:	55                   	push   %ebp
801045a1:	89 e5                	mov    %esp,%ebp
801045a3:	56                   	push   %esi
801045a4:	53                   	push   %ebx
801045a5:	8b 5d 08             	mov    0x8(%ebp),%ebx
  acquire(&lk->lk);
801045a8:	83 ec 0c             	sub    $0xc,%esp
801045ab:	8d 73 04             	lea    0x4(%ebx),%esi
801045ae:	56                   	push   %esi
801045af:	e8 1c 02 00 00       	call   801047d0 <acquire>
  while (lk->locked) {
801045b4:	8b 13                	mov    (%ebx),%edx
801045b6:	83 c4 10             	add    $0x10,%esp
801045b9:	85 d2                	test   %edx,%edx
801045bb:	74 16                	je     801045d3 <acquiresleep+0x33>
801045bd:	8d 76 00             	lea    0x0(%esi),%esi
    sleep(lk, &lk->lk);
801045c0:	83 ec 08             	sub    $0x8,%esp
801045c3:	56                   	push   %esi
801045c4:	53                   	push   %ebx
801045c5:	e8 26 fc ff ff       	call   801041f0 <sleep>
  while (lk->locked) {
801045ca:	8b 03                	mov    (%ebx),%eax
801045cc:	83 c4 10             	add    $0x10,%esp
801045cf:	85 c0                	test   %eax,%eax
801045d1:	75 ed                	jne    801045c0 <acquiresleep+0x20>
  }
  lk->locked = 1;
801045d3:	c7 03 01 00 00 00    	movl   $0x1,(%ebx)
  lk->pid = myproc()->pid;
801045d9:	e8 52 f6 ff ff       	call   80103c30 <myproc>
801045de:	8b 40 10             	mov    0x10(%eax),%eax
801045e1:	89 43 3c             	mov    %eax,0x3c(%ebx)
  release(&lk->lk);
801045e4:	89 75 08             	mov    %esi,0x8(%ebp)
}
801045e7:	8d 65 f8             	lea    -0x8(%ebp),%esp
801045ea:	5b                   	pop    %ebx
801045eb:	5e                   	pop    %esi
801045ec:	5d                   	pop    %ebp
  release(&lk->lk);
801045ed:	e9 9e 02 00 00       	jmp    80104890 <release>
801045f2:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801045f9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80104600 <releasesleep>:

void
releasesleep(struct sleeplock *lk)
{
80104600:	55                   	push   %ebp
80104601:	89 e5                	mov    %esp,%ebp
80104603:	56                   	push   %esi
80104604:	53                   	push   %ebx
80104605:	8b 5d 08             	mov    0x8(%ebp),%ebx
  acquire(&lk->lk);
80104608:	83 ec 0c             	sub    $0xc,%esp
8010460b:	8d 73 04             	lea    0x4(%ebx),%esi
8010460e:	56                   	push   %esi
8010460f:	e8 bc 01 00 00       	call   801047d0 <acquire>
  lk->locked = 0;
80104614:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  lk->pid = 0;
8010461a:	c7 43 3c 00 00 00 00 	movl   $0x0,0x3c(%ebx)
  wakeup(lk);
80104621:	89 1c 24             	mov    %ebx,(%esp)
80104624:	e8 87 fd ff ff       	call   801043b0 <wakeup>
  release(&lk->lk);
80104629:	89 75 08             	mov    %esi,0x8(%ebp)
8010462c:	83 c4 10             	add    $0x10,%esp
}
8010462f:	8d 65 f8             	lea    -0x8(%ebp),%esp
80104632:	5b                   	pop    %ebx
80104633:	5e                   	pop    %esi
80104634:	5d                   	pop    %ebp
  release(&lk->lk);
80104635:	e9 56 02 00 00       	jmp    80104890 <release>
8010463a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi

80104640 <holdingsleep>:

int
holdingsleep(struct sleeplock *lk)
{
80104640:	55                   	push   %ebp
80104641:	89 e5                	mov    %esp,%ebp
80104643:	57                   	push   %edi
80104644:	56                   	push   %esi
80104645:	53                   	push   %ebx
80104646:	31 ff                	xor    %edi,%edi
80104648:	83 ec 18             	sub    $0x18,%esp
8010464b:	8b 5d 08             	mov    0x8(%ebp),%ebx
  int r;
  
  acquire(&lk->lk);
8010464e:	8d 73 04             	lea    0x4(%ebx),%esi
80104651:	56                   	push   %esi
80104652:	e8 79 01 00 00       	call   801047d0 <acquire>
  r = lk->locked && (lk->pid == myproc()->pid);
80104657:	8b 03                	mov    (%ebx),%eax
80104659:	83 c4 10             	add    $0x10,%esp
8010465c:	85 c0                	test   %eax,%eax
8010465e:	74 13                	je     80104673 <holdingsleep+0x33>
80104660:	8b 5b 3c             	mov    0x3c(%ebx),%ebx
80104663:	e8 c8 f5 ff ff       	call   80103c30 <myproc>
80104668:	39 58 10             	cmp    %ebx,0x10(%eax)
8010466b:	0f 94 c0             	sete   %al
8010466e:	0f b6 c0             	movzbl %al,%eax
80104671:	89 c7                	mov    %eax,%edi
  release(&lk->lk);
80104673:	83 ec 0c             	sub    $0xc,%esp
80104676:	56                   	push   %esi
80104677:	e8 14 02 00 00       	call   80104890 <release>
  return r;
}
8010467c:	8d 65 f4             	lea    -0xc(%ebp),%esp
8010467f:	89 f8                	mov    %edi,%eax
80104681:	5b                   	pop    %ebx
80104682:	5e                   	pop    %esi
80104683:	5f                   	pop    %edi
80104684:	5d                   	pop    %ebp
80104685:	c3                   	ret    
80104686:	66 90                	xchg   %ax,%ax
80104688:	66 90                	xchg   %ax,%ax
8010468a:	66 90                	xchg   %ax,%ax
8010468c:	66 90                	xchg   %ax,%ax
8010468e:	66 90                	xchg   %ax,%ax

80104690 <initlock>:
#include "proc.h"
#include "spinlock.h"

void
initlock(struct spinlock *lk, char *name)
{
80104690:	55                   	push   %ebp
80104691:	89 e5                	mov    %esp,%ebp
80104693:	8b 45 08             	mov    0x8(%ebp),%eax
  lk->name = name;
80104696:	8b 55 0c             	mov    0xc(%ebp),%edx
  lk->locked = 0;
80104699:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
  lk->name = name;
8010469f:	89 50 04             	mov    %edx,0x4(%eax)
  lk->cpu = 0;
801046a2:	c7 40 08 00 00 00 00 	movl   $0x0,0x8(%eax)
}
801046a9:	5d                   	pop    %ebp
801046aa:	c3                   	ret    
801046ab:	90                   	nop
801046ac:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

801046b0 <getcallerpcs>:
}

// Record the current call stack in pcs[] by following the %ebp chain.
void
getcallerpcs(void *v, uint pcs[])
{
801046b0:	55                   	push   %ebp
  uint *ebp;
  int i;

  ebp = (uint*)v - 2;
  for(i = 0; i < 10; i++){
801046b1:	31 d2                	xor    %edx,%edx
{
801046b3:	89 e5                	mov    %esp,%ebp
801046b5:	53                   	push   %ebx
  ebp = (uint*)v - 2;
801046b6:	8b 45 08             	mov    0x8(%ebp),%eax
{
801046b9:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  ebp = (uint*)v - 2;
801046bc:	83 e8 08             	sub    $0x8,%eax
801046bf:	90                   	nop
    if(ebp == 0 || ebp < (uint*)KERNBASE || ebp == (uint*)0xffffffff)
801046c0:	8d 98 00 00 00 80    	lea    -0x80000000(%eax),%ebx
801046c6:	81 fb fe ff ff 7f    	cmp    $0x7ffffffe,%ebx
801046cc:	77 1a                	ja     801046e8 <getcallerpcs+0x38>
      break;
    pcs[i] = ebp[1];     // saved %eip
801046ce:	8b 58 04             	mov    0x4(%eax),%ebx
801046d1:	89 1c 91             	mov    %ebx,(%ecx,%edx,4)
  for(i = 0; i < 10; i++){
801046d4:	83 c2 01             	add    $0x1,%edx
    ebp = (uint*)ebp[0]; // saved %ebp
801046d7:	8b 00                	mov    (%eax),%eax
  for(i = 0; i < 10; i++){
801046d9:	83 fa 0a             	cmp    $0xa,%edx
801046dc:	75 e2                	jne    801046c0 <getcallerpcs+0x10>
  }
  for(; i < 10; i++)
    pcs[i] = 0;
}
801046de:	5b                   	pop    %ebx
801046df:	5d                   	pop    %ebp
801046e0:	c3                   	ret    
801046e1:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801046e8:	8d 04 91             	lea    (%ecx,%edx,4),%eax
801046eb:	83 c1 28             	add    $0x28,%ecx
801046ee:	66 90                	xchg   %ax,%ax
    pcs[i] = 0;
801046f0:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
801046f6:	83 c0 04             	add    $0x4,%eax
  for(; i < 10; i++)
801046f9:	39 c1                	cmp    %eax,%ecx
801046fb:	75 f3                	jne    801046f0 <getcallerpcs+0x40>
}
801046fd:	5b                   	pop    %ebx
801046fe:	5d                   	pop    %ebp
801046ff:	c3                   	ret    

80104700 <pushcli>:
// it takes two popcli to undo two pushcli.  Also, if interrupts
// are off, then pushcli, popcli leaves them off.

void
pushcli(void)
{
80104700:	55                   	push   %ebp
80104701:	89 e5                	mov    %esp,%ebp
80104703:	53                   	push   %ebx
80104704:	83 ec 04             	sub    $0x4,%esp
80104707:	9c                   	pushf  
80104708:	5b                   	pop    %ebx
  asm volatile("cli");
80104709:	fa                   	cli    
  int eflags;

  eflags = readeflags();
  cli();
  if(mycpu()->ncli == 0)
8010470a:	e8 81 f4 ff ff       	call   80103b90 <mycpu>
8010470f:	8b 80 a4 00 00 00    	mov    0xa4(%eax),%eax
80104715:	85 c0                	test   %eax,%eax
80104717:	75 11                	jne    8010472a <pushcli+0x2a>
    mycpu()->intena = eflags & FL_IF;
80104719:	81 e3 00 02 00 00    	and    $0x200,%ebx
8010471f:	e8 6c f4 ff ff       	call   80103b90 <mycpu>
80104724:	89 98 a8 00 00 00    	mov    %ebx,0xa8(%eax)
  mycpu()->ncli += 1;
8010472a:	e8 61 f4 ff ff       	call   80103b90 <mycpu>
8010472f:	83 80 a4 00 00 00 01 	addl   $0x1,0xa4(%eax)
}
80104736:	83 c4 04             	add    $0x4,%esp
80104739:	5b                   	pop    %ebx
8010473a:	5d                   	pop    %ebp
8010473b:	c3                   	ret    
8010473c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

80104740 <popcli>:

void
popcli(void)
{
80104740:	55                   	push   %ebp
80104741:	89 e5                	mov    %esp,%ebp
80104743:	83 ec 08             	sub    $0x8,%esp
  asm volatile("pushfl; popl %0" : "=r" (eflags));
80104746:	9c                   	pushf  
80104747:	58                   	pop    %eax
  if(readeflags()&FL_IF)
80104748:	f6 c4 02             	test   $0x2,%ah
8010474b:	75 35                	jne    80104782 <popcli+0x42>
    panic("popcli - interruptible");
  if(--mycpu()->ncli < 0)
8010474d:	e8 3e f4 ff ff       	call   80103b90 <mycpu>
80104752:	83 a8 a4 00 00 00 01 	subl   $0x1,0xa4(%eax)
80104759:	78 34                	js     8010478f <popcli+0x4f>
    panic("popcli");
  if(mycpu()->ncli == 0 && mycpu()->intena)
8010475b:	e8 30 f4 ff ff       	call   80103b90 <mycpu>
80104760:	8b 90 a4 00 00 00    	mov    0xa4(%eax),%edx
80104766:	85 d2                	test   %edx,%edx
80104768:	74 06                	je     80104770 <popcli+0x30>
    sti();
}
8010476a:	c9                   	leave  
8010476b:	c3                   	ret    
8010476c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  if(mycpu()->ncli == 0 && mycpu()->intena)
80104770:	e8 1b f4 ff ff       	call   80103b90 <mycpu>
80104775:	8b 80 a8 00 00 00    	mov    0xa8(%eax),%eax
8010477b:	85 c0                	test   %eax,%eax
8010477d:	74 eb                	je     8010476a <popcli+0x2a>
  asm volatile("sti");
8010477f:	fb                   	sti    
}
80104780:	c9                   	leave  
80104781:	c3                   	ret    
    panic("popcli - interruptible");
80104782:	83 ec 0c             	sub    $0xc,%esp
80104785:	68 43 7e 10 80       	push   $0x80107e43
8010478a:	e8 01 bc ff ff       	call   80100390 <panic>
    panic("popcli");
8010478f:	83 ec 0c             	sub    $0xc,%esp
80104792:	68 5a 7e 10 80       	push   $0x80107e5a
80104797:	e8 f4 bb ff ff       	call   80100390 <panic>
8010479c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

801047a0 <holding>:
{
801047a0:	55                   	push   %ebp
801047a1:	89 e5                	mov    %esp,%ebp
801047a3:	56                   	push   %esi
801047a4:	53                   	push   %ebx
801047a5:	8b 75 08             	mov    0x8(%ebp),%esi
801047a8:	31 db                	xor    %ebx,%ebx
  pushcli();
801047aa:	e8 51 ff ff ff       	call   80104700 <pushcli>
  r = lock->locked && lock->cpu == mycpu();
801047af:	8b 06                	mov    (%esi),%eax
801047b1:	85 c0                	test   %eax,%eax
801047b3:	74 10                	je     801047c5 <holding+0x25>
801047b5:	8b 5e 08             	mov    0x8(%esi),%ebx
801047b8:	e8 d3 f3 ff ff       	call   80103b90 <mycpu>
801047bd:	39 c3                	cmp    %eax,%ebx
801047bf:	0f 94 c3             	sete   %bl
801047c2:	0f b6 db             	movzbl %bl,%ebx
  popcli();
801047c5:	e8 76 ff ff ff       	call   80104740 <popcli>
}
801047ca:	89 d8                	mov    %ebx,%eax
801047cc:	5b                   	pop    %ebx
801047cd:	5e                   	pop    %esi
801047ce:	5d                   	pop    %ebp
801047cf:	c3                   	ret    

801047d0 <acquire>:
{
801047d0:	55                   	push   %ebp
801047d1:	89 e5                	mov    %esp,%ebp
801047d3:	56                   	push   %esi
801047d4:	53                   	push   %ebx
  pushcli(); // disable interrupts to avoid deadlock.
801047d5:	e8 26 ff ff ff       	call   80104700 <pushcli>
  if(holding(lk))
801047da:	8b 5d 08             	mov    0x8(%ebp),%ebx
801047dd:	83 ec 0c             	sub    $0xc,%esp
801047e0:	53                   	push   %ebx
801047e1:	e8 ba ff ff ff       	call   801047a0 <holding>
801047e6:	83 c4 10             	add    $0x10,%esp
801047e9:	85 c0                	test   %eax,%eax
801047eb:	0f 85 83 00 00 00    	jne    80104874 <acquire+0xa4>
801047f1:	89 c6                	mov    %eax,%esi
  asm volatile("lock; xchgl %0, %1" :
801047f3:	ba 01 00 00 00       	mov    $0x1,%edx
801047f8:	eb 09                	jmp    80104803 <acquire+0x33>
801047fa:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
80104800:	8b 5d 08             	mov    0x8(%ebp),%ebx
80104803:	89 d0                	mov    %edx,%eax
80104805:	f0 87 03             	lock xchg %eax,(%ebx)
  while(xchg(&lk->locked, 1) != 0)
80104808:	85 c0                	test   %eax,%eax
8010480a:	75 f4                	jne    80104800 <acquire+0x30>
  __sync_synchronize();
8010480c:	f0 83 0c 24 00       	lock orl $0x0,(%esp)
  lk->cpu = mycpu();
80104811:	8b 5d 08             	mov    0x8(%ebp),%ebx
80104814:	e8 77 f3 ff ff       	call   80103b90 <mycpu>
  getcallerpcs(&lk, lk->pcs);
80104819:	8d 53 0c             	lea    0xc(%ebx),%edx
  lk->cpu = mycpu();
8010481c:	89 43 08             	mov    %eax,0x8(%ebx)
  ebp = (uint*)v - 2;
8010481f:	89 e8                	mov    %ebp,%eax
80104821:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    if(ebp == 0 || ebp < (uint*)KERNBASE || ebp == (uint*)0xffffffff)
80104828:	8d 88 00 00 00 80    	lea    -0x80000000(%eax),%ecx
8010482e:	81 f9 fe ff ff 7f    	cmp    $0x7ffffffe,%ecx
80104834:	77 1a                	ja     80104850 <acquire+0x80>
    pcs[i] = ebp[1];     // saved %eip
80104836:	8b 48 04             	mov    0x4(%eax),%ecx
80104839:	89 0c b2             	mov    %ecx,(%edx,%esi,4)
  for(i = 0; i < 10; i++){
8010483c:	83 c6 01             	add    $0x1,%esi
    ebp = (uint*)ebp[0]; // saved %ebp
8010483f:	8b 00                	mov    (%eax),%eax
  for(i = 0; i < 10; i++){
80104841:	83 fe 0a             	cmp    $0xa,%esi
80104844:	75 e2                	jne    80104828 <acquire+0x58>
}
80104846:	8d 65 f8             	lea    -0x8(%ebp),%esp
80104849:	5b                   	pop    %ebx
8010484a:	5e                   	pop    %esi
8010484b:	5d                   	pop    %ebp
8010484c:	c3                   	ret    
8010484d:	8d 76 00             	lea    0x0(%esi),%esi
80104850:	8d 04 b2             	lea    (%edx,%esi,4),%eax
80104853:	83 c2 28             	add    $0x28,%edx
80104856:	8d 76 00             	lea    0x0(%esi),%esi
80104859:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi
    pcs[i] = 0;
80104860:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
80104866:	83 c0 04             	add    $0x4,%eax
  for(; i < 10; i++)
80104869:	39 d0                	cmp    %edx,%eax
8010486b:	75 f3                	jne    80104860 <acquire+0x90>
}
8010486d:	8d 65 f8             	lea    -0x8(%ebp),%esp
80104870:	5b                   	pop    %ebx
80104871:	5e                   	pop    %esi
80104872:	5d                   	pop    %ebp
80104873:	c3                   	ret    
    panic("acquire");
80104874:	83 ec 0c             	sub    $0xc,%esp
80104877:	68 61 7e 10 80       	push   $0x80107e61
8010487c:	e8 0f bb ff ff       	call   80100390 <panic>
80104881:	eb 0d                	jmp    80104890 <release>
80104883:	90                   	nop
80104884:	90                   	nop
80104885:	90                   	nop
80104886:	90                   	nop
80104887:	90                   	nop
80104888:	90                   	nop
80104889:	90                   	nop
8010488a:	90                   	nop
8010488b:	90                   	nop
8010488c:	90                   	nop
8010488d:	90                   	nop
8010488e:	90                   	nop
8010488f:	90                   	nop

80104890 <release>:
{
80104890:	55                   	push   %ebp
80104891:	89 e5                	mov    %esp,%ebp
80104893:	53                   	push   %ebx
80104894:	83 ec 10             	sub    $0x10,%esp
80104897:	8b 5d 08             	mov    0x8(%ebp),%ebx
  if(!holding(lk))
8010489a:	53                   	push   %ebx
8010489b:	e8 00 ff ff ff       	call   801047a0 <holding>
801048a0:	83 c4 10             	add    $0x10,%esp
801048a3:	85 c0                	test   %eax,%eax
801048a5:	74 22                	je     801048c9 <release+0x39>
  lk->pcs[0] = 0;
801048a7:	c7 43 0c 00 00 00 00 	movl   $0x0,0xc(%ebx)
  lk->cpu = 0;
801048ae:	c7 43 08 00 00 00 00 	movl   $0x0,0x8(%ebx)
  __sync_synchronize();
801048b5:	f0 83 0c 24 00       	lock orl $0x0,(%esp)
  asm volatile("movl $0, %0" : "+m" (lk->locked) : );
801048ba:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
}
801048c0:	8b 5d fc             	mov    -0x4(%ebp),%ebx
801048c3:	c9                   	leave  
  popcli();
801048c4:	e9 77 fe ff ff       	jmp    80104740 <popcli>
    panic("release");
801048c9:	83 ec 0c             	sub    $0xc,%esp
801048cc:	68 69 7e 10 80       	push   $0x80107e69
801048d1:	e8 ba ba ff ff       	call   80100390 <panic>
801048d6:	66 90                	xchg   %ax,%ax
801048d8:	66 90                	xchg   %ax,%ax
801048da:	66 90                	xchg   %ax,%ax
801048dc:	66 90                	xchg   %ax,%ax
801048de:	66 90                	xchg   %ax,%ax

801048e0 <memset>:
#include "types.h"
#include "x86.h"

void*
memset(void *dst, int c, uint n)
{
801048e0:	55                   	push   %ebp
801048e1:	89 e5                	mov    %esp,%ebp
801048e3:	57                   	push   %edi
801048e4:	53                   	push   %ebx
801048e5:	8b 55 08             	mov    0x8(%ebp),%edx
801048e8:	8b 4d 10             	mov    0x10(%ebp),%ecx
  if ((int)dst%4 == 0 && n%4 == 0){
801048eb:	f6 c2 03             	test   $0x3,%dl
801048ee:	75 05                	jne    801048f5 <memset+0x15>
801048f0:	f6 c1 03             	test   $0x3,%cl
801048f3:	74 13                	je     80104908 <memset+0x28>
  asm volatile("cld; rep stosb" :
801048f5:	89 d7                	mov    %edx,%edi
801048f7:	8b 45 0c             	mov    0xc(%ebp),%eax
801048fa:	fc                   	cld    
801048fb:	f3 aa                	rep stos %al,%es:(%edi)
    c &= 0xFF;
    stosl(dst, (c<<24)|(c<<16)|(c<<8)|c, n/4);
  } else
    stosb(dst, c, n);
  return dst;
}
801048fd:	5b                   	pop    %ebx
801048fe:	89 d0                	mov    %edx,%eax
80104900:	5f                   	pop    %edi
80104901:	5d                   	pop    %ebp
80104902:	c3                   	ret    
80104903:	90                   	nop
80104904:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    c &= 0xFF;
80104908:	0f b6 7d 0c          	movzbl 0xc(%ebp),%edi
    stosl(dst, (c<<24)|(c<<16)|(c<<8)|c, n/4);
8010490c:	c1 e9 02             	shr    $0x2,%ecx
8010490f:	89 f8                	mov    %edi,%eax
80104911:	89 fb                	mov    %edi,%ebx
80104913:	c1 e0 18             	shl    $0x18,%eax
80104916:	c1 e3 10             	shl    $0x10,%ebx
80104919:	09 d8                	or     %ebx,%eax
8010491b:	09 f8                	or     %edi,%eax
8010491d:	c1 e7 08             	shl    $0x8,%edi
80104920:	09 f8                	or     %edi,%eax
  asm volatile("cld; rep stosl" :
80104922:	89 d7                	mov    %edx,%edi
80104924:	fc                   	cld    
80104925:	f3 ab                	rep stos %eax,%es:(%edi)
}
80104927:	5b                   	pop    %ebx
80104928:	89 d0                	mov    %edx,%eax
8010492a:	5f                   	pop    %edi
8010492b:	5d                   	pop    %ebp
8010492c:	c3                   	ret    
8010492d:	8d 76 00             	lea    0x0(%esi),%esi

80104930 <memcmp>:

int
memcmp(const void *v1, const void *v2, uint n)
{
80104930:	55                   	push   %ebp
80104931:	89 e5                	mov    %esp,%ebp
80104933:	57                   	push   %edi
80104934:	56                   	push   %esi
80104935:	53                   	push   %ebx
80104936:	8b 5d 10             	mov    0x10(%ebp),%ebx
80104939:	8b 75 08             	mov    0x8(%ebp),%esi
8010493c:	8b 7d 0c             	mov    0xc(%ebp),%edi
  const uchar *s1, *s2;

  s1 = v1;
  s2 = v2;
  while(n-- > 0){
8010493f:	85 db                	test   %ebx,%ebx
80104941:	74 29                	je     8010496c <memcmp+0x3c>
    if(*s1 != *s2)
80104943:	0f b6 16             	movzbl (%esi),%edx
80104946:	0f b6 0f             	movzbl (%edi),%ecx
80104949:	38 d1                	cmp    %dl,%cl
8010494b:	75 2b                	jne    80104978 <memcmp+0x48>
8010494d:	b8 01 00 00 00       	mov    $0x1,%eax
80104952:	eb 14                	jmp    80104968 <memcmp+0x38>
80104954:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80104958:	0f b6 14 06          	movzbl (%esi,%eax,1),%edx
8010495c:	83 c0 01             	add    $0x1,%eax
8010495f:	0f b6 4c 07 ff       	movzbl -0x1(%edi,%eax,1),%ecx
80104964:	38 ca                	cmp    %cl,%dl
80104966:	75 10                	jne    80104978 <memcmp+0x48>
  while(n-- > 0){
80104968:	39 d8                	cmp    %ebx,%eax
8010496a:	75 ec                	jne    80104958 <memcmp+0x28>
      return *s1 - *s2;
    s1++, s2++;
  }

  return 0;
}
8010496c:	5b                   	pop    %ebx
  return 0;
8010496d:	31 c0                	xor    %eax,%eax
}
8010496f:	5e                   	pop    %esi
80104970:	5f                   	pop    %edi
80104971:	5d                   	pop    %ebp
80104972:	c3                   	ret    
80104973:	90                   	nop
80104974:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
      return *s1 - *s2;
80104978:	0f b6 c2             	movzbl %dl,%eax
}
8010497b:	5b                   	pop    %ebx
      return *s1 - *s2;
8010497c:	29 c8                	sub    %ecx,%eax
}
8010497e:	5e                   	pop    %esi
8010497f:	5f                   	pop    %edi
80104980:	5d                   	pop    %ebp
80104981:	c3                   	ret    
80104982:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80104989:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80104990 <memmove>:

void*
memmove(void *dst, const void *src, uint n)
{
80104990:	55                   	push   %ebp
80104991:	89 e5                	mov    %esp,%ebp
80104993:	56                   	push   %esi
80104994:	53                   	push   %ebx
80104995:	8b 45 08             	mov    0x8(%ebp),%eax
80104998:	8b 5d 0c             	mov    0xc(%ebp),%ebx
8010499b:	8b 75 10             	mov    0x10(%ebp),%esi
  const char *s;
  char *d;

  s = src;
  d = dst;
  if(s < d && s + n > d){
8010499e:	39 c3                	cmp    %eax,%ebx
801049a0:	73 26                	jae    801049c8 <memmove+0x38>
801049a2:	8d 0c 33             	lea    (%ebx,%esi,1),%ecx
801049a5:	39 c8                	cmp    %ecx,%eax
801049a7:	73 1f                	jae    801049c8 <memmove+0x38>
    s += n;
    d += n;
    while(n-- > 0)
801049a9:	85 f6                	test   %esi,%esi
801049ab:	8d 56 ff             	lea    -0x1(%esi),%edx
801049ae:	74 0f                	je     801049bf <memmove+0x2f>
      *--d = *--s;
801049b0:	0f b6 0c 13          	movzbl (%ebx,%edx,1),%ecx
801049b4:	88 0c 10             	mov    %cl,(%eax,%edx,1)
    while(n-- > 0)
801049b7:	83 ea 01             	sub    $0x1,%edx
801049ba:	83 fa ff             	cmp    $0xffffffff,%edx
801049bd:	75 f1                	jne    801049b0 <memmove+0x20>
  } else
    while(n-- > 0)
      *d++ = *s++;

  return dst;
}
801049bf:	5b                   	pop    %ebx
801049c0:	5e                   	pop    %esi
801049c1:	5d                   	pop    %ebp
801049c2:	c3                   	ret    
801049c3:	90                   	nop
801049c4:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    while(n-- > 0)
801049c8:	31 d2                	xor    %edx,%edx
801049ca:	85 f6                	test   %esi,%esi
801049cc:	74 f1                	je     801049bf <memmove+0x2f>
801049ce:	66 90                	xchg   %ax,%ax
      *d++ = *s++;
801049d0:	0f b6 0c 13          	movzbl (%ebx,%edx,1),%ecx
801049d4:	88 0c 10             	mov    %cl,(%eax,%edx,1)
801049d7:	83 c2 01             	add    $0x1,%edx
    while(n-- > 0)
801049da:	39 d6                	cmp    %edx,%esi
801049dc:	75 f2                	jne    801049d0 <memmove+0x40>
}
801049de:	5b                   	pop    %ebx
801049df:	5e                   	pop    %esi
801049e0:	5d                   	pop    %ebp
801049e1:	c3                   	ret    
801049e2:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801049e9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

801049f0 <memcpy>:

// memcpy exists to placate GCC.  Use memmove.
void*
memcpy(void *dst, const void *src, uint n)
{
801049f0:	55                   	push   %ebp
801049f1:	89 e5                	mov    %esp,%ebp
  return memmove(dst, src, n);
}
801049f3:	5d                   	pop    %ebp
  return memmove(dst, src, n);
801049f4:	eb 9a                	jmp    80104990 <memmove>
801049f6:	8d 76 00             	lea    0x0(%esi),%esi
801049f9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80104a00 <strncmp>:

int
strncmp(const char *p, const char *q, uint n)
{
80104a00:	55                   	push   %ebp
80104a01:	89 e5                	mov    %esp,%ebp
80104a03:	57                   	push   %edi
80104a04:	56                   	push   %esi
80104a05:	8b 7d 10             	mov    0x10(%ebp),%edi
80104a08:	53                   	push   %ebx
80104a09:	8b 4d 08             	mov    0x8(%ebp),%ecx
80104a0c:	8b 75 0c             	mov    0xc(%ebp),%esi
  while(n > 0 && *p && *p == *q)
80104a0f:	85 ff                	test   %edi,%edi
80104a11:	74 2f                	je     80104a42 <strncmp+0x42>
80104a13:	0f b6 01             	movzbl (%ecx),%eax
80104a16:	0f b6 1e             	movzbl (%esi),%ebx
80104a19:	84 c0                	test   %al,%al
80104a1b:	74 37                	je     80104a54 <strncmp+0x54>
80104a1d:	38 c3                	cmp    %al,%bl
80104a1f:	75 33                	jne    80104a54 <strncmp+0x54>
80104a21:	01 f7                	add    %esi,%edi
80104a23:	eb 13                	jmp    80104a38 <strncmp+0x38>
80104a25:	8d 76 00             	lea    0x0(%esi),%esi
80104a28:	0f b6 01             	movzbl (%ecx),%eax
80104a2b:	84 c0                	test   %al,%al
80104a2d:	74 21                	je     80104a50 <strncmp+0x50>
80104a2f:	0f b6 1a             	movzbl (%edx),%ebx
80104a32:	89 d6                	mov    %edx,%esi
80104a34:	38 d8                	cmp    %bl,%al
80104a36:	75 1c                	jne    80104a54 <strncmp+0x54>
    n--, p++, q++;
80104a38:	8d 56 01             	lea    0x1(%esi),%edx
80104a3b:	83 c1 01             	add    $0x1,%ecx
  while(n > 0 && *p && *p == *q)
80104a3e:	39 fa                	cmp    %edi,%edx
80104a40:	75 e6                	jne    80104a28 <strncmp+0x28>
  if(n == 0)
    return 0;
  return (uchar)*p - (uchar)*q;
}
80104a42:	5b                   	pop    %ebx
    return 0;
80104a43:	31 c0                	xor    %eax,%eax
}
80104a45:	5e                   	pop    %esi
80104a46:	5f                   	pop    %edi
80104a47:	5d                   	pop    %ebp
80104a48:	c3                   	ret    
80104a49:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80104a50:	0f b6 5e 01          	movzbl 0x1(%esi),%ebx
  return (uchar)*p - (uchar)*q;
80104a54:	29 d8                	sub    %ebx,%eax
}
80104a56:	5b                   	pop    %ebx
80104a57:	5e                   	pop    %esi
80104a58:	5f                   	pop    %edi
80104a59:	5d                   	pop    %ebp
80104a5a:	c3                   	ret    
80104a5b:	90                   	nop
80104a5c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

80104a60 <strncpy>:

char*
strncpy(char *s, const char *t, int n)
{
80104a60:	55                   	push   %ebp
80104a61:	89 e5                	mov    %esp,%ebp
80104a63:	56                   	push   %esi
80104a64:	53                   	push   %ebx
80104a65:	8b 45 08             	mov    0x8(%ebp),%eax
80104a68:	8b 5d 0c             	mov    0xc(%ebp),%ebx
80104a6b:	8b 4d 10             	mov    0x10(%ebp),%ecx
  char *os;

  os = s;
  while(n-- > 0 && (*s++ = *t++) != 0)
80104a6e:	89 c2                	mov    %eax,%edx
80104a70:	eb 19                	jmp    80104a8b <strncpy+0x2b>
80104a72:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
80104a78:	83 c3 01             	add    $0x1,%ebx
80104a7b:	0f b6 4b ff          	movzbl -0x1(%ebx),%ecx
80104a7f:	83 c2 01             	add    $0x1,%edx
80104a82:	84 c9                	test   %cl,%cl
80104a84:	88 4a ff             	mov    %cl,-0x1(%edx)
80104a87:	74 09                	je     80104a92 <strncpy+0x32>
80104a89:	89 f1                	mov    %esi,%ecx
80104a8b:	85 c9                	test   %ecx,%ecx
80104a8d:	8d 71 ff             	lea    -0x1(%ecx),%esi
80104a90:	7f e6                	jg     80104a78 <strncpy+0x18>
    ;
  while(n-- > 0)
80104a92:	31 c9                	xor    %ecx,%ecx
80104a94:	85 f6                	test   %esi,%esi
80104a96:	7e 17                	jle    80104aaf <strncpy+0x4f>
80104a98:	90                   	nop
80104a99:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    *s++ = 0;
80104aa0:	c6 04 0a 00          	movb   $0x0,(%edx,%ecx,1)
80104aa4:	89 f3                	mov    %esi,%ebx
80104aa6:	83 c1 01             	add    $0x1,%ecx
80104aa9:	29 cb                	sub    %ecx,%ebx
  while(n-- > 0)
80104aab:	85 db                	test   %ebx,%ebx
80104aad:	7f f1                	jg     80104aa0 <strncpy+0x40>
  return os;
}
80104aaf:	5b                   	pop    %ebx
80104ab0:	5e                   	pop    %esi
80104ab1:	5d                   	pop    %ebp
80104ab2:	c3                   	ret    
80104ab3:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
80104ab9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80104ac0 <safestrcpy>:

// Like strncpy but guaranteed to NUL-terminate.
char*
safestrcpy(char *s, const char *t, int n)
{
80104ac0:	55                   	push   %ebp
80104ac1:	89 e5                	mov    %esp,%ebp
80104ac3:	56                   	push   %esi
80104ac4:	53                   	push   %ebx
80104ac5:	8b 4d 10             	mov    0x10(%ebp),%ecx
80104ac8:	8b 45 08             	mov    0x8(%ebp),%eax
80104acb:	8b 55 0c             	mov    0xc(%ebp),%edx
  char *os;

  os = s;
  if(n <= 0)
80104ace:	85 c9                	test   %ecx,%ecx
80104ad0:	7e 26                	jle    80104af8 <safestrcpy+0x38>
80104ad2:	8d 74 0a ff          	lea    -0x1(%edx,%ecx,1),%esi
80104ad6:	89 c1                	mov    %eax,%ecx
80104ad8:	eb 17                	jmp    80104af1 <safestrcpy+0x31>
80104ada:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    return os;
  while(--n > 0 && (*s++ = *t++) != 0)
80104ae0:	83 c2 01             	add    $0x1,%edx
80104ae3:	0f b6 5a ff          	movzbl -0x1(%edx),%ebx
80104ae7:	83 c1 01             	add    $0x1,%ecx
80104aea:	84 db                	test   %bl,%bl
80104aec:	88 59 ff             	mov    %bl,-0x1(%ecx)
80104aef:	74 04                	je     80104af5 <safestrcpy+0x35>
80104af1:	39 f2                	cmp    %esi,%edx
80104af3:	75 eb                	jne    80104ae0 <safestrcpy+0x20>
    ;
  *s = 0;
80104af5:	c6 01 00             	movb   $0x0,(%ecx)
  return os;
}
80104af8:	5b                   	pop    %ebx
80104af9:	5e                   	pop    %esi
80104afa:	5d                   	pop    %ebp
80104afb:	c3                   	ret    
80104afc:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

80104b00 <strlen>:

int
strlen(const char *s)
{
80104b00:	55                   	push   %ebp
  int n;

  for(n = 0; s[n]; n++)
80104b01:	31 c0                	xor    %eax,%eax
{
80104b03:	89 e5                	mov    %esp,%ebp
80104b05:	8b 55 08             	mov    0x8(%ebp),%edx
  for(n = 0; s[n]; n++)
80104b08:	80 3a 00             	cmpb   $0x0,(%edx)
80104b0b:	74 0c                	je     80104b19 <strlen+0x19>
80104b0d:	8d 76 00             	lea    0x0(%esi),%esi
80104b10:	83 c0 01             	add    $0x1,%eax
80104b13:	80 3c 02 00          	cmpb   $0x0,(%edx,%eax,1)
80104b17:	75 f7                	jne    80104b10 <strlen+0x10>
    ;
  return n;
}
80104b19:	5d                   	pop    %ebp
80104b1a:	c3                   	ret    

80104b1b <swtch>:
# a struct context, and save its address in *old.
# Switch stacks to new and pop previously-saved registers.

.globl swtch
swtch:
  movl 4(%esp), %eax
80104b1b:	8b 44 24 04          	mov    0x4(%esp),%eax
  movl 8(%esp), %edx
80104b1f:	8b 54 24 08          	mov    0x8(%esp),%edx

  # Save old callee-saved registers
  pushl %ebp
80104b23:	55                   	push   %ebp
  pushl %ebx
80104b24:	53                   	push   %ebx
  pushl %esi
80104b25:	56                   	push   %esi
  pushl %edi
80104b26:	57                   	push   %edi

  # Switch stacks
  movl %esp, (%eax)
80104b27:	89 20                	mov    %esp,(%eax)
  movl %edx, %esp
80104b29:	89 d4                	mov    %edx,%esp

  # Load new callee-saved registers
  popl %edi
80104b2b:	5f                   	pop    %edi
  popl %esi
80104b2c:	5e                   	pop    %esi
  popl %ebx
80104b2d:	5b                   	pop    %ebx
  popl %ebp
80104b2e:	5d                   	pop    %ebp
  ret
80104b2f:	c3                   	ret    

80104b30 <fetchint>:
// to a saved program counter, and then the first argument.

// Fetch the int at addr from the current process.
int
fetchint(uint addr, int *ip)
{
80104b30:	55                   	push   %ebp
80104b31:	89 e5                	mov    %esp,%ebp
80104b33:	53                   	push   %ebx
80104b34:	83 ec 04             	sub    $0x4,%esp
80104b37:	8b 5d 08             	mov    0x8(%ebp),%ebx
  struct proc *curproc = myproc();
80104b3a:	e8 f1 f0 ff ff       	call   80103c30 <myproc>

  if(addr >= curproc->sz || addr+4 > curproc->sz)
80104b3f:	8b 00                	mov    (%eax),%eax
80104b41:	39 d8                	cmp    %ebx,%eax
80104b43:	76 1b                	jbe    80104b60 <fetchint+0x30>
80104b45:	8d 53 04             	lea    0x4(%ebx),%edx
80104b48:	39 d0                	cmp    %edx,%eax
80104b4a:	72 14                	jb     80104b60 <fetchint+0x30>
    return -1;
  *ip = *(int*)(addr);
80104b4c:	8b 45 0c             	mov    0xc(%ebp),%eax
80104b4f:	8b 13                	mov    (%ebx),%edx
80104b51:	89 10                	mov    %edx,(%eax)
  return 0;
80104b53:	31 c0                	xor    %eax,%eax
}
80104b55:	83 c4 04             	add    $0x4,%esp
80104b58:	5b                   	pop    %ebx
80104b59:	5d                   	pop    %ebp
80104b5a:	c3                   	ret    
80104b5b:	90                   	nop
80104b5c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    return -1;
80104b60:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80104b65:	eb ee                	jmp    80104b55 <fetchint+0x25>
80104b67:	89 f6                	mov    %esi,%esi
80104b69:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80104b70 <fetchstr>:
// Fetch the nul-terminated string at addr from the current process.
// Doesn't actually copy the string - just sets *pp to point at it.
// Returns length of string, not including nul.
int
fetchstr(uint addr, char **pp)
{
80104b70:	55                   	push   %ebp
80104b71:	89 e5                	mov    %esp,%ebp
80104b73:	53                   	push   %ebx
80104b74:	83 ec 04             	sub    $0x4,%esp
80104b77:	8b 5d 08             	mov    0x8(%ebp),%ebx
  char *s, *ep;
  struct proc *curproc = myproc();
80104b7a:	e8 b1 f0 ff ff       	call   80103c30 <myproc>

  if(addr >= curproc->sz)
80104b7f:	39 18                	cmp    %ebx,(%eax)
80104b81:	76 29                	jbe    80104bac <fetchstr+0x3c>
    return -1;
  *pp = (char*)addr;
80104b83:	8b 4d 0c             	mov    0xc(%ebp),%ecx
80104b86:	89 da                	mov    %ebx,%edx
80104b88:	89 19                	mov    %ebx,(%ecx)
  ep = (char*)curproc->sz;
80104b8a:	8b 00                	mov    (%eax),%eax
  for(s = *pp; s < ep; s++){
80104b8c:	39 c3                	cmp    %eax,%ebx
80104b8e:	73 1c                	jae    80104bac <fetchstr+0x3c>
    if(*s == 0)
80104b90:	80 3b 00             	cmpb   $0x0,(%ebx)
80104b93:	75 10                	jne    80104ba5 <fetchstr+0x35>
80104b95:	eb 39                	jmp    80104bd0 <fetchstr+0x60>
80104b97:	89 f6                	mov    %esi,%esi
80104b99:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi
80104ba0:	80 3a 00             	cmpb   $0x0,(%edx)
80104ba3:	74 1b                	je     80104bc0 <fetchstr+0x50>
  for(s = *pp; s < ep; s++){
80104ba5:	83 c2 01             	add    $0x1,%edx
80104ba8:	39 d0                	cmp    %edx,%eax
80104baa:	77 f4                	ja     80104ba0 <fetchstr+0x30>
    return -1;
80104bac:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
      return s - *pp;
  }
  return -1;
}
80104bb1:	83 c4 04             	add    $0x4,%esp
80104bb4:	5b                   	pop    %ebx
80104bb5:	5d                   	pop    %ebp
80104bb6:	c3                   	ret    
80104bb7:	89 f6                	mov    %esi,%esi
80104bb9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi
80104bc0:	83 c4 04             	add    $0x4,%esp
80104bc3:	89 d0                	mov    %edx,%eax
80104bc5:	29 d8                	sub    %ebx,%eax
80104bc7:	5b                   	pop    %ebx
80104bc8:	5d                   	pop    %ebp
80104bc9:	c3                   	ret    
80104bca:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    if(*s == 0)
80104bd0:	31 c0                	xor    %eax,%eax
      return s - *pp;
80104bd2:	eb dd                	jmp    80104bb1 <fetchstr+0x41>
80104bd4:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
80104bda:	8d bf 00 00 00 00    	lea    0x0(%edi),%edi

80104be0 <argint>:

// Fetch the nth 32-bit system call argument.
int
argint(int n, int *ip)
{
80104be0:	55                   	push   %ebp
80104be1:	89 e5                	mov    %esp,%ebp
80104be3:	56                   	push   %esi
80104be4:	53                   	push   %ebx
  return fetchint((myproc()->tf->esp) + 4 + 4*n, ip);
80104be5:	e8 46 f0 ff ff       	call   80103c30 <myproc>
80104bea:	8b 40 18             	mov    0x18(%eax),%eax
80104bed:	8b 55 08             	mov    0x8(%ebp),%edx
80104bf0:	8b 40 44             	mov    0x44(%eax),%eax
80104bf3:	8d 1c 90             	lea    (%eax,%edx,4),%ebx
  struct proc *curproc = myproc();
80104bf6:	e8 35 f0 ff ff       	call   80103c30 <myproc>
  if(addr >= curproc->sz || addr+4 > curproc->sz)
80104bfb:	8b 00                	mov    (%eax),%eax
  return fetchint((myproc()->tf->esp) + 4 + 4*n, ip);
80104bfd:	8d 73 04             	lea    0x4(%ebx),%esi
  if(addr >= curproc->sz || addr+4 > curproc->sz)
80104c00:	39 c6                	cmp    %eax,%esi
80104c02:	73 1c                	jae    80104c20 <argint+0x40>
80104c04:	8d 53 08             	lea    0x8(%ebx),%edx
80104c07:	39 d0                	cmp    %edx,%eax
80104c09:	72 15                	jb     80104c20 <argint+0x40>
  *ip = *(int*)(addr);
80104c0b:	8b 45 0c             	mov    0xc(%ebp),%eax
80104c0e:	8b 53 04             	mov    0x4(%ebx),%edx
80104c11:	89 10                	mov    %edx,(%eax)
  return 0;
80104c13:	31 c0                	xor    %eax,%eax
}
80104c15:	5b                   	pop    %ebx
80104c16:	5e                   	pop    %esi
80104c17:	5d                   	pop    %ebp
80104c18:	c3                   	ret    
80104c19:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    return -1;
80104c20:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
  return fetchint((myproc()->tf->esp) + 4 + 4*n, ip);
80104c25:	eb ee                	jmp    80104c15 <argint+0x35>
80104c27:	89 f6                	mov    %esi,%esi
80104c29:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80104c30 <argptr>:
// Fetch the nth word-sized system call argument as a pointer
// to a block of memory of size bytes.  Check that the pointer
// lies within the process address space.
int
argptr(int n, char **pp, int size)
{
80104c30:	55                   	push   %ebp
80104c31:	89 e5                	mov    %esp,%ebp
80104c33:	56                   	push   %esi
80104c34:	53                   	push   %ebx
80104c35:	83 ec 10             	sub    $0x10,%esp
80104c38:	8b 5d 10             	mov    0x10(%ebp),%ebx
  int i;
  struct proc *curproc = myproc();
80104c3b:	e8 f0 ef ff ff       	call   80103c30 <myproc>
80104c40:	89 c6                	mov    %eax,%esi
 
  if(argint(n, &i) < 0)
80104c42:	8d 45 f4             	lea    -0xc(%ebp),%eax
80104c45:	83 ec 08             	sub    $0x8,%esp
80104c48:	50                   	push   %eax
80104c49:	ff 75 08             	pushl  0x8(%ebp)
80104c4c:	e8 8f ff ff ff       	call   80104be0 <argint>
    return -1;
  if(size < 0 || (uint)i >= curproc->sz || (uint)i+size > curproc->sz)
80104c51:	83 c4 10             	add    $0x10,%esp
80104c54:	85 c0                	test   %eax,%eax
80104c56:	78 28                	js     80104c80 <argptr+0x50>
80104c58:	85 db                	test   %ebx,%ebx
80104c5a:	78 24                	js     80104c80 <argptr+0x50>
80104c5c:	8b 16                	mov    (%esi),%edx
80104c5e:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104c61:	39 c2                	cmp    %eax,%edx
80104c63:	76 1b                	jbe    80104c80 <argptr+0x50>
80104c65:	01 c3                	add    %eax,%ebx
80104c67:	39 da                	cmp    %ebx,%edx
80104c69:	72 15                	jb     80104c80 <argptr+0x50>
    return -1;
  *pp = (char*)i;
80104c6b:	8b 55 0c             	mov    0xc(%ebp),%edx
80104c6e:	89 02                	mov    %eax,(%edx)
  return 0;
80104c70:	31 c0                	xor    %eax,%eax
}
80104c72:	8d 65 f8             	lea    -0x8(%ebp),%esp
80104c75:	5b                   	pop    %ebx
80104c76:	5e                   	pop    %esi
80104c77:	5d                   	pop    %ebp
80104c78:	c3                   	ret    
80104c79:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    return -1;
80104c80:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80104c85:	eb eb                	jmp    80104c72 <argptr+0x42>
80104c87:	89 f6                	mov    %esi,%esi
80104c89:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80104c90 <argstr>:
// Check that the pointer is valid and the string is nul-terminated.
// (There is no shared writable memory, so the string can't change
// between this check and being used by the kernel.)
int
argstr(int n, char **pp)
{
80104c90:	55                   	push   %ebp
80104c91:	89 e5                	mov    %esp,%ebp
80104c93:	83 ec 20             	sub    $0x20,%esp
  int addr;
  if(argint(n, &addr) < 0)
80104c96:	8d 45 f4             	lea    -0xc(%ebp),%eax
80104c99:	50                   	push   %eax
80104c9a:	ff 75 08             	pushl  0x8(%ebp)
80104c9d:	e8 3e ff ff ff       	call   80104be0 <argint>
80104ca2:	83 c4 10             	add    $0x10,%esp
80104ca5:	85 c0                	test   %eax,%eax
80104ca7:	78 17                	js     80104cc0 <argstr+0x30>
    return -1;
  return fetchstr(addr, pp);
80104ca9:	83 ec 08             	sub    $0x8,%esp
80104cac:	ff 75 0c             	pushl  0xc(%ebp)
80104caf:	ff 75 f4             	pushl  -0xc(%ebp)
80104cb2:	e8 b9 fe ff ff       	call   80104b70 <fetchstr>
80104cb7:	83 c4 10             	add    $0x10,%esp
}
80104cba:	c9                   	leave  
80104cbb:	c3                   	ret    
80104cbc:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    return -1;
80104cc0:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80104cc5:	c9                   	leave  
80104cc6:	c3                   	ret    
80104cc7:	89 f6                	mov    %esi,%esi
80104cc9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80104cd0 <syscall>:
[SYS_close]   sys_close,
};

void
syscall(void)
{
80104cd0:	55                   	push   %ebp
80104cd1:	89 e5                	mov    %esp,%ebp
80104cd3:	53                   	push   %ebx
80104cd4:	83 ec 04             	sub    $0x4,%esp
  int num;
  struct proc *curproc = myproc();
80104cd7:	e8 54 ef ff ff       	call   80103c30 <myproc>
80104cdc:	89 c3                	mov    %eax,%ebx

  num = curproc->tf->eax;
80104cde:	8b 40 18             	mov    0x18(%eax),%eax
80104ce1:	8b 40 1c             	mov    0x1c(%eax),%eax
  if(num > 0 && num < NELEM(syscalls) && syscalls[num]) {
80104ce4:	8d 50 ff             	lea    -0x1(%eax),%edx
80104ce7:	83 fa 14             	cmp    $0x14,%edx
80104cea:	77 1c                	ja     80104d08 <syscall+0x38>
80104cec:	8b 14 85 a0 7e 10 80 	mov    -0x7fef8160(,%eax,4),%edx
80104cf3:	85 d2                	test   %edx,%edx
80104cf5:	74 11                	je     80104d08 <syscall+0x38>
    curproc->tf->eax = syscalls[num]();
80104cf7:	ff d2                	call   *%edx
80104cf9:	8b 53 18             	mov    0x18(%ebx),%edx
80104cfc:	89 42 1c             	mov    %eax,0x1c(%edx)
  } else {
    cprintf("%d %s: unknown sys call %d\n",
            curproc->pid, curproc->name, num);
    curproc->tf->eax = -1;
  }
}
80104cff:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80104d02:	c9                   	leave  
80104d03:	c3                   	ret    
80104d04:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    cprintf("%d %s: unknown sys call %d\n",
80104d08:	50                   	push   %eax
            curproc->pid, curproc->name, num);
80104d09:	8d 43 6c             	lea    0x6c(%ebx),%eax
    cprintf("%d %s: unknown sys call %d\n",
80104d0c:	50                   	push   %eax
80104d0d:	ff 73 10             	pushl  0x10(%ebx)
80104d10:	68 71 7e 10 80       	push   $0x80107e71
80104d15:	e8 46 b9 ff ff       	call   80100660 <cprintf>
    curproc->tf->eax = -1;
80104d1a:	8b 43 18             	mov    0x18(%ebx),%eax
80104d1d:	83 c4 10             	add    $0x10,%esp
80104d20:	c7 40 1c ff ff ff ff 	movl   $0xffffffff,0x1c(%eax)
}
80104d27:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80104d2a:	c9                   	leave  
80104d2b:	c3                   	ret    
80104d2c:	66 90                	xchg   %ax,%ax
80104d2e:	66 90                	xchg   %ax,%ax

80104d30 <argfd.constprop.0>:
#include "fcntl.h"

// Fetch the nth word-sized system call argument as a file descriptor
// and return both the descriptor and the corresponding struct file.
static int
argfd(int n, int *pfd, struct file **pf)
80104d30:	55                   	push   %ebp
80104d31:	89 e5                	mov    %esp,%ebp
80104d33:	56                   	push   %esi
80104d34:	53                   	push   %ebx
80104d35:	89 c3                	mov    %eax,%ebx
{
  int fd;
  struct file *f;

  if(argint(n, &fd) < 0)
80104d37:	8d 45 f4             	lea    -0xc(%ebp),%eax
argfd(int n, int *pfd, struct file **pf)
80104d3a:	89 d6                	mov    %edx,%esi
80104d3c:	83 ec 18             	sub    $0x18,%esp
  if(argint(n, &fd) < 0)
80104d3f:	50                   	push   %eax
80104d40:	6a 00                	push   $0x0
80104d42:	e8 99 fe ff ff       	call   80104be0 <argint>
80104d47:	83 c4 10             	add    $0x10,%esp
80104d4a:	85 c0                	test   %eax,%eax
80104d4c:	78 2a                	js     80104d78 <argfd.constprop.0+0x48>
    return -1;
  if(fd < 0 || fd >= NOFILE || (f=myproc()->ofile[fd]) == 0)
80104d4e:	83 7d f4 0f          	cmpl   $0xf,-0xc(%ebp)
80104d52:	77 24                	ja     80104d78 <argfd.constprop.0+0x48>
80104d54:	e8 d7 ee ff ff       	call   80103c30 <myproc>
80104d59:	8b 55 f4             	mov    -0xc(%ebp),%edx
80104d5c:	8b 44 90 28          	mov    0x28(%eax,%edx,4),%eax
80104d60:	85 c0                	test   %eax,%eax
80104d62:	74 14                	je     80104d78 <argfd.constprop.0+0x48>
    return -1;
  if(pfd)
80104d64:	85 db                	test   %ebx,%ebx
80104d66:	74 02                	je     80104d6a <argfd.constprop.0+0x3a>
    *pfd = fd;
80104d68:	89 13                	mov    %edx,(%ebx)
  if(pf)
    *pf = f;
80104d6a:	89 06                	mov    %eax,(%esi)
  return 0;
80104d6c:	31 c0                	xor    %eax,%eax
}
80104d6e:	8d 65 f8             	lea    -0x8(%ebp),%esp
80104d71:	5b                   	pop    %ebx
80104d72:	5e                   	pop    %esi
80104d73:	5d                   	pop    %ebp
80104d74:	c3                   	ret    
80104d75:	8d 76 00             	lea    0x0(%esi),%esi
    return -1;
80104d78:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80104d7d:	eb ef                	jmp    80104d6e <argfd.constprop.0+0x3e>
80104d7f:	90                   	nop

80104d80 <sys_dup>:
  return -1;
}

int
sys_dup(void)
{
80104d80:	55                   	push   %ebp
  struct file *f;
  int fd;

  if(argfd(0, 0, &f) < 0)
80104d81:	31 c0                	xor    %eax,%eax
{
80104d83:	89 e5                	mov    %esp,%ebp
80104d85:	56                   	push   %esi
80104d86:	53                   	push   %ebx
  if(argfd(0, 0, &f) < 0)
80104d87:	8d 55 f4             	lea    -0xc(%ebp),%edx
{
80104d8a:	83 ec 10             	sub    $0x10,%esp
  if(argfd(0, 0, &f) < 0)
80104d8d:	e8 9e ff ff ff       	call   80104d30 <argfd.constprop.0>
80104d92:	85 c0                	test   %eax,%eax
80104d94:	78 42                	js     80104dd8 <sys_dup+0x58>
    return -1;
  if((fd=fdalloc(f)) < 0)
80104d96:	8b 75 f4             	mov    -0xc(%ebp),%esi
  for(fd = 0; fd < NOFILE; fd++){
80104d99:	31 db                	xor    %ebx,%ebx
  struct proc *curproc = myproc();
80104d9b:	e8 90 ee ff ff       	call   80103c30 <myproc>
80104da0:	eb 0e                	jmp    80104db0 <sys_dup+0x30>
80104da2:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
  for(fd = 0; fd < NOFILE; fd++){
80104da8:	83 c3 01             	add    $0x1,%ebx
80104dab:	83 fb 10             	cmp    $0x10,%ebx
80104dae:	74 28                	je     80104dd8 <sys_dup+0x58>
    if(curproc->ofile[fd] == 0){
80104db0:	8b 54 98 28          	mov    0x28(%eax,%ebx,4),%edx
80104db4:	85 d2                	test   %edx,%edx
80104db6:	75 f0                	jne    80104da8 <sys_dup+0x28>
      curproc->ofile[fd] = f;
80104db8:	89 74 98 28          	mov    %esi,0x28(%eax,%ebx,4)
    return -1;
  filedup(f);
80104dbc:	83 ec 0c             	sub    $0xc,%esp
80104dbf:	ff 75 f4             	pushl  -0xc(%ebp)
80104dc2:	e8 29 c0 ff ff       	call   80100df0 <filedup>
  return fd;
80104dc7:	83 c4 10             	add    $0x10,%esp
}
80104dca:	8d 65 f8             	lea    -0x8(%ebp),%esp
80104dcd:	89 d8                	mov    %ebx,%eax
80104dcf:	5b                   	pop    %ebx
80104dd0:	5e                   	pop    %esi
80104dd1:	5d                   	pop    %ebp
80104dd2:	c3                   	ret    
80104dd3:	90                   	nop
80104dd4:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80104dd8:	8d 65 f8             	lea    -0x8(%ebp),%esp
    return -1;
80104ddb:	bb ff ff ff ff       	mov    $0xffffffff,%ebx
}
80104de0:	89 d8                	mov    %ebx,%eax
80104de2:	5b                   	pop    %ebx
80104de3:	5e                   	pop    %esi
80104de4:	5d                   	pop    %ebp
80104de5:	c3                   	ret    
80104de6:	8d 76 00             	lea    0x0(%esi),%esi
80104de9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80104df0 <sys_read>:

int
sys_read(void)
{
80104df0:	55                   	push   %ebp
  struct file *f;
  int n;
  char *p;

  if(argfd(0, 0, &f) < 0 || argint(2, &n) < 0 || argptr(1, &p, n) < 0)
80104df1:	31 c0                	xor    %eax,%eax
{
80104df3:	89 e5                	mov    %esp,%ebp
80104df5:	83 ec 18             	sub    $0x18,%esp
  if(argfd(0, 0, &f) < 0 || argint(2, &n) < 0 || argptr(1, &p, n) < 0)
80104df8:	8d 55 ec             	lea    -0x14(%ebp),%edx
80104dfb:	e8 30 ff ff ff       	call   80104d30 <argfd.constprop.0>
80104e00:	85 c0                	test   %eax,%eax
80104e02:	78 4c                	js     80104e50 <sys_read+0x60>
80104e04:	8d 45 f0             	lea    -0x10(%ebp),%eax
80104e07:	83 ec 08             	sub    $0x8,%esp
80104e0a:	50                   	push   %eax
80104e0b:	6a 02                	push   $0x2
80104e0d:	e8 ce fd ff ff       	call   80104be0 <argint>
80104e12:	83 c4 10             	add    $0x10,%esp
80104e15:	85 c0                	test   %eax,%eax
80104e17:	78 37                	js     80104e50 <sys_read+0x60>
80104e19:	8d 45 f4             	lea    -0xc(%ebp),%eax
80104e1c:	83 ec 04             	sub    $0x4,%esp
80104e1f:	ff 75 f0             	pushl  -0x10(%ebp)
80104e22:	50                   	push   %eax
80104e23:	6a 01                	push   $0x1
80104e25:	e8 06 fe ff ff       	call   80104c30 <argptr>
80104e2a:	83 c4 10             	add    $0x10,%esp
80104e2d:	85 c0                	test   %eax,%eax
80104e2f:	78 1f                	js     80104e50 <sys_read+0x60>
    return -1;
  return fileread(f, p, n);
80104e31:	83 ec 04             	sub    $0x4,%esp
80104e34:	ff 75 f0             	pushl  -0x10(%ebp)
80104e37:	ff 75 f4             	pushl  -0xc(%ebp)
80104e3a:	ff 75 ec             	pushl  -0x14(%ebp)
80104e3d:	e8 1e c1 ff ff       	call   80100f60 <fileread>
80104e42:	83 c4 10             	add    $0x10,%esp
}
80104e45:	c9                   	leave  
80104e46:	c3                   	ret    
80104e47:	89 f6                	mov    %esi,%esi
80104e49:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi
    return -1;
80104e50:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80104e55:	c9                   	leave  
80104e56:	c3                   	ret    
80104e57:	89 f6                	mov    %esi,%esi
80104e59:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80104e60 <sys_write>:

int
sys_write(void)
{
80104e60:	55                   	push   %ebp
  struct file *f;
  int n;
  char *p;

  if(argfd(0, 0, &f) < 0 || argint(2, &n) < 0 || argptr(1, &p, n) < 0)
80104e61:	31 c0                	xor    %eax,%eax
{
80104e63:	89 e5                	mov    %esp,%ebp
80104e65:	83 ec 18             	sub    $0x18,%esp
  if(argfd(0, 0, &f) < 0 || argint(2, &n) < 0 || argptr(1, &p, n) < 0)
80104e68:	8d 55 ec             	lea    -0x14(%ebp),%edx
80104e6b:	e8 c0 fe ff ff       	call   80104d30 <argfd.constprop.0>
80104e70:	85 c0                	test   %eax,%eax
80104e72:	78 4c                	js     80104ec0 <sys_write+0x60>
80104e74:	8d 45 f0             	lea    -0x10(%ebp),%eax
80104e77:	83 ec 08             	sub    $0x8,%esp
80104e7a:	50                   	push   %eax
80104e7b:	6a 02                	push   $0x2
80104e7d:	e8 5e fd ff ff       	call   80104be0 <argint>
80104e82:	83 c4 10             	add    $0x10,%esp
80104e85:	85 c0                	test   %eax,%eax
80104e87:	78 37                	js     80104ec0 <sys_write+0x60>
80104e89:	8d 45 f4             	lea    -0xc(%ebp),%eax
80104e8c:	83 ec 04             	sub    $0x4,%esp
80104e8f:	ff 75 f0             	pushl  -0x10(%ebp)
80104e92:	50                   	push   %eax
80104e93:	6a 01                	push   $0x1
80104e95:	e8 96 fd ff ff       	call   80104c30 <argptr>
80104e9a:	83 c4 10             	add    $0x10,%esp
80104e9d:	85 c0                	test   %eax,%eax
80104e9f:	78 1f                	js     80104ec0 <sys_write+0x60>
    return -1;
  return filewrite(f, p, n);
80104ea1:	83 ec 04             	sub    $0x4,%esp
80104ea4:	ff 75 f0             	pushl  -0x10(%ebp)
80104ea7:	ff 75 f4             	pushl  -0xc(%ebp)
80104eaa:	ff 75 ec             	pushl  -0x14(%ebp)
80104ead:	e8 3e c1 ff ff       	call   80100ff0 <filewrite>
80104eb2:	83 c4 10             	add    $0x10,%esp
}
80104eb5:	c9                   	leave  
80104eb6:	c3                   	ret    
80104eb7:	89 f6                	mov    %esi,%esi
80104eb9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi
    return -1;
80104ec0:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80104ec5:	c9                   	leave  
80104ec6:	c3                   	ret    
80104ec7:	89 f6                	mov    %esi,%esi
80104ec9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80104ed0 <sys_close>:

int
sys_close(void)
{
80104ed0:	55                   	push   %ebp
80104ed1:	89 e5                	mov    %esp,%ebp
80104ed3:	83 ec 18             	sub    $0x18,%esp
  int fd;
  struct file *f;

  if(argfd(0, &fd, &f) < 0)
80104ed6:	8d 55 f4             	lea    -0xc(%ebp),%edx
80104ed9:	8d 45 f0             	lea    -0x10(%ebp),%eax
80104edc:	e8 4f fe ff ff       	call   80104d30 <argfd.constprop.0>
80104ee1:	85 c0                	test   %eax,%eax
80104ee3:	78 2b                	js     80104f10 <sys_close+0x40>
    return -1;
  myproc()->ofile[fd] = 0;
80104ee5:	e8 46 ed ff ff       	call   80103c30 <myproc>
80104eea:	8b 55 f0             	mov    -0x10(%ebp),%edx
  fileclose(f);
80104eed:	83 ec 0c             	sub    $0xc,%esp
  myproc()->ofile[fd] = 0;
80104ef0:	c7 44 90 28 00 00 00 	movl   $0x0,0x28(%eax,%edx,4)
80104ef7:	00 
  fileclose(f);
80104ef8:	ff 75 f4             	pushl  -0xc(%ebp)
80104efb:	e8 40 bf ff ff       	call   80100e40 <fileclose>
  return 0;
80104f00:	83 c4 10             	add    $0x10,%esp
80104f03:	31 c0                	xor    %eax,%eax
}
80104f05:	c9                   	leave  
80104f06:	c3                   	ret    
80104f07:	89 f6                	mov    %esi,%esi
80104f09:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi
    return -1;
80104f10:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80104f15:	c9                   	leave  
80104f16:	c3                   	ret    
80104f17:	89 f6                	mov    %esi,%esi
80104f19:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80104f20 <sys_fstat>:

int
sys_fstat(void)
{
80104f20:	55                   	push   %ebp
  struct file *f;
  struct stat *st;

  if(argfd(0, 0, &f) < 0 || argptr(1, (void*)&st, sizeof(*st)) < 0)
80104f21:	31 c0                	xor    %eax,%eax
{
80104f23:	89 e5                	mov    %esp,%ebp
80104f25:	83 ec 18             	sub    $0x18,%esp
  if(argfd(0, 0, &f) < 0 || argptr(1, (void*)&st, sizeof(*st)) < 0)
80104f28:	8d 55 f0             	lea    -0x10(%ebp),%edx
80104f2b:	e8 00 fe ff ff       	call   80104d30 <argfd.constprop.0>
80104f30:	85 c0                	test   %eax,%eax
80104f32:	78 2c                	js     80104f60 <sys_fstat+0x40>
80104f34:	8d 45 f4             	lea    -0xc(%ebp),%eax
80104f37:	83 ec 04             	sub    $0x4,%esp
80104f3a:	6a 14                	push   $0x14
80104f3c:	50                   	push   %eax
80104f3d:	6a 01                	push   $0x1
80104f3f:	e8 ec fc ff ff       	call   80104c30 <argptr>
80104f44:	83 c4 10             	add    $0x10,%esp
80104f47:	85 c0                	test   %eax,%eax
80104f49:	78 15                	js     80104f60 <sys_fstat+0x40>
    return -1;
  return filestat(f, st);
80104f4b:	83 ec 08             	sub    $0x8,%esp
80104f4e:	ff 75 f4             	pushl  -0xc(%ebp)
80104f51:	ff 75 f0             	pushl  -0x10(%ebp)
80104f54:	e8 b7 bf ff ff       	call   80100f10 <filestat>
80104f59:	83 c4 10             	add    $0x10,%esp
}
80104f5c:	c9                   	leave  
80104f5d:	c3                   	ret    
80104f5e:	66 90                	xchg   %ax,%ax
    return -1;
80104f60:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80104f65:	c9                   	leave  
80104f66:	c3                   	ret    
80104f67:	89 f6                	mov    %esi,%esi
80104f69:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80104f70 <sys_link>:

// Create the path new as a link to the same inode as old.
int
sys_link(void)
{
80104f70:	55                   	push   %ebp
80104f71:	89 e5                	mov    %esp,%ebp
80104f73:	57                   	push   %edi
80104f74:	56                   	push   %esi
80104f75:	53                   	push   %ebx
  char name[DIRSIZ], *new, *old;
  struct inode *dp, *ip;

  if(argstr(0, &old) < 0 || argstr(1, &new) < 0)
80104f76:	8d 45 d4             	lea    -0x2c(%ebp),%eax
{
80104f79:	83 ec 34             	sub    $0x34,%esp
  if(argstr(0, &old) < 0 || argstr(1, &new) < 0)
80104f7c:	50                   	push   %eax
80104f7d:	6a 00                	push   $0x0
80104f7f:	e8 0c fd ff ff       	call   80104c90 <argstr>
80104f84:	83 c4 10             	add    $0x10,%esp
80104f87:	85 c0                	test   %eax,%eax
80104f89:	0f 88 fb 00 00 00    	js     8010508a <sys_link+0x11a>
80104f8f:	8d 45 d0             	lea    -0x30(%ebp),%eax
80104f92:	83 ec 08             	sub    $0x8,%esp
80104f95:	50                   	push   %eax
80104f96:	6a 01                	push   $0x1
80104f98:	e8 f3 fc ff ff       	call   80104c90 <argstr>
80104f9d:	83 c4 10             	add    $0x10,%esp
80104fa0:	85 c0                	test   %eax,%eax
80104fa2:	0f 88 e2 00 00 00    	js     8010508a <sys_link+0x11a>
    return -1;

  begin_op();
80104fa8:	e8 f3 df ff ff       	call   80102fa0 <begin_op>
  if((ip = namei(old)) == 0){
80104fad:	83 ec 0c             	sub    $0xc,%esp
80104fb0:	ff 75 d4             	pushl  -0x2c(%ebp)
80104fb3:	e8 28 cf ff ff       	call   80101ee0 <namei>
80104fb8:	83 c4 10             	add    $0x10,%esp
80104fbb:	85 c0                	test   %eax,%eax
80104fbd:	89 c3                	mov    %eax,%ebx
80104fbf:	0f 84 ea 00 00 00    	je     801050af <sys_link+0x13f>
    end_op();
    return -1;
  }

  ilock(ip);
80104fc5:	83 ec 0c             	sub    $0xc,%esp
80104fc8:	50                   	push   %eax
80104fc9:	e8 b2 c6 ff ff       	call   80101680 <ilock>
  if(ip->type == T_DIR){
80104fce:	83 c4 10             	add    $0x10,%esp
80104fd1:	66 83 7b 50 01       	cmpw   $0x1,0x50(%ebx)
80104fd6:	0f 84 bb 00 00 00    	je     80105097 <sys_link+0x127>
    iunlockput(ip);
    end_op();
    return -1;
  }

  ip->nlink++;
80104fdc:	66 83 43 56 01       	addw   $0x1,0x56(%ebx)
  iupdate(ip);
80104fe1:	83 ec 0c             	sub    $0xc,%esp
  iunlock(ip);

  if((dp = nameiparent(new, name)) == 0)
80104fe4:	8d 7d da             	lea    -0x26(%ebp),%edi
  iupdate(ip);
80104fe7:	53                   	push   %ebx
80104fe8:	e8 e3 c5 ff ff       	call   801015d0 <iupdate>
  iunlock(ip);
80104fed:	89 1c 24             	mov    %ebx,(%esp)
80104ff0:	e8 6b c7 ff ff       	call   80101760 <iunlock>
  if((dp = nameiparent(new, name)) == 0)
80104ff5:	58                   	pop    %eax
80104ff6:	5a                   	pop    %edx
80104ff7:	57                   	push   %edi
80104ff8:	ff 75 d0             	pushl  -0x30(%ebp)
80104ffb:	e8 00 cf ff ff       	call   80101f00 <nameiparent>
80105000:	83 c4 10             	add    $0x10,%esp
80105003:	85 c0                	test   %eax,%eax
80105005:	89 c6                	mov    %eax,%esi
80105007:	74 5b                	je     80105064 <sys_link+0xf4>
    goto bad;
  ilock(dp);
80105009:	83 ec 0c             	sub    $0xc,%esp
8010500c:	50                   	push   %eax
8010500d:	e8 6e c6 ff ff       	call   80101680 <ilock>
  if(dp->dev != ip->dev || dirlink(dp, name, ip->inum) < 0){
80105012:	83 c4 10             	add    $0x10,%esp
80105015:	8b 03                	mov    (%ebx),%eax
80105017:	39 06                	cmp    %eax,(%esi)
80105019:	75 3d                	jne    80105058 <sys_link+0xe8>
8010501b:	83 ec 04             	sub    $0x4,%esp
8010501e:	ff 73 04             	pushl  0x4(%ebx)
80105021:	57                   	push   %edi
80105022:	56                   	push   %esi
80105023:	e8 f8 cd ff ff       	call   80101e20 <dirlink>
80105028:	83 c4 10             	add    $0x10,%esp
8010502b:	85 c0                	test   %eax,%eax
8010502d:	78 29                	js     80105058 <sys_link+0xe8>
    iunlockput(dp);
    goto bad;
  }
  iunlockput(dp);
8010502f:	83 ec 0c             	sub    $0xc,%esp
80105032:	56                   	push   %esi
80105033:	e8 d8 c8 ff ff       	call   80101910 <iunlockput>
  iput(ip);
80105038:	89 1c 24             	mov    %ebx,(%esp)
8010503b:	e8 70 c7 ff ff       	call   801017b0 <iput>

  end_op();
80105040:	e8 cb df ff ff       	call   80103010 <end_op>

  return 0;
80105045:	83 c4 10             	add    $0x10,%esp
80105048:	31 c0                	xor    %eax,%eax
  ip->nlink--;
  iupdate(ip);
  iunlockput(ip);
  end_op();
  return -1;
}
8010504a:	8d 65 f4             	lea    -0xc(%ebp),%esp
8010504d:	5b                   	pop    %ebx
8010504e:	5e                   	pop    %esi
8010504f:	5f                   	pop    %edi
80105050:	5d                   	pop    %ebp
80105051:	c3                   	ret    
80105052:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    iunlockput(dp);
80105058:	83 ec 0c             	sub    $0xc,%esp
8010505b:	56                   	push   %esi
8010505c:	e8 af c8 ff ff       	call   80101910 <iunlockput>
    goto bad;
80105061:	83 c4 10             	add    $0x10,%esp
  ilock(ip);
80105064:	83 ec 0c             	sub    $0xc,%esp
80105067:	53                   	push   %ebx
80105068:	e8 13 c6 ff ff       	call   80101680 <ilock>
  ip->nlink--;
8010506d:	66 83 6b 56 01       	subw   $0x1,0x56(%ebx)
  iupdate(ip);
80105072:	89 1c 24             	mov    %ebx,(%esp)
80105075:	e8 56 c5 ff ff       	call   801015d0 <iupdate>
  iunlockput(ip);
8010507a:	89 1c 24             	mov    %ebx,(%esp)
8010507d:	e8 8e c8 ff ff       	call   80101910 <iunlockput>
  end_op();
80105082:	e8 89 df ff ff       	call   80103010 <end_op>
  return -1;
80105087:	83 c4 10             	add    $0x10,%esp
}
8010508a:	8d 65 f4             	lea    -0xc(%ebp),%esp
  return -1;
8010508d:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80105092:	5b                   	pop    %ebx
80105093:	5e                   	pop    %esi
80105094:	5f                   	pop    %edi
80105095:	5d                   	pop    %ebp
80105096:	c3                   	ret    
    iunlockput(ip);
80105097:	83 ec 0c             	sub    $0xc,%esp
8010509a:	53                   	push   %ebx
8010509b:	e8 70 c8 ff ff       	call   80101910 <iunlockput>
    end_op();
801050a0:	e8 6b df ff ff       	call   80103010 <end_op>
    return -1;
801050a5:	83 c4 10             	add    $0x10,%esp
801050a8:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801050ad:	eb 9b                	jmp    8010504a <sys_link+0xda>
    end_op();
801050af:	e8 5c df ff ff       	call   80103010 <end_op>
    return -1;
801050b4:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801050b9:	eb 8f                	jmp    8010504a <sys_link+0xda>
801050bb:	90                   	nop
801050bc:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

801050c0 <isdirempty>:

// Is the directory dp empty except for "." and ".." ?
int
isdirempty(struct inode *dp)
{
801050c0:	55                   	push   %ebp
801050c1:	89 e5                	mov    %esp,%ebp
801050c3:	57                   	push   %edi
801050c4:	56                   	push   %esi
801050c5:	53                   	push   %ebx
801050c6:	83 ec 1c             	sub    $0x1c,%esp
801050c9:	8b 75 08             	mov    0x8(%ebp),%esi
  int off;
  struct dirent de;

  for(off=2*sizeof(de); off<dp->size; off+=sizeof(de)){
801050cc:	83 7e 58 20          	cmpl   $0x20,0x58(%esi)
801050d0:	76 3e                	jbe    80105110 <isdirempty+0x50>
801050d2:	bb 20 00 00 00       	mov    $0x20,%ebx
801050d7:	8d 7d d8             	lea    -0x28(%ebp),%edi
801050da:	eb 0c                	jmp    801050e8 <isdirempty+0x28>
801050dc:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
801050e0:	83 c3 10             	add    $0x10,%ebx
801050e3:	3b 5e 58             	cmp    0x58(%esi),%ebx
801050e6:	73 28                	jae    80105110 <isdirempty+0x50>
    if(readi(dp, (char*)&de, off, sizeof(de)) != sizeof(de))
801050e8:	6a 10                	push   $0x10
801050ea:	53                   	push   %ebx
801050eb:	57                   	push   %edi
801050ec:	56                   	push   %esi
801050ed:	e8 6e c8 ff ff       	call   80101960 <readi>
801050f2:	83 c4 10             	add    $0x10,%esp
801050f5:	83 f8 10             	cmp    $0x10,%eax
801050f8:	75 23                	jne    8010511d <isdirempty+0x5d>
      panic("isdirempty: readi");
    if(de.inum != 0)
801050fa:	66 83 7d d8 00       	cmpw   $0x0,-0x28(%ebp)
801050ff:	74 df                	je     801050e0 <isdirempty+0x20>
      return 0;
  }
  return 1;
}
80105101:	8d 65 f4             	lea    -0xc(%ebp),%esp
      return 0;
80105104:	31 c0                	xor    %eax,%eax
}
80105106:	5b                   	pop    %ebx
80105107:	5e                   	pop    %esi
80105108:	5f                   	pop    %edi
80105109:	5d                   	pop    %ebp
8010510a:	c3                   	ret    
8010510b:	90                   	nop
8010510c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80105110:	8d 65 f4             	lea    -0xc(%ebp),%esp
  return 1;
80105113:	b8 01 00 00 00       	mov    $0x1,%eax
}
80105118:	5b                   	pop    %ebx
80105119:	5e                   	pop    %esi
8010511a:	5f                   	pop    %edi
8010511b:	5d                   	pop    %ebp
8010511c:	c3                   	ret    
      panic("isdirempty: readi");
8010511d:	83 ec 0c             	sub    $0xc,%esp
80105120:	68 f8 7e 10 80       	push   $0x80107ef8
80105125:	e8 66 b2 ff ff       	call   80100390 <panic>
8010512a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi

80105130 <sys_unlink>:

//PAGEBREAK!
int
sys_unlink(void)
{
80105130:	55                   	push   %ebp
80105131:	89 e5                	mov    %esp,%ebp
80105133:	57                   	push   %edi
80105134:	56                   	push   %esi
80105135:	53                   	push   %ebx
  struct inode *ip, *dp;
  struct dirent de;
  char name[DIRSIZ], *path;
  uint off;

  if(argstr(0, &path) < 0)
80105136:	8d 45 c0             	lea    -0x40(%ebp),%eax
{
80105139:	83 ec 44             	sub    $0x44,%esp
  if(argstr(0, &path) < 0)
8010513c:	50                   	push   %eax
8010513d:	6a 00                	push   $0x0
8010513f:	e8 4c fb ff ff       	call   80104c90 <argstr>
80105144:	83 c4 10             	add    $0x10,%esp
80105147:	85 c0                	test   %eax,%eax
80105149:	0f 88 51 01 00 00    	js     801052a0 <sys_unlink+0x170>
    return -1;

  begin_op();
  if((dp = nameiparent(path, name)) == 0){
8010514f:	8d 5d ca             	lea    -0x36(%ebp),%ebx
  begin_op();
80105152:	e8 49 de ff ff       	call   80102fa0 <begin_op>
  if((dp = nameiparent(path, name)) == 0){
80105157:	83 ec 08             	sub    $0x8,%esp
8010515a:	53                   	push   %ebx
8010515b:	ff 75 c0             	pushl  -0x40(%ebp)
8010515e:	e8 9d cd ff ff       	call   80101f00 <nameiparent>
80105163:	83 c4 10             	add    $0x10,%esp
80105166:	85 c0                	test   %eax,%eax
80105168:	89 c6                	mov    %eax,%esi
8010516a:	0f 84 37 01 00 00    	je     801052a7 <sys_unlink+0x177>
    end_op();
    return -1;
  }

  ilock(dp);
80105170:	83 ec 0c             	sub    $0xc,%esp
80105173:	50                   	push   %eax
80105174:	e8 07 c5 ff ff       	call   80101680 <ilock>

  // Cannot unlink "." or "..".
  if(namecmp(name, ".") == 0 || namecmp(name, "..") == 0)
80105179:	58                   	pop    %eax
8010517a:	5a                   	pop    %edx
8010517b:	68 bd 78 10 80       	push   $0x801078bd
80105180:	53                   	push   %ebx
80105181:	e8 0a ca ff ff       	call   80101b90 <namecmp>
80105186:	83 c4 10             	add    $0x10,%esp
80105189:	85 c0                	test   %eax,%eax
8010518b:	0f 84 d7 00 00 00    	je     80105268 <sys_unlink+0x138>
80105191:	83 ec 08             	sub    $0x8,%esp
80105194:	68 bc 78 10 80       	push   $0x801078bc
80105199:	53                   	push   %ebx
8010519a:	e8 f1 c9 ff ff       	call   80101b90 <namecmp>
8010519f:	83 c4 10             	add    $0x10,%esp
801051a2:	85 c0                	test   %eax,%eax
801051a4:	0f 84 be 00 00 00    	je     80105268 <sys_unlink+0x138>
    goto bad;

  if((ip = dirlookup(dp, name, &off)) == 0)
801051aa:	8d 45 c4             	lea    -0x3c(%ebp),%eax
801051ad:	83 ec 04             	sub    $0x4,%esp
801051b0:	50                   	push   %eax
801051b1:	53                   	push   %ebx
801051b2:	56                   	push   %esi
801051b3:	e8 f8 c9 ff ff       	call   80101bb0 <dirlookup>
801051b8:	83 c4 10             	add    $0x10,%esp
801051bb:	85 c0                	test   %eax,%eax
801051bd:	89 c3                	mov    %eax,%ebx
801051bf:	0f 84 a3 00 00 00    	je     80105268 <sys_unlink+0x138>
    goto bad;
  ilock(ip);
801051c5:	83 ec 0c             	sub    $0xc,%esp
801051c8:	50                   	push   %eax
801051c9:	e8 b2 c4 ff ff       	call   80101680 <ilock>

  if(ip->nlink < 1)
801051ce:	83 c4 10             	add    $0x10,%esp
801051d1:	66 83 7b 56 00       	cmpw   $0x0,0x56(%ebx)
801051d6:	0f 8e e4 00 00 00    	jle    801052c0 <sys_unlink+0x190>
    panic("unlink: nlink < 1");
  if(ip->type == T_DIR && !isdirempty(ip)){
801051dc:	66 83 7b 50 01       	cmpw   $0x1,0x50(%ebx)
801051e1:	74 65                	je     80105248 <sys_unlink+0x118>
    iunlockput(ip);
    goto bad;
  }

  memset(&de, 0, sizeof(de));
801051e3:	8d 7d d8             	lea    -0x28(%ebp),%edi
801051e6:	83 ec 04             	sub    $0x4,%esp
801051e9:	6a 10                	push   $0x10
801051eb:	6a 00                	push   $0x0
801051ed:	57                   	push   %edi
801051ee:	e8 ed f6 ff ff       	call   801048e0 <memset>
  if(writei(dp, (char*)&de, off, sizeof(de)) != sizeof(de))
801051f3:	6a 10                	push   $0x10
801051f5:	ff 75 c4             	pushl  -0x3c(%ebp)
801051f8:	57                   	push   %edi
801051f9:	56                   	push   %esi
801051fa:	e8 61 c8 ff ff       	call   80101a60 <writei>
801051ff:	83 c4 20             	add    $0x20,%esp
80105202:	83 f8 10             	cmp    $0x10,%eax
80105205:	0f 85 a8 00 00 00    	jne    801052b3 <sys_unlink+0x183>
    panic("unlink: writei");
  if(ip->type == T_DIR){
8010520b:	66 83 7b 50 01       	cmpw   $0x1,0x50(%ebx)
80105210:	74 6e                	je     80105280 <sys_unlink+0x150>
    dp->nlink--;
    iupdate(dp);
  }
  iunlockput(dp);
80105212:	83 ec 0c             	sub    $0xc,%esp
80105215:	56                   	push   %esi
80105216:	e8 f5 c6 ff ff       	call   80101910 <iunlockput>

  ip->nlink--;
8010521b:	66 83 6b 56 01       	subw   $0x1,0x56(%ebx)
  iupdate(ip);
80105220:	89 1c 24             	mov    %ebx,(%esp)
80105223:	e8 a8 c3 ff ff       	call   801015d0 <iupdate>
  iunlockput(ip);
80105228:	89 1c 24             	mov    %ebx,(%esp)
8010522b:	e8 e0 c6 ff ff       	call   80101910 <iunlockput>

  end_op();
80105230:	e8 db dd ff ff       	call   80103010 <end_op>

  return 0;
80105235:	83 c4 10             	add    $0x10,%esp
80105238:	31 c0                	xor    %eax,%eax

bad:
  iunlockput(dp);
  end_op();
  return -1;
}
8010523a:	8d 65 f4             	lea    -0xc(%ebp),%esp
8010523d:	5b                   	pop    %ebx
8010523e:	5e                   	pop    %esi
8010523f:	5f                   	pop    %edi
80105240:	5d                   	pop    %ebp
80105241:	c3                   	ret    
80105242:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
  if(ip->type == T_DIR && !isdirempty(ip)){
80105248:	83 ec 0c             	sub    $0xc,%esp
8010524b:	53                   	push   %ebx
8010524c:	e8 6f fe ff ff       	call   801050c0 <isdirempty>
80105251:	83 c4 10             	add    $0x10,%esp
80105254:	85 c0                	test   %eax,%eax
80105256:	75 8b                	jne    801051e3 <sys_unlink+0xb3>
    iunlockput(ip);
80105258:	83 ec 0c             	sub    $0xc,%esp
8010525b:	53                   	push   %ebx
8010525c:	e8 af c6 ff ff       	call   80101910 <iunlockput>
    goto bad;
80105261:	83 c4 10             	add    $0x10,%esp
80105264:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  iunlockput(dp);
80105268:	83 ec 0c             	sub    $0xc,%esp
8010526b:	56                   	push   %esi
8010526c:	e8 9f c6 ff ff       	call   80101910 <iunlockput>
  end_op();
80105271:	e8 9a dd ff ff       	call   80103010 <end_op>
  return -1;
80105276:	83 c4 10             	add    $0x10,%esp
80105279:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
8010527e:	eb ba                	jmp    8010523a <sys_unlink+0x10a>
    dp->nlink--;
80105280:	66 83 6e 56 01       	subw   $0x1,0x56(%esi)
    iupdate(dp);
80105285:	83 ec 0c             	sub    $0xc,%esp
80105288:	56                   	push   %esi
80105289:	e8 42 c3 ff ff       	call   801015d0 <iupdate>
8010528e:	83 c4 10             	add    $0x10,%esp
80105291:	e9 7c ff ff ff       	jmp    80105212 <sys_unlink+0xe2>
80105296:	8d 76 00             	lea    0x0(%esi),%esi
80105299:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi
    return -1;
801052a0:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801052a5:	eb 93                	jmp    8010523a <sys_unlink+0x10a>
    end_op();
801052a7:	e8 64 dd ff ff       	call   80103010 <end_op>
    return -1;
801052ac:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801052b1:	eb 87                	jmp    8010523a <sys_unlink+0x10a>
    panic("unlink: writei");
801052b3:	83 ec 0c             	sub    $0xc,%esp
801052b6:	68 d1 78 10 80       	push   $0x801078d1
801052bb:	e8 d0 b0 ff ff       	call   80100390 <panic>
    panic("unlink: nlink < 1");
801052c0:	83 ec 0c             	sub    $0xc,%esp
801052c3:	68 bf 78 10 80       	push   $0x801078bf
801052c8:	e8 c3 b0 ff ff       	call   80100390 <panic>
801052cd:	8d 76 00             	lea    0x0(%esi),%esi

801052d0 <create>:

struct inode*
create(char *path, short type, short major, short minor)
{
801052d0:	55                   	push   %ebp
801052d1:	89 e5                	mov    %esp,%ebp
801052d3:	57                   	push   %edi
801052d4:	56                   	push   %esi
801052d5:	53                   	push   %ebx
  struct inode *ip, *dp;
  char name[DIRSIZ];

  if((dp = nameiparent(path, name)) == 0)
801052d6:	8d 75 da             	lea    -0x26(%ebp),%esi
{
801052d9:	83 ec 34             	sub    $0x34,%esp
801052dc:	8b 45 0c             	mov    0xc(%ebp),%eax
801052df:	8b 55 10             	mov    0x10(%ebp),%edx
801052e2:	8b 4d 14             	mov    0x14(%ebp),%ecx
  if((dp = nameiparent(path, name)) == 0)
801052e5:	56                   	push   %esi
801052e6:	ff 75 08             	pushl  0x8(%ebp)
{
801052e9:	89 45 d4             	mov    %eax,-0x2c(%ebp)
801052ec:	89 55 d0             	mov    %edx,-0x30(%ebp)
801052ef:	89 4d cc             	mov    %ecx,-0x34(%ebp)
  if((dp = nameiparent(path, name)) == 0)
801052f2:	e8 09 cc ff ff       	call   80101f00 <nameiparent>
801052f7:	83 c4 10             	add    $0x10,%esp
801052fa:	85 c0                	test   %eax,%eax
801052fc:	0f 84 4e 01 00 00    	je     80105450 <create+0x180>
    return 0;
  ilock(dp);
80105302:	83 ec 0c             	sub    $0xc,%esp
80105305:	89 c3                	mov    %eax,%ebx
80105307:	50                   	push   %eax
80105308:	e8 73 c3 ff ff       	call   80101680 <ilock>

  if((ip = dirlookup(dp, name, 0)) != 0){
8010530d:	83 c4 0c             	add    $0xc,%esp
80105310:	6a 00                	push   $0x0
80105312:	56                   	push   %esi
80105313:	53                   	push   %ebx
80105314:	e8 97 c8 ff ff       	call   80101bb0 <dirlookup>
80105319:	83 c4 10             	add    $0x10,%esp
8010531c:	85 c0                	test   %eax,%eax
8010531e:	89 c7                	mov    %eax,%edi
80105320:	74 3e                	je     80105360 <create+0x90>
    iunlockput(dp);
80105322:	83 ec 0c             	sub    $0xc,%esp
80105325:	53                   	push   %ebx
80105326:	e8 e5 c5 ff ff       	call   80101910 <iunlockput>
    ilock(ip);
8010532b:	89 3c 24             	mov    %edi,(%esp)
8010532e:	e8 4d c3 ff ff       	call   80101680 <ilock>
    if(type == T_FILE && ip->type == T_FILE)
80105333:	83 c4 10             	add    $0x10,%esp
80105336:	66 83 7d d4 02       	cmpw   $0x2,-0x2c(%ebp)
8010533b:	0f 85 9f 00 00 00    	jne    801053e0 <create+0x110>
80105341:	66 83 7f 50 02       	cmpw   $0x2,0x50(%edi)
80105346:	0f 85 94 00 00 00    	jne    801053e0 <create+0x110>
    panic("create: dirlink");

  iunlockput(dp);

  return ip;
}
8010534c:	8d 65 f4             	lea    -0xc(%ebp),%esp
8010534f:	89 f8                	mov    %edi,%eax
80105351:	5b                   	pop    %ebx
80105352:	5e                   	pop    %esi
80105353:	5f                   	pop    %edi
80105354:	5d                   	pop    %ebp
80105355:	c3                   	ret    
80105356:	8d 76 00             	lea    0x0(%esi),%esi
80105359:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi
  if((ip = ialloc(dp->dev, type)) == 0)
80105360:	0f bf 45 d4          	movswl -0x2c(%ebp),%eax
80105364:	83 ec 08             	sub    $0x8,%esp
80105367:	50                   	push   %eax
80105368:	ff 33                	pushl  (%ebx)
8010536a:	e8 a1 c1 ff ff       	call   80101510 <ialloc>
8010536f:	83 c4 10             	add    $0x10,%esp
80105372:	85 c0                	test   %eax,%eax
80105374:	89 c7                	mov    %eax,%edi
80105376:	0f 84 e8 00 00 00    	je     80105464 <create+0x194>
  ilock(ip);
8010537c:	83 ec 0c             	sub    $0xc,%esp
8010537f:	50                   	push   %eax
80105380:	e8 fb c2 ff ff       	call   80101680 <ilock>
  ip->major = major;
80105385:	0f b7 45 d0          	movzwl -0x30(%ebp),%eax
80105389:	66 89 47 52          	mov    %ax,0x52(%edi)
  ip->minor = minor;
8010538d:	0f b7 45 cc          	movzwl -0x34(%ebp),%eax
80105391:	66 89 47 54          	mov    %ax,0x54(%edi)
  ip->nlink = 1;
80105395:	b8 01 00 00 00       	mov    $0x1,%eax
8010539a:	66 89 47 56          	mov    %ax,0x56(%edi)
  iupdate(ip);
8010539e:	89 3c 24             	mov    %edi,(%esp)
801053a1:	e8 2a c2 ff ff       	call   801015d0 <iupdate>
  if(type == T_DIR){  // Create . and .. entries.
801053a6:	83 c4 10             	add    $0x10,%esp
801053a9:	66 83 7d d4 01       	cmpw   $0x1,-0x2c(%ebp)
801053ae:	74 50                	je     80105400 <create+0x130>
  if(dirlink(dp, name, ip->inum) < 0)
801053b0:	83 ec 04             	sub    $0x4,%esp
801053b3:	ff 77 04             	pushl  0x4(%edi)
801053b6:	56                   	push   %esi
801053b7:	53                   	push   %ebx
801053b8:	e8 63 ca ff ff       	call   80101e20 <dirlink>
801053bd:	83 c4 10             	add    $0x10,%esp
801053c0:	85 c0                	test   %eax,%eax
801053c2:	0f 88 8f 00 00 00    	js     80105457 <create+0x187>
  iunlockput(dp);
801053c8:	83 ec 0c             	sub    $0xc,%esp
801053cb:	53                   	push   %ebx
801053cc:	e8 3f c5 ff ff       	call   80101910 <iunlockput>
  return ip;
801053d1:	83 c4 10             	add    $0x10,%esp
}
801053d4:	8d 65 f4             	lea    -0xc(%ebp),%esp
801053d7:	89 f8                	mov    %edi,%eax
801053d9:	5b                   	pop    %ebx
801053da:	5e                   	pop    %esi
801053db:	5f                   	pop    %edi
801053dc:	5d                   	pop    %ebp
801053dd:	c3                   	ret    
801053de:	66 90                	xchg   %ax,%ax
    iunlockput(ip);
801053e0:	83 ec 0c             	sub    $0xc,%esp
801053e3:	57                   	push   %edi
    return 0;
801053e4:	31 ff                	xor    %edi,%edi
    iunlockput(ip);
801053e6:	e8 25 c5 ff ff       	call   80101910 <iunlockput>
    return 0;
801053eb:	83 c4 10             	add    $0x10,%esp
}
801053ee:	8d 65 f4             	lea    -0xc(%ebp),%esp
801053f1:	89 f8                	mov    %edi,%eax
801053f3:	5b                   	pop    %ebx
801053f4:	5e                   	pop    %esi
801053f5:	5f                   	pop    %edi
801053f6:	5d                   	pop    %ebp
801053f7:	c3                   	ret    
801053f8:	90                   	nop
801053f9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    dp->nlink++;  // for ".."
80105400:	66 83 43 56 01       	addw   $0x1,0x56(%ebx)
    iupdate(dp);
80105405:	83 ec 0c             	sub    $0xc,%esp
80105408:	53                   	push   %ebx
80105409:	e8 c2 c1 ff ff       	call   801015d0 <iupdate>
    if(dirlink(ip, ".", ip->inum) < 0 || dirlink(ip, "..", dp->inum) < 0)
8010540e:	83 c4 0c             	add    $0xc,%esp
80105411:	ff 77 04             	pushl  0x4(%edi)
80105414:	68 bd 78 10 80       	push   $0x801078bd
80105419:	57                   	push   %edi
8010541a:	e8 01 ca ff ff       	call   80101e20 <dirlink>
8010541f:	83 c4 10             	add    $0x10,%esp
80105422:	85 c0                	test   %eax,%eax
80105424:	78 1c                	js     80105442 <create+0x172>
80105426:	83 ec 04             	sub    $0x4,%esp
80105429:	ff 73 04             	pushl  0x4(%ebx)
8010542c:	68 bc 78 10 80       	push   $0x801078bc
80105431:	57                   	push   %edi
80105432:	e8 e9 c9 ff ff       	call   80101e20 <dirlink>
80105437:	83 c4 10             	add    $0x10,%esp
8010543a:	85 c0                	test   %eax,%eax
8010543c:	0f 89 6e ff ff ff    	jns    801053b0 <create+0xe0>
      panic("create dots");
80105442:	83 ec 0c             	sub    $0xc,%esp
80105445:	68 19 7f 10 80       	push   $0x80107f19
8010544a:	e8 41 af ff ff       	call   80100390 <panic>
8010544f:	90                   	nop
    return 0;
80105450:	31 ff                	xor    %edi,%edi
80105452:	e9 f5 fe ff ff       	jmp    8010534c <create+0x7c>
    panic("create: dirlink");
80105457:	83 ec 0c             	sub    $0xc,%esp
8010545a:	68 25 7f 10 80       	push   $0x80107f25
8010545f:	e8 2c af ff ff       	call   80100390 <panic>
    panic("create: ialloc");
80105464:	83 ec 0c             	sub    $0xc,%esp
80105467:	68 0a 7f 10 80       	push   $0x80107f0a
8010546c:	e8 1f af ff ff       	call   80100390 <panic>
80105471:	eb 0d                	jmp    80105480 <sys_open>
80105473:	90                   	nop
80105474:	90                   	nop
80105475:	90                   	nop
80105476:	90                   	nop
80105477:	90                   	nop
80105478:	90                   	nop
80105479:	90                   	nop
8010547a:	90                   	nop
8010547b:	90                   	nop
8010547c:	90                   	nop
8010547d:	90                   	nop
8010547e:	90                   	nop
8010547f:	90                   	nop

80105480 <sys_open>:

int
sys_open(void)
{
80105480:	55                   	push   %ebp
80105481:	89 e5                	mov    %esp,%ebp
80105483:	57                   	push   %edi
80105484:	56                   	push   %esi
80105485:	53                   	push   %ebx
  char *path;
  int fd, omode;
  struct file *f;
  struct inode *ip;

  if(argstr(0, &path) < 0 || argint(1, &omode) < 0)
80105486:	8d 45 e0             	lea    -0x20(%ebp),%eax
{
80105489:	83 ec 24             	sub    $0x24,%esp
  if(argstr(0, &path) < 0 || argint(1, &omode) < 0)
8010548c:	50                   	push   %eax
8010548d:	6a 00                	push   $0x0
8010548f:	e8 fc f7 ff ff       	call   80104c90 <argstr>
80105494:	83 c4 10             	add    $0x10,%esp
80105497:	85 c0                	test   %eax,%eax
80105499:	0f 88 1d 01 00 00    	js     801055bc <sys_open+0x13c>
8010549f:	8d 45 e4             	lea    -0x1c(%ebp),%eax
801054a2:	83 ec 08             	sub    $0x8,%esp
801054a5:	50                   	push   %eax
801054a6:	6a 01                	push   $0x1
801054a8:	e8 33 f7 ff ff       	call   80104be0 <argint>
801054ad:	83 c4 10             	add    $0x10,%esp
801054b0:	85 c0                	test   %eax,%eax
801054b2:	0f 88 04 01 00 00    	js     801055bc <sys_open+0x13c>
    return -1;

  begin_op();
801054b8:	e8 e3 da ff ff       	call   80102fa0 <begin_op>

  if(omode & O_CREATE){
801054bd:	f6 45 e5 02          	testb  $0x2,-0x1b(%ebp)
801054c1:	0f 85 a9 00 00 00    	jne    80105570 <sys_open+0xf0>
    if(ip == 0){
      end_op();
      return -1;
    }
  } else {
    if((ip = namei(path)) == 0){
801054c7:	83 ec 0c             	sub    $0xc,%esp
801054ca:	ff 75 e0             	pushl  -0x20(%ebp)
801054cd:	e8 0e ca ff ff       	call   80101ee0 <namei>
801054d2:	83 c4 10             	add    $0x10,%esp
801054d5:	85 c0                	test   %eax,%eax
801054d7:	89 c6                	mov    %eax,%esi
801054d9:	0f 84 ac 00 00 00    	je     8010558b <sys_open+0x10b>
      end_op();
      return -1;
    }
    ilock(ip);
801054df:	83 ec 0c             	sub    $0xc,%esp
801054e2:	50                   	push   %eax
801054e3:	e8 98 c1 ff ff       	call   80101680 <ilock>
    if(ip->type == T_DIR && omode != O_RDONLY){
801054e8:	83 c4 10             	add    $0x10,%esp
801054eb:	66 83 7e 50 01       	cmpw   $0x1,0x50(%esi)
801054f0:	0f 84 aa 00 00 00    	je     801055a0 <sys_open+0x120>
      end_op();
      return -1;
    }
  }

  if((f = filealloc()) == 0 || (fd = fdalloc(f)) < 0){
801054f6:	e8 85 b8 ff ff       	call   80100d80 <filealloc>
801054fb:	85 c0                	test   %eax,%eax
801054fd:	89 c7                	mov    %eax,%edi
801054ff:	0f 84 a6 00 00 00    	je     801055ab <sys_open+0x12b>
  struct proc *curproc = myproc();
80105505:	e8 26 e7 ff ff       	call   80103c30 <myproc>
  for(fd = 0; fd < NOFILE; fd++){
8010550a:	31 db                	xor    %ebx,%ebx
8010550c:	eb 0e                	jmp    8010551c <sys_open+0x9c>
8010550e:	66 90                	xchg   %ax,%ax
80105510:	83 c3 01             	add    $0x1,%ebx
80105513:	83 fb 10             	cmp    $0x10,%ebx
80105516:	0f 84 ac 00 00 00    	je     801055c8 <sys_open+0x148>
    if(curproc->ofile[fd] == 0){
8010551c:	8b 54 98 28          	mov    0x28(%eax,%ebx,4),%edx
80105520:	85 d2                	test   %edx,%edx
80105522:	75 ec                	jne    80105510 <sys_open+0x90>
      fileclose(f);
    iunlockput(ip);
    end_op();
    return -1;
  }
  iunlock(ip);
80105524:	83 ec 0c             	sub    $0xc,%esp
      curproc->ofile[fd] = f;
80105527:	89 7c 98 28          	mov    %edi,0x28(%eax,%ebx,4)
  iunlock(ip);
8010552b:	56                   	push   %esi
8010552c:	e8 2f c2 ff ff       	call   80101760 <iunlock>
  end_op();
80105531:	e8 da da ff ff       	call   80103010 <end_op>

  f->type = FD_INODE;
80105536:	c7 07 02 00 00 00    	movl   $0x2,(%edi)
  f->ip = ip;
  f->off = 0;
  f->readable = !(omode & O_WRONLY);
8010553c:	8b 55 e4             	mov    -0x1c(%ebp),%edx
  f->writable = (omode & O_WRONLY) || (omode & O_RDWR);
8010553f:	83 c4 10             	add    $0x10,%esp
  f->ip = ip;
80105542:	89 77 10             	mov    %esi,0x10(%edi)
  f->off = 0;
80105545:	c7 47 14 00 00 00 00 	movl   $0x0,0x14(%edi)
  f->readable = !(omode & O_WRONLY);
8010554c:	89 d0                	mov    %edx,%eax
8010554e:	f7 d0                	not    %eax
80105550:	83 e0 01             	and    $0x1,%eax
  f->writable = (omode & O_WRONLY) || (omode & O_RDWR);
80105553:	83 e2 03             	and    $0x3,%edx
  f->readable = !(omode & O_WRONLY);
80105556:	88 47 08             	mov    %al,0x8(%edi)
  f->writable = (omode & O_WRONLY) || (omode & O_RDWR);
80105559:	0f 95 47 09          	setne  0x9(%edi)
  return fd;
}
8010555d:	8d 65 f4             	lea    -0xc(%ebp),%esp
80105560:	89 d8                	mov    %ebx,%eax
80105562:	5b                   	pop    %ebx
80105563:	5e                   	pop    %esi
80105564:	5f                   	pop    %edi
80105565:	5d                   	pop    %ebp
80105566:	c3                   	ret    
80105567:	89 f6                	mov    %esi,%esi
80105569:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi
    ip = create(path, T_FILE, 0, 0);
80105570:	6a 00                	push   $0x0
80105572:	6a 00                	push   $0x0
80105574:	6a 02                	push   $0x2
80105576:	ff 75 e0             	pushl  -0x20(%ebp)
80105579:	e8 52 fd ff ff       	call   801052d0 <create>
    if(ip == 0){
8010557e:	83 c4 10             	add    $0x10,%esp
80105581:	85 c0                	test   %eax,%eax
    ip = create(path, T_FILE, 0, 0);
80105583:	89 c6                	mov    %eax,%esi
    if(ip == 0){
80105585:	0f 85 6b ff ff ff    	jne    801054f6 <sys_open+0x76>
      end_op();
8010558b:	e8 80 da ff ff       	call   80103010 <end_op>
      return -1;
80105590:	bb ff ff ff ff       	mov    $0xffffffff,%ebx
80105595:	eb c6                	jmp    8010555d <sys_open+0xdd>
80105597:	89 f6                	mov    %esi,%esi
80105599:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi
    if(ip->type == T_DIR && omode != O_RDONLY){
801055a0:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
801055a3:	85 c9                	test   %ecx,%ecx
801055a5:	0f 84 4b ff ff ff    	je     801054f6 <sys_open+0x76>
    iunlockput(ip);
801055ab:	83 ec 0c             	sub    $0xc,%esp
801055ae:	56                   	push   %esi
801055af:	e8 5c c3 ff ff       	call   80101910 <iunlockput>
    end_op();
801055b4:	e8 57 da ff ff       	call   80103010 <end_op>
    return -1;
801055b9:	83 c4 10             	add    $0x10,%esp
801055bc:	bb ff ff ff ff       	mov    $0xffffffff,%ebx
801055c1:	eb 9a                	jmp    8010555d <sys_open+0xdd>
801055c3:	90                   	nop
801055c4:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
      fileclose(f);
801055c8:	83 ec 0c             	sub    $0xc,%esp
801055cb:	57                   	push   %edi
801055cc:	e8 6f b8 ff ff       	call   80100e40 <fileclose>
801055d1:	83 c4 10             	add    $0x10,%esp
801055d4:	eb d5                	jmp    801055ab <sys_open+0x12b>
801055d6:	8d 76 00             	lea    0x0(%esi),%esi
801055d9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

801055e0 <sys_mkdir>:

int
sys_mkdir(void)
{
801055e0:	55                   	push   %ebp
801055e1:	89 e5                	mov    %esp,%ebp
801055e3:	83 ec 18             	sub    $0x18,%esp
  char *path;
  struct inode *ip;

  begin_op();
801055e6:	e8 b5 d9 ff ff       	call   80102fa0 <begin_op>
  if(argstr(0, &path) < 0 || (ip = create(path, T_DIR, 0, 0)) == 0){
801055eb:	8d 45 f4             	lea    -0xc(%ebp),%eax
801055ee:	83 ec 08             	sub    $0x8,%esp
801055f1:	50                   	push   %eax
801055f2:	6a 00                	push   $0x0
801055f4:	e8 97 f6 ff ff       	call   80104c90 <argstr>
801055f9:	83 c4 10             	add    $0x10,%esp
801055fc:	85 c0                	test   %eax,%eax
801055fe:	78 30                	js     80105630 <sys_mkdir+0x50>
80105600:	6a 00                	push   $0x0
80105602:	6a 00                	push   $0x0
80105604:	6a 01                	push   $0x1
80105606:	ff 75 f4             	pushl  -0xc(%ebp)
80105609:	e8 c2 fc ff ff       	call   801052d0 <create>
8010560e:	83 c4 10             	add    $0x10,%esp
80105611:	85 c0                	test   %eax,%eax
80105613:	74 1b                	je     80105630 <sys_mkdir+0x50>
    end_op();
    return -1;
  }
  iunlockput(ip);
80105615:	83 ec 0c             	sub    $0xc,%esp
80105618:	50                   	push   %eax
80105619:	e8 f2 c2 ff ff       	call   80101910 <iunlockput>
  end_op();
8010561e:	e8 ed d9 ff ff       	call   80103010 <end_op>
  return 0;
80105623:	83 c4 10             	add    $0x10,%esp
80105626:	31 c0                	xor    %eax,%eax
}
80105628:	c9                   	leave  
80105629:	c3                   	ret    
8010562a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    end_op();
80105630:	e8 db d9 ff ff       	call   80103010 <end_op>
    return -1;
80105635:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
8010563a:	c9                   	leave  
8010563b:	c3                   	ret    
8010563c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

80105640 <sys_mknod>:

int
sys_mknod(void)
{
80105640:	55                   	push   %ebp
80105641:	89 e5                	mov    %esp,%ebp
80105643:	83 ec 18             	sub    $0x18,%esp
  struct inode *ip;
  char *path;
  int major, minor;

  begin_op();
80105646:	e8 55 d9 ff ff       	call   80102fa0 <begin_op>
  if((argstr(0, &path)) < 0 ||
8010564b:	8d 45 ec             	lea    -0x14(%ebp),%eax
8010564e:	83 ec 08             	sub    $0x8,%esp
80105651:	50                   	push   %eax
80105652:	6a 00                	push   $0x0
80105654:	e8 37 f6 ff ff       	call   80104c90 <argstr>
80105659:	83 c4 10             	add    $0x10,%esp
8010565c:	85 c0                	test   %eax,%eax
8010565e:	78 60                	js     801056c0 <sys_mknod+0x80>
     argint(1, &major) < 0 ||
80105660:	8d 45 f0             	lea    -0x10(%ebp),%eax
80105663:	83 ec 08             	sub    $0x8,%esp
80105666:	50                   	push   %eax
80105667:	6a 01                	push   $0x1
80105669:	e8 72 f5 ff ff       	call   80104be0 <argint>
  if((argstr(0, &path)) < 0 ||
8010566e:	83 c4 10             	add    $0x10,%esp
80105671:	85 c0                	test   %eax,%eax
80105673:	78 4b                	js     801056c0 <sys_mknod+0x80>
     argint(2, &minor) < 0 ||
80105675:	8d 45 f4             	lea    -0xc(%ebp),%eax
80105678:	83 ec 08             	sub    $0x8,%esp
8010567b:	50                   	push   %eax
8010567c:	6a 02                	push   $0x2
8010567e:	e8 5d f5 ff ff       	call   80104be0 <argint>
     argint(1, &major) < 0 ||
80105683:	83 c4 10             	add    $0x10,%esp
80105686:	85 c0                	test   %eax,%eax
80105688:	78 36                	js     801056c0 <sys_mknod+0x80>
     (ip = create(path, T_DEV, major, minor)) == 0){
8010568a:	0f bf 45 f4          	movswl -0xc(%ebp),%eax
     argint(2, &minor) < 0 ||
8010568e:	50                   	push   %eax
     (ip = create(path, T_DEV, major, minor)) == 0){
8010568f:	0f bf 45 f0          	movswl -0x10(%ebp),%eax
     argint(2, &minor) < 0 ||
80105693:	50                   	push   %eax
80105694:	6a 03                	push   $0x3
80105696:	ff 75 ec             	pushl  -0x14(%ebp)
80105699:	e8 32 fc ff ff       	call   801052d0 <create>
8010569e:	83 c4 10             	add    $0x10,%esp
801056a1:	85 c0                	test   %eax,%eax
801056a3:	74 1b                	je     801056c0 <sys_mknod+0x80>
    end_op();
    return -1;
  }
  iunlockput(ip);
801056a5:	83 ec 0c             	sub    $0xc,%esp
801056a8:	50                   	push   %eax
801056a9:	e8 62 c2 ff ff       	call   80101910 <iunlockput>
  end_op();
801056ae:	e8 5d d9 ff ff       	call   80103010 <end_op>
  return 0;
801056b3:	83 c4 10             	add    $0x10,%esp
801056b6:	31 c0                	xor    %eax,%eax
}
801056b8:	c9                   	leave  
801056b9:	c3                   	ret    
801056ba:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    end_op();
801056c0:	e8 4b d9 ff ff       	call   80103010 <end_op>
    return -1;
801056c5:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
801056ca:	c9                   	leave  
801056cb:	c3                   	ret    
801056cc:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

801056d0 <sys_chdir>:

int
sys_chdir(void)
{
801056d0:	55                   	push   %ebp
801056d1:	89 e5                	mov    %esp,%ebp
801056d3:	56                   	push   %esi
801056d4:	53                   	push   %ebx
801056d5:	83 ec 10             	sub    $0x10,%esp
  char *path;
  struct inode *ip;
  struct proc *curproc = myproc();
801056d8:	e8 53 e5 ff ff       	call   80103c30 <myproc>
801056dd:	89 c6                	mov    %eax,%esi
  
  begin_op();
801056df:	e8 bc d8 ff ff       	call   80102fa0 <begin_op>
  if(argstr(0, &path) < 0 || (ip = namei(path)) == 0){
801056e4:	8d 45 f4             	lea    -0xc(%ebp),%eax
801056e7:	83 ec 08             	sub    $0x8,%esp
801056ea:	50                   	push   %eax
801056eb:	6a 00                	push   $0x0
801056ed:	e8 9e f5 ff ff       	call   80104c90 <argstr>
801056f2:	83 c4 10             	add    $0x10,%esp
801056f5:	85 c0                	test   %eax,%eax
801056f7:	78 77                	js     80105770 <sys_chdir+0xa0>
801056f9:	83 ec 0c             	sub    $0xc,%esp
801056fc:	ff 75 f4             	pushl  -0xc(%ebp)
801056ff:	e8 dc c7 ff ff       	call   80101ee0 <namei>
80105704:	83 c4 10             	add    $0x10,%esp
80105707:	85 c0                	test   %eax,%eax
80105709:	89 c3                	mov    %eax,%ebx
8010570b:	74 63                	je     80105770 <sys_chdir+0xa0>
    end_op();
    return -1;
  }
  ilock(ip);
8010570d:	83 ec 0c             	sub    $0xc,%esp
80105710:	50                   	push   %eax
80105711:	e8 6a bf ff ff       	call   80101680 <ilock>
  if(ip->type != T_DIR){
80105716:	83 c4 10             	add    $0x10,%esp
80105719:	66 83 7b 50 01       	cmpw   $0x1,0x50(%ebx)
8010571e:	75 30                	jne    80105750 <sys_chdir+0x80>
    iunlockput(ip);
    end_op();
    return -1;
  }
  iunlock(ip);
80105720:	83 ec 0c             	sub    $0xc,%esp
80105723:	53                   	push   %ebx
80105724:	e8 37 c0 ff ff       	call   80101760 <iunlock>
  iput(curproc->cwd);
80105729:	58                   	pop    %eax
8010572a:	ff 76 68             	pushl  0x68(%esi)
8010572d:	e8 7e c0 ff ff       	call   801017b0 <iput>
  end_op();
80105732:	e8 d9 d8 ff ff       	call   80103010 <end_op>
  curproc->cwd = ip;
80105737:	89 5e 68             	mov    %ebx,0x68(%esi)
  return 0;
8010573a:	83 c4 10             	add    $0x10,%esp
8010573d:	31 c0                	xor    %eax,%eax
}
8010573f:	8d 65 f8             	lea    -0x8(%ebp),%esp
80105742:	5b                   	pop    %ebx
80105743:	5e                   	pop    %esi
80105744:	5d                   	pop    %ebp
80105745:	c3                   	ret    
80105746:	8d 76 00             	lea    0x0(%esi),%esi
80105749:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi
    iunlockput(ip);
80105750:	83 ec 0c             	sub    $0xc,%esp
80105753:	53                   	push   %ebx
80105754:	e8 b7 c1 ff ff       	call   80101910 <iunlockput>
    end_op();
80105759:	e8 b2 d8 ff ff       	call   80103010 <end_op>
    return -1;
8010575e:	83 c4 10             	add    $0x10,%esp
80105761:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80105766:	eb d7                	jmp    8010573f <sys_chdir+0x6f>
80105768:	90                   	nop
80105769:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    end_op();
80105770:	e8 9b d8 ff ff       	call   80103010 <end_op>
    return -1;
80105775:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
8010577a:	eb c3                	jmp    8010573f <sys_chdir+0x6f>
8010577c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

80105780 <sys_exec>:

int
sys_exec(void)
{
80105780:	55                   	push   %ebp
80105781:	89 e5                	mov    %esp,%ebp
80105783:	57                   	push   %edi
80105784:	56                   	push   %esi
80105785:	53                   	push   %ebx
  char *path, *argv[MAXARG];
  int i;
  uint uargv, uarg;

  if(argstr(0, &path) < 0 || argint(1, (int*)&uargv) < 0){
80105786:	8d 85 5c ff ff ff    	lea    -0xa4(%ebp),%eax
{
8010578c:	81 ec a4 00 00 00    	sub    $0xa4,%esp
  if(argstr(0, &path) < 0 || argint(1, (int*)&uargv) < 0){
80105792:	50                   	push   %eax
80105793:	6a 00                	push   $0x0
80105795:	e8 f6 f4 ff ff       	call   80104c90 <argstr>
8010579a:	83 c4 10             	add    $0x10,%esp
8010579d:	85 c0                	test   %eax,%eax
8010579f:	0f 88 87 00 00 00    	js     8010582c <sys_exec+0xac>
801057a5:	8d 85 60 ff ff ff    	lea    -0xa0(%ebp),%eax
801057ab:	83 ec 08             	sub    $0x8,%esp
801057ae:	50                   	push   %eax
801057af:	6a 01                	push   $0x1
801057b1:	e8 2a f4 ff ff       	call   80104be0 <argint>
801057b6:	83 c4 10             	add    $0x10,%esp
801057b9:	85 c0                	test   %eax,%eax
801057bb:	78 6f                	js     8010582c <sys_exec+0xac>
    return -1;
  }
  memset(argv, 0, sizeof(argv));
801057bd:	8d 85 68 ff ff ff    	lea    -0x98(%ebp),%eax
801057c3:	83 ec 04             	sub    $0x4,%esp
  for(i=0;; i++){
801057c6:	31 db                	xor    %ebx,%ebx
  memset(argv, 0, sizeof(argv));
801057c8:	68 80 00 00 00       	push   $0x80
801057cd:	6a 00                	push   $0x0
801057cf:	8d bd 64 ff ff ff    	lea    -0x9c(%ebp),%edi
801057d5:	50                   	push   %eax
801057d6:	e8 05 f1 ff ff       	call   801048e0 <memset>
801057db:	83 c4 10             	add    $0x10,%esp
801057de:	eb 2c                	jmp    8010580c <sys_exec+0x8c>
    if(i >= NELEM(argv))
      return -1;
    if(fetchint(uargv+4*i, (int*)&uarg) < 0)
      return -1;
    if(uarg == 0){
801057e0:	8b 85 64 ff ff ff    	mov    -0x9c(%ebp),%eax
801057e6:	85 c0                	test   %eax,%eax
801057e8:	74 56                	je     80105840 <sys_exec+0xc0>
      argv[i] = 0;
      break;
    }
    if(fetchstr(uarg, &argv[i]) < 0)
801057ea:	8d 8d 68 ff ff ff    	lea    -0x98(%ebp),%ecx
801057f0:	83 ec 08             	sub    $0x8,%esp
801057f3:	8d 14 31             	lea    (%ecx,%esi,1),%edx
801057f6:	52                   	push   %edx
801057f7:	50                   	push   %eax
801057f8:	e8 73 f3 ff ff       	call   80104b70 <fetchstr>
801057fd:	83 c4 10             	add    $0x10,%esp
80105800:	85 c0                	test   %eax,%eax
80105802:	78 28                	js     8010582c <sys_exec+0xac>
  for(i=0;; i++){
80105804:	83 c3 01             	add    $0x1,%ebx
    if(i >= NELEM(argv))
80105807:	83 fb 20             	cmp    $0x20,%ebx
8010580a:	74 20                	je     8010582c <sys_exec+0xac>
    if(fetchint(uargv+4*i, (int*)&uarg) < 0)
8010580c:	8b 85 60 ff ff ff    	mov    -0xa0(%ebp),%eax
80105812:	8d 34 9d 00 00 00 00 	lea    0x0(,%ebx,4),%esi
80105819:	83 ec 08             	sub    $0x8,%esp
8010581c:	57                   	push   %edi
8010581d:	01 f0                	add    %esi,%eax
8010581f:	50                   	push   %eax
80105820:	e8 0b f3 ff ff       	call   80104b30 <fetchint>
80105825:	83 c4 10             	add    $0x10,%esp
80105828:	85 c0                	test   %eax,%eax
8010582a:	79 b4                	jns    801057e0 <sys_exec+0x60>
      return -1;
  }
  return exec(path, argv);
}
8010582c:	8d 65 f4             	lea    -0xc(%ebp),%esp
    return -1;
8010582f:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80105834:	5b                   	pop    %ebx
80105835:	5e                   	pop    %esi
80105836:	5f                   	pop    %edi
80105837:	5d                   	pop    %ebp
80105838:	c3                   	ret    
80105839:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
  return exec(path, argv);
80105840:	8d 85 68 ff ff ff    	lea    -0x98(%ebp),%eax
80105846:	83 ec 08             	sub    $0x8,%esp
      argv[i] = 0;
80105849:	c7 84 9d 68 ff ff ff 	movl   $0x0,-0x98(%ebp,%ebx,4)
80105850:	00 00 00 00 
  return exec(path, argv);
80105854:	50                   	push   %eax
80105855:	ff b5 5c ff ff ff    	pushl  -0xa4(%ebp)
8010585b:	e8 b0 b1 ff ff       	call   80100a10 <exec>
80105860:	83 c4 10             	add    $0x10,%esp
}
80105863:	8d 65 f4             	lea    -0xc(%ebp),%esp
80105866:	5b                   	pop    %ebx
80105867:	5e                   	pop    %esi
80105868:	5f                   	pop    %edi
80105869:	5d                   	pop    %ebp
8010586a:	c3                   	ret    
8010586b:	90                   	nop
8010586c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

80105870 <sys_pipe>:

int
sys_pipe(void)
{
80105870:	55                   	push   %ebp
80105871:	89 e5                	mov    %esp,%ebp
80105873:	57                   	push   %edi
80105874:	56                   	push   %esi
80105875:	53                   	push   %ebx
  int *fd;
  struct file *rf, *wf;
  int fd0, fd1;

  if(argptr(0, (void*)&fd, 2*sizeof(fd[0])) < 0)
80105876:	8d 45 dc             	lea    -0x24(%ebp),%eax
{
80105879:	83 ec 20             	sub    $0x20,%esp
  if(argptr(0, (void*)&fd, 2*sizeof(fd[0])) < 0)
8010587c:	6a 08                	push   $0x8
8010587e:	50                   	push   %eax
8010587f:	6a 00                	push   $0x0
80105881:	e8 aa f3 ff ff       	call   80104c30 <argptr>
80105886:	83 c4 10             	add    $0x10,%esp
80105889:	85 c0                	test   %eax,%eax
8010588b:	0f 88 ae 00 00 00    	js     8010593f <sys_pipe+0xcf>
    return -1;
  if(pipealloc(&rf, &wf) < 0)
80105891:	8d 45 e4             	lea    -0x1c(%ebp),%eax
80105894:	83 ec 08             	sub    $0x8,%esp
80105897:	50                   	push   %eax
80105898:	8d 45 e0             	lea    -0x20(%ebp),%eax
8010589b:	50                   	push   %eax
8010589c:	e8 9f dd ff ff       	call   80103640 <pipealloc>
801058a1:	83 c4 10             	add    $0x10,%esp
801058a4:	85 c0                	test   %eax,%eax
801058a6:	0f 88 93 00 00 00    	js     8010593f <sys_pipe+0xcf>
    return -1;
  fd0 = -1;
  if((fd0 = fdalloc(rf)) < 0 || (fd1 = fdalloc(wf)) < 0){
801058ac:	8b 7d e0             	mov    -0x20(%ebp),%edi
  for(fd = 0; fd < NOFILE; fd++){
801058af:	31 db                	xor    %ebx,%ebx
  struct proc *curproc = myproc();
801058b1:	e8 7a e3 ff ff       	call   80103c30 <myproc>
801058b6:	eb 10                	jmp    801058c8 <sys_pipe+0x58>
801058b8:	90                   	nop
801058b9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
  for(fd = 0; fd < NOFILE; fd++){
801058c0:	83 c3 01             	add    $0x1,%ebx
801058c3:	83 fb 10             	cmp    $0x10,%ebx
801058c6:	74 60                	je     80105928 <sys_pipe+0xb8>
    if(curproc->ofile[fd] == 0){
801058c8:	8b 74 98 28          	mov    0x28(%eax,%ebx,4),%esi
801058cc:	85 f6                	test   %esi,%esi
801058ce:	75 f0                	jne    801058c0 <sys_pipe+0x50>
      curproc->ofile[fd] = f;
801058d0:	8d 73 08             	lea    0x8(%ebx),%esi
801058d3:	89 7c b0 08          	mov    %edi,0x8(%eax,%esi,4)
  if((fd0 = fdalloc(rf)) < 0 || (fd1 = fdalloc(wf)) < 0){
801058d7:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  struct proc *curproc = myproc();
801058da:	e8 51 e3 ff ff       	call   80103c30 <myproc>
  for(fd = 0; fd < NOFILE; fd++){
801058df:	31 d2                	xor    %edx,%edx
801058e1:	eb 0d                	jmp    801058f0 <sys_pipe+0x80>
801058e3:	90                   	nop
801058e4:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
801058e8:	83 c2 01             	add    $0x1,%edx
801058eb:	83 fa 10             	cmp    $0x10,%edx
801058ee:	74 28                	je     80105918 <sys_pipe+0xa8>
    if(curproc->ofile[fd] == 0){
801058f0:	8b 4c 90 28          	mov    0x28(%eax,%edx,4),%ecx
801058f4:	85 c9                	test   %ecx,%ecx
801058f6:	75 f0                	jne    801058e8 <sys_pipe+0x78>
      curproc->ofile[fd] = f;
801058f8:	89 7c 90 28          	mov    %edi,0x28(%eax,%edx,4)
      myproc()->ofile[fd0] = 0;
    fileclose(rf);
    fileclose(wf);
    return -1;
  }
  fd[0] = fd0;
801058fc:	8b 45 dc             	mov    -0x24(%ebp),%eax
801058ff:	89 18                	mov    %ebx,(%eax)
  fd[1] = fd1;
80105901:	8b 45 dc             	mov    -0x24(%ebp),%eax
80105904:	89 50 04             	mov    %edx,0x4(%eax)
  return 0;
80105907:	31 c0                	xor    %eax,%eax
}
80105909:	8d 65 f4             	lea    -0xc(%ebp),%esp
8010590c:	5b                   	pop    %ebx
8010590d:	5e                   	pop    %esi
8010590e:	5f                   	pop    %edi
8010590f:	5d                   	pop    %ebp
80105910:	c3                   	ret    
80105911:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
      myproc()->ofile[fd0] = 0;
80105918:	e8 13 e3 ff ff       	call   80103c30 <myproc>
8010591d:	c7 44 b0 08 00 00 00 	movl   $0x0,0x8(%eax,%esi,4)
80105924:	00 
80105925:	8d 76 00             	lea    0x0(%esi),%esi
    fileclose(rf);
80105928:	83 ec 0c             	sub    $0xc,%esp
8010592b:	ff 75 e0             	pushl  -0x20(%ebp)
8010592e:	e8 0d b5 ff ff       	call   80100e40 <fileclose>
    fileclose(wf);
80105933:	58                   	pop    %eax
80105934:	ff 75 e4             	pushl  -0x1c(%ebp)
80105937:	e8 04 b5 ff ff       	call   80100e40 <fileclose>
    return -1;
8010593c:	83 c4 10             	add    $0x10,%esp
8010593f:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80105944:	eb c3                	jmp    80105909 <sys_pipe+0x99>
80105946:	66 90                	xchg   %ax,%ax
80105948:	66 90                	xchg   %ax,%ax
8010594a:	66 90                	xchg   %ax,%ax
8010594c:	66 90                	xchg   %ax,%ax
8010594e:	66 90                	xchg   %ax,%ax

80105950 <sys_fork>:
#include "mmu.h"
#include "proc.h"

int
sys_fork(void)
{
80105950:	55                   	push   %ebp
80105951:	89 e5                	mov    %esp,%ebp
  return fork();
}
80105953:	5d                   	pop    %ebp
  return fork();
80105954:	e9 77 e4 ff ff       	jmp    80103dd0 <fork>
80105959:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

80105960 <sys_exit>:

int
sys_exit(void)
{
80105960:	55                   	push   %ebp
80105961:	89 e5                	mov    %esp,%ebp
80105963:	83 ec 08             	sub    $0x8,%esp
  exit();
80105966:	e8 e5 e6 ff ff       	call   80104050 <exit>
  return 0;  // not reached
}
8010596b:	31 c0                	xor    %eax,%eax
8010596d:	c9                   	leave  
8010596e:	c3                   	ret    
8010596f:	90                   	nop

80105970 <sys_wait>:

int
sys_wait(void)
{
80105970:	55                   	push   %ebp
80105971:	89 e5                	mov    %esp,%ebp
  return wait();
}
80105973:	5d                   	pop    %ebp
  return wait();
80105974:	e9 37 e9 ff ff       	jmp    801042b0 <wait>
80105979:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

80105980 <sys_kill>:

int
sys_kill(void)
{
80105980:	55                   	push   %ebp
80105981:	89 e5                	mov    %esp,%ebp
80105983:	83 ec 20             	sub    $0x20,%esp
  int pid;

  if(argint(0, &pid) < 0)
80105986:	8d 45 f4             	lea    -0xc(%ebp),%eax
80105989:	50                   	push   %eax
8010598a:	6a 00                	push   $0x0
8010598c:	e8 4f f2 ff ff       	call   80104be0 <argint>
80105991:	83 c4 10             	add    $0x10,%esp
80105994:	85 c0                	test   %eax,%eax
80105996:	78 18                	js     801059b0 <sys_kill+0x30>
    return -1;
  return kill(pid);
80105998:	83 ec 0c             	sub    $0xc,%esp
8010599b:	ff 75 f4             	pushl  -0xc(%ebp)
8010599e:	e8 6d ea ff ff       	call   80104410 <kill>
801059a3:	83 c4 10             	add    $0x10,%esp
}
801059a6:	c9                   	leave  
801059a7:	c3                   	ret    
801059a8:	90                   	nop
801059a9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    return -1;
801059b0:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
801059b5:	c9                   	leave  
801059b6:	c3                   	ret    
801059b7:	89 f6                	mov    %esi,%esi
801059b9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

801059c0 <sys_getpid>:

int
sys_getpid(void)
{
801059c0:	55                   	push   %ebp
801059c1:	89 e5                	mov    %esp,%ebp
801059c3:	83 ec 08             	sub    $0x8,%esp
  return myproc()->pid;
801059c6:	e8 65 e2 ff ff       	call   80103c30 <myproc>
801059cb:	8b 40 10             	mov    0x10(%eax),%eax
}
801059ce:	c9                   	leave  
801059cf:	c3                   	ret    

801059d0 <sys_sbrk>:

int
sys_sbrk(void)
{
801059d0:	55                   	push   %ebp
801059d1:	89 e5                	mov    %esp,%ebp
801059d3:	53                   	push   %ebx
  int addr;
  int n;

  if(argint(0, &n) < 0)
801059d4:	8d 45 f4             	lea    -0xc(%ebp),%eax
{
801059d7:	83 ec 1c             	sub    $0x1c,%esp
  if(argint(0, &n) < 0)
801059da:	50                   	push   %eax
801059db:	6a 00                	push   $0x0
801059dd:	e8 fe f1 ff ff       	call   80104be0 <argint>
801059e2:	83 c4 10             	add    $0x10,%esp
801059e5:	85 c0                	test   %eax,%eax
801059e7:	78 27                	js     80105a10 <sys_sbrk+0x40>
    return -1;
  addr = myproc()->sz;
801059e9:	e8 42 e2 ff ff       	call   80103c30 <myproc>
  if(growproc(n) < 0)
801059ee:	83 ec 0c             	sub    $0xc,%esp
  addr = myproc()->sz;
801059f1:	8b 18                	mov    (%eax),%ebx
  if(growproc(n) < 0)
801059f3:	ff 75 f4             	pushl  -0xc(%ebp)
801059f6:	e8 55 e3 ff ff       	call   80103d50 <growproc>
801059fb:	83 c4 10             	add    $0x10,%esp
801059fe:	85 c0                	test   %eax,%eax
80105a00:	78 0e                	js     80105a10 <sys_sbrk+0x40>
    return -1;
  return addr;
}
80105a02:	89 d8                	mov    %ebx,%eax
80105a04:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80105a07:	c9                   	leave  
80105a08:	c3                   	ret    
80105a09:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    return -1;
80105a10:	bb ff ff ff ff       	mov    $0xffffffff,%ebx
80105a15:	eb eb                	jmp    80105a02 <sys_sbrk+0x32>
80105a17:	89 f6                	mov    %esi,%esi
80105a19:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80105a20 <sys_sleep>:

int
sys_sleep(void)
{
80105a20:	55                   	push   %ebp
80105a21:	89 e5                	mov    %esp,%ebp
80105a23:	53                   	push   %ebx
  int n;
  uint ticks0;

  if(argint(0, &n) < 0)
80105a24:	8d 45 f4             	lea    -0xc(%ebp),%eax
{
80105a27:	83 ec 1c             	sub    $0x1c,%esp
  if(argint(0, &n) < 0)
80105a2a:	50                   	push   %eax
80105a2b:	6a 00                	push   $0x0
80105a2d:	e8 ae f1 ff ff       	call   80104be0 <argint>
80105a32:	83 c4 10             	add    $0x10,%esp
80105a35:	85 c0                	test   %eax,%eax
80105a37:	0f 88 8a 00 00 00    	js     80105ac7 <sys_sleep+0xa7>
    return -1;
  acquire(&tickslock);
80105a3d:	83 ec 0c             	sub    $0xc,%esp
80105a40:	68 60 90 11 80       	push   $0x80119060
80105a45:	e8 86 ed ff ff       	call   801047d0 <acquire>
  ticks0 = ticks;
  while(ticks - ticks0 < n){
80105a4a:	8b 55 f4             	mov    -0xc(%ebp),%edx
80105a4d:	83 c4 10             	add    $0x10,%esp
  ticks0 = ticks;
80105a50:	8b 1d a0 98 11 80    	mov    0x801198a0,%ebx
  while(ticks - ticks0 < n){
80105a56:	85 d2                	test   %edx,%edx
80105a58:	75 27                	jne    80105a81 <sys_sleep+0x61>
80105a5a:	eb 54                	jmp    80105ab0 <sys_sleep+0x90>
80105a5c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    if(myproc()->killed){
      release(&tickslock);
      return -1;
    }
    sleep(&ticks, &tickslock);
80105a60:	83 ec 08             	sub    $0x8,%esp
80105a63:	68 60 90 11 80       	push   $0x80119060
80105a68:	68 a0 98 11 80       	push   $0x801198a0
80105a6d:	e8 7e e7 ff ff       	call   801041f0 <sleep>
  while(ticks - ticks0 < n){
80105a72:	a1 a0 98 11 80       	mov    0x801198a0,%eax
80105a77:	83 c4 10             	add    $0x10,%esp
80105a7a:	29 d8                	sub    %ebx,%eax
80105a7c:	3b 45 f4             	cmp    -0xc(%ebp),%eax
80105a7f:	73 2f                	jae    80105ab0 <sys_sleep+0x90>
    if(myproc()->killed){
80105a81:	e8 aa e1 ff ff       	call   80103c30 <myproc>
80105a86:	8b 40 24             	mov    0x24(%eax),%eax
80105a89:	85 c0                	test   %eax,%eax
80105a8b:	74 d3                	je     80105a60 <sys_sleep+0x40>
      release(&tickslock);
80105a8d:	83 ec 0c             	sub    $0xc,%esp
80105a90:	68 60 90 11 80       	push   $0x80119060
80105a95:	e8 f6 ed ff ff       	call   80104890 <release>
      return -1;
80105a9a:	83 c4 10             	add    $0x10,%esp
80105a9d:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
  }
  release(&tickslock);
  return 0;
}
80105aa2:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80105aa5:	c9                   	leave  
80105aa6:	c3                   	ret    
80105aa7:	89 f6                	mov    %esi,%esi
80105aa9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi
  release(&tickslock);
80105ab0:	83 ec 0c             	sub    $0xc,%esp
80105ab3:	68 60 90 11 80       	push   $0x80119060
80105ab8:	e8 d3 ed ff ff       	call   80104890 <release>
  return 0;
80105abd:	83 c4 10             	add    $0x10,%esp
80105ac0:	31 c0                	xor    %eax,%eax
}
80105ac2:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80105ac5:	c9                   	leave  
80105ac6:	c3                   	ret    
    return -1;
80105ac7:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80105acc:	eb f4                	jmp    80105ac2 <sys_sleep+0xa2>
80105ace:	66 90                	xchg   %ax,%ax

80105ad0 <sys_uptime>:

// return how many clock tick interrupts have occurred
// since start.
int
sys_uptime(void)
{
80105ad0:	55                   	push   %ebp
80105ad1:	89 e5                	mov    %esp,%ebp
80105ad3:	53                   	push   %ebx
80105ad4:	83 ec 10             	sub    $0x10,%esp
  uint xticks;

  acquire(&tickslock);
80105ad7:	68 60 90 11 80       	push   $0x80119060
80105adc:	e8 ef ec ff ff       	call   801047d0 <acquire>
  xticks = ticks;
80105ae1:	8b 1d a0 98 11 80    	mov    0x801198a0,%ebx
  release(&tickslock);
80105ae7:	c7 04 24 60 90 11 80 	movl   $0x80119060,(%esp)
80105aee:	e8 9d ed ff ff       	call   80104890 <release>
  return xticks;
}
80105af3:	89 d8                	mov    %ebx,%eax
80105af5:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80105af8:	c9                   	leave  
80105af9:	c3                   	ret    

80105afa <alltraps>:

  # vectors.S sends all traps here.
.globl alltraps
alltraps:
  # Build trap frame.
  pushl %ds
80105afa:	1e                   	push   %ds
  pushl %es
80105afb:	06                   	push   %es
  pushl %fs
80105afc:	0f a0                	push   %fs
  pushl %gs
80105afe:	0f a8                	push   %gs
  pushal
80105b00:	60                   	pusha  
  
  # Set up data segments.
  movw $(SEG_KDATA<<3), %ax
80105b01:	66 b8 10 00          	mov    $0x10,%ax
  movw %ax, %ds
80105b05:	8e d8                	mov    %eax,%ds
  movw %ax, %es
80105b07:	8e c0                	mov    %eax,%es

  # Call trap(tf), where tf=%esp
  pushl %esp
80105b09:	54                   	push   %esp
  call trap
80105b0a:	e8 c1 00 00 00       	call   80105bd0 <trap>
  addl $4, %esp
80105b0f:	83 c4 04             	add    $0x4,%esp

80105b12 <trapret>:

  # Return falls through to trapret...
.globl trapret
trapret:
  popal
80105b12:	61                   	popa   
  popl %gs
80105b13:	0f a9                	pop    %gs
  popl %fs
80105b15:	0f a1                	pop    %fs
  popl %es
80105b17:	07                   	pop    %es
  popl %ds
80105b18:	1f                   	pop    %ds
  addl $0x8, %esp  # trapno and errcode
80105b19:	83 c4 08             	add    $0x8,%esp
  iret
80105b1c:	cf                   	iret   
80105b1d:	66 90                	xchg   %ax,%ax
80105b1f:	90                   	nop

80105b20 <tvinit>:
uint ticks;
extern int pageFaultHandler(char *va);

void
tvinit(void)
{
80105b20:	55                   	push   %ebp
  int i;

  for(i = 0; i < 256; i++)
80105b21:	31 c0                	xor    %eax,%eax
{
80105b23:	89 e5                	mov    %esp,%ebp
80105b25:	83 ec 08             	sub    $0x8,%esp
80105b28:	90                   	nop
80105b29:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    SETGATE(idt[i], 0, SEG_KCODE<<3, vectors[i], 0);
80105b30:	8b 14 85 08 b0 10 80 	mov    -0x7fef4ff8(,%eax,4),%edx
80105b37:	c7 04 c5 a2 90 11 80 	movl   $0x8e000008,-0x7fee6f5e(,%eax,8)
80105b3e:	08 00 00 8e 
80105b42:	66 89 14 c5 a0 90 11 	mov    %dx,-0x7fee6f60(,%eax,8)
80105b49:	80 
80105b4a:	c1 ea 10             	shr    $0x10,%edx
80105b4d:	66 89 14 c5 a6 90 11 	mov    %dx,-0x7fee6f5a(,%eax,8)
80105b54:	80 
  for(i = 0; i < 256; i++)
80105b55:	83 c0 01             	add    $0x1,%eax
80105b58:	3d 00 01 00 00       	cmp    $0x100,%eax
80105b5d:	75 d1                	jne    80105b30 <tvinit+0x10>
  SETGATE(idt[T_SYSCALL], 1, SEG_KCODE<<3, vectors[T_SYSCALL], DPL_USER);
80105b5f:	a1 08 b1 10 80       	mov    0x8010b108,%eax

  initlock(&tickslock, "time");
80105b64:	83 ec 08             	sub    $0x8,%esp
  SETGATE(idt[T_SYSCALL], 1, SEG_KCODE<<3, vectors[T_SYSCALL], DPL_USER);
80105b67:	c7 05 a2 92 11 80 08 	movl   $0xef000008,0x801192a2
80105b6e:	00 00 ef 
  initlock(&tickslock, "time");
80105b71:	68 35 7f 10 80       	push   $0x80107f35
80105b76:	68 60 90 11 80       	push   $0x80119060
  SETGATE(idt[T_SYSCALL], 1, SEG_KCODE<<3, vectors[T_SYSCALL], DPL_USER);
80105b7b:	66 a3 a0 92 11 80    	mov    %ax,0x801192a0
80105b81:	c1 e8 10             	shr    $0x10,%eax
80105b84:	66 a3 a6 92 11 80    	mov    %ax,0x801192a6
  initlock(&tickslock, "time");
80105b8a:	e8 01 eb ff ff       	call   80104690 <initlock>
}
80105b8f:	83 c4 10             	add    $0x10,%esp
80105b92:	c9                   	leave  
80105b93:	c3                   	ret    
80105b94:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
80105b9a:	8d bf 00 00 00 00    	lea    0x0(%edi),%edi

80105ba0 <idtinit>:

void
idtinit(void)
{
80105ba0:	55                   	push   %ebp
  pd[0] = size-1;
80105ba1:	b8 ff 07 00 00       	mov    $0x7ff,%eax
80105ba6:	89 e5                	mov    %esp,%ebp
80105ba8:	83 ec 10             	sub    $0x10,%esp
80105bab:	66 89 45 fa          	mov    %ax,-0x6(%ebp)
  pd[1] = (uint)p;
80105baf:	b8 a0 90 11 80       	mov    $0x801190a0,%eax
80105bb4:	66 89 45 fc          	mov    %ax,-0x4(%ebp)
  pd[2] = (uint)p >> 16;
80105bb8:	c1 e8 10             	shr    $0x10,%eax
80105bbb:	66 89 45 fe          	mov    %ax,-0x2(%ebp)
  asm volatile("lidt (%0)" : : "r" (pd));
80105bbf:	8d 45 fa             	lea    -0x6(%ebp),%eax
80105bc2:	0f 01 18             	lidtl  (%eax)
  lidt(idt, sizeof(idt));
}
80105bc5:	c9                   	leave  
80105bc6:	c3                   	ret    
80105bc7:	89 f6                	mov    %esi,%esi
80105bc9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80105bd0 <trap>:

//PAGEBREAK: 41
void
trap(struct trapframe *tf)
{
80105bd0:	55                   	push   %ebp
80105bd1:	89 e5                	mov    %esp,%ebp
80105bd3:	57                   	push   %edi
80105bd4:	56                   	push   %esi
80105bd5:	53                   	push   %ebx
80105bd6:	83 ec 1c             	sub    $0x1c,%esp
80105bd9:	8b 7d 08             	mov    0x8(%ebp),%edi
  if(tf->trapno == T_SYSCALL){
80105bdc:	8b 47 30             	mov    0x30(%edi),%eax
80105bdf:	83 f8 40             	cmp    $0x40,%eax
80105be2:	0f 84 f0 00 00 00    	je     80105cd8 <trap+0x108>
    if(myproc()->killed)
      exit();
    return;
  }

  switch(tf->trapno){
80105be8:	83 e8 0e             	sub    $0xe,%eax
80105beb:	83 f8 31             	cmp    $0x31,%eax
80105bee:	77 10                	ja     80105c00 <trap+0x30>
80105bf0:	ff 24 85 dc 7f 10 80 	jmp    *-0x7fef8024(,%eax,4)
80105bf7:	89 f6                	mov    %esi,%esi
80105bf9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi
    break;
    

  //PAGEBREAK: 13
  default:
    if(myproc() == 0 || (tf->cs&3) == 0){
80105c00:	e8 2b e0 ff ff       	call   80103c30 <myproc>
80105c05:	85 c0                	test   %eax,%eax
80105c07:	0f 84 27 02 00 00    	je     80105e34 <trap+0x264>
80105c0d:	f6 47 3c 03          	testb  $0x3,0x3c(%edi)
80105c11:	0f 84 1d 02 00 00    	je     80105e34 <trap+0x264>

static inline uint
rcr2(void)
{
  uint val;
  asm volatile("movl %%cr2,%0" : "=r" (val));
80105c17:	0f 20 d1             	mov    %cr2,%ecx
      cprintf("unexpected trap %d from cpu %d eip %x (cr2=0x%x)\n",
              tf->trapno, cpuid(), tf->eip, rcr2());
      panic("trap");
    }
    // In user space, assume process misbehaved.
    cprintf("pid %d %s: trap %d err %d on cpu %d "
80105c1a:	8b 57 38             	mov    0x38(%edi),%edx
80105c1d:	89 4d d8             	mov    %ecx,-0x28(%ebp)
80105c20:	89 55 dc             	mov    %edx,-0x24(%ebp)
80105c23:	e8 e8 df ff ff       	call   80103c10 <cpuid>
80105c28:	8b 77 34             	mov    0x34(%edi),%esi
80105c2b:	8b 5f 30             	mov    0x30(%edi),%ebx
80105c2e:	89 45 e4             	mov    %eax,-0x1c(%ebp)
            "eip 0x%x addr 0x%x--kill proc\n",
            myproc()->pid, myproc()->name, tf->trapno,
80105c31:	e8 fa df ff ff       	call   80103c30 <myproc>
80105c36:	89 45 e0             	mov    %eax,-0x20(%ebp)
80105c39:	e8 f2 df ff ff       	call   80103c30 <myproc>
    cprintf("pid %d %s: trap %d err %d on cpu %d "
80105c3e:	8b 4d d8             	mov    -0x28(%ebp),%ecx
80105c41:	8b 55 dc             	mov    -0x24(%ebp),%edx
80105c44:	51                   	push   %ecx
80105c45:	52                   	push   %edx
            myproc()->pid, myproc()->name, tf->trapno,
80105c46:	8b 55 e0             	mov    -0x20(%ebp),%edx
    cprintf("pid %d %s: trap %d err %d on cpu %d "
80105c49:	ff 75 e4             	pushl  -0x1c(%ebp)
80105c4c:	56                   	push   %esi
80105c4d:	53                   	push   %ebx
            myproc()->pid, myproc()->name, tf->trapno,
80105c4e:	83 c2 6c             	add    $0x6c,%edx
    cprintf("pid %d %s: trap %d err %d on cpu %d "
80105c51:	52                   	push   %edx
80105c52:	ff 70 10             	pushl  0x10(%eax)
80105c55:	68 98 7f 10 80       	push   $0x80107f98
80105c5a:	e8 01 aa ff ff       	call   80100660 <cprintf>
            tf->err, cpuid(), tf->eip, rcr2());
    myproc()->killed = 1;
80105c5f:	83 c4 20             	add    $0x20,%esp
80105c62:	e8 c9 df ff ff       	call   80103c30 <myproc>
80105c67:	c7 40 24 01 00 00 00 	movl   $0x1,0x24(%eax)
80105c6e:	66 90                	xchg   %ax,%ax
  }

  // Force process exit if it has been killed and is in user space.
  // (If it is still executing in the kernel, let it keep running
  // until it gets to the regular system call return.)
  if(myproc() && myproc()->killed && (tf->cs&3) == DPL_USER)
80105c70:	e8 bb df ff ff       	call   80103c30 <myproc>
80105c75:	85 c0                	test   %eax,%eax
80105c77:	74 1d                	je     80105c96 <trap+0xc6>
80105c79:	e8 b2 df ff ff       	call   80103c30 <myproc>
80105c7e:	8b 50 24             	mov    0x24(%eax),%edx
80105c81:	85 d2                	test   %edx,%edx
80105c83:	74 11                	je     80105c96 <trap+0xc6>
80105c85:	0f b7 47 3c          	movzwl 0x3c(%edi),%eax
80105c89:	83 e0 03             	and    $0x3,%eax
80105c8c:	66 83 f8 03          	cmp    $0x3,%ax
80105c90:	0f 84 5a 01 00 00    	je     80105df0 <trap+0x220>
    exit();

  // Force process to give up CPU on clock tick.
  // If interrupts were on while locks held, would need to check nlock.
  if(myproc() && myproc()->state == RUNNING &&
80105c96:	e8 95 df ff ff       	call   80103c30 <myproc>
80105c9b:	85 c0                	test   %eax,%eax
80105c9d:	74 0b                	je     80105caa <trap+0xda>
80105c9f:	e8 8c df ff ff       	call   80103c30 <myproc>
80105ca4:	83 78 0c 04          	cmpl   $0x4,0xc(%eax)
80105ca8:	74 66                	je     80105d10 <trap+0x140>
     tf->trapno == T_IRQ0+IRQ_TIMER)
    yield();

  // Check if the process has been killed since we yielded
  if(myproc() && myproc()->killed && (tf->cs&3) == DPL_USER)
80105caa:	e8 81 df ff ff       	call   80103c30 <myproc>
80105caf:	85 c0                	test   %eax,%eax
80105cb1:	74 19                	je     80105ccc <trap+0xfc>
80105cb3:	e8 78 df ff ff       	call   80103c30 <myproc>
80105cb8:	8b 40 24             	mov    0x24(%eax),%eax
80105cbb:	85 c0                	test   %eax,%eax
80105cbd:	74 0d                	je     80105ccc <trap+0xfc>
80105cbf:	0f b7 47 3c          	movzwl 0x3c(%edi),%eax
80105cc3:	83 e0 03             	and    $0x3,%eax
80105cc6:	66 83 f8 03          	cmp    $0x3,%ax
80105cca:	74 35                	je     80105d01 <trap+0x131>
    exit();
}
80105ccc:	8d 65 f4             	lea    -0xc(%ebp),%esp
80105ccf:	5b                   	pop    %ebx
80105cd0:	5e                   	pop    %esi
80105cd1:	5f                   	pop    %edi
80105cd2:	5d                   	pop    %ebp
80105cd3:	c3                   	ret    
80105cd4:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    if(myproc()->killed)
80105cd8:	e8 53 df ff ff       	call   80103c30 <myproc>
80105cdd:	8b 58 24             	mov    0x24(%eax),%ebx
80105ce0:	85 db                	test   %ebx,%ebx
80105ce2:	0f 85 f8 00 00 00    	jne    80105de0 <trap+0x210>
    myproc()->tf = tf;
80105ce8:	e8 43 df ff ff       	call   80103c30 <myproc>
80105ced:	89 78 18             	mov    %edi,0x18(%eax)
    syscall();
80105cf0:	e8 db ef ff ff       	call   80104cd0 <syscall>
    if(myproc()->killed)
80105cf5:	e8 36 df ff ff       	call   80103c30 <myproc>
80105cfa:	8b 48 24             	mov    0x24(%eax),%ecx
80105cfd:	85 c9                	test   %ecx,%ecx
80105cff:	74 cb                	je     80105ccc <trap+0xfc>
}
80105d01:	8d 65 f4             	lea    -0xc(%ebp),%esp
80105d04:	5b                   	pop    %ebx
80105d05:	5e                   	pop    %esi
80105d06:	5f                   	pop    %edi
80105d07:	5d                   	pop    %ebp
      exit();
80105d08:	e9 43 e3 ff ff       	jmp    80104050 <exit>
80105d0d:	8d 76 00             	lea    0x0(%esi),%esi
  if(myproc() && myproc()->state == RUNNING &&
80105d10:	83 7f 30 20          	cmpl   $0x20,0x30(%edi)
80105d14:	75 94                	jne    80105caa <trap+0xda>
    yield();
80105d16:	e8 85 e4 ff ff       	call   801041a0 <yield>
80105d1b:	eb 8d                	jmp    80105caa <trap+0xda>
80105d1d:	8d 76 00             	lea    0x0(%esi),%esi
    if(myproc()!=0 && pageFaultHandler((char*)rcr2())==-1)
80105d20:	e8 0b df ff ff       	call   80103c30 <myproc>
80105d25:	85 c0                	test   %eax,%eax
80105d27:	0f 84 43 ff ff ff    	je     80105c70 <trap+0xa0>
80105d2d:	0f 20 d0             	mov    %cr2,%eax
80105d30:	83 ec 0c             	sub    $0xc,%esp
80105d33:	50                   	push   %eax
80105d34:	e8 57 15 00 00       	call   80107290 <pageFaultHandler>
80105d39:	83 c4 10             	add    $0x10,%esp
80105d3c:	83 f8 ff             	cmp    $0xffffffff,%eax
80105d3f:	0f 85 2b ff ff ff    	jne    80105c70 <trap+0xa0>
80105d45:	eb 16                	jmp    80105d5d <trap+0x18d>
80105d47:	89 f6                	mov    %esi,%esi
80105d49:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi
    if(cpuid() == 0){
80105d50:	e8 bb de ff ff       	call   80103c10 <cpuid>
80105d55:	85 c0                	test   %eax,%eax
80105d57:	0f 84 a3 00 00 00    	je     80105e00 <trap+0x230>
    lapiceoi();
80105d5d:	e8 ee cd ff ff       	call   80102b50 <lapiceoi>
  if(myproc() && myproc()->killed && (tf->cs&3) == DPL_USER)
80105d62:	e8 c9 de ff ff       	call   80103c30 <myproc>
80105d67:	85 c0                	test   %eax,%eax
80105d69:	0f 85 0a ff ff ff    	jne    80105c79 <trap+0xa9>
80105d6f:	e9 22 ff ff ff       	jmp    80105c96 <trap+0xc6>
80105d74:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    uartintr();
80105d78:	e8 53 02 00 00       	call   80105fd0 <uartintr>
    lapiceoi();
80105d7d:	e8 ce cd ff ff       	call   80102b50 <lapiceoi>
    break;
80105d82:	e9 e9 fe ff ff       	jmp    80105c70 <trap+0xa0>
80105d87:	89 f6                	mov    %esi,%esi
80105d89:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi
    cprintf("cpu%d: spurious interrupt at %x:%x\n",
80105d90:	0f b7 5f 3c          	movzwl 0x3c(%edi),%ebx
80105d94:	8b 77 38             	mov    0x38(%edi),%esi
80105d97:	e8 74 de ff ff       	call   80103c10 <cpuid>
80105d9c:	56                   	push   %esi
80105d9d:	53                   	push   %ebx
80105d9e:	50                   	push   %eax
80105d9f:	68 40 7f 10 80       	push   $0x80107f40
80105da4:	e8 b7 a8 ff ff       	call   80100660 <cprintf>
    lapiceoi();
80105da9:	e8 a2 cd ff ff       	call   80102b50 <lapiceoi>
    break;
80105dae:	83 c4 10             	add    $0x10,%esp
80105db1:	e9 ba fe ff ff       	jmp    80105c70 <trap+0xa0>
80105db6:	8d 76 00             	lea    0x0(%esi),%esi
80105db9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi
    kbdintr();
80105dc0:	e8 4b cc ff ff       	call   80102a10 <kbdintr>
    lapiceoi();
80105dc5:	e8 86 cd ff ff       	call   80102b50 <lapiceoi>
    break;
80105dca:	e9 a1 fe ff ff       	jmp    80105c70 <trap+0xa0>
80105dcf:	90                   	nop
    ideintr();
80105dd0:	e8 3b c6 ff ff       	call   80102410 <ideintr>
80105dd5:	eb 86                	jmp    80105d5d <trap+0x18d>
80105dd7:	89 f6                	mov    %esi,%esi
80105dd9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi
      exit();
80105de0:	e8 6b e2 ff ff       	call   80104050 <exit>
80105de5:	e9 fe fe ff ff       	jmp    80105ce8 <trap+0x118>
80105dea:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    exit();
80105df0:	e8 5b e2 ff ff       	call   80104050 <exit>
80105df5:	e9 9c fe ff ff       	jmp    80105c96 <trap+0xc6>
80105dfa:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
      acquire(&tickslock);
80105e00:	83 ec 0c             	sub    $0xc,%esp
80105e03:	68 60 90 11 80       	push   $0x80119060
80105e08:	e8 c3 e9 ff ff       	call   801047d0 <acquire>
      wakeup(&ticks);
80105e0d:	c7 04 24 a0 98 11 80 	movl   $0x801198a0,(%esp)
      ticks++;
80105e14:	83 05 a0 98 11 80 01 	addl   $0x1,0x801198a0
      wakeup(&ticks);
80105e1b:	e8 90 e5 ff ff       	call   801043b0 <wakeup>
      release(&tickslock);
80105e20:	c7 04 24 60 90 11 80 	movl   $0x80119060,(%esp)
80105e27:	e8 64 ea ff ff       	call   80104890 <release>
80105e2c:	83 c4 10             	add    $0x10,%esp
80105e2f:	e9 29 ff ff ff       	jmp    80105d5d <trap+0x18d>
80105e34:	0f 20 d6             	mov    %cr2,%esi
      cprintf("unexpected trap %d from cpu %d eip %x (cr2=0x%x)\n",
80105e37:	8b 5f 38             	mov    0x38(%edi),%ebx
80105e3a:	e8 d1 dd ff ff       	call   80103c10 <cpuid>
80105e3f:	83 ec 0c             	sub    $0xc,%esp
80105e42:	56                   	push   %esi
80105e43:	53                   	push   %ebx
80105e44:	50                   	push   %eax
80105e45:	ff 77 30             	pushl  0x30(%edi)
80105e48:	68 64 7f 10 80       	push   $0x80107f64
80105e4d:	e8 0e a8 ff ff       	call   80100660 <cprintf>
      panic("trap");
80105e52:	83 c4 14             	add    $0x14,%esp
80105e55:	68 3a 7f 10 80       	push   $0x80107f3a
80105e5a:	e8 31 a5 ff ff       	call   80100390 <panic>
80105e5f:	90                   	nop

80105e60 <uartgetc>:
}

static int
uartgetc(void)
{
  if(!uart)
80105e60:	a1 bc b5 10 80       	mov    0x8010b5bc,%eax
{
80105e65:	55                   	push   %ebp
80105e66:	89 e5                	mov    %esp,%ebp
  if(!uart)
80105e68:	85 c0                	test   %eax,%eax
80105e6a:	74 1c                	je     80105e88 <uartgetc+0x28>
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80105e6c:	ba fd 03 00 00       	mov    $0x3fd,%edx
80105e71:	ec                   	in     (%dx),%al
    return -1;
  if(!(inb(COM1+5) & 0x01))
80105e72:	a8 01                	test   $0x1,%al
80105e74:	74 12                	je     80105e88 <uartgetc+0x28>
80105e76:	ba f8 03 00 00       	mov    $0x3f8,%edx
80105e7b:	ec                   	in     (%dx),%al
    return -1;
  return inb(COM1+0);
80105e7c:	0f b6 c0             	movzbl %al,%eax
}
80105e7f:	5d                   	pop    %ebp
80105e80:	c3                   	ret    
80105e81:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    return -1;
80105e88:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80105e8d:	5d                   	pop    %ebp
80105e8e:	c3                   	ret    
80105e8f:	90                   	nop

80105e90 <uartputc.part.0>:
uartputc(int c)
80105e90:	55                   	push   %ebp
80105e91:	89 e5                	mov    %esp,%ebp
80105e93:	57                   	push   %edi
80105e94:	56                   	push   %esi
80105e95:	53                   	push   %ebx
80105e96:	89 c7                	mov    %eax,%edi
80105e98:	bb 80 00 00 00       	mov    $0x80,%ebx
80105e9d:	be fd 03 00 00       	mov    $0x3fd,%esi
80105ea2:	83 ec 0c             	sub    $0xc,%esp
80105ea5:	eb 1b                	jmp    80105ec2 <uartputc.part.0+0x32>
80105ea7:	89 f6                	mov    %esi,%esi
80105ea9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi
    microdelay(10);
80105eb0:	83 ec 0c             	sub    $0xc,%esp
80105eb3:	6a 0a                	push   $0xa
80105eb5:	e8 b6 cc ff ff       	call   80102b70 <microdelay>
  for(i = 0; i < 128 && !(inb(COM1+5) & 0x20); i++)
80105eba:	83 c4 10             	add    $0x10,%esp
80105ebd:	83 eb 01             	sub    $0x1,%ebx
80105ec0:	74 07                	je     80105ec9 <uartputc.part.0+0x39>
80105ec2:	89 f2                	mov    %esi,%edx
80105ec4:	ec                   	in     (%dx),%al
80105ec5:	a8 20                	test   $0x20,%al
80105ec7:	74 e7                	je     80105eb0 <uartputc.part.0+0x20>
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80105ec9:	ba f8 03 00 00       	mov    $0x3f8,%edx
80105ece:	89 f8                	mov    %edi,%eax
80105ed0:	ee                   	out    %al,(%dx)
}
80105ed1:	8d 65 f4             	lea    -0xc(%ebp),%esp
80105ed4:	5b                   	pop    %ebx
80105ed5:	5e                   	pop    %esi
80105ed6:	5f                   	pop    %edi
80105ed7:	5d                   	pop    %ebp
80105ed8:	c3                   	ret    
80105ed9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

80105ee0 <uartinit>:
{
80105ee0:	55                   	push   %ebp
80105ee1:	31 c9                	xor    %ecx,%ecx
80105ee3:	89 c8                	mov    %ecx,%eax
80105ee5:	89 e5                	mov    %esp,%ebp
80105ee7:	57                   	push   %edi
80105ee8:	56                   	push   %esi
80105ee9:	53                   	push   %ebx
80105eea:	bb fa 03 00 00       	mov    $0x3fa,%ebx
80105eef:	89 da                	mov    %ebx,%edx
80105ef1:	83 ec 0c             	sub    $0xc,%esp
80105ef4:	ee                   	out    %al,(%dx)
80105ef5:	bf fb 03 00 00       	mov    $0x3fb,%edi
80105efa:	b8 80 ff ff ff       	mov    $0xffffff80,%eax
80105eff:	89 fa                	mov    %edi,%edx
80105f01:	ee                   	out    %al,(%dx)
80105f02:	b8 0c 00 00 00       	mov    $0xc,%eax
80105f07:	ba f8 03 00 00       	mov    $0x3f8,%edx
80105f0c:	ee                   	out    %al,(%dx)
80105f0d:	be f9 03 00 00       	mov    $0x3f9,%esi
80105f12:	89 c8                	mov    %ecx,%eax
80105f14:	89 f2                	mov    %esi,%edx
80105f16:	ee                   	out    %al,(%dx)
80105f17:	b8 03 00 00 00       	mov    $0x3,%eax
80105f1c:	89 fa                	mov    %edi,%edx
80105f1e:	ee                   	out    %al,(%dx)
80105f1f:	ba fc 03 00 00       	mov    $0x3fc,%edx
80105f24:	89 c8                	mov    %ecx,%eax
80105f26:	ee                   	out    %al,(%dx)
80105f27:	b8 01 00 00 00       	mov    $0x1,%eax
80105f2c:	89 f2                	mov    %esi,%edx
80105f2e:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80105f2f:	ba fd 03 00 00       	mov    $0x3fd,%edx
80105f34:	ec                   	in     (%dx),%al
  if(inb(COM1+5) == 0xFF)
80105f35:	3c ff                	cmp    $0xff,%al
80105f37:	74 5a                	je     80105f93 <uartinit+0xb3>
  uart = 1;
80105f39:	c7 05 bc b5 10 80 01 	movl   $0x1,0x8010b5bc
80105f40:	00 00 00 
80105f43:	89 da                	mov    %ebx,%edx
80105f45:	ec                   	in     (%dx),%al
80105f46:	ba f8 03 00 00       	mov    $0x3f8,%edx
80105f4b:	ec                   	in     (%dx),%al
  ioapicenable(IRQ_COM1, 0);
80105f4c:	83 ec 08             	sub    $0x8,%esp
  for(p="xv6...\n"; *p; p++)
80105f4f:	bb a4 80 10 80       	mov    $0x801080a4,%ebx
  ioapicenable(IRQ_COM1, 0);
80105f54:	6a 00                	push   $0x0
80105f56:	6a 04                	push   $0x4
80105f58:	e8 03 c7 ff ff       	call   80102660 <ioapicenable>
80105f5d:	83 c4 10             	add    $0x10,%esp
  for(p="xv6...\n"; *p; p++)
80105f60:	b8 78 00 00 00       	mov    $0x78,%eax
80105f65:	eb 13                	jmp    80105f7a <uartinit+0x9a>
80105f67:	89 f6                	mov    %esi,%esi
80105f69:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi
80105f70:	83 c3 01             	add    $0x1,%ebx
80105f73:	0f be 03             	movsbl (%ebx),%eax
80105f76:	84 c0                	test   %al,%al
80105f78:	74 19                	je     80105f93 <uartinit+0xb3>
  if(!uart)
80105f7a:	8b 15 bc b5 10 80    	mov    0x8010b5bc,%edx
80105f80:	85 d2                	test   %edx,%edx
80105f82:	74 ec                	je     80105f70 <uartinit+0x90>
  for(p="xv6...\n"; *p; p++)
80105f84:	83 c3 01             	add    $0x1,%ebx
80105f87:	e8 04 ff ff ff       	call   80105e90 <uartputc.part.0>
80105f8c:	0f be 03             	movsbl (%ebx),%eax
80105f8f:	84 c0                	test   %al,%al
80105f91:	75 e7                	jne    80105f7a <uartinit+0x9a>
}
80105f93:	8d 65 f4             	lea    -0xc(%ebp),%esp
80105f96:	5b                   	pop    %ebx
80105f97:	5e                   	pop    %esi
80105f98:	5f                   	pop    %edi
80105f99:	5d                   	pop    %ebp
80105f9a:	c3                   	ret    
80105f9b:	90                   	nop
80105f9c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

80105fa0 <uartputc>:
  if(!uart)
80105fa0:	8b 15 bc b5 10 80    	mov    0x8010b5bc,%edx
{
80105fa6:	55                   	push   %ebp
80105fa7:	89 e5                	mov    %esp,%ebp
  if(!uart)
80105fa9:	85 d2                	test   %edx,%edx
{
80105fab:	8b 45 08             	mov    0x8(%ebp),%eax
  if(!uart)
80105fae:	74 10                	je     80105fc0 <uartputc+0x20>
}
80105fb0:	5d                   	pop    %ebp
80105fb1:	e9 da fe ff ff       	jmp    80105e90 <uartputc.part.0>
80105fb6:	8d 76 00             	lea    0x0(%esi),%esi
80105fb9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi
80105fc0:	5d                   	pop    %ebp
80105fc1:	c3                   	ret    
80105fc2:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80105fc9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80105fd0 <uartintr>:

void
uartintr(void)
{
80105fd0:	55                   	push   %ebp
80105fd1:	89 e5                	mov    %esp,%ebp
80105fd3:	83 ec 14             	sub    $0x14,%esp
  consoleintr(uartgetc);
80105fd6:	68 60 5e 10 80       	push   $0x80105e60
80105fdb:	e8 30 a8 ff ff       	call   80100810 <consoleintr>
}
80105fe0:	83 c4 10             	add    $0x10,%esp
80105fe3:	c9                   	leave  
80105fe4:	c3                   	ret    

80105fe5 <vector0>:
# generated by vectors.pl - do not edit
# handlers
.globl alltraps
.globl vector0
vector0:
  pushl $0
80105fe5:	6a 00                	push   $0x0
  pushl $0
80105fe7:	6a 00                	push   $0x0
  jmp alltraps
80105fe9:	e9 0c fb ff ff       	jmp    80105afa <alltraps>

80105fee <vector1>:
.globl vector1
vector1:
  pushl $0
80105fee:	6a 00                	push   $0x0
  pushl $1
80105ff0:	6a 01                	push   $0x1
  jmp alltraps
80105ff2:	e9 03 fb ff ff       	jmp    80105afa <alltraps>

80105ff7 <vector2>:
.globl vector2
vector2:
  pushl $0
80105ff7:	6a 00                	push   $0x0
  pushl $2
80105ff9:	6a 02                	push   $0x2
  jmp alltraps
80105ffb:	e9 fa fa ff ff       	jmp    80105afa <alltraps>

80106000 <vector3>:
.globl vector3
vector3:
  pushl $0
80106000:	6a 00                	push   $0x0
  pushl $3
80106002:	6a 03                	push   $0x3
  jmp alltraps
80106004:	e9 f1 fa ff ff       	jmp    80105afa <alltraps>

80106009 <vector4>:
.globl vector4
vector4:
  pushl $0
80106009:	6a 00                	push   $0x0
  pushl $4
8010600b:	6a 04                	push   $0x4
  jmp alltraps
8010600d:	e9 e8 fa ff ff       	jmp    80105afa <alltraps>

80106012 <vector5>:
.globl vector5
vector5:
  pushl $0
80106012:	6a 00                	push   $0x0
  pushl $5
80106014:	6a 05                	push   $0x5
  jmp alltraps
80106016:	e9 df fa ff ff       	jmp    80105afa <alltraps>

8010601b <vector6>:
.globl vector6
vector6:
  pushl $0
8010601b:	6a 00                	push   $0x0
  pushl $6
8010601d:	6a 06                	push   $0x6
  jmp alltraps
8010601f:	e9 d6 fa ff ff       	jmp    80105afa <alltraps>

80106024 <vector7>:
.globl vector7
vector7:
  pushl $0
80106024:	6a 00                	push   $0x0
  pushl $7
80106026:	6a 07                	push   $0x7
  jmp alltraps
80106028:	e9 cd fa ff ff       	jmp    80105afa <alltraps>

8010602d <vector8>:
.globl vector8
vector8:
  pushl $8
8010602d:	6a 08                	push   $0x8
  jmp alltraps
8010602f:	e9 c6 fa ff ff       	jmp    80105afa <alltraps>

80106034 <vector9>:
.globl vector9
vector9:
  pushl $0
80106034:	6a 00                	push   $0x0
  pushl $9
80106036:	6a 09                	push   $0x9
  jmp alltraps
80106038:	e9 bd fa ff ff       	jmp    80105afa <alltraps>

8010603d <vector10>:
.globl vector10
vector10:
  pushl $10
8010603d:	6a 0a                	push   $0xa
  jmp alltraps
8010603f:	e9 b6 fa ff ff       	jmp    80105afa <alltraps>

80106044 <vector11>:
.globl vector11
vector11:
  pushl $11
80106044:	6a 0b                	push   $0xb
  jmp alltraps
80106046:	e9 af fa ff ff       	jmp    80105afa <alltraps>

8010604b <vector12>:
.globl vector12
vector12:
  pushl $12
8010604b:	6a 0c                	push   $0xc
  jmp alltraps
8010604d:	e9 a8 fa ff ff       	jmp    80105afa <alltraps>

80106052 <vector13>:
.globl vector13
vector13:
  pushl $13
80106052:	6a 0d                	push   $0xd
  jmp alltraps
80106054:	e9 a1 fa ff ff       	jmp    80105afa <alltraps>

80106059 <vector14>:
.globl vector14
vector14:
  pushl $14
80106059:	6a 0e                	push   $0xe
  jmp alltraps
8010605b:	e9 9a fa ff ff       	jmp    80105afa <alltraps>

80106060 <vector15>:
.globl vector15
vector15:
  pushl $0
80106060:	6a 00                	push   $0x0
  pushl $15
80106062:	6a 0f                	push   $0xf
  jmp alltraps
80106064:	e9 91 fa ff ff       	jmp    80105afa <alltraps>

80106069 <vector16>:
.globl vector16
vector16:
  pushl $0
80106069:	6a 00                	push   $0x0
  pushl $16
8010606b:	6a 10                	push   $0x10
  jmp alltraps
8010606d:	e9 88 fa ff ff       	jmp    80105afa <alltraps>

80106072 <vector17>:
.globl vector17
vector17:
  pushl $17
80106072:	6a 11                	push   $0x11
  jmp alltraps
80106074:	e9 81 fa ff ff       	jmp    80105afa <alltraps>

80106079 <vector18>:
.globl vector18
vector18:
  pushl $0
80106079:	6a 00                	push   $0x0
  pushl $18
8010607b:	6a 12                	push   $0x12
  jmp alltraps
8010607d:	e9 78 fa ff ff       	jmp    80105afa <alltraps>

80106082 <vector19>:
.globl vector19
vector19:
  pushl $0
80106082:	6a 00                	push   $0x0
  pushl $19
80106084:	6a 13                	push   $0x13
  jmp alltraps
80106086:	e9 6f fa ff ff       	jmp    80105afa <alltraps>

8010608b <vector20>:
.globl vector20
vector20:
  pushl $0
8010608b:	6a 00                	push   $0x0
  pushl $20
8010608d:	6a 14                	push   $0x14
  jmp alltraps
8010608f:	e9 66 fa ff ff       	jmp    80105afa <alltraps>

80106094 <vector21>:
.globl vector21
vector21:
  pushl $0
80106094:	6a 00                	push   $0x0
  pushl $21
80106096:	6a 15                	push   $0x15
  jmp alltraps
80106098:	e9 5d fa ff ff       	jmp    80105afa <alltraps>

8010609d <vector22>:
.globl vector22
vector22:
  pushl $0
8010609d:	6a 00                	push   $0x0
  pushl $22
8010609f:	6a 16                	push   $0x16
  jmp alltraps
801060a1:	e9 54 fa ff ff       	jmp    80105afa <alltraps>

801060a6 <vector23>:
.globl vector23
vector23:
  pushl $0
801060a6:	6a 00                	push   $0x0
  pushl $23
801060a8:	6a 17                	push   $0x17
  jmp alltraps
801060aa:	e9 4b fa ff ff       	jmp    80105afa <alltraps>

801060af <vector24>:
.globl vector24
vector24:
  pushl $0
801060af:	6a 00                	push   $0x0
  pushl $24
801060b1:	6a 18                	push   $0x18
  jmp alltraps
801060b3:	e9 42 fa ff ff       	jmp    80105afa <alltraps>

801060b8 <vector25>:
.globl vector25
vector25:
  pushl $0
801060b8:	6a 00                	push   $0x0
  pushl $25
801060ba:	6a 19                	push   $0x19
  jmp alltraps
801060bc:	e9 39 fa ff ff       	jmp    80105afa <alltraps>

801060c1 <vector26>:
.globl vector26
vector26:
  pushl $0
801060c1:	6a 00                	push   $0x0
  pushl $26
801060c3:	6a 1a                	push   $0x1a
  jmp alltraps
801060c5:	e9 30 fa ff ff       	jmp    80105afa <alltraps>

801060ca <vector27>:
.globl vector27
vector27:
  pushl $0
801060ca:	6a 00                	push   $0x0
  pushl $27
801060cc:	6a 1b                	push   $0x1b
  jmp alltraps
801060ce:	e9 27 fa ff ff       	jmp    80105afa <alltraps>

801060d3 <vector28>:
.globl vector28
vector28:
  pushl $0
801060d3:	6a 00                	push   $0x0
  pushl $28
801060d5:	6a 1c                	push   $0x1c
  jmp alltraps
801060d7:	e9 1e fa ff ff       	jmp    80105afa <alltraps>

801060dc <vector29>:
.globl vector29
vector29:
  pushl $0
801060dc:	6a 00                	push   $0x0
  pushl $29
801060de:	6a 1d                	push   $0x1d
  jmp alltraps
801060e0:	e9 15 fa ff ff       	jmp    80105afa <alltraps>

801060e5 <vector30>:
.globl vector30
vector30:
  pushl $0
801060e5:	6a 00                	push   $0x0
  pushl $30
801060e7:	6a 1e                	push   $0x1e
  jmp alltraps
801060e9:	e9 0c fa ff ff       	jmp    80105afa <alltraps>

801060ee <vector31>:
.globl vector31
vector31:
  pushl $0
801060ee:	6a 00                	push   $0x0
  pushl $31
801060f0:	6a 1f                	push   $0x1f
  jmp alltraps
801060f2:	e9 03 fa ff ff       	jmp    80105afa <alltraps>

801060f7 <vector32>:
.globl vector32
vector32:
  pushl $0
801060f7:	6a 00                	push   $0x0
  pushl $32
801060f9:	6a 20                	push   $0x20
  jmp alltraps
801060fb:	e9 fa f9 ff ff       	jmp    80105afa <alltraps>

80106100 <vector33>:
.globl vector33
vector33:
  pushl $0
80106100:	6a 00                	push   $0x0
  pushl $33
80106102:	6a 21                	push   $0x21
  jmp alltraps
80106104:	e9 f1 f9 ff ff       	jmp    80105afa <alltraps>

80106109 <vector34>:
.globl vector34
vector34:
  pushl $0
80106109:	6a 00                	push   $0x0
  pushl $34
8010610b:	6a 22                	push   $0x22
  jmp alltraps
8010610d:	e9 e8 f9 ff ff       	jmp    80105afa <alltraps>

80106112 <vector35>:
.globl vector35
vector35:
  pushl $0
80106112:	6a 00                	push   $0x0
  pushl $35
80106114:	6a 23                	push   $0x23
  jmp alltraps
80106116:	e9 df f9 ff ff       	jmp    80105afa <alltraps>

8010611b <vector36>:
.globl vector36
vector36:
  pushl $0
8010611b:	6a 00                	push   $0x0
  pushl $36
8010611d:	6a 24                	push   $0x24
  jmp alltraps
8010611f:	e9 d6 f9 ff ff       	jmp    80105afa <alltraps>

80106124 <vector37>:
.globl vector37
vector37:
  pushl $0
80106124:	6a 00                	push   $0x0
  pushl $37
80106126:	6a 25                	push   $0x25
  jmp alltraps
80106128:	e9 cd f9 ff ff       	jmp    80105afa <alltraps>

8010612d <vector38>:
.globl vector38
vector38:
  pushl $0
8010612d:	6a 00                	push   $0x0
  pushl $38
8010612f:	6a 26                	push   $0x26
  jmp alltraps
80106131:	e9 c4 f9 ff ff       	jmp    80105afa <alltraps>

80106136 <vector39>:
.globl vector39
vector39:
  pushl $0
80106136:	6a 00                	push   $0x0
  pushl $39
80106138:	6a 27                	push   $0x27
  jmp alltraps
8010613a:	e9 bb f9 ff ff       	jmp    80105afa <alltraps>

8010613f <vector40>:
.globl vector40
vector40:
  pushl $0
8010613f:	6a 00                	push   $0x0
  pushl $40
80106141:	6a 28                	push   $0x28
  jmp alltraps
80106143:	e9 b2 f9 ff ff       	jmp    80105afa <alltraps>

80106148 <vector41>:
.globl vector41
vector41:
  pushl $0
80106148:	6a 00                	push   $0x0
  pushl $41
8010614a:	6a 29                	push   $0x29
  jmp alltraps
8010614c:	e9 a9 f9 ff ff       	jmp    80105afa <alltraps>

80106151 <vector42>:
.globl vector42
vector42:
  pushl $0
80106151:	6a 00                	push   $0x0
  pushl $42
80106153:	6a 2a                	push   $0x2a
  jmp alltraps
80106155:	e9 a0 f9 ff ff       	jmp    80105afa <alltraps>

8010615a <vector43>:
.globl vector43
vector43:
  pushl $0
8010615a:	6a 00                	push   $0x0
  pushl $43
8010615c:	6a 2b                	push   $0x2b
  jmp alltraps
8010615e:	e9 97 f9 ff ff       	jmp    80105afa <alltraps>

80106163 <vector44>:
.globl vector44
vector44:
  pushl $0
80106163:	6a 00                	push   $0x0
  pushl $44
80106165:	6a 2c                	push   $0x2c
  jmp alltraps
80106167:	e9 8e f9 ff ff       	jmp    80105afa <alltraps>

8010616c <vector45>:
.globl vector45
vector45:
  pushl $0
8010616c:	6a 00                	push   $0x0
  pushl $45
8010616e:	6a 2d                	push   $0x2d
  jmp alltraps
80106170:	e9 85 f9 ff ff       	jmp    80105afa <alltraps>

80106175 <vector46>:
.globl vector46
vector46:
  pushl $0
80106175:	6a 00                	push   $0x0
  pushl $46
80106177:	6a 2e                	push   $0x2e
  jmp alltraps
80106179:	e9 7c f9 ff ff       	jmp    80105afa <alltraps>

8010617e <vector47>:
.globl vector47
vector47:
  pushl $0
8010617e:	6a 00                	push   $0x0
  pushl $47
80106180:	6a 2f                	push   $0x2f
  jmp alltraps
80106182:	e9 73 f9 ff ff       	jmp    80105afa <alltraps>

80106187 <vector48>:
.globl vector48
vector48:
  pushl $0
80106187:	6a 00                	push   $0x0
  pushl $48
80106189:	6a 30                	push   $0x30
  jmp alltraps
8010618b:	e9 6a f9 ff ff       	jmp    80105afa <alltraps>

80106190 <vector49>:
.globl vector49
vector49:
  pushl $0
80106190:	6a 00                	push   $0x0
  pushl $49
80106192:	6a 31                	push   $0x31
  jmp alltraps
80106194:	e9 61 f9 ff ff       	jmp    80105afa <alltraps>

80106199 <vector50>:
.globl vector50
vector50:
  pushl $0
80106199:	6a 00                	push   $0x0
  pushl $50
8010619b:	6a 32                	push   $0x32
  jmp alltraps
8010619d:	e9 58 f9 ff ff       	jmp    80105afa <alltraps>

801061a2 <vector51>:
.globl vector51
vector51:
  pushl $0
801061a2:	6a 00                	push   $0x0
  pushl $51
801061a4:	6a 33                	push   $0x33
  jmp alltraps
801061a6:	e9 4f f9 ff ff       	jmp    80105afa <alltraps>

801061ab <vector52>:
.globl vector52
vector52:
  pushl $0
801061ab:	6a 00                	push   $0x0
  pushl $52
801061ad:	6a 34                	push   $0x34
  jmp alltraps
801061af:	e9 46 f9 ff ff       	jmp    80105afa <alltraps>

801061b4 <vector53>:
.globl vector53
vector53:
  pushl $0
801061b4:	6a 00                	push   $0x0
  pushl $53
801061b6:	6a 35                	push   $0x35
  jmp alltraps
801061b8:	e9 3d f9 ff ff       	jmp    80105afa <alltraps>

801061bd <vector54>:
.globl vector54
vector54:
  pushl $0
801061bd:	6a 00                	push   $0x0
  pushl $54
801061bf:	6a 36                	push   $0x36
  jmp alltraps
801061c1:	e9 34 f9 ff ff       	jmp    80105afa <alltraps>

801061c6 <vector55>:
.globl vector55
vector55:
  pushl $0
801061c6:	6a 00                	push   $0x0
  pushl $55
801061c8:	6a 37                	push   $0x37
  jmp alltraps
801061ca:	e9 2b f9 ff ff       	jmp    80105afa <alltraps>

801061cf <vector56>:
.globl vector56
vector56:
  pushl $0
801061cf:	6a 00                	push   $0x0
  pushl $56
801061d1:	6a 38                	push   $0x38
  jmp alltraps
801061d3:	e9 22 f9 ff ff       	jmp    80105afa <alltraps>

801061d8 <vector57>:
.globl vector57
vector57:
  pushl $0
801061d8:	6a 00                	push   $0x0
  pushl $57
801061da:	6a 39                	push   $0x39
  jmp alltraps
801061dc:	e9 19 f9 ff ff       	jmp    80105afa <alltraps>

801061e1 <vector58>:
.globl vector58
vector58:
  pushl $0
801061e1:	6a 00                	push   $0x0
  pushl $58
801061e3:	6a 3a                	push   $0x3a
  jmp alltraps
801061e5:	e9 10 f9 ff ff       	jmp    80105afa <alltraps>

801061ea <vector59>:
.globl vector59
vector59:
  pushl $0
801061ea:	6a 00                	push   $0x0
  pushl $59
801061ec:	6a 3b                	push   $0x3b
  jmp alltraps
801061ee:	e9 07 f9 ff ff       	jmp    80105afa <alltraps>

801061f3 <vector60>:
.globl vector60
vector60:
  pushl $0
801061f3:	6a 00                	push   $0x0
  pushl $60
801061f5:	6a 3c                	push   $0x3c
  jmp alltraps
801061f7:	e9 fe f8 ff ff       	jmp    80105afa <alltraps>

801061fc <vector61>:
.globl vector61
vector61:
  pushl $0
801061fc:	6a 00                	push   $0x0
  pushl $61
801061fe:	6a 3d                	push   $0x3d
  jmp alltraps
80106200:	e9 f5 f8 ff ff       	jmp    80105afa <alltraps>

80106205 <vector62>:
.globl vector62
vector62:
  pushl $0
80106205:	6a 00                	push   $0x0
  pushl $62
80106207:	6a 3e                	push   $0x3e
  jmp alltraps
80106209:	e9 ec f8 ff ff       	jmp    80105afa <alltraps>

8010620e <vector63>:
.globl vector63
vector63:
  pushl $0
8010620e:	6a 00                	push   $0x0
  pushl $63
80106210:	6a 3f                	push   $0x3f
  jmp alltraps
80106212:	e9 e3 f8 ff ff       	jmp    80105afa <alltraps>

80106217 <vector64>:
.globl vector64
vector64:
  pushl $0
80106217:	6a 00                	push   $0x0
  pushl $64
80106219:	6a 40                	push   $0x40
  jmp alltraps
8010621b:	e9 da f8 ff ff       	jmp    80105afa <alltraps>

80106220 <vector65>:
.globl vector65
vector65:
  pushl $0
80106220:	6a 00                	push   $0x0
  pushl $65
80106222:	6a 41                	push   $0x41
  jmp alltraps
80106224:	e9 d1 f8 ff ff       	jmp    80105afa <alltraps>

80106229 <vector66>:
.globl vector66
vector66:
  pushl $0
80106229:	6a 00                	push   $0x0
  pushl $66
8010622b:	6a 42                	push   $0x42
  jmp alltraps
8010622d:	e9 c8 f8 ff ff       	jmp    80105afa <alltraps>

80106232 <vector67>:
.globl vector67
vector67:
  pushl $0
80106232:	6a 00                	push   $0x0
  pushl $67
80106234:	6a 43                	push   $0x43
  jmp alltraps
80106236:	e9 bf f8 ff ff       	jmp    80105afa <alltraps>

8010623b <vector68>:
.globl vector68
vector68:
  pushl $0
8010623b:	6a 00                	push   $0x0
  pushl $68
8010623d:	6a 44                	push   $0x44
  jmp alltraps
8010623f:	e9 b6 f8 ff ff       	jmp    80105afa <alltraps>

80106244 <vector69>:
.globl vector69
vector69:
  pushl $0
80106244:	6a 00                	push   $0x0
  pushl $69
80106246:	6a 45                	push   $0x45
  jmp alltraps
80106248:	e9 ad f8 ff ff       	jmp    80105afa <alltraps>

8010624d <vector70>:
.globl vector70
vector70:
  pushl $0
8010624d:	6a 00                	push   $0x0
  pushl $70
8010624f:	6a 46                	push   $0x46
  jmp alltraps
80106251:	e9 a4 f8 ff ff       	jmp    80105afa <alltraps>

80106256 <vector71>:
.globl vector71
vector71:
  pushl $0
80106256:	6a 00                	push   $0x0
  pushl $71
80106258:	6a 47                	push   $0x47
  jmp alltraps
8010625a:	e9 9b f8 ff ff       	jmp    80105afa <alltraps>

8010625f <vector72>:
.globl vector72
vector72:
  pushl $0
8010625f:	6a 00                	push   $0x0
  pushl $72
80106261:	6a 48                	push   $0x48
  jmp alltraps
80106263:	e9 92 f8 ff ff       	jmp    80105afa <alltraps>

80106268 <vector73>:
.globl vector73
vector73:
  pushl $0
80106268:	6a 00                	push   $0x0
  pushl $73
8010626a:	6a 49                	push   $0x49
  jmp alltraps
8010626c:	e9 89 f8 ff ff       	jmp    80105afa <alltraps>

80106271 <vector74>:
.globl vector74
vector74:
  pushl $0
80106271:	6a 00                	push   $0x0
  pushl $74
80106273:	6a 4a                	push   $0x4a
  jmp alltraps
80106275:	e9 80 f8 ff ff       	jmp    80105afa <alltraps>

8010627a <vector75>:
.globl vector75
vector75:
  pushl $0
8010627a:	6a 00                	push   $0x0
  pushl $75
8010627c:	6a 4b                	push   $0x4b
  jmp alltraps
8010627e:	e9 77 f8 ff ff       	jmp    80105afa <alltraps>

80106283 <vector76>:
.globl vector76
vector76:
  pushl $0
80106283:	6a 00                	push   $0x0
  pushl $76
80106285:	6a 4c                	push   $0x4c
  jmp alltraps
80106287:	e9 6e f8 ff ff       	jmp    80105afa <alltraps>

8010628c <vector77>:
.globl vector77
vector77:
  pushl $0
8010628c:	6a 00                	push   $0x0
  pushl $77
8010628e:	6a 4d                	push   $0x4d
  jmp alltraps
80106290:	e9 65 f8 ff ff       	jmp    80105afa <alltraps>

80106295 <vector78>:
.globl vector78
vector78:
  pushl $0
80106295:	6a 00                	push   $0x0
  pushl $78
80106297:	6a 4e                	push   $0x4e
  jmp alltraps
80106299:	e9 5c f8 ff ff       	jmp    80105afa <alltraps>

8010629e <vector79>:
.globl vector79
vector79:
  pushl $0
8010629e:	6a 00                	push   $0x0
  pushl $79
801062a0:	6a 4f                	push   $0x4f
  jmp alltraps
801062a2:	e9 53 f8 ff ff       	jmp    80105afa <alltraps>

801062a7 <vector80>:
.globl vector80
vector80:
  pushl $0
801062a7:	6a 00                	push   $0x0
  pushl $80
801062a9:	6a 50                	push   $0x50
  jmp alltraps
801062ab:	e9 4a f8 ff ff       	jmp    80105afa <alltraps>

801062b0 <vector81>:
.globl vector81
vector81:
  pushl $0
801062b0:	6a 00                	push   $0x0
  pushl $81
801062b2:	6a 51                	push   $0x51
  jmp alltraps
801062b4:	e9 41 f8 ff ff       	jmp    80105afa <alltraps>

801062b9 <vector82>:
.globl vector82
vector82:
  pushl $0
801062b9:	6a 00                	push   $0x0
  pushl $82
801062bb:	6a 52                	push   $0x52
  jmp alltraps
801062bd:	e9 38 f8 ff ff       	jmp    80105afa <alltraps>

801062c2 <vector83>:
.globl vector83
vector83:
  pushl $0
801062c2:	6a 00                	push   $0x0
  pushl $83
801062c4:	6a 53                	push   $0x53
  jmp alltraps
801062c6:	e9 2f f8 ff ff       	jmp    80105afa <alltraps>

801062cb <vector84>:
.globl vector84
vector84:
  pushl $0
801062cb:	6a 00                	push   $0x0
  pushl $84
801062cd:	6a 54                	push   $0x54
  jmp alltraps
801062cf:	e9 26 f8 ff ff       	jmp    80105afa <alltraps>

801062d4 <vector85>:
.globl vector85
vector85:
  pushl $0
801062d4:	6a 00                	push   $0x0
  pushl $85
801062d6:	6a 55                	push   $0x55
  jmp alltraps
801062d8:	e9 1d f8 ff ff       	jmp    80105afa <alltraps>

801062dd <vector86>:
.globl vector86
vector86:
  pushl $0
801062dd:	6a 00                	push   $0x0
  pushl $86
801062df:	6a 56                	push   $0x56
  jmp alltraps
801062e1:	e9 14 f8 ff ff       	jmp    80105afa <alltraps>

801062e6 <vector87>:
.globl vector87
vector87:
  pushl $0
801062e6:	6a 00                	push   $0x0
  pushl $87
801062e8:	6a 57                	push   $0x57
  jmp alltraps
801062ea:	e9 0b f8 ff ff       	jmp    80105afa <alltraps>

801062ef <vector88>:
.globl vector88
vector88:
  pushl $0
801062ef:	6a 00                	push   $0x0
  pushl $88
801062f1:	6a 58                	push   $0x58
  jmp alltraps
801062f3:	e9 02 f8 ff ff       	jmp    80105afa <alltraps>

801062f8 <vector89>:
.globl vector89
vector89:
  pushl $0
801062f8:	6a 00                	push   $0x0
  pushl $89
801062fa:	6a 59                	push   $0x59
  jmp alltraps
801062fc:	e9 f9 f7 ff ff       	jmp    80105afa <alltraps>

80106301 <vector90>:
.globl vector90
vector90:
  pushl $0
80106301:	6a 00                	push   $0x0
  pushl $90
80106303:	6a 5a                	push   $0x5a
  jmp alltraps
80106305:	e9 f0 f7 ff ff       	jmp    80105afa <alltraps>

8010630a <vector91>:
.globl vector91
vector91:
  pushl $0
8010630a:	6a 00                	push   $0x0
  pushl $91
8010630c:	6a 5b                	push   $0x5b
  jmp alltraps
8010630e:	e9 e7 f7 ff ff       	jmp    80105afa <alltraps>

80106313 <vector92>:
.globl vector92
vector92:
  pushl $0
80106313:	6a 00                	push   $0x0
  pushl $92
80106315:	6a 5c                	push   $0x5c
  jmp alltraps
80106317:	e9 de f7 ff ff       	jmp    80105afa <alltraps>

8010631c <vector93>:
.globl vector93
vector93:
  pushl $0
8010631c:	6a 00                	push   $0x0
  pushl $93
8010631e:	6a 5d                	push   $0x5d
  jmp alltraps
80106320:	e9 d5 f7 ff ff       	jmp    80105afa <alltraps>

80106325 <vector94>:
.globl vector94
vector94:
  pushl $0
80106325:	6a 00                	push   $0x0
  pushl $94
80106327:	6a 5e                	push   $0x5e
  jmp alltraps
80106329:	e9 cc f7 ff ff       	jmp    80105afa <alltraps>

8010632e <vector95>:
.globl vector95
vector95:
  pushl $0
8010632e:	6a 00                	push   $0x0
  pushl $95
80106330:	6a 5f                	push   $0x5f
  jmp alltraps
80106332:	e9 c3 f7 ff ff       	jmp    80105afa <alltraps>

80106337 <vector96>:
.globl vector96
vector96:
  pushl $0
80106337:	6a 00                	push   $0x0
  pushl $96
80106339:	6a 60                	push   $0x60
  jmp alltraps
8010633b:	e9 ba f7 ff ff       	jmp    80105afa <alltraps>

80106340 <vector97>:
.globl vector97
vector97:
  pushl $0
80106340:	6a 00                	push   $0x0
  pushl $97
80106342:	6a 61                	push   $0x61
  jmp alltraps
80106344:	e9 b1 f7 ff ff       	jmp    80105afa <alltraps>

80106349 <vector98>:
.globl vector98
vector98:
  pushl $0
80106349:	6a 00                	push   $0x0
  pushl $98
8010634b:	6a 62                	push   $0x62
  jmp alltraps
8010634d:	e9 a8 f7 ff ff       	jmp    80105afa <alltraps>

80106352 <vector99>:
.globl vector99
vector99:
  pushl $0
80106352:	6a 00                	push   $0x0
  pushl $99
80106354:	6a 63                	push   $0x63
  jmp alltraps
80106356:	e9 9f f7 ff ff       	jmp    80105afa <alltraps>

8010635b <vector100>:
.globl vector100
vector100:
  pushl $0
8010635b:	6a 00                	push   $0x0
  pushl $100
8010635d:	6a 64                	push   $0x64
  jmp alltraps
8010635f:	e9 96 f7 ff ff       	jmp    80105afa <alltraps>

80106364 <vector101>:
.globl vector101
vector101:
  pushl $0
80106364:	6a 00                	push   $0x0
  pushl $101
80106366:	6a 65                	push   $0x65
  jmp alltraps
80106368:	e9 8d f7 ff ff       	jmp    80105afa <alltraps>

8010636d <vector102>:
.globl vector102
vector102:
  pushl $0
8010636d:	6a 00                	push   $0x0
  pushl $102
8010636f:	6a 66                	push   $0x66
  jmp alltraps
80106371:	e9 84 f7 ff ff       	jmp    80105afa <alltraps>

80106376 <vector103>:
.globl vector103
vector103:
  pushl $0
80106376:	6a 00                	push   $0x0
  pushl $103
80106378:	6a 67                	push   $0x67
  jmp alltraps
8010637a:	e9 7b f7 ff ff       	jmp    80105afa <alltraps>

8010637f <vector104>:
.globl vector104
vector104:
  pushl $0
8010637f:	6a 00                	push   $0x0
  pushl $104
80106381:	6a 68                	push   $0x68
  jmp alltraps
80106383:	e9 72 f7 ff ff       	jmp    80105afa <alltraps>

80106388 <vector105>:
.globl vector105
vector105:
  pushl $0
80106388:	6a 00                	push   $0x0
  pushl $105
8010638a:	6a 69                	push   $0x69
  jmp alltraps
8010638c:	e9 69 f7 ff ff       	jmp    80105afa <alltraps>

80106391 <vector106>:
.globl vector106
vector106:
  pushl $0
80106391:	6a 00                	push   $0x0
  pushl $106
80106393:	6a 6a                	push   $0x6a
  jmp alltraps
80106395:	e9 60 f7 ff ff       	jmp    80105afa <alltraps>

8010639a <vector107>:
.globl vector107
vector107:
  pushl $0
8010639a:	6a 00                	push   $0x0
  pushl $107
8010639c:	6a 6b                	push   $0x6b
  jmp alltraps
8010639e:	e9 57 f7 ff ff       	jmp    80105afa <alltraps>

801063a3 <vector108>:
.globl vector108
vector108:
  pushl $0
801063a3:	6a 00                	push   $0x0
  pushl $108
801063a5:	6a 6c                	push   $0x6c
  jmp alltraps
801063a7:	e9 4e f7 ff ff       	jmp    80105afa <alltraps>

801063ac <vector109>:
.globl vector109
vector109:
  pushl $0
801063ac:	6a 00                	push   $0x0
  pushl $109
801063ae:	6a 6d                	push   $0x6d
  jmp alltraps
801063b0:	e9 45 f7 ff ff       	jmp    80105afa <alltraps>

801063b5 <vector110>:
.globl vector110
vector110:
  pushl $0
801063b5:	6a 00                	push   $0x0
  pushl $110
801063b7:	6a 6e                	push   $0x6e
  jmp alltraps
801063b9:	e9 3c f7 ff ff       	jmp    80105afa <alltraps>

801063be <vector111>:
.globl vector111
vector111:
  pushl $0
801063be:	6a 00                	push   $0x0
  pushl $111
801063c0:	6a 6f                	push   $0x6f
  jmp alltraps
801063c2:	e9 33 f7 ff ff       	jmp    80105afa <alltraps>

801063c7 <vector112>:
.globl vector112
vector112:
  pushl $0
801063c7:	6a 00                	push   $0x0
  pushl $112
801063c9:	6a 70                	push   $0x70
  jmp alltraps
801063cb:	e9 2a f7 ff ff       	jmp    80105afa <alltraps>

801063d0 <vector113>:
.globl vector113
vector113:
  pushl $0
801063d0:	6a 00                	push   $0x0
  pushl $113
801063d2:	6a 71                	push   $0x71
  jmp alltraps
801063d4:	e9 21 f7 ff ff       	jmp    80105afa <alltraps>

801063d9 <vector114>:
.globl vector114
vector114:
  pushl $0
801063d9:	6a 00                	push   $0x0
  pushl $114
801063db:	6a 72                	push   $0x72
  jmp alltraps
801063dd:	e9 18 f7 ff ff       	jmp    80105afa <alltraps>

801063e2 <vector115>:
.globl vector115
vector115:
  pushl $0
801063e2:	6a 00                	push   $0x0
  pushl $115
801063e4:	6a 73                	push   $0x73
  jmp alltraps
801063e6:	e9 0f f7 ff ff       	jmp    80105afa <alltraps>

801063eb <vector116>:
.globl vector116
vector116:
  pushl $0
801063eb:	6a 00                	push   $0x0
  pushl $116
801063ed:	6a 74                	push   $0x74
  jmp alltraps
801063ef:	e9 06 f7 ff ff       	jmp    80105afa <alltraps>

801063f4 <vector117>:
.globl vector117
vector117:
  pushl $0
801063f4:	6a 00                	push   $0x0
  pushl $117
801063f6:	6a 75                	push   $0x75
  jmp alltraps
801063f8:	e9 fd f6 ff ff       	jmp    80105afa <alltraps>

801063fd <vector118>:
.globl vector118
vector118:
  pushl $0
801063fd:	6a 00                	push   $0x0
  pushl $118
801063ff:	6a 76                	push   $0x76
  jmp alltraps
80106401:	e9 f4 f6 ff ff       	jmp    80105afa <alltraps>

80106406 <vector119>:
.globl vector119
vector119:
  pushl $0
80106406:	6a 00                	push   $0x0
  pushl $119
80106408:	6a 77                	push   $0x77
  jmp alltraps
8010640a:	e9 eb f6 ff ff       	jmp    80105afa <alltraps>

8010640f <vector120>:
.globl vector120
vector120:
  pushl $0
8010640f:	6a 00                	push   $0x0
  pushl $120
80106411:	6a 78                	push   $0x78
  jmp alltraps
80106413:	e9 e2 f6 ff ff       	jmp    80105afa <alltraps>

80106418 <vector121>:
.globl vector121
vector121:
  pushl $0
80106418:	6a 00                	push   $0x0
  pushl $121
8010641a:	6a 79                	push   $0x79
  jmp alltraps
8010641c:	e9 d9 f6 ff ff       	jmp    80105afa <alltraps>

80106421 <vector122>:
.globl vector122
vector122:
  pushl $0
80106421:	6a 00                	push   $0x0
  pushl $122
80106423:	6a 7a                	push   $0x7a
  jmp alltraps
80106425:	e9 d0 f6 ff ff       	jmp    80105afa <alltraps>

8010642a <vector123>:
.globl vector123
vector123:
  pushl $0
8010642a:	6a 00                	push   $0x0
  pushl $123
8010642c:	6a 7b                	push   $0x7b
  jmp alltraps
8010642e:	e9 c7 f6 ff ff       	jmp    80105afa <alltraps>

80106433 <vector124>:
.globl vector124
vector124:
  pushl $0
80106433:	6a 00                	push   $0x0
  pushl $124
80106435:	6a 7c                	push   $0x7c
  jmp alltraps
80106437:	e9 be f6 ff ff       	jmp    80105afa <alltraps>

8010643c <vector125>:
.globl vector125
vector125:
  pushl $0
8010643c:	6a 00                	push   $0x0
  pushl $125
8010643e:	6a 7d                	push   $0x7d
  jmp alltraps
80106440:	e9 b5 f6 ff ff       	jmp    80105afa <alltraps>

80106445 <vector126>:
.globl vector126
vector126:
  pushl $0
80106445:	6a 00                	push   $0x0
  pushl $126
80106447:	6a 7e                	push   $0x7e
  jmp alltraps
80106449:	e9 ac f6 ff ff       	jmp    80105afa <alltraps>

8010644e <vector127>:
.globl vector127
vector127:
  pushl $0
8010644e:	6a 00                	push   $0x0
  pushl $127
80106450:	6a 7f                	push   $0x7f
  jmp alltraps
80106452:	e9 a3 f6 ff ff       	jmp    80105afa <alltraps>

80106457 <vector128>:
.globl vector128
vector128:
  pushl $0
80106457:	6a 00                	push   $0x0
  pushl $128
80106459:	68 80 00 00 00       	push   $0x80
  jmp alltraps
8010645e:	e9 97 f6 ff ff       	jmp    80105afa <alltraps>

80106463 <vector129>:
.globl vector129
vector129:
  pushl $0
80106463:	6a 00                	push   $0x0
  pushl $129
80106465:	68 81 00 00 00       	push   $0x81
  jmp alltraps
8010646a:	e9 8b f6 ff ff       	jmp    80105afa <alltraps>

8010646f <vector130>:
.globl vector130
vector130:
  pushl $0
8010646f:	6a 00                	push   $0x0
  pushl $130
80106471:	68 82 00 00 00       	push   $0x82
  jmp alltraps
80106476:	e9 7f f6 ff ff       	jmp    80105afa <alltraps>

8010647b <vector131>:
.globl vector131
vector131:
  pushl $0
8010647b:	6a 00                	push   $0x0
  pushl $131
8010647d:	68 83 00 00 00       	push   $0x83
  jmp alltraps
80106482:	e9 73 f6 ff ff       	jmp    80105afa <alltraps>

80106487 <vector132>:
.globl vector132
vector132:
  pushl $0
80106487:	6a 00                	push   $0x0
  pushl $132
80106489:	68 84 00 00 00       	push   $0x84
  jmp alltraps
8010648e:	e9 67 f6 ff ff       	jmp    80105afa <alltraps>

80106493 <vector133>:
.globl vector133
vector133:
  pushl $0
80106493:	6a 00                	push   $0x0
  pushl $133
80106495:	68 85 00 00 00       	push   $0x85
  jmp alltraps
8010649a:	e9 5b f6 ff ff       	jmp    80105afa <alltraps>

8010649f <vector134>:
.globl vector134
vector134:
  pushl $0
8010649f:	6a 00                	push   $0x0
  pushl $134
801064a1:	68 86 00 00 00       	push   $0x86
  jmp alltraps
801064a6:	e9 4f f6 ff ff       	jmp    80105afa <alltraps>

801064ab <vector135>:
.globl vector135
vector135:
  pushl $0
801064ab:	6a 00                	push   $0x0
  pushl $135
801064ad:	68 87 00 00 00       	push   $0x87
  jmp alltraps
801064b2:	e9 43 f6 ff ff       	jmp    80105afa <alltraps>

801064b7 <vector136>:
.globl vector136
vector136:
  pushl $0
801064b7:	6a 00                	push   $0x0
  pushl $136
801064b9:	68 88 00 00 00       	push   $0x88
  jmp alltraps
801064be:	e9 37 f6 ff ff       	jmp    80105afa <alltraps>

801064c3 <vector137>:
.globl vector137
vector137:
  pushl $0
801064c3:	6a 00                	push   $0x0
  pushl $137
801064c5:	68 89 00 00 00       	push   $0x89
  jmp alltraps
801064ca:	e9 2b f6 ff ff       	jmp    80105afa <alltraps>

801064cf <vector138>:
.globl vector138
vector138:
  pushl $0
801064cf:	6a 00                	push   $0x0
  pushl $138
801064d1:	68 8a 00 00 00       	push   $0x8a
  jmp alltraps
801064d6:	e9 1f f6 ff ff       	jmp    80105afa <alltraps>

801064db <vector139>:
.globl vector139
vector139:
  pushl $0
801064db:	6a 00                	push   $0x0
  pushl $139
801064dd:	68 8b 00 00 00       	push   $0x8b
  jmp alltraps
801064e2:	e9 13 f6 ff ff       	jmp    80105afa <alltraps>

801064e7 <vector140>:
.globl vector140
vector140:
  pushl $0
801064e7:	6a 00                	push   $0x0
  pushl $140
801064e9:	68 8c 00 00 00       	push   $0x8c
  jmp alltraps
801064ee:	e9 07 f6 ff ff       	jmp    80105afa <alltraps>

801064f3 <vector141>:
.globl vector141
vector141:
  pushl $0
801064f3:	6a 00                	push   $0x0
  pushl $141
801064f5:	68 8d 00 00 00       	push   $0x8d
  jmp alltraps
801064fa:	e9 fb f5 ff ff       	jmp    80105afa <alltraps>

801064ff <vector142>:
.globl vector142
vector142:
  pushl $0
801064ff:	6a 00                	push   $0x0
  pushl $142
80106501:	68 8e 00 00 00       	push   $0x8e
  jmp alltraps
80106506:	e9 ef f5 ff ff       	jmp    80105afa <alltraps>

8010650b <vector143>:
.globl vector143
vector143:
  pushl $0
8010650b:	6a 00                	push   $0x0
  pushl $143
8010650d:	68 8f 00 00 00       	push   $0x8f
  jmp alltraps
80106512:	e9 e3 f5 ff ff       	jmp    80105afa <alltraps>

80106517 <vector144>:
.globl vector144
vector144:
  pushl $0
80106517:	6a 00                	push   $0x0
  pushl $144
80106519:	68 90 00 00 00       	push   $0x90
  jmp alltraps
8010651e:	e9 d7 f5 ff ff       	jmp    80105afa <alltraps>

80106523 <vector145>:
.globl vector145
vector145:
  pushl $0
80106523:	6a 00                	push   $0x0
  pushl $145
80106525:	68 91 00 00 00       	push   $0x91
  jmp alltraps
8010652a:	e9 cb f5 ff ff       	jmp    80105afa <alltraps>

8010652f <vector146>:
.globl vector146
vector146:
  pushl $0
8010652f:	6a 00                	push   $0x0
  pushl $146
80106531:	68 92 00 00 00       	push   $0x92
  jmp alltraps
80106536:	e9 bf f5 ff ff       	jmp    80105afa <alltraps>

8010653b <vector147>:
.globl vector147
vector147:
  pushl $0
8010653b:	6a 00                	push   $0x0
  pushl $147
8010653d:	68 93 00 00 00       	push   $0x93
  jmp alltraps
80106542:	e9 b3 f5 ff ff       	jmp    80105afa <alltraps>

80106547 <vector148>:
.globl vector148
vector148:
  pushl $0
80106547:	6a 00                	push   $0x0
  pushl $148
80106549:	68 94 00 00 00       	push   $0x94
  jmp alltraps
8010654e:	e9 a7 f5 ff ff       	jmp    80105afa <alltraps>

80106553 <vector149>:
.globl vector149
vector149:
  pushl $0
80106553:	6a 00                	push   $0x0
  pushl $149
80106555:	68 95 00 00 00       	push   $0x95
  jmp alltraps
8010655a:	e9 9b f5 ff ff       	jmp    80105afa <alltraps>

8010655f <vector150>:
.globl vector150
vector150:
  pushl $0
8010655f:	6a 00                	push   $0x0
  pushl $150
80106561:	68 96 00 00 00       	push   $0x96
  jmp alltraps
80106566:	e9 8f f5 ff ff       	jmp    80105afa <alltraps>

8010656b <vector151>:
.globl vector151
vector151:
  pushl $0
8010656b:	6a 00                	push   $0x0
  pushl $151
8010656d:	68 97 00 00 00       	push   $0x97
  jmp alltraps
80106572:	e9 83 f5 ff ff       	jmp    80105afa <alltraps>

80106577 <vector152>:
.globl vector152
vector152:
  pushl $0
80106577:	6a 00                	push   $0x0
  pushl $152
80106579:	68 98 00 00 00       	push   $0x98
  jmp alltraps
8010657e:	e9 77 f5 ff ff       	jmp    80105afa <alltraps>

80106583 <vector153>:
.globl vector153
vector153:
  pushl $0
80106583:	6a 00                	push   $0x0
  pushl $153
80106585:	68 99 00 00 00       	push   $0x99
  jmp alltraps
8010658a:	e9 6b f5 ff ff       	jmp    80105afa <alltraps>

8010658f <vector154>:
.globl vector154
vector154:
  pushl $0
8010658f:	6a 00                	push   $0x0
  pushl $154
80106591:	68 9a 00 00 00       	push   $0x9a
  jmp alltraps
80106596:	e9 5f f5 ff ff       	jmp    80105afa <alltraps>

8010659b <vector155>:
.globl vector155
vector155:
  pushl $0
8010659b:	6a 00                	push   $0x0
  pushl $155
8010659d:	68 9b 00 00 00       	push   $0x9b
  jmp alltraps
801065a2:	e9 53 f5 ff ff       	jmp    80105afa <alltraps>

801065a7 <vector156>:
.globl vector156
vector156:
  pushl $0
801065a7:	6a 00                	push   $0x0
  pushl $156
801065a9:	68 9c 00 00 00       	push   $0x9c
  jmp alltraps
801065ae:	e9 47 f5 ff ff       	jmp    80105afa <alltraps>

801065b3 <vector157>:
.globl vector157
vector157:
  pushl $0
801065b3:	6a 00                	push   $0x0
  pushl $157
801065b5:	68 9d 00 00 00       	push   $0x9d
  jmp alltraps
801065ba:	e9 3b f5 ff ff       	jmp    80105afa <alltraps>

801065bf <vector158>:
.globl vector158
vector158:
  pushl $0
801065bf:	6a 00                	push   $0x0
  pushl $158
801065c1:	68 9e 00 00 00       	push   $0x9e
  jmp alltraps
801065c6:	e9 2f f5 ff ff       	jmp    80105afa <alltraps>

801065cb <vector159>:
.globl vector159
vector159:
  pushl $0
801065cb:	6a 00                	push   $0x0
  pushl $159
801065cd:	68 9f 00 00 00       	push   $0x9f
  jmp alltraps
801065d2:	e9 23 f5 ff ff       	jmp    80105afa <alltraps>

801065d7 <vector160>:
.globl vector160
vector160:
  pushl $0
801065d7:	6a 00                	push   $0x0
  pushl $160
801065d9:	68 a0 00 00 00       	push   $0xa0
  jmp alltraps
801065de:	e9 17 f5 ff ff       	jmp    80105afa <alltraps>

801065e3 <vector161>:
.globl vector161
vector161:
  pushl $0
801065e3:	6a 00                	push   $0x0
  pushl $161
801065e5:	68 a1 00 00 00       	push   $0xa1
  jmp alltraps
801065ea:	e9 0b f5 ff ff       	jmp    80105afa <alltraps>

801065ef <vector162>:
.globl vector162
vector162:
  pushl $0
801065ef:	6a 00                	push   $0x0
  pushl $162
801065f1:	68 a2 00 00 00       	push   $0xa2
  jmp alltraps
801065f6:	e9 ff f4 ff ff       	jmp    80105afa <alltraps>

801065fb <vector163>:
.globl vector163
vector163:
  pushl $0
801065fb:	6a 00                	push   $0x0
  pushl $163
801065fd:	68 a3 00 00 00       	push   $0xa3
  jmp alltraps
80106602:	e9 f3 f4 ff ff       	jmp    80105afa <alltraps>

80106607 <vector164>:
.globl vector164
vector164:
  pushl $0
80106607:	6a 00                	push   $0x0
  pushl $164
80106609:	68 a4 00 00 00       	push   $0xa4
  jmp alltraps
8010660e:	e9 e7 f4 ff ff       	jmp    80105afa <alltraps>

80106613 <vector165>:
.globl vector165
vector165:
  pushl $0
80106613:	6a 00                	push   $0x0
  pushl $165
80106615:	68 a5 00 00 00       	push   $0xa5
  jmp alltraps
8010661a:	e9 db f4 ff ff       	jmp    80105afa <alltraps>

8010661f <vector166>:
.globl vector166
vector166:
  pushl $0
8010661f:	6a 00                	push   $0x0
  pushl $166
80106621:	68 a6 00 00 00       	push   $0xa6
  jmp alltraps
80106626:	e9 cf f4 ff ff       	jmp    80105afa <alltraps>

8010662b <vector167>:
.globl vector167
vector167:
  pushl $0
8010662b:	6a 00                	push   $0x0
  pushl $167
8010662d:	68 a7 00 00 00       	push   $0xa7
  jmp alltraps
80106632:	e9 c3 f4 ff ff       	jmp    80105afa <alltraps>

80106637 <vector168>:
.globl vector168
vector168:
  pushl $0
80106637:	6a 00                	push   $0x0
  pushl $168
80106639:	68 a8 00 00 00       	push   $0xa8
  jmp alltraps
8010663e:	e9 b7 f4 ff ff       	jmp    80105afa <alltraps>

80106643 <vector169>:
.globl vector169
vector169:
  pushl $0
80106643:	6a 00                	push   $0x0
  pushl $169
80106645:	68 a9 00 00 00       	push   $0xa9
  jmp alltraps
8010664a:	e9 ab f4 ff ff       	jmp    80105afa <alltraps>

8010664f <vector170>:
.globl vector170
vector170:
  pushl $0
8010664f:	6a 00                	push   $0x0
  pushl $170
80106651:	68 aa 00 00 00       	push   $0xaa
  jmp alltraps
80106656:	e9 9f f4 ff ff       	jmp    80105afa <alltraps>

8010665b <vector171>:
.globl vector171
vector171:
  pushl $0
8010665b:	6a 00                	push   $0x0
  pushl $171
8010665d:	68 ab 00 00 00       	push   $0xab
  jmp alltraps
80106662:	e9 93 f4 ff ff       	jmp    80105afa <alltraps>

80106667 <vector172>:
.globl vector172
vector172:
  pushl $0
80106667:	6a 00                	push   $0x0
  pushl $172
80106669:	68 ac 00 00 00       	push   $0xac
  jmp alltraps
8010666e:	e9 87 f4 ff ff       	jmp    80105afa <alltraps>

80106673 <vector173>:
.globl vector173
vector173:
  pushl $0
80106673:	6a 00                	push   $0x0
  pushl $173
80106675:	68 ad 00 00 00       	push   $0xad
  jmp alltraps
8010667a:	e9 7b f4 ff ff       	jmp    80105afa <alltraps>

8010667f <vector174>:
.globl vector174
vector174:
  pushl $0
8010667f:	6a 00                	push   $0x0
  pushl $174
80106681:	68 ae 00 00 00       	push   $0xae
  jmp alltraps
80106686:	e9 6f f4 ff ff       	jmp    80105afa <alltraps>

8010668b <vector175>:
.globl vector175
vector175:
  pushl $0
8010668b:	6a 00                	push   $0x0
  pushl $175
8010668d:	68 af 00 00 00       	push   $0xaf
  jmp alltraps
80106692:	e9 63 f4 ff ff       	jmp    80105afa <alltraps>

80106697 <vector176>:
.globl vector176
vector176:
  pushl $0
80106697:	6a 00                	push   $0x0
  pushl $176
80106699:	68 b0 00 00 00       	push   $0xb0
  jmp alltraps
8010669e:	e9 57 f4 ff ff       	jmp    80105afa <alltraps>

801066a3 <vector177>:
.globl vector177
vector177:
  pushl $0
801066a3:	6a 00                	push   $0x0
  pushl $177
801066a5:	68 b1 00 00 00       	push   $0xb1
  jmp alltraps
801066aa:	e9 4b f4 ff ff       	jmp    80105afa <alltraps>

801066af <vector178>:
.globl vector178
vector178:
  pushl $0
801066af:	6a 00                	push   $0x0
  pushl $178
801066b1:	68 b2 00 00 00       	push   $0xb2
  jmp alltraps
801066b6:	e9 3f f4 ff ff       	jmp    80105afa <alltraps>

801066bb <vector179>:
.globl vector179
vector179:
  pushl $0
801066bb:	6a 00                	push   $0x0
  pushl $179
801066bd:	68 b3 00 00 00       	push   $0xb3
  jmp alltraps
801066c2:	e9 33 f4 ff ff       	jmp    80105afa <alltraps>

801066c7 <vector180>:
.globl vector180
vector180:
  pushl $0
801066c7:	6a 00                	push   $0x0
  pushl $180
801066c9:	68 b4 00 00 00       	push   $0xb4
  jmp alltraps
801066ce:	e9 27 f4 ff ff       	jmp    80105afa <alltraps>

801066d3 <vector181>:
.globl vector181
vector181:
  pushl $0
801066d3:	6a 00                	push   $0x0
  pushl $181
801066d5:	68 b5 00 00 00       	push   $0xb5
  jmp alltraps
801066da:	e9 1b f4 ff ff       	jmp    80105afa <alltraps>

801066df <vector182>:
.globl vector182
vector182:
  pushl $0
801066df:	6a 00                	push   $0x0
  pushl $182
801066e1:	68 b6 00 00 00       	push   $0xb6
  jmp alltraps
801066e6:	e9 0f f4 ff ff       	jmp    80105afa <alltraps>

801066eb <vector183>:
.globl vector183
vector183:
  pushl $0
801066eb:	6a 00                	push   $0x0
  pushl $183
801066ed:	68 b7 00 00 00       	push   $0xb7
  jmp alltraps
801066f2:	e9 03 f4 ff ff       	jmp    80105afa <alltraps>

801066f7 <vector184>:
.globl vector184
vector184:
  pushl $0
801066f7:	6a 00                	push   $0x0
  pushl $184
801066f9:	68 b8 00 00 00       	push   $0xb8
  jmp alltraps
801066fe:	e9 f7 f3 ff ff       	jmp    80105afa <alltraps>

80106703 <vector185>:
.globl vector185
vector185:
  pushl $0
80106703:	6a 00                	push   $0x0
  pushl $185
80106705:	68 b9 00 00 00       	push   $0xb9
  jmp alltraps
8010670a:	e9 eb f3 ff ff       	jmp    80105afa <alltraps>

8010670f <vector186>:
.globl vector186
vector186:
  pushl $0
8010670f:	6a 00                	push   $0x0
  pushl $186
80106711:	68 ba 00 00 00       	push   $0xba
  jmp alltraps
80106716:	e9 df f3 ff ff       	jmp    80105afa <alltraps>

8010671b <vector187>:
.globl vector187
vector187:
  pushl $0
8010671b:	6a 00                	push   $0x0
  pushl $187
8010671d:	68 bb 00 00 00       	push   $0xbb
  jmp alltraps
80106722:	e9 d3 f3 ff ff       	jmp    80105afa <alltraps>

80106727 <vector188>:
.globl vector188
vector188:
  pushl $0
80106727:	6a 00                	push   $0x0
  pushl $188
80106729:	68 bc 00 00 00       	push   $0xbc
  jmp alltraps
8010672e:	e9 c7 f3 ff ff       	jmp    80105afa <alltraps>

80106733 <vector189>:
.globl vector189
vector189:
  pushl $0
80106733:	6a 00                	push   $0x0
  pushl $189
80106735:	68 bd 00 00 00       	push   $0xbd
  jmp alltraps
8010673a:	e9 bb f3 ff ff       	jmp    80105afa <alltraps>

8010673f <vector190>:
.globl vector190
vector190:
  pushl $0
8010673f:	6a 00                	push   $0x0
  pushl $190
80106741:	68 be 00 00 00       	push   $0xbe
  jmp alltraps
80106746:	e9 af f3 ff ff       	jmp    80105afa <alltraps>

8010674b <vector191>:
.globl vector191
vector191:
  pushl $0
8010674b:	6a 00                	push   $0x0
  pushl $191
8010674d:	68 bf 00 00 00       	push   $0xbf
  jmp alltraps
80106752:	e9 a3 f3 ff ff       	jmp    80105afa <alltraps>

80106757 <vector192>:
.globl vector192
vector192:
  pushl $0
80106757:	6a 00                	push   $0x0
  pushl $192
80106759:	68 c0 00 00 00       	push   $0xc0
  jmp alltraps
8010675e:	e9 97 f3 ff ff       	jmp    80105afa <alltraps>

80106763 <vector193>:
.globl vector193
vector193:
  pushl $0
80106763:	6a 00                	push   $0x0
  pushl $193
80106765:	68 c1 00 00 00       	push   $0xc1
  jmp alltraps
8010676a:	e9 8b f3 ff ff       	jmp    80105afa <alltraps>

8010676f <vector194>:
.globl vector194
vector194:
  pushl $0
8010676f:	6a 00                	push   $0x0
  pushl $194
80106771:	68 c2 00 00 00       	push   $0xc2
  jmp alltraps
80106776:	e9 7f f3 ff ff       	jmp    80105afa <alltraps>

8010677b <vector195>:
.globl vector195
vector195:
  pushl $0
8010677b:	6a 00                	push   $0x0
  pushl $195
8010677d:	68 c3 00 00 00       	push   $0xc3
  jmp alltraps
80106782:	e9 73 f3 ff ff       	jmp    80105afa <alltraps>

80106787 <vector196>:
.globl vector196
vector196:
  pushl $0
80106787:	6a 00                	push   $0x0
  pushl $196
80106789:	68 c4 00 00 00       	push   $0xc4
  jmp alltraps
8010678e:	e9 67 f3 ff ff       	jmp    80105afa <alltraps>

80106793 <vector197>:
.globl vector197
vector197:
  pushl $0
80106793:	6a 00                	push   $0x0
  pushl $197
80106795:	68 c5 00 00 00       	push   $0xc5
  jmp alltraps
8010679a:	e9 5b f3 ff ff       	jmp    80105afa <alltraps>

8010679f <vector198>:
.globl vector198
vector198:
  pushl $0
8010679f:	6a 00                	push   $0x0
  pushl $198
801067a1:	68 c6 00 00 00       	push   $0xc6
  jmp alltraps
801067a6:	e9 4f f3 ff ff       	jmp    80105afa <alltraps>

801067ab <vector199>:
.globl vector199
vector199:
  pushl $0
801067ab:	6a 00                	push   $0x0
  pushl $199
801067ad:	68 c7 00 00 00       	push   $0xc7
  jmp alltraps
801067b2:	e9 43 f3 ff ff       	jmp    80105afa <alltraps>

801067b7 <vector200>:
.globl vector200
vector200:
  pushl $0
801067b7:	6a 00                	push   $0x0
  pushl $200
801067b9:	68 c8 00 00 00       	push   $0xc8
  jmp alltraps
801067be:	e9 37 f3 ff ff       	jmp    80105afa <alltraps>

801067c3 <vector201>:
.globl vector201
vector201:
  pushl $0
801067c3:	6a 00                	push   $0x0
  pushl $201
801067c5:	68 c9 00 00 00       	push   $0xc9
  jmp alltraps
801067ca:	e9 2b f3 ff ff       	jmp    80105afa <alltraps>

801067cf <vector202>:
.globl vector202
vector202:
  pushl $0
801067cf:	6a 00                	push   $0x0
  pushl $202
801067d1:	68 ca 00 00 00       	push   $0xca
  jmp alltraps
801067d6:	e9 1f f3 ff ff       	jmp    80105afa <alltraps>

801067db <vector203>:
.globl vector203
vector203:
  pushl $0
801067db:	6a 00                	push   $0x0
  pushl $203
801067dd:	68 cb 00 00 00       	push   $0xcb
  jmp alltraps
801067e2:	e9 13 f3 ff ff       	jmp    80105afa <alltraps>

801067e7 <vector204>:
.globl vector204
vector204:
  pushl $0
801067e7:	6a 00                	push   $0x0
  pushl $204
801067e9:	68 cc 00 00 00       	push   $0xcc
  jmp alltraps
801067ee:	e9 07 f3 ff ff       	jmp    80105afa <alltraps>

801067f3 <vector205>:
.globl vector205
vector205:
  pushl $0
801067f3:	6a 00                	push   $0x0
  pushl $205
801067f5:	68 cd 00 00 00       	push   $0xcd
  jmp alltraps
801067fa:	e9 fb f2 ff ff       	jmp    80105afa <alltraps>

801067ff <vector206>:
.globl vector206
vector206:
  pushl $0
801067ff:	6a 00                	push   $0x0
  pushl $206
80106801:	68 ce 00 00 00       	push   $0xce
  jmp alltraps
80106806:	e9 ef f2 ff ff       	jmp    80105afa <alltraps>

8010680b <vector207>:
.globl vector207
vector207:
  pushl $0
8010680b:	6a 00                	push   $0x0
  pushl $207
8010680d:	68 cf 00 00 00       	push   $0xcf
  jmp alltraps
80106812:	e9 e3 f2 ff ff       	jmp    80105afa <alltraps>

80106817 <vector208>:
.globl vector208
vector208:
  pushl $0
80106817:	6a 00                	push   $0x0
  pushl $208
80106819:	68 d0 00 00 00       	push   $0xd0
  jmp alltraps
8010681e:	e9 d7 f2 ff ff       	jmp    80105afa <alltraps>

80106823 <vector209>:
.globl vector209
vector209:
  pushl $0
80106823:	6a 00                	push   $0x0
  pushl $209
80106825:	68 d1 00 00 00       	push   $0xd1
  jmp alltraps
8010682a:	e9 cb f2 ff ff       	jmp    80105afa <alltraps>

8010682f <vector210>:
.globl vector210
vector210:
  pushl $0
8010682f:	6a 00                	push   $0x0
  pushl $210
80106831:	68 d2 00 00 00       	push   $0xd2
  jmp alltraps
80106836:	e9 bf f2 ff ff       	jmp    80105afa <alltraps>

8010683b <vector211>:
.globl vector211
vector211:
  pushl $0
8010683b:	6a 00                	push   $0x0
  pushl $211
8010683d:	68 d3 00 00 00       	push   $0xd3
  jmp alltraps
80106842:	e9 b3 f2 ff ff       	jmp    80105afa <alltraps>

80106847 <vector212>:
.globl vector212
vector212:
  pushl $0
80106847:	6a 00                	push   $0x0
  pushl $212
80106849:	68 d4 00 00 00       	push   $0xd4
  jmp alltraps
8010684e:	e9 a7 f2 ff ff       	jmp    80105afa <alltraps>

80106853 <vector213>:
.globl vector213
vector213:
  pushl $0
80106853:	6a 00                	push   $0x0
  pushl $213
80106855:	68 d5 00 00 00       	push   $0xd5
  jmp alltraps
8010685a:	e9 9b f2 ff ff       	jmp    80105afa <alltraps>

8010685f <vector214>:
.globl vector214
vector214:
  pushl $0
8010685f:	6a 00                	push   $0x0
  pushl $214
80106861:	68 d6 00 00 00       	push   $0xd6
  jmp alltraps
80106866:	e9 8f f2 ff ff       	jmp    80105afa <alltraps>

8010686b <vector215>:
.globl vector215
vector215:
  pushl $0
8010686b:	6a 00                	push   $0x0
  pushl $215
8010686d:	68 d7 00 00 00       	push   $0xd7
  jmp alltraps
80106872:	e9 83 f2 ff ff       	jmp    80105afa <alltraps>

80106877 <vector216>:
.globl vector216
vector216:
  pushl $0
80106877:	6a 00                	push   $0x0
  pushl $216
80106879:	68 d8 00 00 00       	push   $0xd8
  jmp alltraps
8010687e:	e9 77 f2 ff ff       	jmp    80105afa <alltraps>

80106883 <vector217>:
.globl vector217
vector217:
  pushl $0
80106883:	6a 00                	push   $0x0
  pushl $217
80106885:	68 d9 00 00 00       	push   $0xd9
  jmp alltraps
8010688a:	e9 6b f2 ff ff       	jmp    80105afa <alltraps>

8010688f <vector218>:
.globl vector218
vector218:
  pushl $0
8010688f:	6a 00                	push   $0x0
  pushl $218
80106891:	68 da 00 00 00       	push   $0xda
  jmp alltraps
80106896:	e9 5f f2 ff ff       	jmp    80105afa <alltraps>

8010689b <vector219>:
.globl vector219
vector219:
  pushl $0
8010689b:	6a 00                	push   $0x0
  pushl $219
8010689d:	68 db 00 00 00       	push   $0xdb
  jmp alltraps
801068a2:	e9 53 f2 ff ff       	jmp    80105afa <alltraps>

801068a7 <vector220>:
.globl vector220
vector220:
  pushl $0
801068a7:	6a 00                	push   $0x0
  pushl $220
801068a9:	68 dc 00 00 00       	push   $0xdc
  jmp alltraps
801068ae:	e9 47 f2 ff ff       	jmp    80105afa <alltraps>

801068b3 <vector221>:
.globl vector221
vector221:
  pushl $0
801068b3:	6a 00                	push   $0x0
  pushl $221
801068b5:	68 dd 00 00 00       	push   $0xdd
  jmp alltraps
801068ba:	e9 3b f2 ff ff       	jmp    80105afa <alltraps>

801068bf <vector222>:
.globl vector222
vector222:
  pushl $0
801068bf:	6a 00                	push   $0x0
  pushl $222
801068c1:	68 de 00 00 00       	push   $0xde
  jmp alltraps
801068c6:	e9 2f f2 ff ff       	jmp    80105afa <alltraps>

801068cb <vector223>:
.globl vector223
vector223:
  pushl $0
801068cb:	6a 00                	push   $0x0
  pushl $223
801068cd:	68 df 00 00 00       	push   $0xdf
  jmp alltraps
801068d2:	e9 23 f2 ff ff       	jmp    80105afa <alltraps>

801068d7 <vector224>:
.globl vector224
vector224:
  pushl $0
801068d7:	6a 00                	push   $0x0
  pushl $224
801068d9:	68 e0 00 00 00       	push   $0xe0
  jmp alltraps
801068de:	e9 17 f2 ff ff       	jmp    80105afa <alltraps>

801068e3 <vector225>:
.globl vector225
vector225:
  pushl $0
801068e3:	6a 00                	push   $0x0
  pushl $225
801068e5:	68 e1 00 00 00       	push   $0xe1
  jmp alltraps
801068ea:	e9 0b f2 ff ff       	jmp    80105afa <alltraps>

801068ef <vector226>:
.globl vector226
vector226:
  pushl $0
801068ef:	6a 00                	push   $0x0
  pushl $226
801068f1:	68 e2 00 00 00       	push   $0xe2
  jmp alltraps
801068f6:	e9 ff f1 ff ff       	jmp    80105afa <alltraps>

801068fb <vector227>:
.globl vector227
vector227:
  pushl $0
801068fb:	6a 00                	push   $0x0
  pushl $227
801068fd:	68 e3 00 00 00       	push   $0xe3
  jmp alltraps
80106902:	e9 f3 f1 ff ff       	jmp    80105afa <alltraps>

80106907 <vector228>:
.globl vector228
vector228:
  pushl $0
80106907:	6a 00                	push   $0x0
  pushl $228
80106909:	68 e4 00 00 00       	push   $0xe4
  jmp alltraps
8010690e:	e9 e7 f1 ff ff       	jmp    80105afa <alltraps>

80106913 <vector229>:
.globl vector229
vector229:
  pushl $0
80106913:	6a 00                	push   $0x0
  pushl $229
80106915:	68 e5 00 00 00       	push   $0xe5
  jmp alltraps
8010691a:	e9 db f1 ff ff       	jmp    80105afa <alltraps>

8010691f <vector230>:
.globl vector230
vector230:
  pushl $0
8010691f:	6a 00                	push   $0x0
  pushl $230
80106921:	68 e6 00 00 00       	push   $0xe6
  jmp alltraps
80106926:	e9 cf f1 ff ff       	jmp    80105afa <alltraps>

8010692b <vector231>:
.globl vector231
vector231:
  pushl $0
8010692b:	6a 00                	push   $0x0
  pushl $231
8010692d:	68 e7 00 00 00       	push   $0xe7
  jmp alltraps
80106932:	e9 c3 f1 ff ff       	jmp    80105afa <alltraps>

80106937 <vector232>:
.globl vector232
vector232:
  pushl $0
80106937:	6a 00                	push   $0x0
  pushl $232
80106939:	68 e8 00 00 00       	push   $0xe8
  jmp alltraps
8010693e:	e9 b7 f1 ff ff       	jmp    80105afa <alltraps>

80106943 <vector233>:
.globl vector233
vector233:
  pushl $0
80106943:	6a 00                	push   $0x0
  pushl $233
80106945:	68 e9 00 00 00       	push   $0xe9
  jmp alltraps
8010694a:	e9 ab f1 ff ff       	jmp    80105afa <alltraps>

8010694f <vector234>:
.globl vector234
vector234:
  pushl $0
8010694f:	6a 00                	push   $0x0
  pushl $234
80106951:	68 ea 00 00 00       	push   $0xea
  jmp alltraps
80106956:	e9 9f f1 ff ff       	jmp    80105afa <alltraps>

8010695b <vector235>:
.globl vector235
vector235:
  pushl $0
8010695b:	6a 00                	push   $0x0
  pushl $235
8010695d:	68 eb 00 00 00       	push   $0xeb
  jmp alltraps
80106962:	e9 93 f1 ff ff       	jmp    80105afa <alltraps>

80106967 <vector236>:
.globl vector236
vector236:
  pushl $0
80106967:	6a 00                	push   $0x0
  pushl $236
80106969:	68 ec 00 00 00       	push   $0xec
  jmp alltraps
8010696e:	e9 87 f1 ff ff       	jmp    80105afa <alltraps>

80106973 <vector237>:
.globl vector237
vector237:
  pushl $0
80106973:	6a 00                	push   $0x0
  pushl $237
80106975:	68 ed 00 00 00       	push   $0xed
  jmp alltraps
8010697a:	e9 7b f1 ff ff       	jmp    80105afa <alltraps>

8010697f <vector238>:
.globl vector238
vector238:
  pushl $0
8010697f:	6a 00                	push   $0x0
  pushl $238
80106981:	68 ee 00 00 00       	push   $0xee
  jmp alltraps
80106986:	e9 6f f1 ff ff       	jmp    80105afa <alltraps>

8010698b <vector239>:
.globl vector239
vector239:
  pushl $0
8010698b:	6a 00                	push   $0x0
  pushl $239
8010698d:	68 ef 00 00 00       	push   $0xef
  jmp alltraps
80106992:	e9 63 f1 ff ff       	jmp    80105afa <alltraps>

80106997 <vector240>:
.globl vector240
vector240:
  pushl $0
80106997:	6a 00                	push   $0x0
  pushl $240
80106999:	68 f0 00 00 00       	push   $0xf0
  jmp alltraps
8010699e:	e9 57 f1 ff ff       	jmp    80105afa <alltraps>

801069a3 <vector241>:
.globl vector241
vector241:
  pushl $0
801069a3:	6a 00                	push   $0x0
  pushl $241
801069a5:	68 f1 00 00 00       	push   $0xf1
  jmp alltraps
801069aa:	e9 4b f1 ff ff       	jmp    80105afa <alltraps>

801069af <vector242>:
.globl vector242
vector242:
  pushl $0
801069af:	6a 00                	push   $0x0
  pushl $242
801069b1:	68 f2 00 00 00       	push   $0xf2
  jmp alltraps
801069b6:	e9 3f f1 ff ff       	jmp    80105afa <alltraps>

801069bb <vector243>:
.globl vector243
vector243:
  pushl $0
801069bb:	6a 00                	push   $0x0
  pushl $243
801069bd:	68 f3 00 00 00       	push   $0xf3
  jmp alltraps
801069c2:	e9 33 f1 ff ff       	jmp    80105afa <alltraps>

801069c7 <vector244>:
.globl vector244
vector244:
  pushl $0
801069c7:	6a 00                	push   $0x0
  pushl $244
801069c9:	68 f4 00 00 00       	push   $0xf4
  jmp alltraps
801069ce:	e9 27 f1 ff ff       	jmp    80105afa <alltraps>

801069d3 <vector245>:
.globl vector245
vector245:
  pushl $0
801069d3:	6a 00                	push   $0x0
  pushl $245
801069d5:	68 f5 00 00 00       	push   $0xf5
  jmp alltraps
801069da:	e9 1b f1 ff ff       	jmp    80105afa <alltraps>

801069df <vector246>:
.globl vector246
vector246:
  pushl $0
801069df:	6a 00                	push   $0x0
  pushl $246
801069e1:	68 f6 00 00 00       	push   $0xf6
  jmp alltraps
801069e6:	e9 0f f1 ff ff       	jmp    80105afa <alltraps>

801069eb <vector247>:
.globl vector247
vector247:
  pushl $0
801069eb:	6a 00                	push   $0x0
  pushl $247
801069ed:	68 f7 00 00 00       	push   $0xf7
  jmp alltraps
801069f2:	e9 03 f1 ff ff       	jmp    80105afa <alltraps>

801069f7 <vector248>:
.globl vector248
vector248:
  pushl $0
801069f7:	6a 00                	push   $0x0
  pushl $248
801069f9:	68 f8 00 00 00       	push   $0xf8
  jmp alltraps
801069fe:	e9 f7 f0 ff ff       	jmp    80105afa <alltraps>

80106a03 <vector249>:
.globl vector249
vector249:
  pushl $0
80106a03:	6a 00                	push   $0x0
  pushl $249
80106a05:	68 f9 00 00 00       	push   $0xf9
  jmp alltraps
80106a0a:	e9 eb f0 ff ff       	jmp    80105afa <alltraps>

80106a0f <vector250>:
.globl vector250
vector250:
  pushl $0
80106a0f:	6a 00                	push   $0x0
  pushl $250
80106a11:	68 fa 00 00 00       	push   $0xfa
  jmp alltraps
80106a16:	e9 df f0 ff ff       	jmp    80105afa <alltraps>

80106a1b <vector251>:
.globl vector251
vector251:
  pushl $0
80106a1b:	6a 00                	push   $0x0
  pushl $251
80106a1d:	68 fb 00 00 00       	push   $0xfb
  jmp alltraps
80106a22:	e9 d3 f0 ff ff       	jmp    80105afa <alltraps>

80106a27 <vector252>:
.globl vector252
vector252:
  pushl $0
80106a27:	6a 00                	push   $0x0
  pushl $252
80106a29:	68 fc 00 00 00       	push   $0xfc
  jmp alltraps
80106a2e:	e9 c7 f0 ff ff       	jmp    80105afa <alltraps>

80106a33 <vector253>:
.globl vector253
vector253:
  pushl $0
80106a33:	6a 00                	push   $0x0
  pushl $253
80106a35:	68 fd 00 00 00       	push   $0xfd
  jmp alltraps
80106a3a:	e9 bb f0 ff ff       	jmp    80105afa <alltraps>

80106a3f <vector254>:
.globl vector254
vector254:
  pushl $0
80106a3f:	6a 00                	push   $0x0
  pushl $254
80106a41:	68 fe 00 00 00       	push   $0xfe
  jmp alltraps
80106a46:	e9 af f0 ff ff       	jmp    80105afa <alltraps>

80106a4b <vector255>:
.globl vector255
vector255:
  pushl $0
80106a4b:	6a 00                	push   $0x0
  pushl $255
80106a4d:	68 ff 00 00 00       	push   $0xff
  jmp alltraps
80106a52:	e9 a3 f0 ff ff       	jmp    80105afa <alltraps>
80106a57:	66 90                	xchg   %ax,%ax
80106a59:	66 90                	xchg   %ax,%ax
80106a5b:	66 90                	xchg   %ax,%ax
80106a5d:	66 90                	xchg   %ax,%ax
80106a5f:	90                   	nop

80106a60 <walkpgdir>:
// Return the address of the PTE in page table pgdir
// that corresponds to virtual address va.  If alloc!=0,
// create any required page table pages.
static pte_t *
walkpgdir(pde_t *pgdir, const void *va, int alloc)
{
80106a60:	55                   	push   %ebp
80106a61:	89 e5                	mov    %esp,%ebp
80106a63:	57                   	push   %edi
80106a64:	56                   	push   %esi
80106a65:	53                   	push   %ebx
  pde_t *pde;
  pte_t *pgtab;

  pde = &pgdir[PDX(va)];
80106a66:	89 d3                	mov    %edx,%ebx
{
80106a68:	89 d7                	mov    %edx,%edi
  pde = &pgdir[PDX(va)];
80106a6a:	c1 eb 16             	shr    $0x16,%ebx
80106a6d:	8d 34 98             	lea    (%eax,%ebx,4),%esi
{
80106a70:	83 ec 0c             	sub    $0xc,%esp
  if(*pde & PTE_P){
80106a73:	8b 06                	mov    (%esi),%eax
80106a75:	a8 01                	test   $0x1,%al
80106a77:	74 27                	je     80106aa0 <walkpgdir+0x40>
    pgtab = (pte_t*)P2V(PTE_ADDR(*pde));
80106a79:	25 00 f0 ff ff       	and    $0xfffff000,%eax
80106a7e:	8d 98 00 00 00 80    	lea    -0x80000000(%eax),%ebx
    // The permissions here are overly generous, but they can
    // be further restricted by the permissions in the page table
    // entries, if necessary.
    *pde = V2P(pgtab) | PTE_P | PTE_W | PTE_U;
  }
  return &pgtab[PTX(va)];
80106a84:	c1 ef 0a             	shr    $0xa,%edi
}
80106a87:	8d 65 f4             	lea    -0xc(%ebp),%esp
  return &pgtab[PTX(va)];
80106a8a:	89 fa                	mov    %edi,%edx
80106a8c:	81 e2 fc 0f 00 00    	and    $0xffc,%edx
80106a92:	8d 04 13             	lea    (%ebx,%edx,1),%eax
}
80106a95:	5b                   	pop    %ebx
80106a96:	5e                   	pop    %esi
80106a97:	5f                   	pop    %edi
80106a98:	5d                   	pop    %ebp
80106a99:	c3                   	ret    
80106a9a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    if(!alloc || (pgtab = (pte_t*)kalloc()) == 0)
80106aa0:	85 c9                	test   %ecx,%ecx
80106aa2:	74 2c                	je     80106ad0 <walkpgdir+0x70>
80106aa4:	e8 17 be ff ff       	call   801028c0 <kalloc>
80106aa9:	85 c0                	test   %eax,%eax
80106aab:	89 c3                	mov    %eax,%ebx
80106aad:	74 21                	je     80106ad0 <walkpgdir+0x70>
    memset(pgtab, 0, PGSIZE);
80106aaf:	83 ec 04             	sub    $0x4,%esp
80106ab2:	68 00 10 00 00       	push   $0x1000
80106ab7:	6a 00                	push   $0x0
80106ab9:	50                   	push   %eax
80106aba:	e8 21 de ff ff       	call   801048e0 <memset>
    *pde = V2P(pgtab) | PTE_P | PTE_W | PTE_U;
80106abf:	8d 83 00 00 00 80    	lea    -0x80000000(%ebx),%eax
80106ac5:	83 c4 10             	add    $0x10,%esp
80106ac8:	83 c8 07             	or     $0x7,%eax
80106acb:	89 06                	mov    %eax,(%esi)
80106acd:	eb b5                	jmp    80106a84 <walkpgdir+0x24>
80106acf:	90                   	nop
}
80106ad0:	8d 65 f4             	lea    -0xc(%ebp),%esp
      return 0;
80106ad3:	31 c0                	xor    %eax,%eax
}
80106ad5:	5b                   	pop    %ebx
80106ad6:	5e                   	pop    %esi
80106ad7:	5f                   	pop    %edi
80106ad8:	5d                   	pop    %ebp
80106ad9:	c3                   	ret    
80106ada:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi

80106ae0 <mappages>:
// Create PTEs for virtual addresses starting at va that refer to
// physical addresses starting at pa. va and size might not
// be page-aligned.
static int
mappages(pde_t *pgdir, void *va, uint size, uint pa, int perm)
{
80106ae0:	55                   	push   %ebp
80106ae1:	89 e5                	mov    %esp,%ebp
80106ae3:	57                   	push   %edi
80106ae4:	56                   	push   %esi
80106ae5:	53                   	push   %ebx
  char *a, *last;
  pte_t *pte;

  a = (char*)PGROUNDDOWN((uint)va);
80106ae6:	89 d3                	mov    %edx,%ebx
80106ae8:	81 e3 00 f0 ff ff    	and    $0xfffff000,%ebx
{
80106aee:	83 ec 1c             	sub    $0x1c,%esp
80106af1:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  last = (char*)PGROUNDDOWN(((uint)va) + size - 1);
80106af4:	8d 44 0a ff          	lea    -0x1(%edx,%ecx,1),%eax
80106af8:	8b 7d 08             	mov    0x8(%ebp),%edi
80106afb:	25 00 f0 ff ff       	and    $0xfffff000,%eax
80106b00:	89 45 e0             	mov    %eax,-0x20(%ebp)
  for(;;){
    if((pte = walkpgdir(pgdir, a, 1)) == 0)
      return -1;
    if(*pte & PTE_P)
      panic("remap");
    *pte = pa | perm | PTE_P;
80106b03:	8b 45 0c             	mov    0xc(%ebp),%eax
80106b06:	29 df                	sub    %ebx,%edi
80106b08:	83 c8 01             	or     $0x1,%eax
80106b0b:	89 45 dc             	mov    %eax,-0x24(%ebp)
80106b0e:	eb 15                	jmp    80106b25 <mappages+0x45>
    if(*pte & PTE_P)
80106b10:	f6 00 01             	testb  $0x1,(%eax)
80106b13:	75 45                	jne    80106b5a <mappages+0x7a>
    *pte = pa | perm | PTE_P;
80106b15:	0b 75 dc             	or     -0x24(%ebp),%esi
    if(a == last)
80106b18:	3b 5d e0             	cmp    -0x20(%ebp),%ebx
    *pte = pa | perm | PTE_P;
80106b1b:	89 30                	mov    %esi,(%eax)
    if(a == last)
80106b1d:	74 31                	je     80106b50 <mappages+0x70>
      break;
    a += PGSIZE;
80106b1f:	81 c3 00 10 00 00    	add    $0x1000,%ebx
    if((pte = walkpgdir(pgdir, a, 1)) == 0)
80106b25:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80106b28:	b9 01 00 00 00       	mov    $0x1,%ecx
80106b2d:	89 da                	mov    %ebx,%edx
80106b2f:	8d 34 3b             	lea    (%ebx,%edi,1),%esi
80106b32:	e8 29 ff ff ff       	call   80106a60 <walkpgdir>
80106b37:	85 c0                	test   %eax,%eax
80106b39:	75 d5                	jne    80106b10 <mappages+0x30>
    pa += PGSIZE;
  }
  return 0;
}
80106b3b:	8d 65 f4             	lea    -0xc(%ebp),%esp
      return -1;
80106b3e:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80106b43:	5b                   	pop    %ebx
80106b44:	5e                   	pop    %esi
80106b45:	5f                   	pop    %edi
80106b46:	5d                   	pop    %ebp
80106b47:	c3                   	ret    
80106b48:	90                   	nop
80106b49:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80106b50:	8d 65 f4             	lea    -0xc(%ebp),%esp
  return 0;
80106b53:	31 c0                	xor    %eax,%eax
}
80106b55:	5b                   	pop    %ebx
80106b56:	5e                   	pop    %esi
80106b57:	5f                   	pop    %edi
80106b58:	5d                   	pop    %ebp
80106b59:	c3                   	ret    
      panic("remap");
80106b5a:	83 ec 0c             	sub    $0xc,%esp
80106b5d:	68 ac 80 10 80       	push   $0x801080ac
80106b62:	e8 29 98 ff ff       	call   80100390 <panic>
80106b67:	89 f6                	mov    %esi,%esi
80106b69:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80106b70 <seginit>:
{
80106b70:	55                   	push   %ebp
80106b71:	89 e5                	mov    %esp,%ebp
80106b73:	83 ec 18             	sub    $0x18,%esp
  c = &cpus[cpuid()];
80106b76:	e8 95 d0 ff ff       	call   80103c10 <cpuid>
80106b7b:	69 c0 b0 00 00 00    	imul   $0xb0,%eax,%eax
  pd[0] = size-1;
80106b81:	ba 2f 00 00 00       	mov    $0x2f,%edx
80106b86:	66 89 55 f2          	mov    %dx,-0xe(%ebp)
  c->gdt[SEG_KCODE] = SEG(STA_X|STA_R, 0, 0xffffffff, 0);
80106b8a:	c7 80 f8 37 11 80 ff 	movl   $0xffff,-0x7feec808(%eax)
80106b91:	ff 00 00 
80106b94:	c7 80 fc 37 11 80 00 	movl   $0xcf9a00,-0x7feec804(%eax)
80106b9b:	9a cf 00 
  c->gdt[SEG_KDATA] = SEG(STA_W, 0, 0xffffffff, 0);
80106b9e:	c7 80 00 38 11 80 ff 	movl   $0xffff,-0x7feec800(%eax)
80106ba5:	ff 00 00 
80106ba8:	c7 80 04 38 11 80 00 	movl   $0xcf9200,-0x7feec7fc(%eax)
80106baf:	92 cf 00 
  c->gdt[SEG_UCODE] = SEG(STA_X|STA_R, 0, 0xffffffff, DPL_USER);
80106bb2:	c7 80 08 38 11 80 ff 	movl   $0xffff,-0x7feec7f8(%eax)
80106bb9:	ff 00 00 
80106bbc:	c7 80 0c 38 11 80 00 	movl   $0xcffa00,-0x7feec7f4(%eax)
80106bc3:	fa cf 00 
  c->gdt[SEG_UDATA] = SEG(STA_W, 0, 0xffffffff, DPL_USER);
80106bc6:	c7 80 10 38 11 80 ff 	movl   $0xffff,-0x7feec7f0(%eax)
80106bcd:	ff 00 00 
80106bd0:	c7 80 14 38 11 80 00 	movl   $0xcff200,-0x7feec7ec(%eax)
80106bd7:	f2 cf 00 
  lgdt(c->gdt, sizeof(c->gdt));
80106bda:	05 f0 37 11 80       	add    $0x801137f0,%eax
  pd[1] = (uint)p;
80106bdf:	66 89 45 f4          	mov    %ax,-0xc(%ebp)
  pd[2] = (uint)p >> 16;
80106be3:	c1 e8 10             	shr    $0x10,%eax
80106be6:	66 89 45 f6          	mov    %ax,-0xa(%ebp)
  asm volatile("lgdt (%0)" : : "r" (pd));
80106bea:	8d 45 f2             	lea    -0xe(%ebp),%eax
80106bed:	0f 01 10             	lgdtl  (%eax)
}
80106bf0:	c9                   	leave  
80106bf1:	c3                   	ret    
80106bf2:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80106bf9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80106c00 <switchkvm>:
// Switch h/w page table register to the kernel-only page table,
// for when no process is running.
void
switchkvm(void)
{
  lcr3(V2P(kpgdir));   // switch to the kernel page table
80106c00:	a1 a4 98 11 80       	mov    0x801198a4,%eax
{
80106c05:	55                   	push   %ebp
80106c06:	89 e5                	mov    %esp,%ebp
  lcr3(V2P(kpgdir));   // switch to the kernel page table
80106c08:	05 00 00 00 80       	add    $0x80000000,%eax
}

static inline void
lcr3(uint val)
{
  asm volatile("movl %0,%%cr3" : : "r" (val));
80106c0d:	0f 22 d8             	mov    %eax,%cr3
}
80106c10:	5d                   	pop    %ebp
80106c11:	c3                   	ret    
80106c12:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80106c19:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80106c20 <switchuvm>:

// Switch TSS and h/w page table to correspond to process p.
void
switchuvm(struct proc *p)
{
80106c20:	55                   	push   %ebp
80106c21:	89 e5                	mov    %esp,%ebp
80106c23:	57                   	push   %edi
80106c24:	56                   	push   %esi
80106c25:	53                   	push   %ebx
80106c26:	83 ec 1c             	sub    $0x1c,%esp
80106c29:	8b 5d 08             	mov    0x8(%ebp),%ebx
  if(p == 0)
80106c2c:	85 db                	test   %ebx,%ebx
80106c2e:	0f 84 cb 00 00 00    	je     80106cff <switchuvm+0xdf>
    panic("switchuvm: no process");
  if(p->kstack == 0)
80106c34:	8b 43 08             	mov    0x8(%ebx),%eax
80106c37:	85 c0                	test   %eax,%eax
80106c39:	0f 84 da 00 00 00    	je     80106d19 <switchuvm+0xf9>
    panic("switchuvm: no kstack");
  if(p->pgdir == 0)
80106c3f:	8b 43 04             	mov    0x4(%ebx),%eax
80106c42:	85 c0                	test   %eax,%eax
80106c44:	0f 84 c2 00 00 00    	je     80106d0c <switchuvm+0xec>
    panic("switchuvm: no pgdir");

  pushcli();
80106c4a:	e8 b1 da ff ff       	call   80104700 <pushcli>
  mycpu()->gdt[SEG_TSS] = SEG16(STS_T32A, &mycpu()->ts,
80106c4f:	e8 3c cf ff ff       	call   80103b90 <mycpu>
80106c54:	89 c6                	mov    %eax,%esi
80106c56:	e8 35 cf ff ff       	call   80103b90 <mycpu>
80106c5b:	89 c7                	mov    %eax,%edi
80106c5d:	e8 2e cf ff ff       	call   80103b90 <mycpu>
80106c62:	89 45 e4             	mov    %eax,-0x1c(%ebp)
80106c65:	83 c7 08             	add    $0x8,%edi
80106c68:	e8 23 cf ff ff       	call   80103b90 <mycpu>
80106c6d:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
80106c70:	83 c0 08             	add    $0x8,%eax
80106c73:	ba 67 00 00 00       	mov    $0x67,%edx
80106c78:	c1 e8 18             	shr    $0x18,%eax
80106c7b:	66 89 96 98 00 00 00 	mov    %dx,0x98(%esi)
80106c82:	66 89 be 9a 00 00 00 	mov    %di,0x9a(%esi)
80106c89:	88 86 9f 00 00 00    	mov    %al,0x9f(%esi)
  mycpu()->gdt[SEG_TSS].s = 0;
  mycpu()->ts.ss0 = SEG_KDATA << 3;
  mycpu()->ts.esp0 = (uint)p->kstack + KSTACKSIZE;
  // setting IOPL=0 in eflags *and* iomb beyond the tss segment limit
  // forbids I/O instructions (e.g., inb and outb) from user space
  mycpu()->ts.iomb = (ushort) 0xFFFF;
80106c8f:	bf ff ff ff ff       	mov    $0xffffffff,%edi
  mycpu()->gdt[SEG_TSS] = SEG16(STS_T32A, &mycpu()->ts,
80106c94:	83 c1 08             	add    $0x8,%ecx
80106c97:	c1 e9 10             	shr    $0x10,%ecx
80106c9a:	88 8e 9c 00 00 00    	mov    %cl,0x9c(%esi)
80106ca0:	b9 99 40 00 00       	mov    $0x4099,%ecx
80106ca5:	66 89 8e 9d 00 00 00 	mov    %cx,0x9d(%esi)
  mycpu()->ts.ss0 = SEG_KDATA << 3;
80106cac:	be 10 00 00 00       	mov    $0x10,%esi
  mycpu()->gdt[SEG_TSS].s = 0;
80106cb1:	e8 da ce ff ff       	call   80103b90 <mycpu>
80106cb6:	80 a0 9d 00 00 00 ef 	andb   $0xef,0x9d(%eax)
  mycpu()->ts.ss0 = SEG_KDATA << 3;
80106cbd:	e8 ce ce ff ff       	call   80103b90 <mycpu>
80106cc2:	66 89 70 10          	mov    %si,0x10(%eax)
  mycpu()->ts.esp0 = (uint)p->kstack + KSTACKSIZE;
80106cc6:	8b 73 08             	mov    0x8(%ebx),%esi
80106cc9:	e8 c2 ce ff ff       	call   80103b90 <mycpu>
80106cce:	81 c6 00 10 00 00    	add    $0x1000,%esi
80106cd4:	89 70 0c             	mov    %esi,0xc(%eax)
  mycpu()->ts.iomb = (ushort) 0xFFFF;
80106cd7:	e8 b4 ce ff ff       	call   80103b90 <mycpu>
80106cdc:	66 89 78 6e          	mov    %di,0x6e(%eax)
  asm volatile("ltr %0" : : "r" (sel));
80106ce0:	b8 28 00 00 00       	mov    $0x28,%eax
80106ce5:	0f 00 d8             	ltr    %ax
  ltr(SEG_TSS << 3);
  lcr3(V2P(p->pgdir));  // switch to process's address space
80106ce8:	8b 43 04             	mov    0x4(%ebx),%eax
80106ceb:	05 00 00 00 80       	add    $0x80000000,%eax
  asm volatile("movl %0,%%cr3" : : "r" (val));
80106cf0:	0f 22 d8             	mov    %eax,%cr3
  popcli();
}
80106cf3:	8d 65 f4             	lea    -0xc(%ebp),%esp
80106cf6:	5b                   	pop    %ebx
80106cf7:	5e                   	pop    %esi
80106cf8:	5f                   	pop    %edi
80106cf9:	5d                   	pop    %ebp
  popcli();
80106cfa:	e9 41 da ff ff       	jmp    80104740 <popcli>
    panic("switchuvm: no process");
80106cff:	83 ec 0c             	sub    $0xc,%esp
80106d02:	68 b2 80 10 80       	push   $0x801080b2
80106d07:	e8 84 96 ff ff       	call   80100390 <panic>
    panic("switchuvm: no pgdir");
80106d0c:	83 ec 0c             	sub    $0xc,%esp
80106d0f:	68 dd 80 10 80       	push   $0x801080dd
80106d14:	e8 77 96 ff ff       	call   80100390 <panic>
    panic("switchuvm: no kstack");
80106d19:	83 ec 0c             	sub    $0xc,%esp
80106d1c:	68 c8 80 10 80       	push   $0x801080c8
80106d21:	e8 6a 96 ff ff       	call   80100390 <panic>
80106d26:	8d 76 00             	lea    0x0(%esi),%esi
80106d29:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80106d30 <inituvm>:

// Load the initcode into address 0 of pgdir.
// sz must be less than a page.
void
inituvm(pde_t *pgdir, char *init, uint sz)
{
80106d30:	55                   	push   %ebp
80106d31:	89 e5                	mov    %esp,%ebp
80106d33:	57                   	push   %edi
80106d34:	56                   	push   %esi
80106d35:	53                   	push   %ebx
80106d36:	83 ec 1c             	sub    $0x1c,%esp
80106d39:	8b 75 10             	mov    0x10(%ebp),%esi
80106d3c:	8b 45 08             	mov    0x8(%ebp),%eax
80106d3f:	8b 7d 0c             	mov    0xc(%ebp),%edi
  char *mem;

  if(sz >= PGSIZE)
80106d42:	81 fe ff 0f 00 00    	cmp    $0xfff,%esi
{
80106d48:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  if(sz >= PGSIZE)
80106d4b:	77 49                	ja     80106d96 <inituvm+0x66>
    panic("inituvm: more than a page");
  mem = kalloc();
80106d4d:	e8 6e bb ff ff       	call   801028c0 <kalloc>
  memset(mem, 0, PGSIZE);
80106d52:	83 ec 04             	sub    $0x4,%esp
  mem = kalloc();
80106d55:	89 c3                	mov    %eax,%ebx
  memset(mem, 0, PGSIZE);
80106d57:	68 00 10 00 00       	push   $0x1000
80106d5c:	6a 00                	push   $0x0
80106d5e:	50                   	push   %eax
80106d5f:	e8 7c db ff ff       	call   801048e0 <memset>
  mappages(pgdir, 0, PGSIZE, V2P(mem), PTE_W|PTE_U);
80106d64:	58                   	pop    %eax
80106d65:	8d 83 00 00 00 80    	lea    -0x80000000(%ebx),%eax
80106d6b:	b9 00 10 00 00       	mov    $0x1000,%ecx
80106d70:	5a                   	pop    %edx
80106d71:	6a 06                	push   $0x6
80106d73:	50                   	push   %eax
80106d74:	31 d2                	xor    %edx,%edx
80106d76:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80106d79:	e8 62 fd ff ff       	call   80106ae0 <mappages>
  memmove(mem, init, sz);
80106d7e:	89 75 10             	mov    %esi,0x10(%ebp)
80106d81:	89 7d 0c             	mov    %edi,0xc(%ebp)
80106d84:	83 c4 10             	add    $0x10,%esp
80106d87:	89 5d 08             	mov    %ebx,0x8(%ebp)
}
80106d8a:	8d 65 f4             	lea    -0xc(%ebp),%esp
80106d8d:	5b                   	pop    %ebx
80106d8e:	5e                   	pop    %esi
80106d8f:	5f                   	pop    %edi
80106d90:	5d                   	pop    %ebp
  memmove(mem, init, sz);
80106d91:	e9 fa db ff ff       	jmp    80104990 <memmove>
    panic("inituvm: more than a page");
80106d96:	83 ec 0c             	sub    $0xc,%esp
80106d99:	68 f1 80 10 80       	push   $0x801080f1
80106d9e:	e8 ed 95 ff ff       	call   80100390 <panic>
80106da3:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
80106da9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80106db0 <loaduvm>:

// Load a program segment into pgdir.  addr must be page-aligned
// and the pages from addr to addr+sz must already be mapped.
int
loaduvm(pde_t *pgdir, char *addr, struct inode *ip, uint offset, uint sz)
{
80106db0:	55                   	push   %ebp
80106db1:	89 e5                	mov    %esp,%ebp
80106db3:	57                   	push   %edi
80106db4:	56                   	push   %esi
80106db5:	53                   	push   %ebx
80106db6:	83 ec 0c             	sub    $0xc,%esp
  uint i, pa, n;
  pte_t *pte;

  if((uint) addr % PGSIZE != 0)
80106db9:	f7 45 0c ff 0f 00 00 	testl  $0xfff,0xc(%ebp)
80106dc0:	0f 85 91 00 00 00    	jne    80106e57 <loaduvm+0xa7>
    panic("loaduvm: addr must be page aligned");
  for(i = 0; i < sz; i += PGSIZE){
80106dc6:	8b 75 18             	mov    0x18(%ebp),%esi
80106dc9:	31 db                	xor    %ebx,%ebx
80106dcb:	85 f6                	test   %esi,%esi
80106dcd:	75 1a                	jne    80106de9 <loaduvm+0x39>
80106dcf:	eb 6f                	jmp    80106e40 <loaduvm+0x90>
80106dd1:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80106dd8:	81 c3 00 10 00 00    	add    $0x1000,%ebx
80106dde:	81 ee 00 10 00 00    	sub    $0x1000,%esi
80106de4:	39 5d 18             	cmp    %ebx,0x18(%ebp)
80106de7:	76 57                	jbe    80106e40 <loaduvm+0x90>
    if((pte = walkpgdir(pgdir, addr+i, 0)) == 0)
80106de9:	8b 55 0c             	mov    0xc(%ebp),%edx
80106dec:	8b 45 08             	mov    0x8(%ebp),%eax
80106def:	31 c9                	xor    %ecx,%ecx
80106df1:	01 da                	add    %ebx,%edx
80106df3:	e8 68 fc ff ff       	call   80106a60 <walkpgdir>
80106df8:	85 c0                	test   %eax,%eax
80106dfa:	74 4e                	je     80106e4a <loaduvm+0x9a>
      panic("loaduvm: address should exist");
    pa = PTE_ADDR(*pte);
80106dfc:	8b 00                	mov    (%eax),%eax
    if(sz - i < PGSIZE)
      n = sz - i;
    else
      n = PGSIZE;
    if(readi(ip, P2V(pa), offset+i, n) != n)
80106dfe:	8b 4d 14             	mov    0x14(%ebp),%ecx
    if(sz - i < PGSIZE)
80106e01:	bf 00 10 00 00       	mov    $0x1000,%edi
    pa = PTE_ADDR(*pte);
80106e06:	25 00 f0 ff ff       	and    $0xfffff000,%eax
    if(sz - i < PGSIZE)
80106e0b:	81 fe ff 0f 00 00    	cmp    $0xfff,%esi
80106e11:	0f 46 fe             	cmovbe %esi,%edi
    if(readi(ip, P2V(pa), offset+i, n) != n)
80106e14:	01 d9                	add    %ebx,%ecx
80106e16:	05 00 00 00 80       	add    $0x80000000,%eax
80106e1b:	57                   	push   %edi
80106e1c:	51                   	push   %ecx
80106e1d:	50                   	push   %eax
80106e1e:	ff 75 10             	pushl  0x10(%ebp)
80106e21:	e8 3a ab ff ff       	call   80101960 <readi>
80106e26:	83 c4 10             	add    $0x10,%esp
80106e29:	39 f8                	cmp    %edi,%eax
80106e2b:	74 ab                	je     80106dd8 <loaduvm+0x28>
      return -1;
  }
  return 0;
}
80106e2d:	8d 65 f4             	lea    -0xc(%ebp),%esp
      return -1;
80106e30:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80106e35:	5b                   	pop    %ebx
80106e36:	5e                   	pop    %esi
80106e37:	5f                   	pop    %edi
80106e38:	5d                   	pop    %ebp
80106e39:	c3                   	ret    
80106e3a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
80106e40:	8d 65 f4             	lea    -0xc(%ebp),%esp
  return 0;
80106e43:	31 c0                	xor    %eax,%eax
}
80106e45:	5b                   	pop    %ebx
80106e46:	5e                   	pop    %esi
80106e47:	5f                   	pop    %edi
80106e48:	5d                   	pop    %ebp
80106e49:	c3                   	ret    
      panic("loaduvm: address should exist");
80106e4a:	83 ec 0c             	sub    $0xc,%esp
80106e4d:	68 0b 81 10 80       	push   $0x8010810b
80106e52:	e8 39 95 ff ff       	call   80100390 <panic>
    panic("loaduvm: addr must be page aligned");
80106e57:	83 ec 0c             	sub    $0xc,%esp
80106e5a:	68 d4 81 10 80       	push   $0x801081d4
80106e5f:	e8 2c 95 ff ff       	call   80100390 <panic>
80106e64:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
80106e6a:	8d bf 00 00 00 00    	lea    0x0(%edi),%edi

80106e70 <deallocuvm>:
// newsz.  oldsz and newsz need not be page-aligned, nor does newsz
// need to be less than oldsz.  oldsz can be larger than the actual
// process size.  Returns the new process size.
int
deallocuvm(pde_t *pgdir, uint oldsz, uint newsz)
{
80106e70:	55                   	push   %ebp
80106e71:	89 e5                	mov    %esp,%ebp
80106e73:	57                   	push   %edi
80106e74:	56                   	push   %esi
80106e75:	53                   	push   %ebx
80106e76:	83 ec 1c             	sub    $0x1c,%esp
  pte_t *pte;
  uint a, pa;
  struct proc *curproc = myproc();
80106e79:	e8 b2 cd ff ff       	call   80103c30 <myproc>
80106e7e:	89 c7                	mov    %eax,%edi

  if(newsz >= oldsz)
80106e80:	8b 45 0c             	mov    0xc(%ebp),%eax
80106e83:	39 45 10             	cmp    %eax,0x10(%ebp)
80106e86:	73 5c                	jae    80106ee4 <deallocuvm+0x74>
    return oldsz;

  a = PGROUNDUP(newsz);
80106e88:	8b 45 10             	mov    0x10(%ebp),%eax
80106e8b:	8d 98 ff 0f 00 00    	lea    0xfff(%eax),%ebx
80106e91:	81 e3 00 f0 ff ff    	and    $0xfffff000,%ebx
  for(; a  < oldsz; a += PGSIZE){
80106e97:	39 5d 0c             	cmp    %ebx,0xc(%ebp)
80106e9a:	76 45                	jbe    80106ee1 <deallocuvm+0x71>
80106e9c:	8d b7 40 01 00 00    	lea    0x140(%edi),%esi
80106ea2:	eb 16                	jmp    80106eba <deallocuvm+0x4a>
80106ea4:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

    pte = walkpgdir(pgdir, (char*)a, 0);
    if(!pte)
      a = PGADDR(PDX(a) + 1, 0, 0) - PGSIZE;
    else if((*pte & PTE_P) != 0){
80106ea8:	8b 10                	mov    (%eax),%edx
80106eaa:	f6 c2 01             	test   $0x1,%dl
80106ead:	75 41                	jne    80106ef0 <deallocuvm+0x80>
  for(; a  < oldsz; a += PGSIZE){
80106eaf:	81 c3 00 10 00 00    	add    $0x1000,%ebx
80106eb5:	39 5d 0c             	cmp    %ebx,0xc(%ebp)
80106eb8:	76 27                	jbe    80106ee1 <deallocuvm+0x71>
    pte = walkpgdir(pgdir, (char*)a, 0);
80106eba:	8b 45 08             	mov    0x8(%ebp),%eax
80106ebd:	31 c9                	xor    %ecx,%ecx
80106ebf:	89 da                	mov    %ebx,%edx
80106ec1:	e8 9a fb ff ff       	call   80106a60 <walkpgdir>
    if(!pte)
80106ec6:	85 c0                	test   %eax,%eax
80106ec8:	75 de                	jne    80106ea8 <deallocuvm+0x38>
      a = PGADDR(PDX(a) + 1, 0, 0) - PGSIZE;
80106eca:	81 e3 00 00 c0 ff    	and    $0xffc00000,%ebx
80106ed0:	81 c3 00 f0 3f 00    	add    $0x3ff000,%ebx
  for(; a  < oldsz; a += PGSIZE){
80106ed6:	81 c3 00 10 00 00    	add    $0x1000,%ebx
80106edc:	39 5d 0c             	cmp    %ebx,0xc(%ebp)
80106edf:	77 d9                	ja     80106eba <deallocuvm+0x4a>
      }

      *pte = 0;
    }
  }
  return newsz;
80106ee1:	8b 45 10             	mov    0x10(%ebp),%eax
}
80106ee4:	8d 65 f4             	lea    -0xc(%ebp),%esp
80106ee7:	5b                   	pop    %ebx
80106ee8:	5e                   	pop    %esi
80106ee9:	5f                   	pop    %edi
80106eea:	5d                   	pop    %ebp
80106eeb:	c3                   	ret    
80106eec:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
      if(*pte & PTE_PG)
80106ef0:	f6 c6 02             	test   $0x2,%dh
80106ef3:	75 55                	jne    80106f4a <deallocuvm+0xda>
      if(pa == 0)
80106ef5:	81 e2 00 f0 ff ff    	and    $0xfffff000,%edx
80106efb:	74 5a                	je     80106f57 <deallocuvm+0xe7>
      kfree(v);
80106efd:	83 ec 0c             	sub    $0xc,%esp
      char *v = P2V(pa);
80106f00:	81 c2 00 00 00 80    	add    $0x80000000,%edx
80106f06:	89 45 e4             	mov    %eax,-0x1c(%ebp)
      kfree(v);
80106f09:	52                   	push   %edx
80106f0a:	e8 91 b7 ff ff       	call   801026a0 <kfree>
80106f0f:	8d 97 00 01 00 00    	lea    0x100(%edi),%edx
80106f15:	83 c4 10             	add    $0x10,%esp
80106f18:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80106f1b:	eb 0a                	jmp    80106f27 <deallocuvm+0xb7>
80106f1d:	8d 76 00             	lea    0x0(%esi),%esi
80106f20:	83 c2 04             	add    $0x4,%edx
      for(i=0; i<MAX_PSYC_PAGES; i++){
80106f23:	39 f2                	cmp    %esi,%edx
80106f25:	74 18                	je     80106f3f <deallocuvm+0xcf>
        if(curproc->physPages[i]==(char*)a){
80106f27:	3b 1a                	cmp    (%edx),%ebx
80106f29:	75 f5                	jne    80106f20 <deallocuvm+0xb0>
          curproc->physPages[i]=(char*)-1; // free the spot in the array
80106f2b:	c7 02 ff ff ff ff    	movl   $0xffffffff,(%edx)
80106f31:	83 c2 04             	add    $0x4,%edx
          curproc->numOfPhysPages--;
80106f34:	83 af 40 01 00 00 01 	subl   $0x1,0x140(%edi)
      for(i=0; i<MAX_PSYC_PAGES; i++){
80106f3b:	39 f2                	cmp    %esi,%edx
80106f3d:	75 e8                	jne    80106f27 <deallocuvm+0xb7>
      *pte = 0;
80106f3f:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
80106f45:	e9 65 ff ff ff       	jmp    80106eaf <deallocuvm+0x3f>
        panic("PTE_PG");
80106f4a:	83 ec 0c             	sub    $0xc,%esp
80106f4d:	68 29 81 10 80       	push   $0x80108129
80106f52:	e8 39 94 ff ff       	call   80100390 <panic>
        panic("kfree");
80106f57:	83 ec 0c             	sub    $0xc,%esp
80106f5a:	68 30 81 10 80       	push   $0x80108130
80106f5f:	e8 2c 94 ff ff       	call   80100390 <panic>
80106f64:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
80106f6a:	8d bf 00 00 00 00    	lea    0x0(%edi),%edi

80106f70 <freevm>:

// Free a page table and all the physical memory pages
// in the user part.
void
freevm(pde_t *pgdir)
{
80106f70:	55                   	push   %ebp
80106f71:	89 e5                	mov    %esp,%ebp
80106f73:	57                   	push   %edi
80106f74:	56                   	push   %esi
80106f75:	53                   	push   %ebx
80106f76:	83 ec 0c             	sub    $0xc,%esp
80106f79:	8b 75 08             	mov    0x8(%ebp),%esi
  uint i;

  if(pgdir == 0)
80106f7c:	85 f6                	test   %esi,%esi
80106f7e:	74 59                	je     80106fd9 <freevm+0x69>
    panic("freevm: no pgdir");
  deallocuvm(pgdir, KERNBASE, 0);
80106f80:	83 ec 04             	sub    $0x4,%esp
80106f83:	89 f3                	mov    %esi,%ebx
80106f85:	8d be 00 10 00 00    	lea    0x1000(%esi),%edi
80106f8b:	6a 00                	push   $0x0
80106f8d:	68 00 00 00 80       	push   $0x80000000
80106f92:	56                   	push   %esi
80106f93:	e8 d8 fe ff ff       	call   80106e70 <deallocuvm>
80106f98:	83 c4 10             	add    $0x10,%esp
80106f9b:	eb 0a                	jmp    80106fa7 <freevm+0x37>
80106f9d:	8d 76 00             	lea    0x0(%esi),%esi
80106fa0:	83 c3 04             	add    $0x4,%ebx
  for(i = 0; i < NPDENTRIES; i++){
80106fa3:	39 fb                	cmp    %edi,%ebx
80106fa5:	74 23                	je     80106fca <freevm+0x5a>
    if(pgdir[i] & PTE_P){
80106fa7:	8b 03                	mov    (%ebx),%eax
80106fa9:	a8 01                	test   $0x1,%al
80106fab:	74 f3                	je     80106fa0 <freevm+0x30>
      char * v = P2V(PTE_ADDR(pgdir[i]));
80106fad:	25 00 f0 ff ff       	and    $0xfffff000,%eax
      kfree(v);
80106fb2:	83 ec 0c             	sub    $0xc,%esp
80106fb5:	83 c3 04             	add    $0x4,%ebx
      char * v = P2V(PTE_ADDR(pgdir[i]));
80106fb8:	05 00 00 00 80       	add    $0x80000000,%eax
      kfree(v);
80106fbd:	50                   	push   %eax
80106fbe:	e8 dd b6 ff ff       	call   801026a0 <kfree>
80106fc3:	83 c4 10             	add    $0x10,%esp
  for(i = 0; i < NPDENTRIES; i++){
80106fc6:	39 fb                	cmp    %edi,%ebx
80106fc8:	75 dd                	jne    80106fa7 <freevm+0x37>
    }
  }
  kfree((char*)pgdir);
80106fca:	89 75 08             	mov    %esi,0x8(%ebp)
}
80106fcd:	8d 65 f4             	lea    -0xc(%ebp),%esp
80106fd0:	5b                   	pop    %ebx
80106fd1:	5e                   	pop    %esi
80106fd2:	5f                   	pop    %edi
80106fd3:	5d                   	pop    %ebp
  kfree((char*)pgdir);
80106fd4:	e9 c7 b6 ff ff       	jmp    801026a0 <kfree>
    panic("freevm: no pgdir");
80106fd9:	83 ec 0c             	sub    $0xc,%esp
80106fdc:	68 36 81 10 80       	push   $0x80108136
80106fe1:	e8 aa 93 ff ff       	call   80100390 <panic>
80106fe6:	8d 76 00             	lea    0x0(%esi),%esi
80106fe9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80106ff0 <setupkvm>:
{
80106ff0:	55                   	push   %ebp
80106ff1:	89 e5                	mov    %esp,%ebp
80106ff3:	56                   	push   %esi
80106ff4:	53                   	push   %ebx
  if((pgdir = (pde_t*)kalloc()) == 0)
80106ff5:	e8 c6 b8 ff ff       	call   801028c0 <kalloc>
80106ffa:	85 c0                	test   %eax,%eax
80106ffc:	89 c6                	mov    %eax,%esi
80106ffe:	74 42                	je     80107042 <setupkvm+0x52>
  memset(pgdir, 0, PGSIZE);
80107000:	83 ec 04             	sub    $0x4,%esp
  for(k = kmap; k < &kmap[NELEM(kmap)]; k++)
80107003:	bb 20 b4 10 80       	mov    $0x8010b420,%ebx
  memset(pgdir, 0, PGSIZE);
80107008:	68 00 10 00 00       	push   $0x1000
8010700d:	6a 00                	push   $0x0
8010700f:	50                   	push   %eax
80107010:	e8 cb d8 ff ff       	call   801048e0 <memset>
80107015:	83 c4 10             	add    $0x10,%esp
                (uint)k->phys_start, k->perm) < 0) {
80107018:	8b 43 04             	mov    0x4(%ebx),%eax
    if(mappages(pgdir, k->virt, k->phys_end - k->phys_start,
8010701b:	8b 4b 08             	mov    0x8(%ebx),%ecx
8010701e:	83 ec 08             	sub    $0x8,%esp
80107021:	8b 13                	mov    (%ebx),%edx
80107023:	ff 73 0c             	pushl  0xc(%ebx)
80107026:	50                   	push   %eax
80107027:	29 c1                	sub    %eax,%ecx
80107029:	89 f0                	mov    %esi,%eax
8010702b:	e8 b0 fa ff ff       	call   80106ae0 <mappages>
80107030:	83 c4 10             	add    $0x10,%esp
80107033:	85 c0                	test   %eax,%eax
80107035:	78 19                	js     80107050 <setupkvm+0x60>
  for(k = kmap; k < &kmap[NELEM(kmap)]; k++)
80107037:	83 c3 10             	add    $0x10,%ebx
8010703a:	81 fb 60 b4 10 80    	cmp    $0x8010b460,%ebx
80107040:	75 d6                	jne    80107018 <setupkvm+0x28>
}
80107042:	8d 65 f8             	lea    -0x8(%ebp),%esp
80107045:	89 f0                	mov    %esi,%eax
80107047:	5b                   	pop    %ebx
80107048:	5e                   	pop    %esi
80107049:	5d                   	pop    %ebp
8010704a:	c3                   	ret    
8010704b:	90                   	nop
8010704c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
      freevm(pgdir);
80107050:	83 ec 0c             	sub    $0xc,%esp
80107053:	56                   	push   %esi
      return 0;
80107054:	31 f6                	xor    %esi,%esi
      freevm(pgdir);
80107056:	e8 15 ff ff ff       	call   80106f70 <freevm>
      return 0;
8010705b:	83 c4 10             	add    $0x10,%esp
}
8010705e:	8d 65 f8             	lea    -0x8(%ebp),%esp
80107061:	89 f0                	mov    %esi,%eax
80107063:	5b                   	pop    %ebx
80107064:	5e                   	pop    %esi
80107065:	5d                   	pop    %ebp
80107066:	c3                   	ret    
80107067:	89 f6                	mov    %esi,%esi
80107069:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80107070 <kvmalloc>:
{
80107070:	55                   	push   %ebp
80107071:	89 e5                	mov    %esp,%ebp
80107073:	83 ec 08             	sub    $0x8,%esp
  kpgdir = setupkvm();
80107076:	e8 75 ff ff ff       	call   80106ff0 <setupkvm>
8010707b:	a3 a4 98 11 80       	mov    %eax,0x801198a4
  lcr3(V2P(kpgdir));   // switch to the kernel page table
80107080:	05 00 00 00 80       	add    $0x80000000,%eax
80107085:	0f 22 d8             	mov    %eax,%cr3
}
80107088:	c9                   	leave  
80107089:	c3                   	ret    
8010708a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi

80107090 <clearpteu>:

// Clear PTE_U on a page. Used to create an inaccessible
// page beneath the user stack.
void
clearpteu(pde_t *pgdir, char *uva)
{
80107090:	55                   	push   %ebp
  pte_t *pte;

  pte = walkpgdir(pgdir, uva, 0);
80107091:	31 c9                	xor    %ecx,%ecx
{
80107093:	89 e5                	mov    %esp,%ebp
80107095:	83 ec 08             	sub    $0x8,%esp
  pte = walkpgdir(pgdir, uva, 0);
80107098:	8b 55 0c             	mov    0xc(%ebp),%edx
8010709b:	8b 45 08             	mov    0x8(%ebp),%eax
8010709e:	e8 bd f9 ff ff       	call   80106a60 <walkpgdir>
  if(pte == 0)
801070a3:	85 c0                	test   %eax,%eax
801070a5:	74 05                	je     801070ac <clearpteu+0x1c>
    panic("clearpteu");
  *pte &= ~PTE_U;
801070a7:	83 20 fb             	andl   $0xfffffffb,(%eax)
}
801070aa:	c9                   	leave  
801070ab:	c3                   	ret    
    panic("clearpteu");
801070ac:	83 ec 0c             	sub    $0xc,%esp
801070af:	68 47 81 10 80       	push   $0x80108147
801070b4:	e8 d7 92 ff ff       	call   80100390 <panic>
801070b9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

801070c0 <copyuvm>:

// Given a parent process's page table, create a copy
// of it for a child.
pde_t*
copyuvm(pde_t *pgdir, uint sz)
{
801070c0:	55                   	push   %ebp
801070c1:	89 e5                	mov    %esp,%ebp
801070c3:	57                   	push   %edi
801070c4:	56                   	push   %esi
801070c5:	53                   	push   %ebx
801070c6:	83 ec 1c             	sub    $0x1c,%esp
  pde_t *d;
  pte_t *pte;
  uint pa, i, flags;
  char *mem;

  if((d = setupkvm()) == 0)
801070c9:	e8 22 ff ff ff       	call   80106ff0 <setupkvm>
801070ce:	85 c0                	test   %eax,%eax
801070d0:	89 45 e0             	mov    %eax,-0x20(%ebp)
801070d3:	0f 84 9f 00 00 00    	je     80107178 <copyuvm+0xb8>
    return 0;
  for(i = 0; i < sz; i += PGSIZE){
801070d9:	8b 4d 0c             	mov    0xc(%ebp),%ecx
801070dc:	85 c9                	test   %ecx,%ecx
801070de:	0f 84 94 00 00 00    	je     80107178 <copyuvm+0xb8>
801070e4:	31 ff                	xor    %edi,%edi
801070e6:	eb 4a                	jmp    80107132 <copyuvm+0x72>
801070e8:	90                   	nop
801070e9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

    pa = PTE_ADDR(*pte);
    flags = PTE_FLAGS(*pte);
    if((mem = kalloc()) == 0)
      goto bad;
    memmove(mem, (char*)P2V(pa), PGSIZE);
801070f0:	83 ec 04             	sub    $0x4,%esp
801070f3:	81 c3 00 00 00 80    	add    $0x80000000,%ebx
801070f9:	68 00 10 00 00       	push   $0x1000
801070fe:	53                   	push   %ebx
801070ff:	50                   	push   %eax
80107100:	e8 8b d8 ff ff       	call   80104990 <memmove>
    if(mappages(d, (void*)i, PGSIZE, V2P(mem), flags) < 0) {
80107105:	58                   	pop    %eax
80107106:	8d 86 00 00 00 80    	lea    -0x80000000(%esi),%eax
8010710c:	b9 00 10 00 00       	mov    $0x1000,%ecx
80107111:	5a                   	pop    %edx
80107112:	ff 75 e4             	pushl  -0x1c(%ebp)
80107115:	50                   	push   %eax
80107116:	89 fa                	mov    %edi,%edx
80107118:	8b 45 e0             	mov    -0x20(%ebp),%eax
8010711b:	e8 c0 f9 ff ff       	call   80106ae0 <mappages>
80107120:	83 c4 10             	add    $0x10,%esp
80107123:	85 c0                	test   %eax,%eax
80107125:	78 61                	js     80107188 <copyuvm+0xc8>
  for(i = 0; i < sz; i += PGSIZE){
80107127:	81 c7 00 10 00 00    	add    $0x1000,%edi
8010712d:	39 7d 0c             	cmp    %edi,0xc(%ebp)
80107130:	76 46                	jbe    80107178 <copyuvm+0xb8>
    if((pte = walkpgdir(pgdir, (void *) i, 0)) == 0)
80107132:	8b 45 08             	mov    0x8(%ebp),%eax
80107135:	31 c9                	xor    %ecx,%ecx
80107137:	89 fa                	mov    %edi,%edx
80107139:	e8 22 f9 ff ff       	call   80106a60 <walkpgdir>
8010713e:	85 c0                	test   %eax,%eax
80107140:	74 61                	je     801071a3 <copyuvm+0xe3>
    if(!(*pte & PTE_P))
80107142:	8b 00                	mov    (%eax),%eax
80107144:	a8 01                	test   $0x1,%al
80107146:	74 4e                	je     80107196 <copyuvm+0xd6>
    pa = PTE_ADDR(*pte);
80107148:	89 c3                	mov    %eax,%ebx
    flags = PTE_FLAGS(*pte);
8010714a:	25 ff 0f 00 00       	and    $0xfff,%eax
    pa = PTE_ADDR(*pte);
8010714f:	81 e3 00 f0 ff ff    	and    $0xfffff000,%ebx
    flags = PTE_FLAGS(*pte);
80107155:	89 45 e4             	mov    %eax,-0x1c(%ebp)
    if((mem = kalloc()) == 0)
80107158:	e8 63 b7 ff ff       	call   801028c0 <kalloc>
8010715d:	85 c0                	test   %eax,%eax
8010715f:	89 c6                	mov    %eax,%esi
80107161:	75 8d                	jne    801070f0 <copyuvm+0x30>
    }
  }
  return d;

bad:
  freevm(d);
80107163:	83 ec 0c             	sub    $0xc,%esp
80107166:	ff 75 e0             	pushl  -0x20(%ebp)
80107169:	e8 02 fe ff ff       	call   80106f70 <freevm>
  return 0;
8010716e:	83 c4 10             	add    $0x10,%esp
80107171:	c7 45 e0 00 00 00 00 	movl   $0x0,-0x20(%ebp)
}
80107178:	8b 45 e0             	mov    -0x20(%ebp),%eax
8010717b:	8d 65 f4             	lea    -0xc(%ebp),%esp
8010717e:	5b                   	pop    %ebx
8010717f:	5e                   	pop    %esi
80107180:	5f                   	pop    %edi
80107181:	5d                   	pop    %ebp
80107182:	c3                   	ret    
80107183:	90                   	nop
80107184:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
      kfree(mem);
80107188:	83 ec 0c             	sub    $0xc,%esp
8010718b:	56                   	push   %esi
8010718c:	e8 0f b5 ff ff       	call   801026a0 <kfree>
      goto bad;
80107191:	83 c4 10             	add    $0x10,%esp
80107194:	eb cd                	jmp    80107163 <copyuvm+0xa3>
      panic("copyuvm: page not present");
80107196:	83 ec 0c             	sub    $0xc,%esp
80107199:	68 6b 81 10 80       	push   $0x8010816b
8010719e:	e8 ed 91 ff ff       	call   80100390 <panic>
      panic("copyuvm: pte should exist");
801071a3:	83 ec 0c             	sub    $0xc,%esp
801071a6:	68 51 81 10 80       	push   $0x80108151
801071ab:	e8 e0 91 ff ff       	call   80100390 <panic>

801071b0 <uva2ka>:

//PAGEBREAK!
// Map user virtual address to kernel address.
char*
uva2ka(pde_t *pgdir, char *uva)
{
801071b0:	55                   	push   %ebp
  pte_t *pte;

  pte = walkpgdir(pgdir, uva, 0);
801071b1:	31 c9                	xor    %ecx,%ecx
{
801071b3:	89 e5                	mov    %esp,%ebp
801071b5:	83 ec 08             	sub    $0x8,%esp
  pte = walkpgdir(pgdir, uva, 0);
801071b8:	8b 55 0c             	mov    0xc(%ebp),%edx
801071bb:	8b 45 08             	mov    0x8(%ebp),%eax
801071be:	e8 9d f8 ff ff       	call   80106a60 <walkpgdir>
  if((*pte & PTE_P) == 0)
801071c3:	8b 00                	mov    (%eax),%eax
    return 0;
  if((*pte & PTE_U) == 0)
    return 0;
  return (char*)P2V(PTE_ADDR(*pte));
}
801071c5:	c9                   	leave  
  if((*pte & PTE_U) == 0)
801071c6:	89 c2                	mov    %eax,%edx
  return (char*)P2V(PTE_ADDR(*pte));
801071c8:	25 00 f0 ff ff       	and    $0xfffff000,%eax
  if((*pte & PTE_U) == 0)
801071cd:	83 e2 05             	and    $0x5,%edx
  return (char*)P2V(PTE_ADDR(*pte));
801071d0:	05 00 00 00 80       	add    $0x80000000,%eax
801071d5:	83 fa 05             	cmp    $0x5,%edx
801071d8:	ba 00 00 00 00       	mov    $0x0,%edx
801071dd:	0f 45 c2             	cmovne %edx,%eax
}
801071e0:	c3                   	ret    
801071e1:	eb 0d                	jmp    801071f0 <copyout>
801071e3:	90                   	nop
801071e4:	90                   	nop
801071e5:	90                   	nop
801071e6:	90                   	nop
801071e7:	90                   	nop
801071e8:	90                   	nop
801071e9:	90                   	nop
801071ea:	90                   	nop
801071eb:	90                   	nop
801071ec:	90                   	nop
801071ed:	90                   	nop
801071ee:	90                   	nop
801071ef:	90                   	nop

801071f0 <copyout>:
// Copy len bytes from p to user address va in page table pgdir.
// Most useful when pgdir is not the current page table.
// uva2ka ensures this only works for PTE_U pages.
int
copyout(pde_t *pgdir, uint va, void *p, uint len)
{
801071f0:	55                   	push   %ebp
801071f1:	89 e5                	mov    %esp,%ebp
801071f3:	57                   	push   %edi
801071f4:	56                   	push   %esi
801071f5:	53                   	push   %ebx
801071f6:	83 ec 1c             	sub    $0x1c,%esp
801071f9:	8b 5d 14             	mov    0x14(%ebp),%ebx
801071fc:	8b 55 0c             	mov    0xc(%ebp),%edx
801071ff:	8b 7d 10             	mov    0x10(%ebp),%edi
  char *buf, *pa0;
  uint n, va0;

  buf = (char*)p;
  while(len > 0){
80107202:	85 db                	test   %ebx,%ebx
80107204:	75 40                	jne    80107246 <copyout+0x56>
80107206:	eb 70                	jmp    80107278 <copyout+0x88>
80107208:	90                   	nop
80107209:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    va0 = (uint)PGROUNDDOWN(va);
    pa0 = uva2ka(pgdir, (char*)va0);
    if(pa0 == 0)
      return -1;
    n = PGSIZE - (va - va0);
80107210:	8b 55 e4             	mov    -0x1c(%ebp),%edx
80107213:	89 f1                	mov    %esi,%ecx
80107215:	29 d1                	sub    %edx,%ecx
80107217:	81 c1 00 10 00 00    	add    $0x1000,%ecx
8010721d:	39 d9                	cmp    %ebx,%ecx
8010721f:	0f 47 cb             	cmova  %ebx,%ecx
    if(n > len)
      n = len;
    memmove(pa0 + (va - va0), buf, n);
80107222:	29 f2                	sub    %esi,%edx
80107224:	83 ec 04             	sub    $0x4,%esp
80107227:	01 d0                	add    %edx,%eax
80107229:	51                   	push   %ecx
8010722a:	57                   	push   %edi
8010722b:	50                   	push   %eax
8010722c:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
8010722f:	e8 5c d7 ff ff       	call   80104990 <memmove>
    len -= n;
    buf += n;
80107234:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
  while(len > 0){
80107237:	83 c4 10             	add    $0x10,%esp
    va = va0 + PGSIZE;
8010723a:	8d 96 00 10 00 00    	lea    0x1000(%esi),%edx
    buf += n;
80107240:	01 cf                	add    %ecx,%edi
  while(len > 0){
80107242:	29 cb                	sub    %ecx,%ebx
80107244:	74 32                	je     80107278 <copyout+0x88>
    va0 = (uint)PGROUNDDOWN(va);
80107246:	89 d6                	mov    %edx,%esi
    pa0 = uva2ka(pgdir, (char*)va0);
80107248:	83 ec 08             	sub    $0x8,%esp
    va0 = (uint)PGROUNDDOWN(va);
8010724b:	89 55 e4             	mov    %edx,-0x1c(%ebp)
8010724e:	81 e6 00 f0 ff ff    	and    $0xfffff000,%esi
    pa0 = uva2ka(pgdir, (char*)va0);
80107254:	56                   	push   %esi
80107255:	ff 75 08             	pushl  0x8(%ebp)
80107258:	e8 53 ff ff ff       	call   801071b0 <uva2ka>
    if(pa0 == 0)
8010725d:	83 c4 10             	add    $0x10,%esp
80107260:	85 c0                	test   %eax,%eax
80107262:	75 ac                	jne    80107210 <copyout+0x20>
  }
  return 0;
}
80107264:	8d 65 f4             	lea    -0xc(%ebp),%esp
      return -1;
80107267:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
8010726c:	5b                   	pop    %ebx
8010726d:	5e                   	pop    %esi
8010726e:	5f                   	pop    %edi
8010726f:	5d                   	pop    %ebp
80107270:	c3                   	ret    
80107271:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80107278:	8d 65 f4             	lea    -0xc(%ebp),%esp
  return 0;
8010727b:	31 c0                	xor    %eax,%eax
}
8010727d:	5b                   	pop    %ebx
8010727e:	5e                   	pop    %esi
8010727f:	5f                   	pop    %edi
80107280:	5d                   	pop    %ebp
80107281:	c3                   	ret    
80107282:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80107289:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80107290 <pageFaultHandler>:
      //   cprintf("on\n");
      // else
      //   cprintf("off\n");


int pageFaultHandler(char *va){
80107290:	55                   	push   %ebp
80107291:	89 e5                	mov    %esp,%ebp
80107293:	57                   	push   %edi
80107294:	56                   	push   %esi
80107295:	53                   	push   %ebx
80107296:	81 ec 1c 10 00 00    	sub    $0x101c,%esp

    struct proc *curproc = myproc();
8010729c:	e8 8f c9 ff ff       	call   80103c30 <myproc>
    pte_t *diskPage_pte = walkpgdir(curproc->pgdir, va, 1);
801072a1:	8b 55 08             	mov    0x8(%ebp),%edx
    struct proc *curproc = myproc();
801072a4:	89 c6                	mov    %eax,%esi
    pte_t *diskPage_pte = walkpgdir(curproc->pgdir, va, 1);
801072a6:	8b 40 04             	mov    0x4(%eax),%eax
801072a9:	b9 01 00 00 00       	mov    $0x1,%ecx
801072ae:	e8 ad f7 ff ff       	call   80106a60 <walkpgdir>

    if(!(*diskPage_pte & PTE_U)){
801072b3:	8b 10                	mov    (%eax),%edx
801072b5:	f6 c2 04             	test   $0x4,%dl
801072b8:	0f 84 82 01 00 00    	je     80107440 <pageFaultHandler+0x1b0>
801072be:	89 c7                	mov    %eax,%edi
      lcr3(V2P(curproc->pgdir)); // flushing the TLB

      //cprintf("3\n");
    }

    return 0;
801072c0:	31 c0                	xor    %eax,%eax
    if(*diskPage_pte & PTE_PG){
801072c2:	80 e6 02             	and    $0x2,%dh
801072c5:	75 09                	jne    801072d0 <pageFaultHandler+0x40>
}
801072c7:	8d 65 f4             	lea    -0xc(%ebp),%esp
801072ca:	5b                   	pop    %ebx
801072cb:	5e                   	pop    %esi
801072cc:	5f                   	pop    %edi
801072cd:	5d                   	pop    %ebp
801072ce:	c3                   	ret    
801072cf:	90                   	nop
      int swapfileIndex = PGROUNDDOWN((int)va) / PGSIZE;
801072d0:	8b 45 08             	mov    0x8(%ebp),%eax
801072d3:	c1 f8 0c             	sar    $0xc,%eax
      int swapfileLoc = curproc->swapFileLocations[swapfileIndex];
801072d6:	8b 84 86 80 00 00 00 	mov    0x80(%esi,%eax,4),%eax
      readFromSwapFile(curproc, buff, swapfileLoc, PGSIZE); // copying the page from file to buffer
801072dd:	68 00 10 00 00       	push   $0x1000
      int swapfileLoc = curproc->swapFileLocations[swapfileIndex];
801072e2:	89 85 e4 ef ff ff    	mov    %eax,-0x101c(%ebp)
      readFromSwapFile(curproc, buff, swapfileLoc, PGSIZE); // copying the page from file to buffer
801072e8:	50                   	push   %eax
801072e9:	8d 85 e8 ef ff ff    	lea    -0x1018(%ebp),%eax
801072ef:	50                   	push   %eax
801072f0:	56                   	push   %esi
801072f1:	e8 8a af ff ff       	call   80102280 <readFromSwapFile>
      if(curproc->numOfPhysPages<MAX_PSYC_PAGES){
801072f6:	8b 8e 40 01 00 00    	mov    0x140(%esi),%ecx
801072fc:	83 c4 10             	add    $0x10,%esp
801072ff:	83 f9 0f             	cmp    $0xf,%ecx
80107302:	7f 34                	jg     80107338 <pageFaultHandler+0xa8>
  return ret;
}

int getSpotInPhysPages(){
  int i;
  struct proc *curproc = myproc();
80107304:	e8 27 c9 ff ff       	call   80103c30 <myproc>
  for(i=0;i<MAX_PSYC_PAGES;i++){
80107309:	31 db                	xor    %ebx,%ebx
8010730b:	eb 0f                	jmp    8010731c <pageFaultHandler+0x8c>
8010730d:	8d 76 00             	lea    0x0(%esi),%esi
80107310:	83 c3 01             	add    $0x1,%ebx
80107313:	83 fb 10             	cmp    $0x10,%ebx
80107316:	0f 84 14 01 00 00    	je     80107430 <pageFaultHandler+0x1a0>
    if(curproc->physPages[i]==(char*)-1)
8010731c:	83 bc 98 00 01 00 00 	cmpl   $0xffffffff,0x100(%eax,%ebx,4)
80107323:	ff 
80107324:	75 ea                	jne    80107310 <pageFaultHandler+0x80>
        curproc->numOfPhysPages++;
80107326:	83 86 40 01 00 00 01 	addl   $0x1,0x140(%esi)
8010732d:	e9 9d 00 00 00       	jmp    801073cf <pageFaultHandler+0x13f>
80107332:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
      int page2swap = curproc->pageToSwap; // page to swap    OR   new spot in physPages
80107338:	8b 9e 48 01 00 00    	mov    0x148(%esi),%ebx
      char *va_physPage = curproc->physPages[page2swap];
8010733e:	8b 84 9e 00 01 00 00 	mov    0x100(%esi,%ebx,4),%eax
80107345:	89 85 e0 ef ff ff    	mov    %eax,-0x1020(%ebp)
      curproc->pageToSwap = (curproc->pageToSwap+1) % (curproc->numOfPhysPages); // round robin hood
8010734b:	8d 43 01             	lea    0x1(%ebx),%eax
8010734e:	99                   	cltd   
8010734f:	f7 f9                	idiv   %ecx
      pte_t *physPage_pte = walkpgdir(curproc->pgdir, va_physPage, 1);
80107351:	8b 46 04             	mov    0x4(%esi),%eax
80107354:	b9 01 00 00 00       	mov    $0x1,%ecx
      curproc->pageToSwap = (curproc->pageToSwap+1) % (curproc->numOfPhysPages); // round robin hood
80107359:	89 96 48 01 00 00    	mov    %edx,0x148(%esi)
      pte_t *physPage_pte = walkpgdir(curproc->pgdir, va_physPage, 1);
8010735f:	8b 95 e0 ef ff ff    	mov    -0x1020(%ebp),%edx
80107365:	e8 f6 f6 ff ff       	call   80106a60 <walkpgdir>
      uint pa = PTE_ADDR(*physPage_pte);
8010736a:	8b 10                	mov    (%eax),%edx
      writeToSwapFile(curproc, (char*)P2V(pa), swapfileLoc, PGSIZE); // copying the physical page to the file (swapping)
8010736c:	68 00 10 00 00       	push   $0x1000
80107371:	ff b5 e4 ef ff ff    	pushl  -0x101c(%ebp)
      uint pa = PTE_ADDR(*physPage_pte);
80107377:	89 85 d8 ef ff ff    	mov    %eax,-0x1028(%ebp)
8010737d:	81 e2 00 f0 ff ff    	and    $0xfffff000,%edx
      writeToSwapFile(curproc, (char*)P2V(pa), swapfileLoc, PGSIZE); // copying the physical page to the file (swapping)
80107383:	81 c2 00 00 00 80    	add    $0x80000000,%edx
80107389:	52                   	push   %edx
8010738a:	56                   	push   %esi
8010738b:	89 95 dc ef ff ff    	mov    %edx,-0x1024(%ebp)
80107391:	e8 ba ae ff ff       	call   80102250 <writeToSwapFile>
      *physPage_pte = ((*physPage_pte) | PTE_PG) ^ PTE_P;
80107396:	8b 8d d8 ef ff ff    	mov    -0x1028(%ebp),%ecx
8010739c:	8b 11                	mov    (%ecx),%edx
8010739e:	80 ce 02             	or     $0x2,%dh
801073a1:	89 d0                	mov    %edx,%eax
      kfree(P2V(pa));
801073a3:	8b 95 dc ef ff ff    	mov    -0x1024(%ebp),%edx
      *physPage_pte = ((*physPage_pte) | PTE_PG) ^ PTE_P;
801073a9:	83 f0 01             	xor    $0x1,%eax
801073ac:	89 01                	mov    %eax,(%ecx)
      int swapfileIndex_physPage = PGROUNDDOWN((int)va_physPage) / PGSIZE;
801073ae:	8b 85 e0 ef ff ff    	mov    -0x1020(%ebp),%eax
      curproc->swapFileLocations[swapfileIndex_physPage] = swapfileLoc;
801073b4:	8b 8d e4 ef ff ff    	mov    -0x101c(%ebp),%ecx
      int swapfileIndex_physPage = PGROUNDDOWN((int)va_physPage) / PGSIZE;
801073ba:	c1 f8 0c             	sar    $0xc,%eax
      curproc->swapFileLocations[swapfileIndex_physPage] = swapfileLoc;
801073bd:	89 8c 86 80 00 00 00 	mov    %ecx,0x80(%esi,%eax,4)
      kfree(P2V(pa));
801073c4:	89 14 24             	mov    %edx,(%esp)
801073c7:	e8 d4 b2 ff ff       	call   801026a0 <kfree>
801073cc:	83 c4 10             	add    $0x10,%esp
      char *new_pa = kalloc();
801073cf:	e8 ec b4 ff ff       	call   801028c0 <kalloc>
      if(new_pa == 0){
801073d4:	85 c0                	test   %eax,%eax
      char *new_pa = kalloc();
801073d6:	89 c2                	mov    %eax,%edx
      if(new_pa == 0){
801073d8:	74 70                	je     8010744a <pageFaultHandler+0x1ba>
      memmove(new_pa, buff, PGSIZE); // copying the swapped page from the buffer to the physical memory
801073da:	8d 85 e8 ef ff ff    	lea    -0x1018(%ebp),%eax
801073e0:	83 ec 04             	sub    $0x4,%esp
801073e3:	89 95 e4 ef ff ff    	mov    %edx,-0x101c(%ebp)
801073e9:	68 00 10 00 00       	push   $0x1000
801073ee:	50                   	push   %eax
801073ef:	52                   	push   %edx
801073f0:	e8 9b d5 ff ff       	call   80104990 <memmove>
      *diskPage_pte = V2P(new_pa) | PTE_W|PTE_U | PTE_P;
801073f5:	8b 95 e4 ef ff ff    	mov    -0x101c(%ebp),%edx
      curproc->physPages[page2swap] = va;
801073fb:	8b 45 08             	mov    0x8(%ebp),%eax
      *diskPage_pte = V2P(new_pa) | PTE_W|PTE_U | PTE_P;
801073fe:	81 c2 00 00 00 80    	add    $0x80000000,%edx
      curproc->physPages[page2swap] = va;
80107404:	89 84 9e 00 01 00 00 	mov    %eax,0x100(%esi,%ebx,4)
      *diskPage_pte = V2P(new_pa) | PTE_W|PTE_U | PTE_P;
8010740b:	83 ca 07             	or     $0x7,%edx
8010740e:	89 17                	mov    %edx,(%edi)
      lcr3(V2P(curproc->pgdir)); // flushing the TLB
80107410:	8b 46 04             	mov    0x4(%esi),%eax
80107413:	05 00 00 00 80       	add    $0x80000000,%eax
80107418:	0f 22 d8             	mov    %eax,%cr3
8010741b:	83 c4 10             	add    $0x10,%esp
}
8010741e:	8d 65 f4             	lea    -0xc(%ebp),%esp
    return 0;
80107421:	31 c0                	xor    %eax,%eax
}
80107423:	5b                   	pop    %ebx
80107424:	5e                   	pop    %esi
80107425:	5f                   	pop    %edi
80107426:	5d                   	pop    %ebp
80107427:	c3                   	ret    
80107428:	90                   	nop
80107429:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
          panic("error in pageFaultHandler");
80107430:	83 ec 0c             	sub    $0xc,%esp
80107433:	68 9d 81 10 80       	push   $0x8010819d
80107438:	e8 53 8f ff ff       	call   80100390 <panic>
8010743d:	8d 76 00             	lea    0x0(%esi),%esi
      return -1;
80107440:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80107445:	e9 7d fe ff ff       	jmp    801072c7 <pageFaultHandler+0x37>
        cprintf("allocuvm out of memory\n");
8010744a:	83 ec 0c             	sub    $0xc,%esp
8010744d:	68 85 81 10 80       	push   $0x80108185
80107452:	e8 09 92 ff ff       	call   80100660 <cprintf>
        return -1;
80107457:	83 c4 10             	add    $0x10,%esp
8010745a:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
8010745f:	e9 63 fe ff ff       	jmp    801072c7 <pageFaultHandler+0x37>
80107464:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
8010746a:	8d bf 00 00 00 00    	lea    0x0(%edi),%edi

80107470 <swapPage>:
int swapPage(){
80107470:	55                   	push   %ebp
80107471:	89 e5                	mov    %esp,%ebp
80107473:	57                   	push   %edi
80107474:	56                   	push   %esi
80107475:	53                   	push   %ebx
80107476:	83 ec 1c             	sub    $0x1c,%esp
  struct proc *curproc = myproc();
80107479:	e8 b2 c7 ff ff       	call   80103c30 <myproc>
  int ret = curproc->pageToSwap;
8010747e:	8b b8 48 01 00 00    	mov    0x148(%eax),%edi
  struct proc *curproc = myproc();
80107484:	89 c3                	mov    %eax,%ebx
  pte_t *physPage_pte = walkpgdir(curproc->pgdir, va_physPage, 1);
80107486:	b9 01 00 00 00       	mov    $0x1,%ecx
  char *va_physPage = curproc->physPages[curproc->pageToSwap];
8010748b:	8b b4 b8 00 01 00 00 	mov    0x100(%eax,%edi,4),%esi
  curproc->pageToSwap = (curproc->pageToSwap+1) % (curproc->numOfPhysPages);
80107492:	8d 47 01             	lea    0x1(%edi),%eax
80107495:	99                   	cltd   
80107496:	f7 bb 40 01 00 00    	idivl  0x140(%ebx)
  pte_t *physPage_pte = walkpgdir(curproc->pgdir, va_physPage, 1);
8010749c:	8b 43 04             	mov    0x4(%ebx),%eax
  curproc->pageToSwap = (curproc->pageToSwap+1) % (curproc->numOfPhysPages);
8010749f:	89 93 48 01 00 00    	mov    %edx,0x148(%ebx)
  pte_t *physPage_pte = walkpgdir(curproc->pgdir, va_physPage, 1);
801074a5:	89 f2                	mov    %esi,%edx
  int swapfileIndex_physPage = PGROUNDDOWN((int)va_physPage) / PGSIZE;
801074a7:	c1 fe 0c             	sar    $0xc,%esi
  pte_t *physPage_pte = walkpgdir(curproc->pgdir, va_physPage, 1);
801074aa:	e8 b1 f5 ff ff       	call   80106a60 <walkpgdir>
  uint pa = PTE_ADDR(*physPage_pte);
801074af:	8b 10                	mov    (%eax),%edx
  writeToSwapFile(curproc, (char*)P2V(pa), curproc->lastSpotInFile, PGSIZE); // copying the physical page to the file (swapping)
801074b1:	68 00 10 00 00       	push   $0x1000
801074b6:	ff b3 44 01 00 00    	pushl  0x144(%ebx)
  uint pa = PTE_ADDR(*physPage_pte);
801074bc:	89 45 e0             	mov    %eax,-0x20(%ebp)
801074bf:	81 e2 00 f0 ff ff    	and    $0xfffff000,%edx
  writeToSwapFile(curproc, (char*)P2V(pa), curproc->lastSpotInFile, PGSIZE); // copying the physical page to the file (swapping)
801074c5:	81 c2 00 00 00 80    	add    $0x80000000,%edx
801074cb:	52                   	push   %edx
801074cc:	53                   	push   %ebx
801074cd:	89 55 e4             	mov    %edx,-0x1c(%ebp)
801074d0:	e8 7b ad ff ff       	call   80102250 <writeToSwapFile>
  *physPage_pte = ((*physPage_pte) | PTE_PG) ^ PTE_P;
801074d5:	8b 4d e0             	mov    -0x20(%ebp),%ecx
  kfree(P2V(pa));
801074d8:	8b 55 e4             	mov    -0x1c(%ebp),%edx
  *physPage_pte = ((*physPage_pte) | PTE_PG) ^ PTE_P;
801074db:	8b 01                	mov    (%ecx),%eax
801074dd:	80 cc 02             	or     $0x2,%ah
801074e0:	83 f0 01             	xor    $0x1,%eax
801074e3:	89 01                	mov    %eax,(%ecx)
  curproc->swapFileLocations[swapfileIndex_physPage] = curproc->lastSpotInFile;
801074e5:	8b 83 44 01 00 00    	mov    0x144(%ebx),%eax
801074eb:	89 84 b3 80 00 00 00 	mov    %eax,0x80(%ebx,%esi,4)
  curproc->lastSpotInFile = curproc->lastSpotInFile + PGSIZE;
801074f2:	05 00 10 00 00       	add    $0x1000,%eax
801074f7:	89 83 44 01 00 00    	mov    %eax,0x144(%ebx)
  kfree(P2V(pa));
801074fd:	89 14 24             	mov    %edx,(%esp)
80107500:	e8 9b b1 ff ff       	call   801026a0 <kfree>
}
80107505:	8d 65 f4             	lea    -0xc(%ebp),%esp
80107508:	89 f8                	mov    %edi,%eax
8010750a:	5b                   	pop    %ebx
8010750b:	5e                   	pop    %esi
8010750c:	5f                   	pop    %edi
8010750d:	5d                   	pop    %ebp
8010750e:	c3                   	ret    
8010750f:	90                   	nop

80107510 <allocuvm>:
{
80107510:	55                   	push   %ebp
80107511:	89 e5                	mov    %esp,%ebp
80107513:	57                   	push   %edi
80107514:	56                   	push   %esi
80107515:	53                   	push   %ebx
80107516:	83 ec 1c             	sub    $0x1c,%esp
  struct proc *curproc = myproc();
80107519:	e8 12 c7 ff ff       	call   80103c30 <myproc>
8010751e:	89 c6                	mov    %eax,%esi
  if(newsz >= KERNBASE)
80107520:	8b 45 10             	mov    0x10(%ebp),%eax
80107523:	85 c0                	test   %eax,%eax
80107525:	89 45 e0             	mov    %eax,-0x20(%ebp)
80107528:	0f 88 5a 01 00 00    	js     80107688 <allocuvm+0x178>
  if(newsz < oldsz)
8010752e:	3b 45 0c             	cmp    0xc(%ebp),%eax
    return oldsz;
80107531:	8b 45 0c             	mov    0xc(%ebp),%eax
  if(newsz < oldsz)
80107534:	0f 82 ce 00 00 00    	jb     80107608 <allocuvm+0xf8>
  a = PGROUNDUP(oldsz);
8010753a:	05 ff 0f 00 00       	add    $0xfff,%eax
8010753f:	25 00 f0 ff ff       	and    $0xfffff000,%eax
  for(; a < newsz; a += PGSIZE){
80107544:	39 45 10             	cmp    %eax,0x10(%ebp)
  a = PGROUNDUP(oldsz);
80107547:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  for(; a < newsz; a += PGSIZE){
8010754a:	0f 86 bb 00 00 00    	jbe    8010760b <allocuvm+0xfb>
  struct proc *curproc = myproc();
80107550:	e8 db c6 ff ff       	call   80103c30 <myproc>
  for(i=0;i<MAX_PSYC_PAGES;i++){
80107555:	31 db                	xor    %ebx,%ebx
80107557:	eb 13                	jmp    8010756c <allocuvm+0x5c>
80107559:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80107560:	83 c3 01             	add    $0x1,%ebx
80107563:	83 fb 10             	cmp    $0x10,%ebx
80107566:	0f 84 b4 00 00 00    	je     80107620 <allocuvm+0x110>
    if(curproc->physPages[i]==(char*)-1)
8010756c:	83 bc 98 00 01 00 00 	cmpl   $0xffffffff,0x100(%eax,%ebx,4)
80107573:	ff 
80107574:	75 ea                	jne    80107560 <allocuvm+0x50>
    if(curproc->numOfPhysPages==MAX_PSYC_PAGES){
80107576:	83 be 40 01 00 00 10 	cmpl   $0x10,0x140(%esi)
8010757d:	0f 84 af 00 00 00    	je     80107632 <allocuvm+0x122>
    mem = kalloc();
80107583:	e8 38 b3 ff ff       	call   801028c0 <kalloc>
    if(mem == 0){
80107588:	85 c0                	test   %eax,%eax
    mem = kalloc();
8010758a:	89 c7                	mov    %eax,%edi
    if(mem == 0){
8010758c:	0f 84 bd 00 00 00    	je     8010764f <allocuvm+0x13f>
    memset(mem, 0, PGSIZE);
80107592:	83 ec 04             	sub    $0x4,%esp
80107595:	68 00 10 00 00       	push   $0x1000
8010759a:	6a 00                	push   $0x0
8010759c:	50                   	push   %eax
8010759d:	e8 3e d3 ff ff       	call   801048e0 <memset>
    if(mappages(pgdir, (char*)a, PGSIZE, V2P(mem), PTE_W|PTE_U) < 0){
801075a2:	58                   	pop    %eax
801075a3:	8d 87 00 00 00 80    	lea    -0x80000000(%edi),%eax
801075a9:	b9 00 10 00 00       	mov    $0x1000,%ecx
801075ae:	5a                   	pop    %edx
801075af:	8b 55 e4             	mov    -0x1c(%ebp),%edx
801075b2:	6a 06                	push   $0x6
801075b4:	50                   	push   %eax
801075b5:	8b 45 08             	mov    0x8(%ebp),%eax
801075b8:	e8 23 f5 ff ff       	call   80106ae0 <mappages>
801075bd:	83 c4 10             	add    $0x10,%esp
801075c0:	85 c0                	test   %eax,%eax
801075c2:	0f 88 e0 00 00 00    	js     801076a8 <allocuvm+0x198>
    curproc->physPages[newSpotInPhysPages] = (char*)a;
801075c8:	8b 45 e4             	mov    -0x1c(%ebp),%eax
801075cb:	89 84 9e 00 01 00 00 	mov    %eax,0x100(%esi,%ebx,4)
    if(curproc->numOfPhysPages<MAX_PSYC_PAGES)
801075d2:	8b 86 40 01 00 00    	mov    0x140(%esi),%eax
801075d8:	83 f8 0f             	cmp    $0xf,%eax
801075db:	7f 09                	jg     801075e6 <allocuvm+0xd6>
      curproc->numOfPhysPages++;
801075dd:	83 c0 01             	add    $0x1,%eax
801075e0:	89 86 40 01 00 00    	mov    %eax,0x140(%esi)
  for(; a < newsz; a += PGSIZE){
801075e6:	81 45 e4 00 10 00 00 	addl   $0x1000,-0x1c(%ebp)
801075ed:	8b 45 e4             	mov    -0x1c(%ebp),%eax
801075f0:	39 45 10             	cmp    %eax,0x10(%ebp)
801075f3:	0f 87 57 ff ff ff    	ja     80107550 <allocuvm+0x40>
}
801075f9:	8b 45 e0             	mov    -0x20(%ebp),%eax
801075fc:	8d 65 f4             	lea    -0xc(%ebp),%esp
801075ff:	5b                   	pop    %ebx
80107600:	5e                   	pop    %esi
80107601:	5f                   	pop    %edi
80107602:	5d                   	pop    %ebp
80107603:	c3                   	ret    
80107604:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    return oldsz;
80107608:	89 45 e0             	mov    %eax,-0x20(%ebp)
}
8010760b:	8b 45 e0             	mov    -0x20(%ebp),%eax
8010760e:	8d 65 f4             	lea    -0xc(%ebp),%esp
80107611:	5b                   	pop    %ebx
80107612:	5e                   	pop    %esi
80107613:	5f                   	pop    %edi
80107614:	5d                   	pop    %ebp
80107615:	c3                   	ret    
80107616:	8d 76 00             	lea    0x0(%esi),%esi
80107619:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi
    if(curproc->numOfPhysPages==MAX_PSYC_PAGES){
80107620:	83 be 40 01 00 00 10 	cmpl   $0x10,0x140(%esi)
      return i;
  }

  return -1;
80107627:	bb ff ff ff ff       	mov    $0xffffffff,%ebx
    if(curproc->numOfPhysPages==MAX_PSYC_PAGES){
8010762c:	0f 85 51 ff ff ff    	jne    80107583 <allocuvm+0x73>
      if(curproc->swapFile==0){
80107632:	8b 4e 7c             	mov    0x7c(%esi),%ecx
80107635:	85 c9                	test   %ecx,%ecx
80107637:	74 61                	je     8010769a <allocuvm+0x18a>
      newSpotInPhysPages = swapPage();
80107639:	e8 32 fe ff ff       	call   80107470 <swapPage>
8010763e:	89 c3                	mov    %eax,%ebx
    mem = kalloc();
80107640:	e8 7b b2 ff ff       	call   801028c0 <kalloc>
    if(mem == 0){
80107645:	85 c0                	test   %eax,%eax
    mem = kalloc();
80107647:	89 c7                	mov    %eax,%edi
    if(mem == 0){
80107649:	0f 85 43 ff ff ff    	jne    80107592 <allocuvm+0x82>
      cprintf("allocuvm out of memory\n");
8010764f:	83 ec 0c             	sub    $0xc,%esp
80107652:	68 85 81 10 80       	push   $0x80108185
80107657:	e8 04 90 ff ff       	call   80100660 <cprintf>
      deallocuvm(pgdir, newsz, oldsz);
8010765c:	83 c4 0c             	add    $0xc,%esp
8010765f:	ff 75 0c             	pushl  0xc(%ebp)
80107662:	ff 75 10             	pushl  0x10(%ebp)
80107665:	ff 75 08             	pushl  0x8(%ebp)
80107668:	e8 03 f8 ff ff       	call   80106e70 <deallocuvm>
      return 0;
8010766d:	c7 45 e0 00 00 00 00 	movl   $0x0,-0x20(%ebp)
80107674:	83 c4 10             	add    $0x10,%esp
}
80107677:	8b 45 e0             	mov    -0x20(%ebp),%eax
8010767a:	8d 65 f4             	lea    -0xc(%ebp),%esp
8010767d:	5b                   	pop    %ebx
8010767e:	5e                   	pop    %esi
8010767f:	5f                   	pop    %edi
80107680:	5d                   	pop    %ebp
80107681:	c3                   	ret    
80107682:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    return 0;
80107688:	c7 45 e0 00 00 00 00 	movl   $0x0,-0x20(%ebp)
}
8010768f:	8b 45 e0             	mov    -0x20(%ebp),%eax
80107692:	8d 65 f4             	lea    -0xc(%ebp),%esp
80107695:	5b                   	pop    %ebx
80107696:	5e                   	pop    %esi
80107697:	5f                   	pop    %edi
80107698:	5d                   	pop    %ebp
80107699:	c3                   	ret    
        createSwapFile(curproc);
8010769a:	83 ec 0c             	sub    $0xc,%esp
8010769d:	56                   	push   %esi
8010769e:	e8 0d ab ff ff       	call   801021b0 <createSwapFile>
801076a3:	83 c4 10             	add    $0x10,%esp
801076a6:	eb 91                	jmp    80107639 <allocuvm+0x129>
      cprintf("allocuvm out of memory (2)\n");
801076a8:	83 ec 0c             	sub    $0xc,%esp
801076ab:	68 b7 81 10 80       	push   $0x801081b7
801076b0:	e8 ab 8f ff ff       	call   80100660 <cprintf>
      deallocuvm(pgdir, newsz, oldsz);
801076b5:	83 c4 0c             	add    $0xc,%esp
801076b8:	ff 75 0c             	pushl  0xc(%ebp)
801076bb:	ff 75 10             	pushl  0x10(%ebp)
801076be:	ff 75 08             	pushl  0x8(%ebp)
801076c1:	e8 aa f7 ff ff       	call   80106e70 <deallocuvm>
      kfree(mem);
801076c6:	89 3c 24             	mov    %edi,(%esp)
801076c9:	e8 d2 af ff ff       	call   801026a0 <kfree>
      return 0;
801076ce:	c7 45 e0 00 00 00 00 	movl   $0x0,-0x20(%ebp)
801076d5:	83 c4 10             	add    $0x10,%esp
}
801076d8:	8b 45 e0             	mov    -0x20(%ebp),%eax
801076db:	8d 65 f4             	lea    -0xc(%ebp),%esp
801076de:	5b                   	pop    %ebx
801076df:	5e                   	pop    %esi
801076e0:	5f                   	pop    %edi
801076e1:	5d                   	pop    %ebp
801076e2:	c3                   	ret    
801076e3:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
801076e9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

801076f0 <getSpotInPhysPages>:
int getSpotInPhysPages(){
801076f0:	55                   	push   %ebp
801076f1:	89 e5                	mov    %esp,%ebp
801076f3:	83 ec 08             	sub    $0x8,%esp
  struct proc *curproc = myproc();
801076f6:	e8 35 c5 ff ff       	call   80103c30 <myproc>
  for(i=0;i<MAX_PSYC_PAGES;i++){
801076fb:	31 d2                	xor    %edx,%edx
801076fd:	eb 09                	jmp    80107708 <getSpotInPhysPages+0x18>
801076ff:	90                   	nop
80107700:	83 c2 01             	add    $0x1,%edx
80107703:	83 fa 10             	cmp    $0x10,%edx
80107706:	74 18                	je     80107720 <getSpotInPhysPages+0x30>
    if(curproc->physPages[i]==(char*)-1)
80107708:	83 bc 90 00 01 00 00 	cmpl   $0xffffffff,0x100(%eax,%edx,4)
8010770f:	ff 
80107710:	75 ee                	jne    80107700 <getSpotInPhysPages+0x10>
80107712:	89 d0                	mov    %edx,%eax
80107714:	c9                   	leave  
80107715:	c3                   	ret    
80107716:	8d 76 00             	lea    0x0(%esi),%esi
80107719:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi
  return -1;
80107720:	ba ff ff ff ff       	mov    $0xffffffff,%edx
80107725:	c9                   	leave  
80107726:	89 d0                	mov    %edx,%eax
80107728:	c3                   	ret    
